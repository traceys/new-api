<?php
/**
 *
 * 统一访问入口
 */
define("VISIT_START_TIME",microtime(true));
header('Access-Control-Allow-Origin:*');
require_once dirname(__FILE__) . '/init.php';

    \PhalApi\DI()->redis = function () {
           return new \PhalApi\Redis\Lite(\PhalApi\DI()->config->get("app.redis.servers"));
      };


$pai = new \PhalApi\PhalApi();

$pai->response()->output();

