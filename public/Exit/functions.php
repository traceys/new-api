<?php
// require "Mda/function.php";
use PhalApi\Exception\BadRequestException;
require_once 'Bankcard/bankList.php';

///随机数
function getRandomString($len, $chars=null)
{
    if (is_null($chars)) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    }
    mt_srand(10000000*(double)microtime());
    for ($i = 0, $str = '', $lc = strlen($chars)-1; $i < $len; $i++) {
        $str .= $chars[mt_rand(0, $lc)];
    }
    return $str;
}
///随机数纯数字
function getRandomStringNum($len, $chars=null)
{
    if (is_null($chars)) {
        $chars = "0123456789";
    }
    mt_srand(10000000*(double)microtime());
    for ($i = 0, $str = '', $lc = strlen($chars)-1; $i < $len; $i++) {
        $str .= $chars[mt_rand(0, $lc)];
    }
    return $str;
}
//计算时间
function format_date($time){
    $t=time()-$time;
    $f=array(
        '31536000'=>'年',
        '2592000'=>'个月',
        '604800'=>'星期',
        '86400'=>'天',
        '3600'=>'小时',
        '60'=>'分钟',
        '1'=>'秒'
    );
    foreach ($f as $k=>$v)    {
        if (0 !=$c=floor($t/(int)$k)) {
            return $c.$v.'前';
        }
    }
}


//抓取接口内容
function curls($url){
    ///发送请求
    $ch=curl_init($url);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($ch,CURLOPT_BINARYTRANSFER,true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $output= curl_exec($ch);
    curl_close($ch);
    ///记录日志
    return json_decode($output,true);
}


function apiGet($server,$data=array()){
    $host="http://devapi.yusj.vip/";
    $url=$host."?s=$server";
    $data["token"]=Admin_token;
   // $caches=cache("apireq_".$server."_".json_encode($data));
//     if($caches!=null){
//         return $caches;
//     }

    foreach($data as $k=>$v){
        $url.="&$k=$v";
    }
    \PhalApi\DI()->logger->log("apireq",\PhalApi\DI()->AID." ".$server,$data);
    $rs=false;
    try{
        $curl = new \PhalApi\CUrl();
        $curl=$curl->setHeader(array('AID:'.\PhalApi\DI()->AID));
        $rs = $curl->get($url, 3000);
        \PhalApi\DI()->logger->log("apires",\PhalApi\DI()->AID." ".$server,$rs);
        $rs=json_decode($rs,true);
        if($rs["ret"]==200){
            $rs=$rs["data"];
            //cache("apireq_".$server."_".json_encode($data),$rs);
        }else{
            $rs=false;
        }
        
    }catch(exceptions $e){
        \PhalApi\DI()->logger->log("apires",\PhalApi\DI()->AID." ".$server,$e);
    }  
    return $rs;
}

function apiPost($server,$data=array()){
    $host="http://devapi.yusj.vip/";
    $url=$host."?s=$server&token=".Admin_token;
//     foreach($data as $k=>$v){
//         $url.="&$k=$v";
//     }

    \PhalApi\DI()->logger->log("apireq",\PhalApi\DI()->AID." ".$server,$data);
    $rs=false;
    try{
        $curl = new \PhalApi\CUrl();
        $curl=$curl->setHeader(array('AID:'.\PhalApi\DI()->AID));
        $rs = $curl->post($url,$data, 3000);
        \PhalApi\DI()->logger->log("apires",\PhalApi\DI()->AID." ".$server,$rs);
        $rs=json_decode($rs,true);
        if($rs["ret"]==200){
            $rs=$rs["data"];
        }else{
            $rs=false;
        }
        
    }catch(exceptions $e){
        \PhalApi\DI()->logger->log("apires",\PhalApi\DI()->AID." ".$server,$e);
    }  
    return $rs;
}

function checkAuth($server=false){
    $server=$server==false?$_REQUEST["s"]:$server;
    if(\PhalApi\DI()->uid<0){
        outError("尚未登录");
    }
    if(\PhalApi\DI()->uid>0){
        // $apiDo=new DomainApi();
        $check=apiGet("Auth.Apis.Check",["fid"=>\PhalApi\DI()->uid,"server"=>$server,"ftype"=>"user"]);
        if($check==false){outError("权限不足");}
    }
    // var_dump(\PhalApi\DI()->uid);
}

function userInfo($id){
    if(intval($id)<=0){
        return ["Id"=>-1,"name"=>"","username"=>"","telphone"=>""];
    }
    $info=apiGet("User.User.Userone",["uid"=>$id]);
    if($info==false){
        return ["Id"=>-1,"name"=>"","username"=>"","telphone"=>""];
    }
    return $info;
}
///post抓取数据
function http_request( $url, $post = '', $timeout = 5 ){
    if( empty( $url ) ){
        return ;
    }
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    //头部文件
    curl_setopt($ch, CURLOPT_HEADER, 0);
    //数据流输出
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    //取消ssl验证
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

    if( $post != '' && !empty( $post ) ){
        //post方式提交
        curl_setopt($ch, CURLOPT_POST, 1);
        //提交数据
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        //数据格式
    }
    //提交数据
    curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
    //抓取返回
    $result = curl_exec($ch);
    curl_close($ch);
    ///记录日志
    return $result;
}
///发送短信
function sendmall($mall,$content)
{
    $flag = 0;
    $params='';
    //要post的数据
    $argv = array(
        'sn'=>'SDK-DGG-010-00141', ////替换成您自己的序列号
        'pwd'=>strtoupper(md5('SDK-DGG-010-00141'.'5c-Ea8c-')), //此处密码需要加密 加密方式为 md5(sn+password) 32位大写
        'mobile'=>$mall,//手机号 多个用英文的逗号隔开 post理论没有长度限制.推荐群发一次小于等于10000个手机号
        'content'=>$content.'[育视界]',//iconv( "GB2312", "gb2312//IGNORE" ,'您好测试短信[XXX公司]'),//'您好测试,短信测试[签名]',//短信内容
        'ext'=>'',
        'stime'=>'',//定时时间 格式为2011-6-29 11:09:21
        'msgfmt'=>'',
        'rrid'=>''
    );
    foreach ($argv as $key=>$value)
    {
        if ($flag!=0) {
            $params .= "&";
            $flag = 1;
        }
        $params.= $key."="; $params.= urlencode($value);// urlencode($value);
        $flag = 1;
    }
    $length = strlen($params);
    //创建socket连接
    $fp = fsockopen("sdk.entinfo.cn",8061,$errno,$errstr,10) or exit($errstr."--->".$errno);
    //构造post请求的头
    $header = "POST /webservice.asmx/mdsmssend HTTP/1.1\r\n";
    $header .= "Host:sdk.entinfo.cn\r\n";
    $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
    $header .= "Content-Length: ".$length."\r\n";
    $header .= "Connection: Close\r\n\r\n";
    //添加post的字符串
    $header .= $params."\r\n";
    //发送post的数据
    //echo $header;
    //exit;
    fputs($fp,$header);
    $inheader = 1;
    while (!feof($fp)) {
        $line = fgets($fp,1024); //去除请求包的头只显示页面的返回数据
        if ($inheader && ($line == "\n" || $line == "\r\n")) {
            $inheader = 0;
        }
        if ($inheader == 0) {
            // echo $line;
        }
    }
    //<string xmlns="http://tempuri.org/">-5</string>
    $line=str_replace("<string xmlns=\"http://tempuri.org/\">","",$line);
    $line=str_replace("</string>","",$line);
    $result=explode("-",$line);
    // echo $line."-------------";
    if(count($result)>1){
        return false;
    }else{
        return true;
    }
}
///异常返回
function exceptions($msg,$status)
{
    throw new BadRequestException($msg,$status);
}

////过滤参数
function  guolu($code,$rcode)
{
  $data=[];
  for($i=0;$i<count($code);$i++)
  {
      if($rcode[$i]!=null){
          $data[$code[$i]]=$rcode[$i];
      }
  }
  return $data;
}
///添加用户操作记录
function addhistory($uid, $act, $value, $to_id, $type)
{
    $url="http://test.yusj.vip/?s=User.User.Addhistory&uid=".$uid."&act=".$act."&value=".$value."&to_id=".$to_id."&type=".$type."";
    return curls($url)["data"];
}
///监听事件
function listenevent()
{
    $bs="register";
    $on_statu="失败";
    $url="http://test.yusj.vip/?s=User.Event.Eventselect&event_name=".$bs."";
    $urls=curls($url)["data"];
    if($urls!=null){
        $urlr=explode(",",$urls["event_value"]);
        foreach ($urlr as $value)
        {
            curls($value);
        }
        $on_statu="成功";
    }
    $logurl="http://test.yusj.vip/?s=User.Event.Eventlogadd&event=".$bs."&on_statu=".$on_statu."";
    curls($logurl);
}

//接口调用函数
 function requestResource($url,$type='get',$data=null)
 {
     $url = 'http://test.yusj.vip/?s='.$url;
     if ($type == 'get'){
         $curl = new \PhalApi\CUrl();
         $rs = json_decode($curl->get($url, 3000),true);
     }
     if ($type == 'post'){
         $curl = new \PhalApi\CUrl(2);
         $rs = json_decode($curl->post($url,$data, 3000),true);

     }
     if ($rs['ret'] != 200){
         return false;
     }
     return $rs['data'];
 }
//删除数组中指定值
function array_remove($data, $key){
    if(!array_key_exists($key, $data)){
        return $data;
    }
    $keys = array_keys($data);
    $index = array_search($key, $keys);
    if($index !== FALSE){
        array_splice($data, $index, 1);
    }
    return $data;
}

function cache($key,$value=NULL,$time=3600){
    // if($value!==NULL){
    //     var_dump("写入缓存");
    // }else{
    //     var_dump("读取缓存");
    // }
    
   
    if(is_array($key)){
        $key=implode("_",$key);
    }
    // var_dump($key);
    if($value===NULL){
        // var_dump("获取缓存");
        return PhalApi\DI()->redis->get_forever($key);
    }else{
        // var_dump("存入缓存");
        PhalApi\DI()->redis->set_time($key, $value, $time);
    }
}

function clearnCache($key,$allFlag=false){
    if(is_array($key)){
        $key=implode("_",$key);
    }
    // var_dump("清理缓存");
    // var_dump($key);
    if($allFlag==false){
        PhalApi\DI()->redis->del($key);
    }else{
        PhalApi\DI()->redis->delKeys($key);
    }
}

function outError($msg,$code=0)
{
    throw new BadRequestException($msg, $code);
    # code...
}


/**
 * author fenghao@ipoxiao.com
 * date 2015/07/17 18:50
 * 替换银行卡、手机号码为**。
 * @param type $str 要替换的字符串
 * @param type $startlen 开始长度 默认4
 * @param type $endlen 结束长度 默认3
 * @return type
 */
function strreplace($str, $startlen = 4, $endlen = 3)
{
    $repstr = "";
    if (strlen($str) < ($startlen + $endlen + 1)) {
        return $str;
    }
    $count = strlen($str) - $startlen - $endlen;
    for ($i = 0; $i < $count; $i++) {
        $repstr .= "*";
    }
    return preg_replace('/(\d{' . $startlen . '})\d+(\d{' . $endlen . '})/', '${1}' . $repstr . '${2}', $str);
}

function dateTime($time=false){
    if($time!=false){
        return date("Y-m-d H:i:s",$time);
    }else{
        return date("Y-m-d H:i:s");
    }
}

function IPaddress($ip){
    $ipHeader=explode(".",$ip);
    // var_dump($ipHeader);
    if(count($ipHeader)!=4){
        return ["country"=>"未知","region"=>"未知","city"=>"未知","area"=>"未知"];
    }
    $cahce=cache(["addressFromIp",$ipHeader[0],$ipHeader[1],$ipHeader[2]]);
    if($cahce){
        // var_dump("使用缓存");
        return $cahce;
    }
    
    try{
        @$jsons=file_get_contents("http://ip.taobao.com/service/getIpInfo.php?ip=$ip",false,stream_context_create(array(
            "http"=>array(
                    // "method"=>"GET",
                    "timeout"=>3
                    ),
            )));
    }catch(Exception $e){
        return ["country"=>"未知","region"=>"未知","city"=>"未知","area"=>"未知"];
    }   
   
    try{
        $jsonObj=json_decode($jsons,true);
    }catch(Exception $e){
        return ["country"=>"未知","region"=>"未知","city"=>"未知","area"=>"未知"];
    }

    if($jsonObj["code"]!=0){
        return ["country"=>"未知","region"=>"未知","city"=>"未知","area"=>"未知"];
    }
    if( $jsonObj["data"]["region"]==null || $jsonObj["data"]["region"]==false){
        return ["country"=>"未知","region"=>"未知","city"=>"未知","area"=>"未知"];
    }

    $address=["country"=>$jsonObj["data"]["country"],"region"=>$jsonObj["data"]["region"],"city"=>$jsonObj["data"]["city"],"area"=>$jsonObj["data"]["area"]];
    cache(["addressFromIp",$ipHeader[0],$ipHeader[1],$ipHeader[2]],$address);
    return $address;

}

function userMessage($uid,$type="系统",$message="",$mtype=false,$code=null){
    if($mtype==false){
        apiGet("Push.Push.Tsfq",["uid"=>$uid,"type"=>"站内信","types"=>$type,"value"=>$message,"searchToken"=>Admin_token,'code'=>$code]);
        apiGet("Push.Push.Tsfq",["uid"=>$uid,"type"=>"短信","types"=>$type,"value"=>$message,"searchToken"=>Admin_token,'code'=>$code]);
        return;
    }

    if($mtype==1){
        apiGet("Push.Push.Tsfq",["uid"=>$uid,"type"=>"站内信","types"=>$type,"value"=>$message,"searchToken"=>Admin_token,'code'=>$code]);
        // apiGet("Push.Push.Tsfq",["uid"=>$uid,"type"=>"短信","types"=>$type,"value"=>$message]);
        return;
    }
    if($mtype==2){
        apiGet("Push.Push.Tsfq",["uid"=>$uid,"type"=>"短信","types"=>$type,"value"=>$message,"searchToken"=>Admin_token,'code'=>$code]);
        // apiGet("Push.Push.Tsfq",["uid"=>$uid,"type"=>"短信","types"=>$type,"value"=>$message]);
        return;
    }

    // return apiGet("Push.Push.Tsfq",["uid"=>$uid,"type"=>"站内信","types"=>$type,"value"=>$message]);
}


function visitLog($data){
    // var_dump($data);
    // $now=dateTime();
    // $now=substr($now,0,10);
    // var_dump($now);
    return PhalApi\DI()->redis->set_rPush("user_visit_log",$data,0);
}

function getVisitLog(){
    // var_dump($data);
    $now=dateTime();
    $now=substr($now,0,10);
    // var_dump($now);
    $data=PhalApi\DI()->redis->get_lall("user_visit_log");
    // LTRIM get_lTrim
    // var_dump(count($data));
    PhalApi\DI()->redis->gets_lTrim("user_visit_log",count($data),-1,0);
    return $data;
}

function outputArgs($data){
    PhalApi\DI()->outputArgs=$data;
}

///加积分
function addjifen($event_name,$uid)
{
    try 
    {
      $eventinfo=apiGet("User.Event.eventselect",array('event_name'=>$event_name));
      apiGet("Currency.receive.addcursd",array('uid'=>$uid,'money_type'=>3,'num'=>$eventinfo["event_value"]));
      $message="您已完成积分任务".$eventinfo["event_zhname"]."，获得".$eventinfo["event_value"]."积分。育视界，让教育无处不在。";
      userMessage($uid,"积分通知",$message,1);
    } 
    catch (Exception $e) {
    }
}
///站内信初始化
function mesgpos($uid)
{
   $info=apiGet("Push.Push.selone",array('uid'=>$uid));
   $max=apiGet("Push.Push.selmaxid");
   if($info!=null)
   {
       if($info["intposition"]==0){
           apiGet("Push.Push.updinione",array('uid'=>$uid,'inipid'=>$max["maxid"]));
       }
   }
   else
   {
      apiGet("Push.Push.addone",array('uid'=>$uid,'pid'=>$max["maxid"],'inipid'=>$max["maxid"]));
   }
    
}

//将指定时间戳转换为时间格式
function timeout($val,$type = 'datetime'){
    switch ($type){
        case 'datetime':
            $data = date("Y-m-d H:i:s",$val);
            break;
        case 'date':
            $data = date("Y-m-d",$val);
            break;
        case 'time':
            $data = date("H:i:s",$val);
            break;
    }
    return $data;
}

//提取地址市级，区级，县级
function getcity_region($adress,&$city=null,&$region=null)
{
  $city1=isset(explode('市',$adress)[1])?explode('市',$adress)[0]."市":null;
  $city2=isset(explode('州',$adress)[1])?explode('州',$adress)[0]."州":null;
  $city3=isset(explode('区',$adress)[1])?explode('区',$adress)[0]."区":null;
  if ($city1!=null){
      $region=isset(explode('市',$adress)[1])?explode('市',$adress)[1]:null;
      $region1=isset(explode('县',$region)[1])?explode('县',$region)[0]."县":null;
      $region2=isset(explode('区',$region)[1])?explode('区',$region)[0]."区":null;
      if($region1!=null){
          $city=$city1;
          $region=$region1;return;
      }
      if($region2!=null){
          $city=$city1;
          $region=$region2;return;
      }
    }
   if ($city2!=null){
      $region=isset(explode('州',$adress)[1])?explode('州',$adress)[1]:null;
      $region1=isset(explode('县',$region)[1])?explode('县',$region)[0]."县":null;
      $region2=isset(explode('区',$region)[1])?explode('区',$region)[0]."区":null;
      if($region1!=null){
          $city=$city2;
          $region=$region1;return;
      }
      if($region2!=null){
          $city=$city2;
          $region=$region2;return;
      }
   }
   if($city3!=null){
      $region=isset(explode('区',$adress)[1])?explode('区',$adress)[1]:null;
      $region1=isset(explode('县',$region)[1])?explode('县',$region)[0]."县":null;
      $region2=isset(explode('区',$adress)[1])?explode('区',$adress)[1]."区":null;
      if($region1!=null){
          $city=$city3;
          $region=$region1;return;
      }
      if($region2!=null){
          $city=$city3;
          $region=$region2;return;
      }
  }
}
?>