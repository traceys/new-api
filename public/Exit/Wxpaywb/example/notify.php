<?php
date_default_timezone_set('Asia/Shanghai');
ini_set('date.timezone','Asia/Shanghai');
error_reporting(E_ERROR);

require_once "../lib/WxPay.Api.php";
require_once '../lib/WxPay.Notify.php';
require_once 'log.php';

//初始化日志
$logHandler= new CLogFileHandler("../logs/".date('Y-m-d').'.log');

$log = Log::Init($logHandler, 15);

class PayNotifyCallBack extends WxPayNotify
{
	//查询订单
	public function Queryorder($transaction_id)
	{
		$input = new WxPayOrderQuery();
		$input->SetTransaction_id($transaction_id);
		$result = WxPayApi::orderQuery($input);
		Log::DEBUG("query:" . json_encode($result));
		if(array_key_exists("return_code", $result)
			&& array_key_exists("result_code", $result)
			&& $result["return_code"] == "SUCCESS"
			&& $result["result_code"] == "SUCCESS")
		{
			return true;
		}
		return false;
	}
	
	//重写回调处理函数
	public function NotifyProcess($data, &$msg)
	{
		Log::DEBUG("call back:" . json_encode($data));
		$notfiyOutput = array();
		
		if(!array_key_exists("transaction_id", $data)){
			$msg = "输入参数不正确";
			return $msg;
		}
		//查询订单，判断订单真实性
		if(!$this->Queryorder($data["transaction_id"])){
			$msg = "订单查询失败";
			return $msg;
		}
		return 1;
	}
}


$notify = new PayNotifyCallBack();


//$a = json_encode($_POST);
//
//Log::DEBUG($a);



$xml = file_get_contents('php://input');
// Log::DEBUG("xml:" . $xml);
$data=toArray($xml);
$transaction_id=$data["transaction_id"];
 $res =  $notify->Queryorder($transaction_id);



//查询订单并返回接口信息
if ($res){
    Log::DEBUG('SUCCESS');
$url = "https://test.yusj.vip/?s=Order.receive.notifycurrencys";

$data = [
'data'=>[
 'data'=>base64_encode(json_encode([
     'out_trade_no'=>$data['out_trade_no'],
     'trade_no'=>$transaction_id,
     'type'=>'wxwb',
     'desc'=>'支付成功'
     ])),
 'pay_info'=>base64_encode(json_encode($_POST)),
 'sign'=>'798bdd1ec31d173a'
]
];

$output = curl_request($url,$data);


 
// 　　$post_data = $data;




    // $ch=curl_init('https://abc.yusj.vip/index.php?r=user/pagereturn&out_trade_no='.$data['out_trade_no'].'&trade_no='.$transaction_id.'&trade_status='.$data['return_code'].'');
    // curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    // curl_setopt($ch,CURLOPT_BINARYTRANSFER,true);
    // $output=curl_exec($ch);

    // curl_close($ch);
    if ($output){
        $notify->Handle(true);
    }else{
        $notify->Handle(false);
    }

}else{

	echo '支付失败';
    // Log::DEBUG('FALSE');
    // $ch=curl_init('https://abc.yusj.vip/index.php?r=user/pagereturn&out_trade_no=false&trade_no=false&trade_status=false');
    // curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    // curl_setopt($ch,CURLOPT_BINARYTRANSFER,true);
    // $output=curl_exec($ch);
    // curl_close($ch);
    // $notify->Handle(false);
}


//xml包含的JSON转数组
function toArray($xml){
//禁止引用外部xml实体
    libxml_disable_entity_loader(true);
    $result= json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
    return $result;
} 
//curl调用
function curl_request($url,$post='',$cookie='', $returnCookie=0){
          $curl = curl_init();
          curl_setopt($curl, CURLOPT_URL, $url);
          curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)');
          curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
          curl_setopt($curl, CURLOPT_AUTOREFERER, 1);

          curl_setopt($curl, CURLOPT_REFERER, "http://XXX");
          if($post) {
             curl_setopt($curl, CURLOPT_POST, 1);
             curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($post));
         }
         if($cookie) {
             curl_setopt($curl, CURLOPT_COOKIE, $cookie);
         } 
         curl_setopt($curl, CURLOPT_HEADER, $returnCookie);
         curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
         $data = curl_exec($curl);
         if (curl_errno($curl)) {
             return curl_error($curl);
         }
         curl_close($curl);
         if($returnCookie){
             list($header, $body) = explode("\r\n\r\n", $data, 2);
             preg_match_all("/Set\-Cookie:([^;]*);/", $header, $matches);
             $info['cookie']  = substr($matches[1][0], 1);
             $info['content'] = $body;
             return $info;
         }else{
            return $data;
         }
 }
