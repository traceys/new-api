<?php
// 报告所有 PHP 错误
error_reporting(-1);
  

define('__VOD_CLIENT_ROOT', dirname(__DIR__));

// 设置VodClient, BosClient的Access Key ID、Secret Access Key和ENDPOINT
// 你仅需修改其中的ak、sk，其他部分请勿修改。
$my_credentials = array(
    'ak' => 'a2288452b7ae4794bf4f6d7378fd42a8',
    'sk' => 'f427a7133c58458ebec426281cc01745',
);
$g_vod_configs = array(
    'credentials' => $my_credentials,
    'endpoint' => 'http://vod.bj.baidubce.com',
);

$g_bos_configs = array(
    'credentials' => $my_credentials,
    'endpoint' => 'http://bj.bcebos.com',
);

// 设置log的格式和级别
$STDERR = fopen('php://stderr', 'w+');
$__handler = new \Monolog\Handler\StreamHandler($STDERR, \Monolog\Logger::DEBUG);
$__handler->setFormatter(
    new \Monolog\Formatter\LineFormatter(null, null, false, true)
);
\BaiduBce\Log\LogFactory::setInstance(
    new \BaiduBce\Log\MonoLogFactory(array($__handler))
);
\BaiduBce\Log\LogFactory::setLogLevel(\Psr\Log\LogLevel::DEBUG);