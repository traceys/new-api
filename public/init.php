<?php
/**
 * 统一初始化
 */

// 定义项目路径
defined('API_ROOT') || define('API_ROOT', dirname(__FILE__) . '/..');
///定义全局常量
define("Admin_token",'123456789');
// 引入composer
require_once API_ROOT . '/vendor/autoload.php';

// 时区设置
date_default_timezone_set('Asia/Shanghai');

// 引入DI服务
include API_ROOT . '/config/di.php';

// 调试模式
if (\PhalApi\DI()->debug) {
    // 启动追踪器
    \PhalApi\DI()->tracer->mark();

    error_reporting(E_ALL);
    ini_set('display_errors', 'On'); 
}
///全局变量AID
\PhalApi\DI()->AID="";
\PhalApi\DI()->outputArgs=array();
// 翻译语言包设定
\PhalApi\SL('zh_cn');

///引用redis
//$config = array('host' => '127.0.0.1', 'port' => 6379);
//$di->cache = new PhalApi\Cache\RedisCache($config);

//自定义使用第三方sdk包
require_once ("Exit/functions.php");