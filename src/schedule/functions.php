<?php
namespace Schedule;
//将指定时间戳转换为时间格式
function timeout($val){
    return date("Y-m-d H:i:s",time());
}
//随机生成一个优惠
function getRandomString($len, $chars=null)
{
    if (is_null($chars)) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    }
    mt_srand(10000000*(double)microtime());
    for ($i = 0, $str = '', $lc = strlen($chars)-1; $i < $len; $i++) {
        $str .= $chars[mt_rand(0, $lc)];
    }
    return $str;
}
//解析类型后面的数据
function timemonth($val){
    $arr =  explode('_',$val);
    return $arr;
}
