<?php
namespace Auth\Model;

use PhalApi\Model\NotORMModel as NotORM;

/**

CREATE TABLE `phalapi_curd` (
    `id` int(10) NOT NULL AUTO_INCREMENT,
    `title` varchar(20) DEFAULT NULL,
    `content` text,
    `state` tinyint(4) DEFAULT NULL,
    `post_date` datetime DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

 */

class Conf extends NotORM {

    protected function getTableName($id) {
        return 'auth_conf';
    }

    /**新增权限 */
    public function _insert($data){
        $demo=["type"=>1,"service"=>1];
        $newData=\Auth\createDataFromTemp($demo,$data);
        if($newData==false){
            return false;
        }
        $rs=$this->getORM()
            ->insert($newData);
        if($rs){
            clearnCache("auth.model",true);
        }
        return $rs;
    }

    /**修改权限 */

    public function _update($type,$service,$on_status=1,$statu=1){
        $rs=$this->getORM()
            // ->where("(type,service,statu)",[$id,1])
            ->where(["type"=>$type,"service"=>$service,"statu"=>1])
            ->update(["on_status"=>$on_status,"statu"=>$statu]);
        if($rs==1){
            clearnCache("auth.model",true);
        }
        return $rs;
    }

    public function _had($type,$service){
        $caches=cache(["auth.model.conf.had",$type,$service]);
        if($caches!==null){
            return $caches;
        }
        $rs=$this->getORM()
            ->select("type,service,`key`,on_status,statu")
            // ->where("type,service,statu",[$type,$service,1])
            ->where(["type"=>$type,"service"=>$service,'statu'=>1])
            ->fetchOne();
        cache(["auth.model.conf.had",$type,$service],$rs);
       
        return $rs;
    }

    public function _info($id){
        $caches=cache(["auth.model.conf.info",$id]);
        if($caches!==null){
            return $caches;
        }
        $rs=$this->getORM()
            ->select("type,service,`key`,on_status")
            // ->where("(id,statu)",[$id,1])
            ->where(["key"=>$id,"statu"=>1])
            ->fetchOne();
        cache(["auth.model.conf.info",$id],$rs);
        
        return $rs;
    }

    public function _list($type=false){
        $caches=cache(["auth.model.conf.list",$type]);
        if($caches!==null){
            return $caches;
        }
        $rs=$this->getORM()
            ->select("type,service,`key`,on_status,des,class")
            ->where(["statu"=>1]);
        if($type!=false){
            $rs=$rs->where(["type"=>$type]);
        }
        $rs=$rs->fetchAll();
        cache(["auth.model.conf.had",$type],$rs);
        
        return $rs;
    }

    public function getApiList(){
        return $this->getORM()
            ->select("*")
            ->where(["statu"=>1,"type"=>"api"])
            ->fetchAll();
    }

    public function getListItems($state, $page, $perpage) {
        return $this->getORM()
            ->select('*')
            ->where('state', $state)
            ->order('post_date DESC')
            ->limit(($page - 1) * $perpage, $perpage)
            ->fetchAll();
    }

    public function getListTotal($state) {
        $total = $this->getORM()
            ->where('state', $state)
            ->count('id');

        return intval($total);
    }
}
