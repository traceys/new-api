<?php
namespace Auth\Model;

use PhalApi\Model\NotORMModel as NotORM;

/**

CREATE TABLE `phalapi_curd` (
    `id` int(10) NOT NULL AUTO_INCREMENT,
    `title` varchar(20) DEFAULT NULL,
    `content` text,
    `state` tinyint(4) DEFAULT NULL,
    `post_date` datetime DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

 */

class Api extends NotORM {

    protected function getTableName($id) {
        return 'auth_api';
    }

    /**新增权限 */
    public function _insert($data){
        $demo=["ftype"=>1,"fid"=>1,"service"=>1,"value"=>1,"on_status"=>1,"statu"=>1];
        $newData=\Auth\createDataFromTemp($demo,$data); 
        /**service转换为KEY */

        /**构造权限值 */

        if($newData==false){
            return false;
        }
        $rs=$this->getORM()
            ->insert($newData);
        if($rs){
            clearnCache(["auth.model.api.info",$data["ftype"],$data["fid"]]);
        }
        return $rs;
    }

    /**修改权限 */

    public function _update($ftype,$fid,$value,$on_status,$statu){ 
        $auth=$this->_info($ftype,$fid);  
        $rs=$this->getORM()
            ->where(["ftype"=>$ftype,"fid"=>$fid,"statu"=>1])
            ->update(["on_status"=>$on_status,"statu"=>$statu,'value'=>$value]);
        if($rs){
            clearnCache(["auth.model.api.info",$ftype,$fid]);
        }
        return $rs;
    }

    /**获取权限值 */

    public function _info($ftype,$fid){
        $caches=cache(["auth.model.api.info",$ftype,$fid]);
        if($caches!==null){
            return $caches;
        }
        $rs=$this->getORM()
            ->select("id,ftype,fid,value,on_status")
            // ->where("(id,statu)",[$id,1])
            ->where(["ftype"=>$ftype,"fid"=>$fid,"statu"=>1])
            ->fetchOne();
        cache(["auth.model.api.info",$ftype,$fid],$rs);
         
        return $rs;
    }

    

    public function getListItems($state, $page, $perpage) {
        return $this->getORM()
            ->select('*')
            ->where('state', $state)
            ->order('post_date DESC')
            ->limit(($page - 1) * $perpage, $perpage)
            ->fetchAll();
    }

    public function getListTotal($state) {
        $total = $this->getORM()
            ->where('state', $state)
            ->count('id');

        return intval($total);
    }
}
