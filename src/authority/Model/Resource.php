<?php
namespace Auth\Model;

use PhalApi\Model\NotORMModel as NotORM;

/**

CREATE TABLE `phalapi_curd` (
    `id` int(10) NOT NULL AUTO_INCREMENT,
    `title` varchar(20) DEFAULT NULL,
    `content` text,
    `state` tinyint(4) DEFAULT NULL,
    `post_date` datetime DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

 */

class Resource extends NotORM {

    protected function getTableName($id) {
        return 'auth_resource';
    }

    /**新增权限 */
    public function _insert($data){
        $demo=["ftype"=>1,"fid"=>1,"res_type"=>1,"res_id"=>1,"value"=>1];
        $newData=\Auth\createDataFromTemp($demo,$data); 
        /**service转换为KEY */

        /**构造权限值 */

        if($newData==false){
            return false;
        }
        $rs=$this->getORM()
            ->insert($newData);
            if($rs){
                // clearnCache(["auth.model.res.info",$data["ftype"],$data["fid"],$data["res_type"],$data["res_id"]]);
                clearnCache(["auth.model.res"],true);
            }
        return $rs;
    }

    /**修改权限 */

    public function _update($ftype,$fid,$res_type,$res_id,$value,$on_status,$statu){ 
        // $auth=$this->info($ftype,$fid);  
        $rs=$this->getORM()
            // ->where("(ftype,fid,res_type,res_id,statu)",[$id,1])
            ->where(["ftype"=>$ftype,"fid"=>$fid,"res_type"=>$res_type,"res_id"=>$res_id,"statu"=>1])
            ->update(["on_status"=>$on_status,"statu"=>$statu,'value'=>$value]);
        if($rs){
            // clearnCache(["auth.model.res.info",$ftype,$fid,$res_type,$res_id]);
            clearnCache(["auth.model.res"],true);
        }
        return $rs;
    }

    /**获取权限值 */

    public function _info($ftype,$fid,$res_type,$res_id){
        $caches=cache(["auth.model.res.info",$ftype,$fid,$res_type,$res_id]);
        if($caches!==null){
            return $caches;
        }
        $rs=$this->getORM()
            ->select("id,ftype,fid,res_type,res_id,value,on_status")
            ->where(["ftype"=>$ftype,"fid"=>$fid,"res_type"=>$res_type,"res_id"=>$res_id,"statu"=>1])
            ->fetchOne();
        cache(["auth.model.res.info",$ftype,$fid,$res_type,$res_id],$rs);
            
        return $rs;
    }

    public function getListItems($state, $page, $perpage) {
        return $this->getORM()
            ->select('*')
            ->where('state', $state)
            ->order('post_date DESC')
            ->limit(($page - 1) * $perpage, $perpage)
            ->fetchAll();
    }

    public function getListTotal($state) {
        $total = $this->getORM()
            ->where('state', $state)
            ->count('id');

        return intval($total);
    }
}
