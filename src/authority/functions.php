<?php
namespace Auth;

function createDataFromTemp($demo,$data,$all=false){
    $newData=array();
    foreach($demo as $k=>$v){
        //$demo[$k]=$data[$k]?$data[$k]:$v;
        if(isset($data[$k])){
            $newData[$k]=$data[$k];
        }else{
            if($all){
                return false;
            }
        }
    }
    if(count(key($newData))==0){
        return false;
    }
    return $newData;
}


function dateTime($time=false){
    if($time!=false){
        return date("Y-m-d H:i:s",$time);
    }else{
        return date("Y-m-d H:i:s");
    }
}


function treeIndexForTeam($data,&$indexList,$index=[]){
    foreach($data["children"] as $k=>$v){
        $newIndex=$index;
        $newIndex[]=$k;
        $indexList[$k]=$newIndex;
        treeIndexForTeam($v,$indexList,$newIndex);
    }
}

function strToLin($strs){
    $strs=str_split($strs,1);
    $da=[];
    for($i=0;$i<count($strs);$i++){
        $da[]=pack("C",intval($strs[$i]));
    }
    $data=join($da);
    return $data;
}

function createValue($length=10240){
    // $t="0"*$length;
    // $t=str_repeat("0",$length);
    // return $t;
    $da=[];
    for($i=0;$i<$length;$i++){
        $da[]=pack("C",0);
    }
    $data=join($da);
    return $data;
}

function setValue($t,$index,$value=1){
    // $arr=str_split($t);
    $t[$index]=pack("C",$value);
    return $t;
}

function userAddTime($endTime,$long){
    $now=time();
    if($endTime==""){
        $endTime=$now;
    }else{
        $endTime=strtotime($endTime);
    }
    
    $startTime=$now>$endTime?$now:$endTime;
    return date("Y-m-d H:i:s",$startTime+ $long);
}