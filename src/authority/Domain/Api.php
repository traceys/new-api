<?php
namespace Auth\Domain;

use Auth\Model\Api as ModelApi;
use Auth\Model\Conf as ModelConf;

class Api {

    /**配置权限 */
    public function set($ftype,$fid,$server,$v,$on_status=0,$statu=0){
        $confModel=new ModelConf();
        $apiModel=new ModelApi();

        //获取server对应的key位
        $confInfo=$confModel->_had("api",$server);
        if(!$confInfo){
            return false;
        }
        $key=$confInfo["key"];
        //已有权限获取
        $infos=$apiModel->_info($ftype,$fid);
        $info=$infos?$infos["value"]:\Auth\createValue();
        $info=\Auth\setValue($info,$key,$v);
        // $info[$key]=$v;
        if($infos){
            $on_status=$on_status!=0?$on_status:$infos["on_status"];
            $statu=$statu!=0?$statu:1;
            $rs=$apiModel->_update($ftype,$fid,$info,$on_status,$statu);
        }else{
            $on_status=$on_status!=0?$on_status:1;
            $statu=$statu!=0?$statu:1;
            $rs=$apiModel->_insert(["ftype"=>$ftype,"fid"=>$fid,"value"=>$info,"on_status"=>$on_status,"statu"=>$statu]);
        }
        
        
        
        
        return $rs;
    }

    public function setAdmin(){
        $apiModel=new ModelApi();
        $ftype="team";
        $fid=320;
        $infos=$apiModel->_info($ftype,$fid);
        $info=\Auth\createValue();
        for($n=0;$n<10240;$n++){
            $info=\Auth\setValue($info,$n,1);
        }
        if($infos){
            $on_status=1;
            $statu=1;
            $rs=$apiModel->_update($ftype,$fid,$info,$on_status,$statu);
        }else{
            $on_status=1;
            $statu=1;
            $rs=$apiModel->_insert(["ftype"=>$ftype,"fid"=>$fid,"value"=>$info,"on_status"=>$on_status,"statu"=>$statu]);
        }
        return $rs;
    }

    /**获取权限数据 */
    public function get($ftype,$fid){
        $apiModel=new ModelApi();
        $rs=$apiModel->_info($ftype,$fid);
        if($rs){
            $rs["value"]=implode("",unpack("C*",$rs["value"]));
        }
        return $rs;
    }

    /**验证权限 */
    public function check($ftype,$fid,$server){
        // $caches=cache(["auth.domain.api.check",$ftype,$fid,$server]);
        // if($caches!==null){
        //     // var_dump("缓存");
        //     return $caches;
        // }
        $confModel=new ModelConf();
        $apiModel=new ModelApi();

        //获取server对应的key位
        $confInfo=$confModel->_had("api",$server);
        if(!$confInfo){
            return false;
        }
        $key=$confInfo["key"];
        //获取所属团队的权限，合并权限
        
        $values=\Auth\createValue();
        if($ftype=="user"){
            $list=apiGet("Team.User.TeamList",["uid"=>$fid]);
            // var_dump($list);
            if($list!==false){
                foreach($list as $k=>$v){
                    $info=$apiModel->_info("team",$v["tid"],$server);
                    
                    $info=$info?$info["value"]:\Auth\createValue();
                   
                    $values=$info|$values;
                }
            //     var_dump($info);
            //         // var_dump(unpack("C*",$info["value"]));
            // var_dump(unpack("C*",$values));
            }
            
            // $list=[1,4];
        
            
        }
       
        //已有权限获取
        $info=$apiModel->_info($ftype,$fid);
        $info=$info?$info["value"]:\Auth\createValue();
        $values=$info|$values;
        $rs=$values[$key]==pack("C",1);
        cache(["auth.domain.api.check",$ftype,$fid,$server],$rs);
        
        return $rs;
    }
    public function createValue($length=10240){
        $t="0"*$length;
        return $t;
    }

    public function getApiList(){
        $model=new ModelConf();
        return $model->getApiList();
    }
}
