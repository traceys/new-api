<?php
namespace Auth\Domain;

use Auth\Model\Resource as ModelRes;
use Auth\Model\Conf as ModelConf;

class Resource {

    /**配置权限 */
    public function set($ftype,$fid,$res_type,$res_id,$server,$v,$on_status=0,$statu=0){
        $confModel=new ModelConf();
        $resModel=new ModelRes();

        //获取server对应的key位
        $confInfo=$confModel->_had("res",$server);
        if(!$confInfo){
            return false;
        }
        $key=$confInfo["key"];
        //已有权限获取
        $infos=$resModel->_info($ftype,$fid,$res_type,$res_id);
        $info=$infos?$infos["value"]:\Auth\createValue();

        $info=\Auth\setValue($info,$key,$v);
        if($infos){
            $on_status=$on_status!=0?$on_status:$infos["on_status"];
            $statu=$statu!=0?$statu:1;
            $rs=$resModel->_update($ftype,$fid,$res_type,$res_id,$info,$on_status,$statu);
        }else{
            $on_status=$on_status!=0?$on_status:1;
            $statu=$statu!=0?$statu:1;
            $rs=$resModel->_insert(["ftype"=>$ftype,"fid"=>$fid,"res_type"=>$res_type,"res_id"=>$res_id,"value"=>$info,"on_status"=>$on_status,"statu"=>$statu]);
        }
        if($rs){
            // clearnCache(["auth.domain.res.get",$ftype,$fid,$res_type,$res_id]);
            // clearnCache(["auth.domain.res.check",$ftype,$fid,$res_type,$res_id,$server]);
            clearnCache(["auth.domain.res"],true);
        }
        // $on_status=$on_status!=0?$on_status:$info["on_status"];
        // $statu=$statu!=0?$statu:$info["statu"];
        // $rs=$resModel->_update($ftype,$fid,$res_type,$res_id,$info,$on_status,$statu);
        
        return $rs;
    }

    /**获取权限数据 */
    public function get($ftype,$fid,$res_type,$res_id){
        $caches=cache(["auth.domain.res.get",$ftype,$fid,$res_type,$res_id]);
        if($caches!==null){
            var_dump("缓存");
            return $caches;
        }
        $resModel=new ModelRes();
        $rs=$resModel->_info($ftype,$fid,$res_type,$res_id);
        if($rs){
            $rs["value"]=implode("",unpack("C*",$rs["value"]));
        }
        cache(["auth.domain.res.get",$ftype,$fid,$res_type,$res_id],$rs);
        
        
        return $rs;
    }
    public function setAdmin($res_type,$res_id){
        $apiModel=new ModelRes();
        $ftype="team";
        $fid=320;
        $infos=$apiModel->_info($ftype,$fid);
        $info=\Auth\createValue();
        for($n=0;$n<10240;$n++){
            $info=\Auth\setValue($info,$n,1);
        }
        if($infos){
            $on_status=1;
            $statu=1;
            $rs=$apiModel->_update($ftype,$fid,$info,$res_type,$res_id,$on_status,$statu);
        }else{
            $on_status=1;
            $statu=1;
            $rs=$apiModel->_insert(["ftype"=>$ftype,"fid"=>$fid,"res_type"=>$res_type,"res_id"=>$res_id,"value"=>$info,"on_status"=>$on_status,"statu"=>$statu]);
        }
        clearnCache(["auth.domain.res"],true);
        return $rs;
    }

    /**验证权限 */
    public function check($ftype,$fid,$res_type,$res_id,$server){
        $caches=cache(["auth.domain.res.check",$ftype,$fid,$res_type,$res_id,$server]);
        if($caches!==null){
            // var_dump("缓存");
            return $caches;
        }
        // var_dump($caches);
        $confModel=new ModelConf();
        $resModel=new ModelRes();

        //获取server对应的key位
        $confInfo=$confModel->_had("res",$server);
        if(!$confInfo){
            return false;
        }
        $key=$confInfo["key"];
        //获取所属团队的权限，合并权限
        $values=\Auth\createValue();
        if($ftype=="user"){
            $list=apiGet("Team.User.TeamList",["uid"=>$fid]);
            if($list){
                foreach($list as $k=>$v){
                    $info=$resModel->_info("team",$v["tid"],$res_type,$res_id);
                    $info=$info?$info["value"]:\Auth\createValue();
                    $values=$info|$values;
                }
            }
            // $list=[1,4];
        
            // var_dump($values[$key]==pack("C",1));
        }
       
        //已有权限获取
        $info=$resModel->_info($ftype,$fid,$res_type,$res_id);
        // var_dump(implode("",unpack("C*",$info["value"])));
        $info=$info?$info["value"]:\Auth\createValue();
        // var_dump($info[$key]==pack("C",1));
        $values=$info|$values;
        $rs=$values[$key]==pack("C",1);
        // var_dump($rs);
        cache(["auth.domain.res.check",$ftype,$fid,$res_type,$res_id,$server],$rs);
        return $rs;
    }
    
}
