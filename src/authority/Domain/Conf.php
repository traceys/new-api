<?php
namespace Auth\Domain;

use Auth\Model\Api as ModelApi;
use Auth\Model\Conf as ModelConf;

class Conf {

    /**配置权限 */
    public function set($type,$server,$on_status=0,$statu=0){
        // die("123");
        $confModel=new ModelConf();
        $apiModel=new ModelApi();

        //获取server对应的key位
        $confInfo=$confModel->_had($type,$server);
        // var_dump($confInfo);
        if(!$confInfo){
            $rs=$confModel->_insert(["type"=>$type,"service"=>$server]);
        }else{
            $on_status=$on_status!=0?$on_status:$confInfo["on_status"];
            $statu=$statu!=0?$statu:$confInfo["statu"];
            
            $rs=$confModel->_update($type,$server,$on_status,$statu);
        }
        
        return $rs;
    }

    /**获取权限数据 */
    public function get($type,$server){
        $apiModel=new ModelApi();
        $rs=$apiModel->_info($type,$server);
        return $rs;
    }

    public function _list($type=false){
        $apiModel=new ModelConf();
        $rs=$apiModel->_list($type);
        return $rs;
    }

    
}
