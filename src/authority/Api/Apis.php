<?php
namespace Auth\Api;

use PhalApi\Api;
use Auth\Domain\Conf as DomainConf;
use Auth\Domain\Api as DomainApi;
use Auth\Domain\Resource as DomainResource;
/**
 * 默认接口服务类
 *
 * @author: dogstar <chanzonghuang@gmail.com> 2014-10-04
 */

class Apis extends Api {

	public function getRules() {
        return array(
            '_list' => array(
                'token' 	=> array('name' => 'token', 'desc' => '用户token'),
            ),
            'set' => array(
                'token' 	=> array('name' => 'token', 'desc' => '用户token'),
                'ftype' 	=> array('name' => 'ftype', 'desc' => '用户token'),
                'fid' 	=> array('name' => 'fid', 'desc' => '用户token'),
                'service' 	=> array('name' => 'server', 'desc' => '用户token'),
                'value' 	=> array('name' => 'value', 'desc' => '用户token'),
                'on_status' 	=> array('name' => 'on_status', 'desc' => '用户token'),
                'statu' 	=> array('name' => 'statu', 'desc' => '用户token'),
            ),
            'get' => array(
                'token' 	=> array('name' => 'token', 'desc' => '用户token'),
                'ftype' 	=> array('name' => 'ftype', 'desc' => '用户token'),
                'fid' 	=> array('name' => 'fid', 'desc' => '用户token'),
            ),
            'check' => array(
                'token' 	=> array('name' => 'token', 'desc' => '用户token'),
                'ftype' 	=> array('name' => 'ftype', 'desc' => '用户token'),
                'fid' 	=> array('name' => 'fid', 'desc' => '用户token'),
                'service' 	=> array('name' => 'server', 'desc' => '用户token'),
            )
        );
	}
	
	/**
	 * 默认接口服务
     * @desc 默认接口服务，当未指定接口服务时执行此接口服务
	 * @return string title 标题
	 * @return string content 内容
	 * @return string version 版本，格式：X.X.X
	 * @return int time 当前时间戳
     * @exception 400 非法请求，参数传递错误
	 */
	public function _list() {
        $confDo=new DomainConf();
        $rs=$confDo->_list("api");
        return $rs;
    }
    
    public function set(){
        checkAuth();
        $apiDo=new DomainApi();
        $rs=$apiDo->set($this->ftype,$this->fid,$this->service,$this->value,$this->on_status,$this->statu);
        return $rs;
    }

    public function get(){
        $apiDo=new DomainApi();
        $rs=$apiDo->get($this->ftype,$this->fid);
        return $rs;
    }

    public function check(){
        // if(DI()->uid<0){
        //     outError("尚未登录");
        // }
        // if(DI()->uid>0){
        //     $apiDo=new DomainApi();
        //     $check=$apiDo->check("user",DI()->uid,"Auth.Apis.Check");
        //     if($check==false){outError("权限不足");}
        // }
        $apiDo=new DomainApi();
        $rs=$apiDo->check($this->ftype,$this->fid,$this->service);
        return $rs;
    }
    public function setAdmin(){
        $apiDo=new DomainApi();
        $rs=$apiDo->setAdmin();
        return $rs;
    }

    public function apiList(){
        $apiDo=new DomainApi();
        $rs=$apiDo->getApiList();
        return $rs;
    }
}
