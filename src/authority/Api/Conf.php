<?php
namespace Auth\Api;

use PhalApi\Api;
use Auth\Domain\Conf as DomainConf;
use Auth\Domain\Api as DomainApi;
use Auth\Domain\Resource as DomainResource;
/**
 * 默认接口服务类
 *
 * @author: dogstar <chanzonghuang@gmail.com> 2014-10-04
 */

class Conf extends Api {

	public function getRules() {
        return array(
            '_list' => array(
                'token' 	=> array('name' => 'token', 'desc' => '用户token'),
                'type' 	=> array('name' => 'type', 'desc' => '权限资源的类型'),
            ),
            'set' => array(
                'token' 	=> array('name' => 'token', 'desc' => '用户token'),
                'type' 	=> array('name' => 'type', 'desc' => '用户token'),
                'service' 	=> array('name' => 'server', 'desc' => '用户token'),
                'on_status' 	=> array('name' => 'on_status','default'=>0, 'desc' => '用户token'),
                'statu' 	=> array('name' => 'statu','default'=>0, 'desc' => '用户token'),
            ),
        );
	}
	
	/**
	 * 默认接口服务
     * @desc 默认接口服务，当未指定接口服务时执行此接口服务
	 * @return string title 标题
	 * @return string content 内容
	 * @return string version 版本，格式：X.X.X
	 * @return int time 当前时间戳
     * @exception 400 非法请求，参数传递错误
	 */
	public function _list() {
        $confDo=new DomainConf();
        $rs=$confDo->_list($this->type?$this->type:false);
        return $rs;
    }
    
    public function set(){
        checkAuth();
        $confDo=new DomainConf();
        $rs=$confDo->set($this->type,$this->service,$this->on_status,$this->statu);
        return $rs;
    }
}
