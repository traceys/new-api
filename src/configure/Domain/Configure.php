<?php
namespace Configure\Domain;
use Configure\Model\Configure\Watch as Mwatch;
use Configure\Model\Configure\History as Mhistory;
use Configure\Model\Configure\Banks as Mbanks;
class Configure
{
   ///添加配置
   public function addconfig($type, $key, $value, $on_status,$statu,$uid)
   {
       $con=new Mwatch();
       $ucon=$con->sel($type,$key);
       if($ucon!=null)
       {
           $code=["type","key","value","on_status","statu"];
           $rcode=[$type,$key,$value,$on_status,$statu];
           $data=guolu($code, $rcode);
           $con->upd($data, $ucon["id"]);
       }
       else
       {
         $con->add($type, $key, $value, $on_status,$statu);
       }
       
      $his=new Mhistory();
      $his->add($uid, $type, $key, $value, 1);
      return true;
   }
   ///查询列表
   public function selfig($type,$page){
       $con=new Mwatch();
       return $con->sellist($type,$page);
   }
   ///查询单个
   public function selone($type,$key){
       $con=new Mwatch();
       return $con->sel($type,$key);
   }
   ///查询该会员的分会
   public function seluser($uid){
       $con=new Mwatch();
       return $con->seluser($uid);
   }
   ///修改全部收益比例
   public function updbi($type,$value)
   {
       $con=new Mwatch();
       return $con->updbi($type, $value);
   }
   ///查询收益比例
   public function selbi($type)
   {
       $con=new Mwatch();
       return $con->selbi($type);
   }
   ///修改默认收益比例
   public function updmo($id,$value)
   {
       $con=new Mwatch();
       return $con->updmo($id,$value);
   }
   ///查询该会员的分会是否存在
   public function isseluser($uid)
   {
       $con=new Mwatch();
       return $con->isseluser($uid);
   }
   
   ///查询地区
   public function region($region)
   {
       $con=new Mwatch();
       return $con->region($region);
   }
   ///查询地区
   public function likebranch($key)
   {
       $con=new Mwatch();
       return $con->likebranch($key);
   }
   ////银行条件查询
   public function listbanks($pro,$city,$bank)
   {
       $banks=new Mbanks();
       return $banks->listbanks($pro, $city, $bank);
   }
   ///分会条件查询
   public function listbranch($region,$pro,$city,$branch_name,$boss,$phone,$page)
   {
       $branch=new Mwatch();
       return $branch->listbranch($region, $pro, $city, $branch_name, $boss, $phone, $page);
   }
   ///修改配置系统资料
   public function upconfid($type, $key, $value, $on_status,$statu,$id)
   {
       $con=new Mwatch();
       $code=["type","key","value","on_status","statu"];
       $rcode=[$type,$key,$value,$on_status,$statu];
       $data=guolu($code, $rcode);
       return $con->upd($data, $id);
   }
   ///id查询
   public function selid($id){
       $con=new Mwatch();
       return $con->selid($id);
   }
   ///查询省份
   public function getpro($city)
   {
       $con=new Mwatch();
       return $con->getpro($city);
   }
}