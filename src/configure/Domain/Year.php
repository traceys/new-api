<?php
namespace Configure\Domain;
use Configure\Model\Configure\Year as Myear;
class Year
{
    //添加
    public function add($years,$honer_id)
    {
        $year=new Myear();
        return  $year->add($years, $honer_id);
    }
    ///查询
    public function sel($honer_id)
    {
        $year=new Myear();
        return $year->sel($honer_id);
    }
    ///删除
    public function del($id)
    {
        $year=new Myear();
        return $year->del($id);
    }
}