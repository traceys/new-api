<?php
namespace Configure\Model\Configure;
use PhalApi\Model\NotORMModel as Model;

class Honer extends Model
{
    ///添加荣誉
    public function add($teacher_id,$honer_id,$year)
    {
        $data=array(
            "teacher_id"=>$teacher_id,
            "honer_id"=>$honer_id,
            "year"=>$year,
            "statu"=>1
        );
        $this->getORM()->insert($data);
    }
    ///查询荣誉有教师
    public function selteacher($honer_id){
        $sql="select h.*,y.year as year,y.id as yearid from api_configure_honer as h left join api_configure_year as y on h.year=y.id where h.honer_id='".$honer_id."' and h.statu=1 and y.statu=1  order by y.year ASC";
        $info= $this->getORM()->queryAll($sql);
        if($info!=null){
            return $info;
        }
        else{
            return [];
        }
    }
    ///查询教师荣誉
    public function selhoner($teacher_id){
        $sql="select * from api_configure_honer as h left join api_configure_year as y on h.year=y.id LEFT JOIN api_configure_watch as w on w.id=h.honer_id  where h.teacher_id='".$teacher_id."' and h.statu=1 and y.statu=1 and w.statu=1 and w.on_status=1  order by y.year ASC";
        $info=$this->getORM()->queryAll($sql);
        if($info!=null){
            return $info;
        }
        else{
            return [];
        }
    }
    ///删除荣誉教师
    public function del($id)
    {
        $this->getORM()->where(["id"=>$id])->update(["statu"=>-1]);
    }
    ///查询重复
    public function selct($teacher_id,$honer_id,$year){
        $sql="select * from api_configure_honer where teacher_id='".$teacher_id."' and honer_id='".$honer_id."' and year='".$year."' and statu=1"; 
        return $this->getORM()->queryAll($sql);
        
    }
}