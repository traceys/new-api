<?php
namespace Configure\Model\Configure;
use PhalApi\Model\NotORMModel as Model;

class Banks extends Model
{
   ////条件查询
   public function listbanks($pro,$city,$banks)
   {
       $sql="select * from api_configure_banks where 1=1";
       if($pro!=null){
           $sql.=" and  province='".$pro."'";
       }
       if($city!=null)
       {
           $sql.=" and  city='".$city."'";
       }
       if($banks!=null)
       {
           $sql.=" and bank_name='".$banks."'";   
       }
       return  $this->getORM()->queryAll($sql);
   }
}