<?php
namespace Configure\Model\Configure;
use PhalApi\Model\NotORMModel as Model;

class Watch extends Model
{
    ///添加
    public function add($type,$key,$value,$on_status,$statu)
    {
        $data=array(
            'type'=>$type,
            'key'=>$key,
            'value'=>$value,
            'on_status'=>$on_status,
            'statu'=>$statu
        );
        $this->getORM()->insert($data);
        clearnCache(["configure.configure.model"],true);
    }
    ///查询单个
    public function sel($type,$key){
        $con=$this->getORM()->where(['type'=>$type,'`key`'=>$key,'statu'=>1,'on_status'=>1])->fetchOne();
        return $con;
    }
    ///id查询
    public function selid($id){
        $con=$this->getORM()->where(['id'=>$id,'statu'=>1,'on_status'=>1])->fetchOne();
        return $con;
    }
    ///查询列表
    public function sellist($type,$page)
    {
        $count=$this->sellistcount($type);
        $list=cache(["configure.configure.model.configure.sellist",$type,$page]);
        outputArgs(["num"=>$count,"pagenum"=>40,"page"=>$page]);
        if($list!=null){
            return $list;
        }
        $con=$this->getORM()->select('*')->where(['type'=>$type,'statu'=>1,'on_status'=>1])->order(" id desc");
        if($page){
            $con=$con->limit(($page-1)*40,40);
        }
        $con=$con->fetchAll();
        cache(["configure.configure.model.configure.sellist",$type,$page],$con);
        return $con;
    }
    
    ///查询列表
    public function sellistcount($type)
    {
        $count=cache(["configure.configure.model.configure.sellistcount",$type]);
        if($count!=null){
            return $count;
        }
        $con=$this->getORM()->select(" count(*) as num")->where(['type'=>$type,'statu'=>1,'on_status'=>1]);
        $con=$con->fetchOne();
        cache(["configure.configure.model.configure.sellistcount",$type],$con["num"]);
        return $con["num"];
    }
    ///修改
    public function upd($data,$id)
    {
        $this->getORM()->where(['id'=>$id])->update($data);
        clearnCache(["configure.configure.model"],true);
    }
    ///查询该会员的分会
    public function seluser($uid){
        $sql="select * from api_configure_watch where type='branch' and value like '%\"mid\":\"".$uid."\"%' and on_status=1 and statu=1 order by id desc";
        return  $this->getORM()->queryAll($sql);
    }
    ///查询该会员的分会是否存在
    public function isseluser($uid)
    {
        if($uid!=null)
        {
          $sql="select * from api_configure_watch where type='branch' and value like '%\"mid\":\"".$uid."\"%'  and on_status=1 and statu=1";
          return  $this->getORM()->queryAll($sql);
        }
    }
    ///查询地区
    public function region($region){
        $sql="select * from api_configure_watch where type='branch' and  value like '%\"region\":".$region."%' and on_status=1 and statu=1";
        return  $this->getORM()->queryAll($sql);
    }
    ///分会条件查询
    public function listbranch($region,$pro,$city,$branch_name,$boss,$phone,$page)
    {
        $count=$this->listbranchcount($region, $pro, $city, $branch_name, $boss, $phone);
        $list=cache(["configure.configure.model.configure.listbranch",$region,$pro,$city,$branch_name,$boss,$phone,$page]);
        outputArgs(["num"=>$count,"pagenum"=>40,"page"=>$page]);
        if($list!=null){
            return $list;
        }
        $sql="select * from api_configure_watch where type='branch' and on_status=1 and statu=1";
        if($region!=null)
        {
            $sql.="  and  value like '%\"region\":%".$region."%'";
        }
        if($pro!=null)
        {
            $sql.="  and  value like '%\"region\":%".$pro."%'";
        }
        if($city!=null)
        {
            $sql.="  and  value like '%\"region\":%".$city."%'";
        }
        if($boss!=null){
            $sql.="  and  value like '%\"boss\":\"".$boss."\"%'";
        }
        if($phone!=null){
            $sql.="  and  value like '%\"phone\":\"".$phone."\"%'";
        }
        if($branch_name!=null){
            $sql.="  and  `key` like '%".$branch_name."%'";
        }
        if($page>0)
        {
            $pre= ($page-1)*40;
            $next=40;
            $sql.=" limit ".$pre.",".$next."";
        }
        $info=$this->getORM()->queryAll($sql);
        cache(["configure.configure.model.configure.listbranch",$region,$pro,$city,$branch_name,$boss,$phone,$page],$info);
        return $info;
    }
    ///分会条件查询
    public function listbranchcount($region,$pro,$city,$branch_name,$boss,$phone)
    {
        $count=cache(["configure.configure.model.configure.listbranchcount",$region,$pro,$city,$branch_name,$boss,$phone]);
        if($count!=null){
            return $count;
        }
        $sql="select count(*) as num from api_configure_watch where type='branch' and on_status=1 and statu=1";
        if($region!=null)
        {
            $sql.="  and  value like '%\"region\":%".$region."%'";
        }
        if($pro!=null)
        {
            $sql.="  and  value like '%\"region\":%".$pro."%'";
        }
        if($city!=null)
        {
            $sql.="  and  value like '%\"region\":%".$city."%'";
        }
        if($boss!=null){
            $sql.="  and  value like '%\"boss\":\"".$boss."\"%'";
        }
        if($phone!=null){
            $sql.="  and  value like '%\"phone\":\"".$phone."\"%'";
        }
        if($branch_name!=null){
            $sql.="  and  `key` like '%".$branch_name."%'";
        }
        $info=$this->getORM()->queryAll($sql);
        cache(["configure.configure.model.configure.listbranchcount",$region,$pro,$city,$branch_name,$boss,$phone],$info[0]["num"]);
        return $info[0]["num"];
    }
    ///修改全部收益比例
    public function updbi($type,$value)
    {
        $this->getORM()->where(["type"=>$type])->update(["value"=>$value]);
        clearnCache(["configure.configure.model"],true);
    }
    ///查询收益比例
    public function selbi($type){
        return $this->getORM()->where(['type'=>$type,'statu'=>1,'on_status'=>1])->fetchAll();
    }
    ///修改默认收益比例
    public function updmo($id,$value)
    {
        $this->getORM()->where(["id"=>$id])->update(["value"=>$value]);
        clearnCache(["configure.configure.model"],true);
    }
    ///查询地区
    public function likebranch($key)
    {
        $sql="select * from api_configure_watch where type='branch' and  `key` like '%".$key."%' and on_status=1 and statu=1";
        return  $this->getORM()->queryAll($sql);
    }
    ///查询省份
    public function getpro($city)
    {
        $sql="select * from api_configure_watch where type='adress' and  `value` like '%".$city."%' and on_status=1 and statu=1";
        return  $this->getORM()->queryAll($sql);
    }
}