<?php
namespace Configure\Model\Configure;
use PhalApi\Model\NotORMModel as Model;

class History extends Model
{
  ///添加历史记录
   public function add($uid,$type,$key,$value,$done_status){
       $data=array(
          'uid'=>$uid,
          'type'=>$type,
          'key'=>$key,
          'value'=>$value,
          'done_status'=>$done_status,
          'time'=>date('Y-m-d H:i:s',time()),
          'statu'=>1
       );
       $this->getORM()->insert($data);
   }
}