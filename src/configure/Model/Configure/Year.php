<?php
namespace Configure\Model\Configure;
use PhalApi\Model\NotORMModel as Model;

class Year extends Model
{
    //添加
    public function add($year,$honer_id)
    {
          $data=array(
            "year"=>$year,
            "honer_id"=>$honer_id,
            "statu"=>1
        );
        $this->getORM()->insert($data);
    }
    ///查询
    public function sel($honer_id)
    {
       return  $this->getORM()->where(["honer_id"=>$honer_id,"statu"=>1])->order("year DESC")->fetchAll();
    }
    ///删除
    public function del($id)
    {
       $this->getORM()->where(["id"=>$id])->update(["statu"=>-1]);   
    }
}