<?php
namespace Configure\Api;
use PhalApi\Api;
use Configure\Domain\Year as Dyear;

class Year extends Api
{
    public function getRules()
    {
        return array(
           'add' => array(
                'honer_id' => array('name' => 'honer_id'),
                'year' => array('name' => 'year'),
            ),
            'sel' => array(
                'honer_id' => array('name' => 'honer_id'),
            ),
            'del' => array(
                'id' => array('name' => 'id'),
            ),
        );
    }
    /**
     * 添加荣誉年份
     */
    public function add()
    {
        $year=new Dyear();
        return $year->add($this->year, $this->honer_id);
    }
    /**
     * 查询荣誉有哪些年份
     */
    public function sel()
    {
        $year=new Dyear();
        return $year->sel($this->honer_id);
    }
    /**
     * 删除荣誉年份
     */
    public function del()
    {
        $year=new Dyear();
        return $year->del($this->id);
    }
}