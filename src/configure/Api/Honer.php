<?php
namespace Configure\Api;
use PhalApi\Api;
use Configure\Domain\Honer as Dhoner;

class Honer extends Api
{
    public function getRules()
    {
        return array(
           'add' => array(
                'teacher_id' => array('name' => 'teacher_id'),
                'honer_id' => array('name' => 'honer_id'),
                'year' => array('name' => 'year'),
            ),
            'selteacher' => array(
                'honer_id' => array('name' => 'honer_id'),
            ),
            'selhoner' => array(
                'teacher_id' => array('name' => 'teacher_id'),
            ),
            'del' => array(
                'id' => array('name' => 'id'),
            ),
            'selrel_name' => array(
                'rel_name' => array('name' => 'rel_name'),
            ),
        );
    }
    /**
     * 添加荣誉的教师
     */
    public function add()
    {
        $honer=new Dhoner();
        return  $honer->add($this->teacher_id, $this->honer_id, $this->year);
    }
    /**
     * 查询荣誉有教师
     */
    public function selteacher()
    {
        $honer=new Dhoner();
        $info=$honer->selteacher($this->honer_id);
        for($i=0;$i<count($info);$i++)
        {
           $teacher=apiGet("User.User.userone",array("uid"=>$info[$i]["teacher_id"]));
           if($teacher!=null){
               $info[$i]["rel_name"]=$teacher["rel_name"];
           }
           else{
               $info[$i]["rel_name"]=$info[$i]["teacher_id"];
           }
        }
        return $info;
    }
    /**
     * 查询教师有哪些荣誉
     */
    public function selhoner()
    {
        $honer=new Dhoner();
        return $honer->selhoner($this->teacher_id);
    }
    /**
     * 删除荣誉中的教师
     */
    public function del()
    {
        $honer=new Dhoner();
        return $honer->del($this->id);
    }
    /**
     * 教师姓名模糊查询
     */
    public function selrel_name()
    {
       $info=[];
       $h=0;
       $user=apiGet("User.User.selrel_name",array("rel_name"=>$this->rel_name));
       for($i=0;$i<count($user);$i++)
       {
          $teacher=apiGet("User.certificate.certificateList",array("like"=>"uid","serach"=>$user[$i]["Id"],"on_statu"=>1));
          if($teacher!=null){
              $info[$h]=$teacher[0];
              $h++;
          }
       }
       return $info;
    }
    /**
     * 
     */
}