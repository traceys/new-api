<?php
namespace Configure\Api;
use PhalApi\Api;
use Configure\Domain\Configure as config;

class Configure extends Api
{
    public function getRules()
    {
        return array(
            'addconfig' => array(
                'type' => array('name' => 'type','require' => true),
                'key' => array('name' => 'key','require' => true),
                'value' => array('name' => 'value'),
                'on_status' => array('name' => 'on_status'),
                'statu' => array('name' => 'statu'),
                'token' => array('name' => 'token'),
              ),
            'selfig' => array(
                'type' => array('name' => 'type','require' => true,'source' => 'get'),
                'token' => array('name' => 'token','source' => 'get'),
                'page' => array('name' => 'page','source' => 'get'),
            ),
            'selone' => array(
                'type' => array('name' => 'type','require' => true,'source' => 'get'),
                'key' => array('name' => 'key','require' => true,'source' => 'get'),
                'token' => array('name' => 'token','require' => true,'source' => 'get'),
            ),
            'seluser' => array(
                'uid' => array('name' => 'uid','require' => true,'source' => 'get'),
            ),
            'selalluser' => array(
                'token' => array('name' => 'token','require' => true,'source' => 'get'),
            ),
            'updbi' => array(
                'bili' => array('name' => 'bili','require' => true,'source' => 'get'),
                'type' => array('name' => 'type','require' => true,'source' => 'get'),
            ),
            'isseluser' => array(
                'uid' => array('name' => 'uid','require' => true,'source' => 'get'),
                //'telphone' => array('name' => 'telphone','require' => true,'source' => 'get'),
            ),
            'region' => array(
                'region' => array('name' => 'region','require' => true,'source' => 'get'),
            ),
            'likebranch' => array(
                'key' => array('name' => 'key','require' => true,'source' => 'get'),
            ),
            'listbanks' => array(
                'pro' => array('name' => 'pro','source' => 'get'),
                'city' => array('name' => 'city','source' => 'get'),
                'banks' => array('name' => 'banks','source' => 'get'),
            ),
            'listbranch' => array(
                'region' => array('name' => 'region','source' => 'get'),
                'pro' => array('name' => 'pro','source' => 'get'),
                'city' => array('name' => 'city','source' => 'get'),
                'branch_name' => array('name' => 'branch_name','source' => 'get'),
                'boss' => array('name' => 'boss','source' => 'get'),
                'phone' => array('name' => 'phone','source' => 'get'),
                'page' => array('name' => 'page','source' => 'get'),
            ),
            'upconfid' => array(
                'type' => array('name' => 'type'),
                'key' => array('name' => 'key'),
                'value' => array('name' => 'value'),
                'on_status' => array('name' => 'on_status'),
                'statu' => array('name' => 'statu'),
                'id' => array('name' => 'id'),
            ),
            'selid' => array(
                'id' => array('name' => 'id','source' => 'get'),
            ),
            'getpro' => array(
                'city' => array('name' => 'city'),
            ),
        );
    }
    /**
     * 添加配置信息
     */
    public function addconfig()
    {
        $user=apiGet("User.User.Selecttoken",array("searchToken"=>$this->token));
        if($user!=null)
        {
            $con=new config();
            return $con->addconfig($this->type,$this->key,$this->value,$this->on_status,$this->statu,$user["Id"]);
        }
        else
        {
          exceptions("token不存在！", -100);
        }
    }
    /**
     * 获取配置列表
     */
    public function selfig()
    {
//         $url="test.yusj.vip?s=User.User.Tokenverify&token=".$this->token."";
//         $isverify=curls($url)["data"];
//         if($isverify!=false)
//         {
            $con=new config();
            return  $con->selfig($this->type,$this->page);
//         }
//         else
//         {
//             exceptions("token不存在！", -100);
//         }
    }
    /**
     * 获取单个配置
     */
    public function selone()
    {
//         $url="test.yusj.vip?s=User.User.Tokenverify&token=".$this->token."";
//         $isverify=curls($url)["data"];
//         if($isverify!=false)
//         {
            $con=new config();
            return  $con->selone($this->type,$this->key);
//         }
//         else
//         {
//             exceptions("token不存在！", -100);
//         }
    }
    /**
     * 查询会员所属的分会
     * 
     */
    public function seluser(){
        $con=new config();
        return $con->seluser($this->uid);
    }
    /**
     * 查询会员所属的分会
     *
     */
    public function selalluser()
    {
        $user=apiGet("User.User.Selecttoken",array("searchToken"=>$this->token));
        if($user!=null)
        {
           $con=new config();
           return $con->seluser($user["Id"]);
        }
        else{
            exceptions("token不存在！", -100);
        }
    }
    /**
     * 修改收益比例
     */
    public function updbi()
    {
        if($this->type==0)
        {
          $con=new config();
          $con->updbi('getMoneyFromPay', $this->bili);
          $con->updbi('userGetMoneyFromSpread', $this->bili);
          return true;
        }
        else
        {
            $con=new config();
            $info=$con->selbi('getMoneyFromPay');
            $infos=$con->selbi('userGetMoneyFromSpread');
            for ($i=0;$i<count($infos);$i++)
            {
                if($infos[$i]["value"]==$info[0]["value"])
                {
                    $con->updmo($infos[$i]["id"], $this->bili);
                }
            }
            $con->updmo($info[0]["id"], $this->bili);
            return true;
        }
    }
    /***
     * 查询会员机构是否存在
     */
    public function isseluser()
    {
        $con=new config();
        $info=$con->isseluser($this->uid);
        if($info!=null)
        {
            return $info;
        }
        else{
            return false;
        }
    }
   /***
     * 查询地区分会
     */
    public function region()
    {
        $con=new config();
        $info=$con->region($this->region);
        if($info!=null)
        {
            return $info;
        }
        else{
            return false;
        }
    }
    /**
     * 模糊查询分会信息
     */
    public function likebranch()
    {
       $con=new config();
       return $con->likebranch($this->key);
    }
    /**
     * 银行条件查询
     */
    public function listbanks()
    {
       $bank=new config();
       return $bank->listbanks($this->pro, $this->city, $this->banks);
    }
    /**
     * 分会条件查询
     */
    public function listbranch()
    {
        $branch=new config();
        return $branch->listbranch($this->region, $this->pro, $this->city, $this->branch_name, $this->boss, $this->phone, $this->page);
    }
    /**
     * 修改配置系统资料
     */
    public function upconfid()
    {
        $conf=new config();
        return $conf->upconfid($this->type, $this->key, $this->value, $this->on_status, $this->statu, $this->id);
    }
    /**
     * id查询
     */
    public function selid(){
        $conf=new config();
        return $conf->selid($this->id);
    }
    /**查询省份*/
    public function getpro()
    {
       $conf=new config();
       $info=$conf->getpro($this->city);
       if($info!=null){
           return $info[0]["key"];
       }
    }
}