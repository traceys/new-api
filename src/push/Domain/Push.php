<?php
namespace Push\Domain;
use Push\Model\Push\Code as Mcode;
use Push\Model\Push\Watch as Mwatch;
use Push\Model\Push\Position as Mposition;
class Push
{
    ///推送码
    public function tscode($uid,$type,$code,$statu)
    {
        $ts=new Mcode();
        $tsc=$ts->selcode($uid, $type);
        if($tsc!=null)
        {
            $ts->tsupcode($uid, $type, $code, $statu, $tsc['id']);
        } 
        else{
            $ts->tscode($uid, $type, $code, $statu);
        }
    }
    ///修改推送已读反馈信息
    public function ydupd($message_id, $read){
        $wa=new Mwatch();
        $wa->upd($message_id, $read);
    }
    ///发起短信推送
    public function tsfq($uid, $type,$types, $to_uid, $code, $value){
        
        $fa=new Mwatch();
        switch ($type)
        {
            case '短信':
            $sd=sendmall($code, $value);
            if($sd==true){
                $res=1;
            }
            else{
                $res=-1;
            }
            break;
            case "站内信":
                $res=1;
            break;
            default:
                $res=1;
        }
        return $fa->add($uid, $type,$types, $to_uid, $code, $value, $res);
    }
    ///查询推送历史
    public function tsls($uid, $code, $type,$read_res, $page,$num)
    {
        $ps=new Mposition();
        $pid=$ps->selone($uid);
        $ls=new Mwatch();
        $info=$ls->sel($uid, $code, $type,$read_res, $page,$num,$pid["intposition"]);
        if($info!=null&&$uid!=null)
        {
            if($pid!=null)
            {
                if($info[0]["id"]>$pid["position"])
                {
                    $ps->updone($uid,$info[0]["id"]); 
                }
            }
            for ($i=0;$i<count($info);$i++)
            {
                $wa=new Mwatch();
                $wa->upd($info[$i]["id"], 1);
            }
        }
        return $info;
    }
    ///删除消息
    public function del($message_id,$uid){
        $ls=new Mwatch();
        $ls->del($message_id,$uid);
        return true;
    }
    ///查询站内信未读总量
    public function selread($uid)
    {
        $ls=new Mwatch();
        $ps=new Mposition();
        $pd=$ps->selone($uid);
        if($pd!=null)
        {
            $pid=$pd["position"];
        }
        else{
            $max=$ls->selmaxid();
            $pid=$max["maxid"];
        }
        return $ls->selread($uid,$pid);
    }
    ///查询
    public function selone($uid)
    {
        $ps=new Mposition();
        return $ps->selone($uid);
    }
    ///查询最大ID
    public function selmaxid(){
        $ls=new Mwatch();
        return $ls->selmaxid();
    }
    public function updinione($uid,$inipid)
    {
        $ps=new Mposition();
        return $ps->updinione($uid, $inipid);
    }
    public function addone($uid,$pid,$inipid)
    {
        $ps=new Mposition();
        return $ps->addone($uid,$pid,$inipid);
    }
}