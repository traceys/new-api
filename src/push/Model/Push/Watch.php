<?php
namespace Push\Model\Push;
use PhalApi\Model\NotORMModel as Model;

class Watch extends Model
{
    ///添加推送信息
    public function add($uid,$type,$types,$to_uid,$code,$value,$res)
    {
        $data=array(
            'uid'=>$uid,
            'type'=>$type,
            'types'=>$types,
            'to_uid'=>$to_uid,
            'code'=>$code,
            'value'=>$value,
            'time'=>date('Y-m-d H:i:s',time()),
            'res'=>$res,
            'read_res'=>0,
            'statu'=>1
        );
        $this->getORM()->insert($data);
        clearnCache(["push.push.model"],true);
    }
    ///修改推送信息
    public function upd($message_id,$read)
    {
        $data=array(
            'read_res'=>$read
        );
        $this->getORM()->where(["id"=>$message_id])->update($data);
        clearnCache(["push.push.model"],true);
    }
    ///删除
    public function del($message_id,$uid)
    {
        $data=array(
            'statu'=>-1
        );
        $this->getORM()->where(["id"=>$message_id,"to_uid"=>$uid])->update($data);
        clearnCache(["push.push.model"],true);
    }
    ///查询
    public function sel($uid,$code,$type,$read_res,$page,$num,$inipid)
    {
        $count=$this->selcount($uid, $code, $type, $read_res,$inipid);
        $list=cache(["push.push.model.push.watch.sel",$uid,$code,$type,$read_res,$page,$num,$inipid]);
        outputArgs(["num"=>$count,"pagenum"=>$num,"page"=>$page]);
        if($list!=null){
            return $list;
        }
        $sql='select * from api_push_watch where  statu=1';
        if($uid!=null){
            $sql.=" and (to_uid=".$uid." or to_uid=-1)";
        }
        if($code!=null){
            $sql.=" and code like '%".$code."%'";
        }
        if($type!=null){
            $sql.=" and type='".$type."'";
        }
        if($read_res!=null){
            $sql.=" and read_res=".$read_res."";
        }
        if($inipid!=null){
            $sql.=" and id>".$inipid."";
        }
        $sql.=" order by id DESC";
        if($page>0)
        {
           $pre= ($page-1)*$num;
           $next=$num;
           $sql.=" limit ".$pre.",".$next."" ;
        }
        $info=$this->getORM()->queryAll($sql);
        cache(["push.push.model.push.watch.sel",$uid,$code,$type,$read_res,$page,$num,$inipid],$info);
        return $info;
    }
    ///查询
    public function selcount($uid,$code,$type,$read_res,$inipid)
    {
        $count=cache(["push.push.model.push.watch.selcount",$uid,$code,$type,$read_res,$inipid]);
        if($count!=null){
            return $count;
        }
        $sql='select count(*) as num from api_push_watch where  statu=1';
        if($uid!=null){
            $sql.=" and (to_uid=".$uid." or to_uid=-1)";
        }
        if($code!=null){
            $sql.=" and code like '%".$code."%'";
        }
        if($type!=null){
            $sql.=" and type='".$type."'";
        }
        if($read_res!=null){
            $sql.=" and read_res=".$read_res."";
        }
        if($inipid!=null){
            $sql.=" and id>".$inipid."";
        }
        $info=$this->getORM()->queryAll($sql);
        cache(["push.push.model.push.watch.selcount",$uid,$code,$type,$read_res,$inipid],$info[0]["num"]);
        return $info[0]["num"];
    }
    ///查询站内信未读总量
    public function selread($uid,$pid)
    {
        $sql="select count(*) as num  from api_push_watch where statu=1 and read_res=0 and (to_uid=".$uid." or to_uid=-1) and type='站内信' and id>".$pid."";
        return $this->getORM()->queryAll($sql);
    }
    ///查询站内信最大ID
    public function selmaxid()
    {
       return  $this->getORM()->select("max(id) as maxid")->fetchOne();
    }
}