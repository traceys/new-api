<?php
namespace Push\Model\Push;
use PhalApi\Model\NotORMModel as Model;

class Code extends Model
{
    ///发起推送
    public function tscode($uid,$type,$code,$statu)
    {
        $data=array(
            'uid'=>$uid,
            'type'=>$type,
            'code'=>$code,
            'statu'=>$statu
        );
        $this->getORM()->insert($data);      
    }
    ///查询推送是否存在
    public function selcode($uid,$type){
      return  $this->getORM()->select('*')->where(['uid'=>$uid,'type'=>$type,'statu'=>1])->fetchOne();
    }
    ///修改推送
    public function tsupcode($uid,$type,$code,$statu,$id)
    {
        $data=array(
            'uid'=>$uid,
            'type'=>$type,
            'code'=>$code,
            'statu'=>$statu
        );
        $this->getORM()->where(["id"=>$id])->update($data);
    }
}