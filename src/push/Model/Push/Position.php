<?php
namespace Push\Model\Push;
use PhalApi\Model\NotORMModel as Model;

class Position extends Model
{
    //查询
    public function selone($uid)
    {
      return  $this->getORM()->where(["uid"=>$uid])->fetchOne();
    }
    ///修改
    public function updone($uid,$pid)
    {
      return $this->getORM()->where(["uid"=>$uid])->update(["position"=>$pid]);
    }
    ///添加
    public function addone($uid,$pid,$inipid)
    {
      return $this->getORM()->insert(["uid"=>$uid,"position"=>$pid,"intposition"=>$inipid]);
    }
    ///添加
    public function updinione($uid,$inipid)
    {
        return $this->getORM()->where(["uid"=>$uid])->update(["intposition"=>$inipid]);
    }
}