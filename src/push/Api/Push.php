<?php
namespace Push\Api;
use PhalApi\Api;
use Push\Domain\Push as DPush;

class Push extends Api
{
    public function getRules()
    {
        return array(
            'pushtype' => array(
                'uid' => array('name' => 'uid','require' => true,'source' => 'get'),
                'type' => array('name' => 'type','require' => true,'source' =>'get'),
                'code' => array('name' => 'code','require' => true,'source' =>'get'),
                'statu' => array('name' => 'statu','require' => true,'source' =>'get'),
                'token' => array('name' => 'token','require' => true,'source' => 'get'),
            ),
            'ydupd' => array(
                'message_id' => array('name' => 'message_id','require' => true,'source' => 'get'),
                'read' => array('name' => 'read','source' =>'get'),
                'token' => array('name' => 'token','require' => true,'source' => 'get'),
            ),
            'tsfq' => array(
                'uid' => array('name' => 'uid','require' => true,'source' => 'get'),
                'type' => array('name' => 'type','require' => true,'source' =>'get'),
                'types' => array('name' => 'types','source' =>'get'),
                'code' => array('name' => 'code','require' => true,'source' =>'get'),
                'value' => array('name' => 'value','require' => true,'source' =>'get'),
                'searchToken' => array('name' => 'searchToken','require' => true,'source' => 'get'),
            ),
            'tsls' => array(
                'uid' => array('name' => 'uid','source' => 'get'),
                'code' => array('name' => 'code','source' =>'get'),
                'type' => array('name' => 'type','source' =>'get'),
                'page' => array('name' => 'page','source' =>'get'),
                'token' => array('name' => 'token','source' => 'get'),
            ),
            'seltsls' => array(
                'uid' => array('name' => 'uid','source' => 'get'),
                'code' => array('name' => 'code','source' =>'get'),
                'type' => array('name' => 'type','source' =>'get'),
                'page' => array('name' => 'page','source' =>'get'),
                'read_res' => array('name' => 'read_res','source' => 'get'),
            ),
            'delmesg' => array(
                'message_id' => array('name' => 'message_id','require' => true,'source' => 'get'),
                'token' => array('name' => 'token','source' => 'get'),
            ),
            'selread' => array(
                'token' => array('name' => 'token','require' => true,'source' => 'get'),
            ),
            'updinione' => array(
                'uid' => array('name' => 'uid','require' => true,'source' => 'get'),
                'inipid' => array('name' => 'inipid','require' => true,'source' => 'get'),
            ),
            'addone' => array(
                'uid' => array('name' => 'uid','require' => true,'source' => 'get'),
                'pid' => array('name' => 'pid','require' => true,'source' => 'get'),
                'inipid' => array('name' => 'inipid','require' => true,'source' => 'get'),
            ),
            'selone' => array(
                'uid' => array('name' => 'uid','require' => true,'source' => 'get'),
            ),
        );
    } 
    /**
     * 推送推荐配置
     */  
    public function pushtype()
    {
//         $url="test.yusj.vip?s=User.User.Tokenverify&token=".$this->token."";
//         $isverify=curls($url)["data"];
//         if($isverify!=false)
//         {
            $pu=new DPush();
            return $pu->tscode($this->uid,$this->type,$this->code,$this->statu);
//         }
//         else
//         {
//             exceptions("token不存在！", -100);
//         }
    }
   /**
    * 推送修改已读反馈信息
    */
    public function ydupd()
    {
//         $url="test.yusj.vip?s=User.User.Tokenverify&token=".$this->token."";
//         $isverify=curls($url)["data"];
//         if($isverify!=false)
//         {
            $pu=new DPush();
            return $pu->ydupd($this->message_id,$this->read);
//         }
//         else
//         {
//             exceptions("token不存在！", -100);
//         }
    }
    /**
     * 推送发起
     */
    public function tsfq()
    {
        $uid=apiGet("User.User.Selecttoken", array('searchToken'=>$this->searchToken));
        if($uid!=false)
        {
            $ts=new DPush();
            $code="";
            if($this->code==null){
               $info=apiGet("User.User.userone",array("uid"=>$this->uid));
               $code=$info["telphone"];
            }
            else{
                $code=$this->code;
            }
            return $ts->tsfq($uid["Id"], $this->type,$this->types, $this->uid,$code,$this->value);
        }
        else
        {
            exceptions("token不存在！", -100);
        }
    }
    /**
     * 推送历史
     */
    public function tsls()
    {
         $uid=apiGet("User.User.Selecttoken", array('searchToken'=>$this->token));
         if($uid!=false)
         {
            $ts=new DPush();
            return $ts->tsls($uid["Id"], $this->code, $this->type,null,$this->page,8);
         }
         else
         {
            exceptions("token不存在！", -100);
         }
    }
    /**
     * 条件查询推送历史
     */
    public function seltsls()
    {
        $ts=new DPush();
        return $ts->tsls($this->uid, $this->code, $this->type,$this->read_res,$this->page,40);
    }
    /**
     * 删除
     */
    public function delmesg()
    {
        $uid=apiGet("User.User.Selecttoken", array('searchToken'=>$this->token));
        if($uid!=false)
        {
          $ts=new DPush();
          return $ts->del($this->message_id,$uid["Id"]);
        }
        else
        {
          exceptions("token不存在！", -100);
        }
    }
    /**
     * 查询站内信未读总量
     */
    public function selread()
    {
        $uid=apiGet("User.User.Selecttoken", array('searchToken'=>$this->token));
        if($uid!=false)
        {
          $ts=new DPush();
          return $ts->selread($uid["Id"]);
        }
        else
        {
            exceptions("token不存在！", -100);
        }
    }
    ///查询站内信初始化信息
    public function selone()
    {
        $ps=new DPush();
        return $ps->selone($this->uid);
    }
    ///查询最大ID
    public function selmaxid()
    {
        $ls=new DPush();
        return $ls->selmaxid();
    }
    ///修改初始位置
    public function updinione()
    {
        $ps=new DPush();
        return $ps->updinione($this->uid, $this->inipid);
    }
    ///修改
    public function addone()
    {
        $ps=new DPush();
        return   $ps->addone($this->uid,$this->pid,$this->inipid);
    }
}