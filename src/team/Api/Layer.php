<?php
namespace Team\Api;

use PhalApi\Api;
use Team\Domain\Team as DomainTeam;
use Team\Domain\Layer as DomainLayer;
use Team\Domain\Tree as DomainTree;
use Team\Domain\User as DomainUser;
/**
 * 默认接口服务类
 *
 * @author: dogstar <chanzonghuang@gmail.com> 2014-10-04
 */

class Layer extends Api {

	public function getRules() {
        return array(
            'list' => array(
                'token' 	=> array('name' => 'token', 'desc' => '用户token'),
            ),
            /**"type"=>1,"name"=>1,"desc"=>1,"user_name"=>1,"on_status"=>1 */
            'set' => array(
                'token' 	=> array('name' => 'token', 'desc' => '用户token'),
                'parent' 	=> array('name' => 'parent', 'desc' => '用户token'),
                'name' 	=> array('name' => 'name', 'desc' => '用户token'),
                'children' 	=> array('name' => 'children', 'desc' => '用户token'),
                'on_status' 	=> array('name' => 'on_status', 'desc' => '用户token'),
                'statu' 	=> array('name' => 'statu',"default"=>1, 'desc' => '用户token'),
            ),
            'check' => array(
                'token' 	=> array('name' => 'token', 'desc' => '用户token'),
                'parent' 	=> array('name' => 'parent', 'desc' => '用户token'),
                'children' 	=> array('name' => 'children', 'desc' => '用户token'),
            ),
            'childrenList' => array(
                'token' 	=> array('name' => 'token', 'desc' => '用户token'),
                'parent' 	=> array('name' => 'parent', 'desc' => '用户token'),
            )
        );
	}
	
	/**
	 * 默认接口服务
     * @desc 默认接口服务，当未指定接口服务时执行此接口服务
	 * @return string title 标题
	 * @return string content 内容
	 * @return string version 版本，格式：X.X.X
	 * @return int time 当前时间戳
     * @exception 400 非法请求，参数传递错误
	 */
    /**考虑是否在这里实现用户所在团队列表的接口 */
	public function list() {
        $confDo=new DomainLayer();
        $rs=$confDo->list();
        return $rs;
    }
    
    public function set(){
        checkAuth();
        $apiDo=new DomainLayer();
        // $data=[
        //     "type"=>$this->type,
        //     "name"=>$this->name,
        //     "desc"=>$this->desc,
        //     "user_name"=>$this->user_name,
        //     "on_status"=>$this->on_status,
        // ];
        $rs=$apiDo->set($this->parent,$this->children,$this->name,$this->on_status,$this->statu);
        return $rs;
    }

    public function check(){
        $apiDo=new DomainLayer();
        
        $rs=$apiDo->check($this->parent,$this->children);
        return $rs;
    }

    public function childrenList(){
        $apiDo=new DomainLayer();
        $rs=$apiDo->childrenList($this->parent);
        return $rs;
    }
}
