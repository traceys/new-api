<?php
namespace Team\Api;

use PhalApi\Api;
use Team\Domain\Team as DomainTeam;
use Team\Domain\Layer as DomainLayer;
use Team\Domain\Tree as DomainTree;
use Team\Domain\User as DomainUser;
/**
 * 默认接口服务类
 *
 * @author: dogstar <chanzonghuang@gmail.com> 2014-10-04
 */

class User extends Api {

	public function getRules() {
        return array(
            '_list' => array(
                'token' 	=> array('name' => 'token', 'desc' => '用户token'),
                'tid' 	=> array('name' => 'tid', 'desc' => '用户token'),
                'page' 	=> array('name' => 'page', 'default'=>-1,'desc' => '用户token'),
                'startTime' 	=> array('name' => 'startTime', 'default'=>false,'desc' => '用户token'),
                'endTime' 	=> array('name' => 'endTime','default'=>false, 'desc' => '用户token'),
            ),
            'count' => array(
                'token' 	=> array('name' => 'token', 'desc' => '用户token'),
                'tid' 	=> array('name' => 'tid', 'desc' => '用户token'),
            ),
            'teamList' => array(
                'token' 	=> array('name' => 'token', 'desc' => '用户token'),
                'id' 	=> array('name' => 'id', 'desc' => '用户token'),
            ),
            /**"type"=>1,"name"=>1,"desc"=>1,"user_name"=>1,"on_status"=>1 */
            'add' => array(
                'token' 	=> array('name' => 'token', 'desc' => '用户token'),
                'type' 	=> array('name' => 'type', 'desc' => '用户token'),
                'tid' 	=> array('name' => 'tid', 'desc' => '用户token'),
                'uid' 	=> array('name' => 'uid', 'desc' => '用户token'),
                'end_time' 	=> array('name' => 'end_time', 'desc' => '用户token'),
                'long_time' 	=> array('name' => 'long_time',"default"=>0,  'desc' => '用户token'),
                'on_status' 	=> array('name' => 'on_status',"default"=>1, 'desc' => '用户token'),
            ),
            'del' => array(
                'token' 	=> array('name' => 'token', 'desc' => '用户token'),
                
                'tid' 	=> array('name' => 'tid', 'desc' => '用户token'),
                'uid' 	=> array('name' => 'uid', 'desc' => '用户token'),
                
                'on_status' 	=> array('name' => 'on_status',"default"=>-1,  'desc' => '用户token'),
                'statu' 	=> array('name' => 'statu', "default"=>-1, 'desc' => '用户token'),
            ),
            'info' => array(
                'token' 	=> array('name' => 'token', 'desc' => '用户token'),
                
                'tid' 	=> array('name' => 'tid', 'desc' => '用户token'),
                'uid' 	=> array('name' => 'uid', 'desc' => '用户token'),
            ),
            'teamList' => array(
                'token' 	=> array('name' => 'token', 'desc' => '用户token'),
                'uid' 	=> array('name' => 'uid', 'desc' => '用户token'),
                'had' 	=> array('name' => 'had', 'desc' => '是否显示已过期的团队'),
            ),
            'update' => array(
                'token' 	=> array('name' => 'token', 'desc' => '用户token'),
                'type' 	=> array('name' => 'type', 'desc' => '用户token'),
                'tid' 	=> array('name' => 'tid', 'desc' => '用户token'),
                'uid' 	=> array('name' => 'uid', 'desc' => '用户token'),
                'end_time' 	=> array('name' => 'end_time', 'desc' => '用户token'),
                'on_status' 	=> array('name' => 'on_status',"default"=>1,  'desc' => '用户token'),
                'statu' 	=> array('name' => 'statu',"default"=>1,  'desc' => '用户token'),
            ),
            'addTime' => array(
                'token' 	=> array('name' => 'token', 'desc' => '用户token'),
                'tid' 	=> array('name' => 'tid', 'desc' => '用户token'),
                'uid' 	=> array('name' => 'uid', 'desc' => '用户token'),
                'end_time' 	=> array('name' => 'end_time',"default"=>false,  'desc' => '用户token'),
                'long_time' 	=> array('name' => 'long_time',"default"=>0,  'desc' => '用户token'),
            ),
            'getSuperior' => array(
                'tid' 	=> array('name' => 'tid', 'desc' => '团队tid'),
                'uid' 	=> array('name' => 'uid', 'desc' => '会员uid'),
            
            ),
            'listin' => array(
                'tid' 	=> array('name' => 'tid', 'desc' => '团队tid'),
            
            ),
            // "countNumJigou"
        );
	}
	
	/**
	 * 默认接口服务
     * @desc 默认接口服务，当未指定接口服务时执行此接口服务
	 * @return string title 标题
	 * @return string content 内容
	 * @return string version 版本，格式：X.X.X
	 * @return int time 当前时间戳
     * @exception 400 非法请求，参数传递错误
	 */
    /**考虑是否在这里实现用户所在团队列表的接口 */
	public function _list() {
        $confDo=new DomainUser();
        // var_dump($this->startTime);
        $rs=$confDo->_list($this->tid,$this->page,$this->startTime!=""?$this->startTime:false,$this->endTime!=""?$this->endTime:false);
        return $rs;
    }
    public function listin() {
        $confDo=new DomainUser();
        // var_dump($this->startTime);
        $rs=$confDo->listin($this->tid);
        return $rs;
    }
    
    public function count() {
        $confDo=new DomainUser();
        // var_dump($this->startTime);
        $rs=$confDo->teamUserCount($this->tid);
        return $rs;
    }
    
    public function add(){
        checkAuth();
        $apiDo=new DomainUser();
        // $data=[
        //     "utype"=>$this->type,
        //     "tid"=>$this->tid,
        //     "uid"=>$this->uid,
        //     "end_time"=>$this->end_time?$end_time:,
        //     "on_status"=>$this->on_status,
        // ];
        
        $rs=$apiDo->add($this->tid,$this->uid,$this->type,isset($this->end_time)?$this->end_time:"2032-01-01 00:00:00",$this->on_status,$this->long_time);
        return $rs;
    }

    public function del(){
        checkAuth();
        $apiDo=new DomainUser();
        $rs=$apiDo->del($this->tid,$this->uid,$this->on_status,$this->statu);
        return $rs;
    }

    public function addTime(){
        checkAuth();
        $apiDo=new DomainUser();
        $rs=$apiDo->addTime($this->tid,$this->uid,$this->end_time,$this->long);
        return $rs;
    }

    public function update(){
        checkAuth();
        $apiDo=new DomainUser();
        // $data=[
        //     "type"=>$this->type,
        //     "name"=>$this->name,
        //     "desc"=>$this->desc,
        //     "user_name"=>$this->user_name,
        //     "on_status"=>$this->on_status,
        //     "statu"=>$this->statu,
        // ];
        // var_dump($this->end_time?$this->end_time:"2032-01-01 00:00:00");
        // die();
        $rs=$apiDo->update($this->tid,$this->uid,$this->type,$this->end_time?$this->end_time:"2032-01-01 00:00:00",$this->on_status);
        return $rs;
    }

    public function info(){
        $apiDo=new DomainUser();
        $rs=$apiDo->info($this->tid,$this->uid);
        return $rs;
    }

    public function teamList(){
        $apiDo=new DomainUser();
        $rs=$apiDo->teamList($this->uid,$this->had);
        return $rs;
    }

    public function countNumJigou(){
        $domain=new DomainUser();
        $rs=$domain->countNumJigou();
        return $rs;
    }

    public function test(){
        
        $apiDo=new DomainUser();
        $rs=$apiDo->addProCode();
        return $rs;
    }

    public function clearnOutUser(){
        // checkAuth();
        $apiDo=new DomainUser();
        $rs=$apiDo->clearnOutUser();
        return $rs;
    }
    public function timeOutUser(){
        $apiDo=new DomainUser();
        $rs=$apiDo->getTimeOutMessage();
        return $rs;
    }
    /**查询上级信息**/
    public function getSuperior()
    {
        $apiDo=new DomainTree();
        $tree=new DomainUser();
        $team=new DomainTeam();
        $uid=null;
        $teamlist=$tree->teamList($this->uid,true);
        foreach ($teamlist as $key=>$value)
        {
            $ftid=$apiDo->gettid($value["id"]);
            if($ftid==$this->tid)
            {
                $uid=$value["sup_uid"];
            }
        }
        return $uid;
    }
}
