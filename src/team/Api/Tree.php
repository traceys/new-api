<?php
namespace Team\Api;

use PhalApi\Api;
use Team\Domain\Team as DomainTeam;
use Team\Domain\Layer as DomainLayer;
use Team\Domain\Tree as DomainTree;
use Team\Domain\User as DomainUser;
/**
 * 默认接口服务类
 *
 * @author: dogstar <chanzonghuang@gmail.com> 2014-10-04
 */

class Tree extends Api {

	public function getRules() {
        return array(
            'tree' => array(
                'token' 	=> array('name' => 'token', 'desc' => '用户token'),
                'pid' 	=> array('name' => 'pid', 'desc' => '用户token'),
                
            ),
            'childrenList' => array(
                'token' 	=> array('name' => 'token', 'desc' => '用户token'),
                'pid' 	=> array('name' => 'pid', 'desc' => '用户token'),
                
            ),
            /**"type"=>1,"name"=>1,"desc"=>1,"user_name"=>1,"on_status"=>1 */
            'add' => array(
                'token' 	=> array('name' => 'token', 'desc' => '用户token'),
                'pid' 	=> array('name' => 'pid', 'desc' => '用户token'),
                'cid' 	=> array('name' => 'cid', 'desc' => '用户token'),
            ),
            'check' => array(
                'token' 	=> array('name' => 'token', 'desc' => '用户token'),
                'pid' 	=> array('name' => 'pid', 'desc' => '用户token'),
                'cid' 	=> array('name' => 'cid', 'desc' => '用户token'),
            ),
            'del' => array(
                'token' 	=> array('name' => 'token', 'desc' => '用户token'),
                'pid' 	=> array('name' => 'pid', 'desc' => '用户token'),
                'cid' 	=> array('name' => 'cid', 'desc' => '用户token'),
            ),
            'gettid' => array(
                'tid' 	=> array('name' => 'tid', 'desc' => '用户token'),
            
            ),
            'getfuinfo' => array(
                'tid' 	=> array('name' => 'tid', 'desc' => '团队tid'),
                'uid' 	=> array('name' => 'uid', 'desc' => '会员uid'),
            
            ),
        );
	}
	/**
	 * 默认接口服务
     * @desc 默认接口服务，当未指定接口服务时执行此接口服务
	 * @return string title 标题
	 * @return string content 内容
	 * @return string version 版本，格式：X.X.X
	 * @return int time 当前时间戳
     * @exception 400 非法请求，参数传递错误
	 */
    /**考虑是否在这里实现用户所在团队列表的接口 */
	public function tree() {
        $confDo=new DomainTree();
        $rs=$confDo->tree($this->pid);
        return $rs;
    }
    
    public function add(){
        checkAuth();
        $apiDo=new DomainTree();
        // $data=[
        //     "type"=>$this->type,
        //     "name"=>$this->name,
        //     "desc"=>$this->desc,
        //     "user_name"=>$this->user_name,
        //     "on_status"=>$this->on_status,
        // ];
        $rs=$apiDo->add($this->pid,$this->cid);
        return $rs;
    }
    public function del(){
        checkAuth();
        $apiDo=new DomainTree();
        // $data=[
        //     "type"=>$this->type,
        //     "name"=>$this->name,
        //     "desc"=>$this->desc,
        //     "user_name"=>$this->user_name,
        //     "on_status"=>$this->on_status,
        // ];
        $rs=$apiDo->del($this->pid,$this->cid);
        return $rs;
    }

    public function check(){
        $apiDo=new DomainTree();
        $rs=$apiDo->check($this->pid,$this->cid);
        return $rs;
    }

    public function childrenList(){
        $apiDo=new DomainTree();
        $rs=$apiDo->childrenList($this->pid);
        return $rs;
    }
    /**查询团队机构id/
     *
     */
    public function gettid()
    {
        $apiDo=new DomainTree();
         return $apiDo->gettid($this->tid);
    }
    /**查看父级团队获取上级或本级部门**/
    public function getfuinfo()
    {
        $apiDo=new DomainTree();
        $tree=new DomainUser();
        $team=new DomainTeam();
        $bumen=null;
        $teamlist=$tree->teamList($this->uid,true);
        foreach ($teamlist as $key=>$value)
        {
            $ftid=$apiDo->gettid($value["id"]);
            if($ftid==$this->tid)
            {   
              $treeinfo=$apiDo->getfuinfo($value["id"]);
              if($treeinfo["tid"]!=$ftid)
              {
               $teaminfof=$team->info($treeinfo["tid"])."-";
              }
              else
              {
               $teaminfof=null;
              }
              $teaminfoz=$team->info($treeinfo["c_tid"]);
              $bumen=$teaminfof["name"].$teaminfoz["name"];
            }
        }
        return $bumen;
    }
    
}
