<?php
namespace Team\Api;

use PhalApi\Api;
use Team\Domain\Team as DomainTeam;
use Team\Domain\Layer as DomainLayer;
use Team\Domain\Tree as DomainTree;
use Team\Domain\User as DomainUser;
/**
 * 默认接口服务类
 *
 * @author: dogstar <chanzonghuang@gmail.com> 2014-10-04
 */

class Team extends Api {

	public function getRules() {
        return array(
            'list' => array(
                'token' 	=> array('name' => 'token', 'desc' => '用户token'),
            ),
            /**"type"=>1,"name"=>1,"desc"=>1,"user_name"=>1,"on_status"=>1 */
            'add' => array(
                'token' 	=> array('name' => 'token', 'desc' => '用户token'),
                'type' 	=> array('name' => 'type', 'desc' => '用户token'),
                'name' 	=> array('name' => 'name', 'desc' => '用户token'),
                'desc' 	=> array('name' => 'desc', 'desc' => '用户token'),
                'user_name' 	=> array('name' => 'user_name', 'desc' => '用户token'),
                'on_status' 	=> array('name' => 'on_status', 'desc' => '用户token'),
            ),
            'update' => array(
                'token' 	=> array('name' => 'token', 'desc' => '用户token'),
                'id' 	=> array('name' => 'id', 'desc' => '用户token'),
                'type' 	=> array('name' => 'type', 'desc' => '用户token'),
                'name' 	=> array('name' => 'name', 'desc' => '用户token'),
                'desc' 	=> array('name' => 'desc', 'desc' => '用户token'),
                'user_name' 	=> array('name' => 'user_name', 'desc' => '用户token'),
                'on_status' 	=> array('name' => 'on_status', 'desc' => '用户token'),
                'statu' 	=> array('name' => 'statu', 'desc' => '用户token'),
            ),
            'info' => array(
                'token' 	=> array('name' => 'token', 'desc' => '用户token'),
                'id' 	=> array('name' => 'id', 'desc' => '用户token'),
            ),
            'adddepartment' => array(
//                 'token' 	=> array('name' => 'token', 'desc' => '用户token'),
//                 'type' 	=> array('name' => 'type', 'desc' => '用户token'),
                'name' 	=> array('name' => 'name', 'desc' => '部门名称'),
                'pid' 	=> array('name' => 'pid', 'desc' => '上级部门ID'),
                'open' 	=> array('name' => 'open', 'desc' => '是否公开'),
//                 'user_name' 	=> array('name' => 'user_name', 'desc' => '用户token'),
//                 'on_status' 	=> array('name' => 'on_status', 'desc' => '用户token'),
            ),
            'deldepartment' => array(
                'tid' 	=> array('name' => 'tid', 'desc' => '用户token'),
            ),
        );
	}
	
	/**
	 * 默认接口服务
     * @desc 默认接口服务，当未指定接口服务时执行此接口服务
	 * @return string title 标题
	 * @return string content 内容
	 * @return string version 版本，格式：X.X.X
	 * @return int time 当前时间戳
     * @exception 400 非法请求，参数传递错误
	 */
    /**考虑是否在这里实现用户所在团队列表的接口 */
	public function _list() {
        $confDo=new DomainTeam();
        $rs=$confDo->list();
        return $rs;
    }
    
    public function add(){
        checkAuth();
        $apiDo=new DomainTeam();
        $data=[
            "type"=>$this->type,
            "name"=>$this->name,
            "desc"=>$this->desc,
            "user_name"=>$this->user_name,
            "on_status"=>$this->on_status,
        ];
        $rs=$apiDo->insert($data);
        return $rs;
    }

    public function update(){
        checkAuth();
        $apiDo=new DomainTeam();
        $data=[
            "type"=>$this->type,
            "name"=>$this->name,
            "desc"=>$this->desc,
            "user_name"=>$this->user_name,
            "on_status"=>$this->on_status,
            "statu"=>$this->statu,
        ];
        $rs=$apiDo->update($this->id,$data);
        return $rs;
    }

    public function info(){
        $apiDo=new DomainTeam();
        $rs=$apiDo->info($this->id);
        return $rs;
    }
    /**
     * 添加部门
     */    
    public function adddepartment()
    {
       $apiDo=new DomainTeam();
        $data=[
            "type"=>"leaf",
            "name"=>$this->name,
            "desc"=>"部门",
            "user_name"=>"子部门",
            "on_status"=>$this->open,
        ];
        $cid=$apiDo->insert($data);
        $apitree=new DomainTree();
        $apitree->add($this->pid, $cid);
        return true;
    }
    /**
     * 删除部门
     */
    public function deldepartment(){
        $confDo=new DomainUser();
        $rs=$confDo->_list($this->tid,-1,false,false);
        if($rs!=null)
        {
            exceptions("该部门下还有员工,无法删除！！", 6);
        }
        else{
            $apiDo=new DomainTeam();
            $apiDo->deldepartment($this->tid);
            return true;
        }
    }
}
