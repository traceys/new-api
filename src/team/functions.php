<?php
namespace Team;

function hello() {
    return 'Hey, man~';
}

function userAddTime($endTime,$long){
    $now=time();
    $endTime=strtotime($endTime);
    $startTime=$now>$endTime?$now:$endTime;
    // var_dump($startTime);
    return date($startTime+ $long);
}
