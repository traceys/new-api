<?php
namespace Team\Domain;

use Team\Model\Layer as ModelLayer;
use Team\Model\Team as ModelTeam;
use Team\Model\Tree as ModelTree;
use Team\Model\User as ModelUser;

class Tree {

    public function add($pid,$cid) {
        $model=new ModelTree();
        $modelTeam=new ModelTeam();
        $modelLayer=new ModelLayer();


        $info=$model->info($pid,$cid);
        if($info){
            return false;
        }
        $pInfo=$modelTeam->info($pid);
        $cInfo=$modelTeam->info($cid);
        if(!$pInfo || !$cInfo){
            return false;
        }

//         $checkLayer=$modelLayer->hadLayer($pInfo["type"],$cInfo["type"]);
//         if(!$checkLayer){
//             return false;
//         }
        // $had=$modelLayer->info($children);
        
        $rs=$model->_insert($pid,$cid);
        return $rs;
    }

    public function del($pid,$cid){
        $model=new ModelTree();
        $rs=$model->_update($pid,$cid,-1);
        return $rs;
    }

    public function childrenList($tid) {
        $model = new ModelTree();
        return $model->list($tid);
    }


    public function tree($tid) {
        $model=new ModelTree();
        $infos=$model->getTree($tid);
        return $infos;
    }

    public function check($pid,$cid){
        $model=new ModelTree();
        $infos=$model->info($pid,$cid);
        return $infos;
    }
    /**获取顶级团队tid*/
    public function getonetid($tid)
    {
        $model=new ModelTree();
        $tid=$model->getonetid($tid);
        return $tid;
    }
    /**查询团队机构id/
     *
     */
    public function gettid($tid)
    {
        $model=new ModelTree();
        $tid=$model->gettid($tid);
        return $tid;
    }
    /**查看父级团队**/
    public function getfuinfo($tid)
    {
        $model=new ModelTree();
        $rs=$model->getfuinfo($tid);
        return $rs;
    }
}
