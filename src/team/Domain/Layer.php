<?php
namespace Team\Domain;

use Team\Model\Layer as ModelLayer;
use Team\Model\Team as ModelTeam;
use Team\Model\Tree as ModelTree;
use Team\Model\User as ModelUser;

class Layer {

    public function set($parent,$children,$name,$on_status=0,$statu=0) {
        $modelLayer=new ModelLayer();
        
        $had=$modelLayer->info($children);
        
        $infos=$modelLayer->hadLayer($parent,$children);
        $on_status=$on_status!=0?$on_status:$infos["on_status"];
        $statu=$statu!=0?$statu:$infos["statu"];
        if($infos){
            $rs=$modelLayer->_update($parent,$children,["on_status"=>$on_status,"name"=>$name,"statu"=>$statu]);
            return $rs;
        }
        if($had){
            return false;
        }
        
        $rs=$modelLayer->_insert(["parent"=>$parent,"children"=>$children,"on_status"=>$on_status?$on_status:1,"name"=>$name]);
        return $rs;
    }

    public function check($parent, $children) {
        $modelLayer=new ModelLayer();
        $infos=$modelLayer->hadLayer($parent,$children);
        return $infos;
    }

    public function childrenList($parent) {
        $model = new ModelLayer();
        return $model->list($parent);
    }

    
}
