<?php
namespace Team\Domain;

use Team\Model\Layer as ModelLayer;
use Team\Model\Team as ModelTeam;
use Team\Model\Tree as ModelTree;
use Team\Model\User as ModelUser;
use Team\Model\UserLog as ModelLog;

class User {

    public function add($tid,$uid,$utype,$end_time="2032-01-01 00:00:00",$on_status=1,$long=0) {
        $modelUser=new ModelUser();
        $modelTeam=new ModelTeam();

        $teamInfo=$modelTeam->info($tid);
        if(!$teamInfo){
            return false;
        }
        $userInfo=$modelUser->info($tid,$uid);
        if($userInfo){
            if($long==0){
                $end_time=$end_time;
            }else{
                $end_time=\Auth\userAddTime($userInfo["end_time"],$long);
            }
            if($teamInfo["type"]=="vip团队"){
                //注册推广码
                $this->addProCode($uid,$end_time);
                $this->addShouyi($uid);
                if(\PhalApi\DI()->uid==0){
                addjifen("xufvip",$uid);
                userMessage($uid,"VIP服务","您已成功升级为育视界VIP会员，到期时间为".$end_time."，体验更多会员视频点播权益，尽在育视界。详询400-991-4418。育视界，让教育无处不在。");
            }
            }

            // if($teamInfo["type"]=="subaccount"){
            //     $subbInfo=$this->getSubbInfo($teamInfo["id"]);
            //     userMessage($uid,"vip升级成功","您已成功升级为育视界VIP会员，到期时间为".$end_time."，体验更多会员视频点播权益，尽在育视界。详询400-991-4418。育视界，让教育无处不在。");
            // }
            $rs=$modelUser->_update($tid,$uid,$utype,$end_time,1);
            $this->userLog($tid,$uid,"update",$end_time,1);
            
            return $userInfo["id"];
        }

        // $had=$modelLayer->info($children);
        if($long==0){
            $end_time=$end_time;
        }else{
            $end_time=\Auth\userAddTime("",$long);
        }
        if($teamInfo["type"]=="vip团队"){
            //注册推广码
            if(\PhalApi\DI()->uid==0){
            addjifen("cwvip",$uid);
            userMessage($uid,"VIP服务","您已成功升级为育视界VIP会员，到期时间为".$end_time."，体验更多会员视频点播权益，尽在育视界。详询400-991-4418。育视界，让教育无处不在。");
        }
            $this->addProCode($uid,$end_time);
            $this->addShouyi($uid);

        }

        // if($teamInfo["type"]=="subaccount"){
        //     userMessage($uid,"vip升级成功","您已成功升级为育视界VIP会员，到期时间为".$end_time."，体验更多会员视频点播权益，尽在育视界。详询400-991-4418。育视界，让教育无处不在。");
        // }

        $rs=$modelUser->_insert($tid,["uid"=>$uid,"utype"=>$utype,"end_time"=>$end_time,"on_status"=>$on_status]);
        $this->userLog($tid,$uid,"create",$end_time,$on_status);
        return $rs;
    }

    public function del($tid,$uid,$on_status=-1,$statu=-1) {
        $model=new ModelUser();
        $modelLayer=new ModelLayer();
        $had=$model->info($tid,$uid);
        if(!$had){
            return false;
        }
        
        $rs=$model->_update($tid,$uid,false,false,$on_status,$statu);
        $this->userLog($tid,$uid,"delete",$had["end_time"],$on_status);
        return $rs;
    }

    public function update($tid,$uid,$type,$end_time=false,$on_status=false,$statu=1) {
        $model=new ModelUser();
        $modelTeam=new ModelTeam();
        $had=$model->info($tid,$uid);
        if(!$had){
            return false;
        }
        $teamInfo=$modelTeam->info($tid);
        if($teamInfo["type"]=="vip团队"){
            //注册推广码
            if(\PhalApi\DI()->uid==0){
            addjifen("xufvip",$uid);
            userMessage($uid,"VIP服务","您已成功升级为育视界VIP会员，到期时间为".$end_time===false?$had["end_time"]:$end_time."，体验更多会员视频点播权益，尽在育视界。详询400-991-4418。育视界，让教育无处不在。");
            }
            $this->addProCode($uid,$end_time);
            $this->addShouyi($uid);
        }
        
        $rs=$model->_update($tid,$uid,$type,$end_time===false?$had["end_time"]:$end_time,$on_status===false?$had["on_status"]:$on_status,$statu);
        $this->userLog($tid,$uid,"update",$end_time===false?$had["end_time"]:$end_time,$on_status===false?$had["on_status"]:$on_status);
        return $rs;
    }

    public function addTime($tid,$uid,$end_time=false,$long=0) {
        $model=new ModelUser();
        $modelLayer=new ModelLayer();
        $had=$model->info($tid,$uid);
        if(!$had){
            return false;
        }
        // var_dump($end_time);
        if($end_time==false){
            $end_time=\Auth\userAddTime($had["end_time"],$long);
        }
        $teamInfo=$modelTeam->info($tid);
        if($teamInfo["type"]=="vip团队"){
            //注册推广码
            if(\PhalApi\DI()->uid==0){
                addjifen("xufvip",$uid);
                userMessage($uid,"VIP服务","您已成功升级为育视界VIP会员，到期时间为".$end_time."，体验更多会员视频点播权益，尽在育视界。详询400-991-4418。育视界，让教育无处不在。");
            }
            
            $this->addProCode($uid,$end_time);
            $this->addShouyi($uid);
        }
        // var_dump($end_time);
        // var_dump($had["end_time"]);
        // var_dump($long);
        $rs=$model->_update($tid,$uid,$had["utype"],$end_time,1);
        $this->userLog($tid,$uid,"update",$end_time,1);
        // $on_status!==false?$had["end_time"]:$end_time
        return $rs;
    }


    public function info($tid,$uid) {
        $model=new ModelUser();
        $infos=$model->info($tid,$uid);

        /**这里根据返回结束时间，选择是否关闭该用户在团队内的数据 */
        return $infos;
    }


    public function _list($tid,$page,$startTime,$endTime) {
        $model = new ModelUser();
        // var_dump($startTime);
        return $model->_list($tid,$page,$startTime,$endTime);
    }

    /**用户所在团队列表 */
    public function teamList($uid,$had=false){
        $model = new ModelUser();
        $modelTeam=new ModelTeam();
        $list=$model->teamList($uid,$had);
        if($list!=false){
            for($i=0;$i<count($list);$i++){
                $teamInfo=$modelTeam->info($list[$i]["tid"]);
                unset($teamInfo["Id"]);
                unset($teamInfo["on_status"]);
                unset($teamInfo["statu"]);
                $list[$i]=array_merge($list[$i],$teamInfo);
            }
            
        }
        return $list;
    }

    public function teamUserCount($tid){
        $model = new ModelUser();
        $res=$model->_countNum($tid);
        return $res;
    }

    public function countNumJigou(){
        $model = new ModelUser();
        $res=$model->_countNumJigou();
        return $res;
    }

    public function addProCode($uid,$time){
        $res=apiGet("Currency.receive.findUidPro",["uid"=>$uid,"type"=>"PromotionCode"]);
        if($res){
            $code=$res[0]["key"];
            $rs=apiGet("Currency.receive.disstoptimme",["code"=>$code,"time"=>$time]);
        }else{
            $rs=apiGet("Currency.receive.appenddis",["uid"=>$uid,"type"=>"PromotionCode","item_id"=>1,"reduce_num"=>10,"num"=>1000000,"time"=>$time,"astype"=>"discount","startTime"=>dateTime()]);
        }
        return $rs;
    }

    public function getTimeOutMessage(){
        $model = new ModelUser();
        $res=$model->getTimeOutUser();
        //园所到期通知
        // var_dump($res);
        // die();
        for($n=0;$n<count($res[0]);$n++){
            $subbInfo=$this->getSubbInfo($res[0][$n]["uid"]);
            // var_dump($subbInfo);
            $userInfo=userInfo($res[0][$n]["uid"]);
            userMessage($res[0][$n]["uid"],"园所服务","".$subbInfo["username"]."园长您好，您育视界账号 ".$userInfo["name"]."的机构会员身份即将到期，到期时间".$res[0][$n]["end_time"]."，为避免影响账号功能的使用，请尽快联系区域总监进行续约。详询400-991-4418。育视界，让教育无处不在。",1);
        }
        //VIP到期通知

        for($n=0;$n<count($res[1]);$n++){
            // $subbInfo=$this->getSubbInfo($res[1][$n]["uid"]);
            $userInfo=userInfo($res[1][$n]["uid"]);
            // var_dump($userInfo);
            userMessage($res[1][$n]["uid"],"VIP服务","尊敬的VIP会员".$userInfo["name"]."，您的VIP身份即将到期，到期时间".$res[1][$n]["end_time"]."，为避免影响账号功能的使用，请及时续费哦。详询400-991-4418。育视界，让教育无处不在。",1);
        }
        return true;
    }


    public function addShouyi($uid){
        $res=apiGet("Configure.Configure.Selone",["key"=>$uid,"type"=>"userGetMoneyFromSpread"]);
        if($res){
            return ;
            // $code=$res[0]["key"];
            // $rs=apiGet("Currency.receive.disstoptimme",["code"=>$code,"time"=>$time]);
        }
        $res=apiGet("Configure.Configure.Selone",["key"=>"vip","type"=>"getMoneyFromPay"]);
        $value=$res["value"];
        $rs=apiGet("Configure.Configure.Addconfig",["key"=>$uid,"type"=>"userGetMoneyFromSpread","value"=>$value,"on_status"=>1,"statu"=>1]);
        
        return $rs;
    }

    public function userLog($tid,$uid,$act,$endTime,$status){
        $model=new ModelLog();
        $model->_insert(["tid"=>$tid,"uid"=>$uid,"act"=>$act,"end_time"=>$endTime,"on_status"=>$status]);
    }

    public function clearnOutUser(){
        $model=new ModelUser();
        $list=$model->getOutUser();
        for($n=0;$n<count($list);$n++){
            $this->userLog($list[$n]["tid"],$list[$n]["uid"],"timeout",$list[$n]["end_time"],-1);
        }
        return $model->clearnOutUser();
    }

    protected function getSubbInfo($tid){
        if($tid<1){return false;}
        $res=apiGet("User.Mechanism.teammech",["tid"=>$tid]);
        // var_dump($res);
        return count($res)>0?$res[0]:[];
    }

    public function listin($tid) {
        $model=new ModelUser();
        $list=$model->listin($tid);
        return $list;
    }
}
