<?php
namespace Team\Domain;

use Team\Model\Layer as ModelLayer;
use Team\Model\Team as ModelTeam;
use Team\Model\Tree as ModelTree;
use Team\Model\User as ModelUser;

class Team {

    public function insert($teamInfo) {
        $model=new ModelTeam();
        
        // $had=$modelLayer->info($children);
        
        $rs=$model->_insert($teamInfo);
        return $rs;
    }

    public function update($id,$teamInfo) {
        $modelLayer=new ModelTeam();
        
        $had=$modelLayer->info($id);
        if(!$had){
            return false;
        }
        
        $rs=$modelLayer->_update($id,$teamInfo);
        return $rs;
    }


    public function info($id) {
        $modelLayer=new ModelTeam();
        $infos=$modelLayer->info($id);
        return $infos;
    }

    /**ɾ������*/
    public function deldepartment($id)
    {
        $modelLayer=new ModelTeam();
        $modelLayer->deldepartment($id);
        return true;
    }
}
