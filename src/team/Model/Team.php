<?php
namespace Team\Model;

use PhalApi\Model\NotORMModel as NotORM;

/**

CREATE TABLE `phalapi_curd` (
    `id` int(10) NOT NULL AUTO_INCREMENT,
    `title` varchar(20) DEFAULT NULL,
    `content` text,
    `state` tinyint(4) DEFAULT NULL,
    `post_date` datetime DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

 */

class Team extends NotORM {

    protected function getTableName($id) {
        return 'team_team';
    }


    /**创建团队 */
    public function _insert($data){
        $demo=["type"=>1,"name"=>1,"desc"=>1,"user_name"=>1,"on_status"=>1,"statu"=>1];
        $data=\Auth\createDataFromTemp($demo,$data);
        if($data==false){
            return false;
        }
        $rs=$this->getORM()
            ->insert($data);
        if($rs){
            clearnCache("team.team.model",true);
        }
        $id=$this->getORM()->insert_id();
        return $id;
    }

    /**查询团队是否存在 */
    //public function hadTeam($id)
    /**修改团队 */
    public function _update($id,$data){
        $demo=["type"=>1,"name"=>1,"desc"=>1,"user_name"=>1,"on_status"=>1];
        $data=\Auth\createDataFromTemp($demo,$data);
        if($data==false){
            return false;
        }
        $rs=$this->getORM()
            // ->where("(id,statu)",[$id,1])
            ->where(["id"=>$id,"statu"=>1])
            ->update($data);
        if($rs){
            clearnCache("team.team.model",true);
        }
        return $rs;
    }

    /**团队信息 */

    public function info($id){
        $caches=cache(["team.team.model.team.info",$id]);
        if($caches!==null){
            return $caches;
        }
        $teamInfo=$this->getORM()
            ->select("id,type,user_name,name,`desc`,on_status,statu")
            ->where("(id)",[$id])
            ->where(["statu"=>1])
            ->fetchOne();
        cache(["team.team.model.team.info",$id],$teamInfo,6000);
            
        return $teamInfo;
    }

    /**团队列表 */
    public function _list($idList,$order="id",$page=1){
        $caches=cache(["team.team.model.team.list",$idList,$order,$page]);
        if($caches!==null){
            return $caches;
        }
        $pageNum=10;
        $list=$this->getORM()
            ->select("id,type,user_name,name,`desc`,on_status")
            ->where("(id)",[$idList])
            ->where(["statu"=>1])
            ->order($order)
            ->limit(($page-1)*$pageNum,$pageNum)
            ->fetchAll();
        cache(["team.team.model.team.list",$idList,$order,$page],$list,600);
        return $list;
    }

     /**删除部门*/
    public function deldepartment($id)
    {
       $this->getORM()->where(["id"=>$id])->update(["statu"=>-1]);
       clearnCache("team.team.model",true);
       return true;
    }
    // public function getListItems($state, $page, $perpage) {
    //     $caches=cache(["team.team.model.team.list",$idList,$order,$page]);
    //     if($caches!==null){
    //         return $caches;
    //     }
    //     return $this->getORM()
    //         ->select('*')
    //         ->where('state', $state)
    //         ->order('post_date DESC')
    //         ->limit(($page - 1) * $perpage, $perpage)
    //         ->fetchAll();
    // }

    // public function getListTotal($state) {
    //     $total = $this->getORM()
    //         ->where('state', $state)
    //         ->count('id');

    //     return intval($total);
    // }
}
