<?php
namespace Team\Model;

use PhalApi\Model\NotORMModel as NotORM;

/**

CREATE TABLE `phalapi_curd` (
    `id` int(10) NOT NULL AUTO_INCREMENT,
    `title` varchar(20) DEFAULT NULL,
    `content` text,
    `state` tinyint(4) DEFAULT NULL,
    `post_date` datetime DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

 */

class Tree extends NotORM {

    protected function getTableName($id) {
        return 'team_tree';
    }
    /**团队父级子链接 */
    public function _insert($pid,$cid){
        // $demo=["uid"=>1,"utype"=>1,"on_status"=>1];
        // $newData=createDataFromTemp($demo);
        $newData["time"]=\Auth\dateTime();
        $newData["tid"]=$pid;
        $newData["c_tid"]=$cid;
        $rs=$this->getORM()
            ->insert($newData);
        if($rs){
            clearnCache("team.tree.model.",true);
        }
        return $rs;
    }

    /**子链管理 */

    public function _update($pid,$cid,$statu=1){
        $rs=$this->getORM() 
            // ->where("(tid,c_tid,statu)",[$pid,$cid,1])
            ->where(["tid"=>$pid,"c_tid"=>$cid,"statu"=>1])
            ->update(["statu"=>$statu]);
        if($rs){
            clearnCache("team.tree.model.",true);
        }
        return $rs;
    }

    /**构建树结构与索引 */
    public function create_tree(){
        $caches=cache(["team.tree.model.tree.createTree"]);
        if($caches!==null){
            return $caches;
        }
        $rs=$this->getORM()->queryAll("select * from api_team_tree as tr left join api_team_team as te on tr.tid=te.Id");
//             ->select("id,tid,c_tid,time")
//             ->where(["statu"=>1])
//             ->fetchAll();
            // var_dump($rs);
        $childrenFlag=[];
        foreach($rs as $k=>$v){
            $tid='children'.$v["tid"];
            $cid='children'.$v["c_tid"];

            $childrenFlag[$v["tid"]]=isset($childrenFlag[$v["tid"]])?$childrenFlag[$v["tid"]]:0;
            $childrenFlag[$v["c_tid"]]=1;
            $$tid=isset($$tid)?$$tid:["info"=>[],"children"=>[]];
            $$cid=isset($$cid)?$$cid:["info"=>[],"children"=>[]];
            $$tid["info"]=$v;
            $$tid["children"][$v["c_tid"]]=&$$cid;
        }
        //筛选出顶层ID
        $rootId=1;
        foreach($childrenFlag as $k=>$v){
            if($v==0){
                $rootId=$k;
                break;
            }
        }
        //构建索引
        $indexList=[];
        $first='children'.$rootId;
        \Auth\treeIndexForTeam($$first,$indexList);

        $rs=[$$first,$indexList];
        // var_dump($indexList);
        $caches=cache(["team.tree.model.tree.createTree"],$rs,6000);
        if($caches!==null){
            return $caches;
        }
        return $rs;
    }
    /**树内快速搜索 */
    /**是否需要构建树内团队列表？ */

//     public function getTree($id){
//         clearnCache("team.tree.model.",true);
//         $caches=cache(["team.tree.model.tree.getTree",$id]);
//         if($caches!==null){
//             return $caches;
//         }
//         $data=$this->create_tree();
//         $indexList=$data[1];
//         var_dump($indexList);
//         $tree=$data[0];
//         $go=$indexList[$id]?$indexList[$id]:false;
//         if($go==false){
//             return false;
//         }
//         for($n=0;$n<count($go);$n++){
//             $tree=$tree["children"][$go[$n]];
//         }
//         cache(["team.tree.model.tree.getTree",$id],$tree);
//         return $tree;
//     }
    /**查询团队机构id/
     * 
     */
    public function gettid($tid)
    {
        clearnCache("team.tree.model.",true);
        $data=$this->create_tree();
        $indexList=$data[1];
        $tree=$data[0];
        $go=isset($indexList[$tid])?$indexList[$tid]:false;
        if($go==false){
            return false;
        }
        else{
            return $go[0];
        }
    }
    /**团队树形结构**/
    public function getTree($tid)
    {
        $caches=cache(["team.tree.model.tree.getTree",$tid]);
        if($caches!==null){
            return $caches;
        }
        $rs=$this->getORM()->queryAll("select Id as tid,name from api_team_team  where Id='".$tid."' and on_status=1 and statu=1");
        if($rs!=null)
        {
          $tree=$rs[0];
          $this->tree($tid,$tree);
        }
        cache(["team.tree.model.tree.getTree",$tid],$tree);
        return $tree;
    }
    public function tree($tid,&$tree)
    {
        $rs=$this->getORM()->queryAll("select c_tid as tid,name from api_team_tree as tr left join api_team_team as te on tr.tid=te.Id where tr.tid='".$tid."' and te.on_status=1 and te.statu=1 and tr.statu=1");
        foreach ($rs as $key=>$value)
        {
          $tree["children"][$key]=$value;
          $this->tree($value["tid"],$tree["children"][$key]);
        }
    }
    /**获取顶级团队tid*/
    public function getonetid($tid)
    {
        $data=$this->create_tree();
        $indexList=$data[1];
        $tree=$data[0];
        $go=$indexList[$tid]?$indexList[$tid]:false;
        if($go==false){
            return false;
        }
        else{
            return $go[0];
        }
    }
    /**父子团队是否存在 */
    public function info($tid,$cid){
        $caches=cache(["team.tree.model.tree.info",$tid,$cid]);
        if($caches!==null){
            return $caches;
        }
        $rs=$this->getORM()
            ->select("id,tid,c_tid,time")
            // ->where("(tid,uid,statu)",[$tid,$uid,1])
            ->where(["tid"=>$tid,"c_tid"=>$cid,"statu"=>1])
            ->fetchOne();
        cache(["team.tree.model.tree.info",$tid,$cid],$rs);
            
        return $rs;
    }

    /**子团队列表 */
    public function _list($tid){
        $caches=cache(["team.tree.model.tree.list",$tid]);
        if($caches!==null){
            return $caches;
        }
        $rs=$this->getORM()
            ->select("id,tid,c_tid,time")
            // ->where("(tid,statu)",[$tid,1])
            ->where(["tid"=>$tid,"statu"=>1])
            ->fetchAll();
        cache(["team.tree.model.tree.list",$tid],$rs);
        
        //需要对接用户系统查询用户昵称
        return $rs;
    }
    

    public function getListTotal($state) {
        $total = $this->getORM()
            ->where('state', $state)
            ->count('id');

        return intval($total);
    }
   /**查看父级团队**/
    public function getfuinfo($tid)
    {
        $rs=$this->getORM()
        ->select("id,tid,c_tid,time")
        ->where(["c_tid"=>$tid,"statu"=>1])
        ->fetchOne();
        return $rs;
    }

}
