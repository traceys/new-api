<?php
namespace Team\Model;

use PhalApi\Model\NotORMModel as NotORM;

/**

CREATE TABLE `phalapi_curd` (
    `id` int(10) NOT NULL AUTO_INCREMENT,
    `title` varchar(20) DEFAULT NULL,
    `content` text,
    `state` tinyint(4) DEFAULT NULL,
    `post_date` datetime DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

 */

class User extends NotORM {

    protected function getTableName($id) {
        return 'team_user';
    }


    /**团队增加人员 */
    public function _insert($id,$data){
        $demo=["uid"=>1,"utype"=>1,"end_time"=>1,"on_status"=>1];
        $newData=\Auth\createDataFromTemp($demo,$data);
        $newData["create_time"]=\Auth\dateTime();
        $newData["tid"]=$id;
        $rs=$this->getORM()
            ->insert($newData);
        // if($rs){
            clearnCache("team.user.model",true);
        // }
        return $rs;
    }

    /**团队人员管理 */

    public function _update($id,$uid,$utype=false,$endTime="3000-01-01 00:00:00",$on_status=1,$statu=1){
        $data=["on_status"=>$on_status,"statu"=>$statu];
        if($utype!=false){
            $data["utype"]=$utype;
        }
        if($endTime!=false){
            $data["end_time"]=$endTime;
        }
        $rs=$this->getORM()
            // ->where("(tid,uid,statu)",[$id,$uid,1])
            ->where(["tid"=>$id,"uid"=>$uid,"statu"=>1])
            ->update($data);
        // if($rs){
            clearnCache("team.user.model",true);
        // }
        return $rs;
    }

    /**用户是否存在 */
    public function info($tid,$uid){
        $caches=cache(["team.user.model.user.info",$tid,$uid]);
        if($caches!==null){
            return $caches;
        }
        $rs=$this->getORM()
            ->select("id,tid,uid,utype,create_time,end_time,on_status")
            // ->where("(tid,uid,statu)",[$tid,$uid,1])
            ->where(["tid"=>$tid,"uid"=>$uid,"statu"=>1])
            ->fetchOne();
        cache(["team.user.model.user.teamList",$tid,$uid],$rs,6000);
        return $rs;
    }

    /**团队人员统计总数 */
    public function _countNum($tid){
        $caches=cache(["team.user.model.user.countNum",$tid]);
        if($caches!==null){
            return $caches;
        }
        $yestoday=dateTime(time()-3600*24);
        $yestoday=substr($yestoday,0,10);
        $today=dateTime();
        $today=substr($today,0,10);
        $rs=$this->getORM()
            ->select("count(id) as allnum,count(CASE WHEN create_time>'$yestoday' and create_time<'$today' THEN 1 ELSE NULL END) as yestodaynum,count( CASE WHEN create_time>'$today' then 1 else null end) as todaynum")
            // ->where("(tid,statu)",[$tid,1])
            ->where(["tid"=>$tid,"statu"=>1]);
        // if($startTime!==false){
        //     // var_dump($startTime);
        //     $rs=$rs->where("end_time > ? and end_time < ?",[$startTime,$endTime]);
        // }
        // if($page>0){
        //     $rs=$rs->limit(($page-1)*20,20);
        // }
            $rs=$rs->fetchOne();
        //需要对接用户系统查询用户昵称
        cache(["team.user.model.user.countNum",$tid],$rs,6000);
        return $rs;
    }
    /**机构人员数 */
    /**团队人员统计总数 */
    public function _countNumJigou(){
        $caches=cache(["team.user.model.user.countNumJigou"]);
        if($caches!==null){
            return $caches;
        }
    
        $rs=$this->getORM()
            ->queryAll(" select count(1) as num from api_team_user as a left join api_team_team as b on b.id=a.tid where a.statu=1 
            and b.statu=1 and b.type='subaccount'");
            // ->select("count(id) as allnum")
            // // ->where("(tid,statu)",[$tid,1])
            // ->where(["tid"=>$tid,"statu"=>1]);
        // if($startTime!==false){
        //     // var_dump($startTime);
        //     $rs=$rs->where("end_time > ? and end_time < ?",[$startTime,$endTime]);
        // }
        // if($page>0){
        //     $rs=$rs->limit(($page-1)*20,20);
        // }
            $rs=$rs[0]["num"];
        //需要对接用户系统查询用户昵称
        cache(["team.user.model.user.countNumJigou"],$rs,6000);
        return $rs;
    }

    /**团队人员总数列表 */
    public function _listNum($tid,$startTime=false,$endTime=false){
        $caches=cache(["team.user.model.user.list",$tid,$startTime,$endTime]);
        if($caches!==null){
            return $caches;
        }
        $rs=$this->getORM()
            ->select("count(id) as allnum,count(CASE WHEN `on_status` >0 THEN 1 ELSE NULL END) as had,count( CASE WHEN end_time>'".dateTime()."' then 1 else null end) as oknum")
            // ->where("(tid,statu)",[$tid,1])
            ->where(["tid"=>$tid,"statu"=>1]);
        if($startTime!==false){
            // var_dump($startTime);
            $rs=$rs->where("end_time > ? and end_time < ?",[$startTime,$endTime]);
        }else{
            $rs=$rs->where("end_time >  ?",[dateTime()]);
        }
        // if($page>0){
        //     $rs=$rs->limit(($page-1)*20,20);
        // }
            $rs=$rs->fetchOne();
        //需要对接用户系统查询用户昵称
        cache(["team.user.model.user.listNum",$tid,$startTime,$endTime],$rs,6000);
        return $rs;
    }

    /**团队人员列表 */
    public function _list($tid,$page=-1,$startTime=false,$endTime=false){
        $allNum=$this->_listNum($tid,$startTime=false,$endTime=false);
        if($allNum){
            outputArgs(["num"=>$allNum["allnum"],"pagenum"=>20,"onnum"=>$allNum["had"],"oknum"=>$allNum["oknum"],"page"=>$page]);
        }
        $caches=cache(["team.user.model.user.list",$tid,$page,$startTime,$endTime]);
        if($caches!==null){
            return $caches;
        }
        $rs=$this->getORM()
            ->select("id,tid,uid,utype,create_time,end_time,on_status,station,work_num")
            // ->where("(tid,statu)",[$tid,1])
            ->where(["tid"=>$tid,"statu"=>1,"on_status"=>1]);
        if($startTime!==false){
            // var_dump($startTime);
            $rs=$rs->where("end_time > ? and end_time < ?",[$startTime,$endTime]);
        }
        if($page>0){
            $rs=$rs->limit(($page-1)*40,40);
        }
            $rs=$rs->fetchAll();
        //需要对接用户系统查询用户昵称
        cache(["team.user.model.user.list",$tid,$page,$startTime,$endTime],$rs,6000);
        return $rs;
    }

    /**用户所在团队列表 */
    public function teamList($uid,$had=false){
        clearnCache("team.user.model",true);
        $caches=cache(["team.user.model.user.teamList",$uid,$had]);
        if($caches!==null){
            return $caches;
        }
        $rs=$this->getORM()
            ->select("id,tid,uid,utype,create_time,end_time,on_status,sup_uid")
            // ->where("(tid,statu)",[$tid,1])
            ->where(["uid"=>$uid,"statu"=>1]);
        if($had==false){
            $rs=$rs->where("end_time > ?",\Auth\dateTime());
        }
            $rs=$rs->fetchAll();
        cache(["team.user.model.user.teamList",$uid,$had],$rs,6000);
        //需要对接用户系统查询用户昵称
        return $rs;
    }

    /**过期用户表 */

    public function getOutUser(){
        $res = $this->getORM()
            ->select('*')
            ->where(["statu"=>1,"on_status"=>1])
            ->where("end_time < ?",dateTime())
            ->fetchAll();
        return $res;
    }

    public function getTimeOutUser(){
        
        $res317 = $this->getORM()
            ->select('*')
            ->where(["statu"=>1,"on_status"=>1,"tid"=>317])
            ->where("end_time > ? and end_time < ?",[dateTime(time()+3600*24*30),dateTime(time()+3600*24*31)])
            ->fetchAll();

        $res318 = $this->getORM()
            ->select('*')
            ->where(["statu"=>1,"on_status"=>1,"tid"=>318])
            ->where("end_time > ? and end_time < ?",[dateTime(time()+3600*24*3),dateTime(time()+3600*24*4)])
            ->fetchAll();
        return [$res317,$res318];
    }

    /**过期用户清理 */
    public function clearnOutUser(){
        $res = $this->getORM()
            // ->select('*')
            ->where(["statu"=>1,"on_status"=>1])
            ->where("end_time < ?",dateTime())
            ->update(["on_status"=>-1]);
        return $res;
    }
    

    public function getListTotal($state) {
        $total = $this->getORM()
            ->where('state', $state)
            ->count('id');

        return intval($total);
    }
    public function listin($tid) 
    {
        $sql="select * from api_team_user where tid in(".$tid.") and on_status=1 and statu=1";
        return $this->getORM()->queryAll($sql);
    }
    //添加部门成员
    public function adduser(){
        $data=array(
          
        );
    }
}
