<?php
namespace Team\Model;

use PhalApi\Model\NotORMModel as NotORM;

/**

CREATE TABLE `phalapi_curd` (
    `id` int(10) NOT NULL AUTO_INCREMENT,
    `title` varchar(20) DEFAULT NULL,
    `content` text,
    `state` tinyint(4) DEFAULT NULL,
    `post_date` datetime DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

 */

class Layer extends NotORM {

    protected function getTableName($id) {
        return 'team_layer';
    }


    /**增加层级规则 */
    public function _insert($data){
        $demo=["parent"=>1,"children"=>1,"name"=>1,"on_status"=>1];
        $newData=\Auth\createDataFromTemp($demo,$data);
        // var_dump($data);
        $rs=$this->getORM()
            ->insert($newData);
        return $rs;
    }

    /**修改层级 */

    public function _update($parent,$children,$data){
        $demo=["name"=>1,"on_status"=>1,"statu"=>1];
        $newData=\Auth\createDataFromTemp($demo,$data);
        $rs=$this->getORM()
            // ->where("(id,statu)",[$id,1])
            ->where(["parent"=>$parent,"children"=>$children])
            ->update($newData);
        return $rs;
    }

    /**层级是否存在 */
    public function info($type){
        // $caches=cache(["team.layer.model.layer.info",$type]);
        // if($caches!==null){
        //     return $caches;
        // }
        $rs=$this->getORM()
            ->select("id,parent,children,name,on_status")
            // ->where("(children,statu)",[$type,1])
            ->where(["children"=>$type,"statu"=>1])
            ->fetchOne();
        cache(["team.layer.model.layer.info",$type],$rs,6000);
            
        return $rs;
    }
    /**层级关系是否存在 */

    public function hadLayer($parent,$children){
        // $caches=cache(["team.layer.model.layer.hadLayer",$parent,$children]);
        // if($caches!==null){
        //     return $caches;
        // }
        $rs=$this->getORM()
            ->select("id")
            // ->where("(parent,children,statu)",[$parent,$type,1])
            ->where(["parent"=>$parent,"children"=>$children,"statu"=>1])
            ->fetchOne();
        $rs=$rs?true:false;
            cache(["team.layer.model.layer.hadLayer",$parent,$children],$rs,6000);
            
            return $rs;
    }
    /**子层级列表 */
    public function _list($type){
        // $caches=cache(["team.layer.model.layer.list",$type]);
        // if($caches!==null){
        //     return $caches;
        // }
        $rs= $this->getORM()
            ->select("id,parent,children,name,on_status")
            // ->where("(parent,statu)",[$type,1])
            ->where(["parent"=>$type,"statu"=>1])
            ->fetchAll();
        cache(["team.layer.model.layer.list",$type],$rs);
           
        return $rs;
    }

    public function getListTotal($state) {
        $total = $this->getORM()
            ->where('state', $state)
            ->count('id');

        return intval($total);
    }
}
