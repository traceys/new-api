<?php
namespace Team\Model;

use PhalApi\Model\NotORMModel as NotORM;

/**

CREATE TABLE `phalapi_curd` (
    `id` int(10) NOT NULL AUTO_INCREMENT,
    `title` varchar(20) DEFAULT NULL,
    `content` text,
    `state` tinyint(4) DEFAULT NULL,
    `post_date` datetime DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

 */

class UserLog extends NotORM {

    protected function getTableName($id) {
        return 'team_user_log';
    }


    /**团队增加人员 */
    public function _insert($newData){
        $newData["time"]=dateTime();
        $newData["fuid"]=\PhalApi\DI()->uid;
        // $newData["tid"]=$id;
        $rs=$this->getORM()
            ->insert($newData);
        return $rs;
    }


   
}
