<?php
namespace User\Api;
use PhalApi\Api;
use User\Domain\School as Dschool;
class School extends Api
{
    public function getRules()
    {
        return array(
            'sellist' => array(
                'shool_name' => array('name' => 'shool_name','require' => true,'source' => 'get'),
            ),
        );
    }
    /**
     * pc端幼儿园条件查询
     */
    public function sellist()
    {
        $sch=new Dschool();
        return $this->sellist($this->shool_name);
    }
}