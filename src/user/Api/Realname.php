<?php
namespace User\Api;
use PhalApi\Api;
use User\Domain\Realname as Drealname;
use User\Domain\User as  Duser;
use User\Domain\Funuser as Dfunuser;

class Realname extends Api
{
    public function getRules()
    {
        return array(
            'addreal' => array(
                'token' => array('name' => 'token','require' => true,'source' => 'get','desc' =>'用户token'),
                'name' => array('name' => 'name','require' => true,'source' =>'get','desc' =>'真实姓名'),
                'code' => array('name' => 'code','require' => true,'source' =>'get','desc' =>'身份证号'),
                'head_img' => array('name' => 'head_img','source' =>'get','desc' =>'正面图'),
                'back_img' => array('name' => 'back_img','source' =>'get','desc' =>'背面图'),
            ),
            'selstatu' => array(
                'token' => array('name' => 'token','require' => true,'source' => 'get','desc' => '用户token'),
                'type' => array('name' => 'type','require' => true,'source' => 'get','desc' => '状态类型'),
                'order' => array('name' => 'order','require' => true,'source' =>'get','desc' => '排序 如 time'),
                'page' => array('name' => 'page','require' => true,'source' =>'get','desc' => '分页数'),
            ),
            'selreal' => array(
                'token' => array('name' => 'token','source' => 'get','desc' => '用户token'),
                'id' => array('name' => 'id','source' => 'get','desc' => '用户id'),
            ),
            'updreal' => array(
                'token' => array('name' => 'token','require' => true,'source' => 'get','desc' => '用户token'),
                'id' => array('name' => 'id','require' => true,'source' => 'get','desc' => '实名信息的ID'),
                'status' => array('name' => 'status','require' => true,'source' => 'get','desc' => '实名信息审核结果'),
            ),
            'appaddreal' => array(
                'token' => array('name' => 'token','require' => true,'source' => 'get','desc' =>'用户token'),
                'name' => array('name' => 'name','require' => true,'source' =>'get','desc' =>'真实姓名'),
                'code' => array('name' => 'code','require' => true,'source' =>'get','desc' =>'身份证号'),
            ),
         );
    }
    /**
     *育视界实名认证
     */
    public function addreal()
    {
        $user=new Duser();
        $uid=$user->tokenverify($this->token);
        if($uid!=false)
        {
            $info=$user->seltoken($uid);
            if($info["rel_status"]==1)
            {
                exceptions("该用户已认证！", 1);
            }
            else
            {
                $cs= cache(["user.rel.status",$uid]);
                //\PhalApi\DI()->redis->get_time('user_rel_status_'.$uid);
                if($cs>=3){
                    exceptions("认证次数已满！", 2);
                }
                else
                {
                    $fun=new Dfunuser();
                    $rel=$fun->myname($this->name, $this->code);
                    $num=$cs+1;
                    if($rel==true){
                        $status=1;
                    }
                    else{
                        $status=-1;
                    }
                    $today = strtotime(date('Y-m-d', time()));
                    $end = $today + 24 * 60 * 60;
                    $now=$end-time();
                    cache(["user.rel.status",$uid],$num,$now);
                    //\PhalApi\DI()->redis->set_time('user_rel_status_'.$uid,$num,$now);
                    if($rel==true)
                    {
                        $name=new Drealname();
                        $name->addreal($uid, $this->name, $this->code, $this->head_img, $this->back_img,$status);
                        $user->updaterel($uid, $this->name, $this->code, $status);
                       ///加积分
                       addjifen("smrz",$uid);
                       $message="您已完成实名认证，更多会员功能已经开启，快登陆您的育视界账号进行体验吧。详询400-991-4418。育视界，让教育无处不在。";
                       userMessage($uid,"完成实名认证",$message,2);
                       return "认证成功！";
                    }
                    else{
                       exceptions("认证失败！",3); 
                    }
                }
            }
        }
        else
        {
            exceptions("token不存在！", -100);
        }
    }
    /**
     *app实名认证
     */
    public function appaddreal()
    {
        $user=new Duser();
        $uid=$user->tokenverify($this->token);
        if($uid!=false)
        {
            $info=$user->seltoken($uid);
            if($info["rel_status"]==1)
            {
                exceptions("该用户已认证！", 1);
            }
            else
            {
                $cs= cache(["user.rel.status",$uid]);
                if($cs>=3){
                    exceptions("认证次数已满！", 2);
                }
                else
                {
                    $fun=new Dfunuser();
                    $rel=$fun->myname($this->name, $this->code);
                    $num=$cs+1;
                    if($rel==true){
                        $status=1;
                    }
                    else{
                        $status=-1;
                    }
                    $today = strtotime(date('Y-m-d', time()));
                    $end = $today + 24 * 60 * 60;
                    $now=$end-time();
                    cache(["user.rel.status",$uid],$num,$now);
                    if($rel==true)
                    {
                        $name=new Drealname();
                        $name->addreal($uid, $this->name, $this->code, $this->head_img, $this->back_img,$status);
                        $user->updaterel($uid, $this->name, $this->code, $status);
                        return "认证成功！";
                    }
                    else{
                        exceptions("认证失败！",3);
                    }
                }
            }
        }
        else
        {
            exceptions("token不存在！", -100);
        }
    }
    /**
     * 实名认证状态查询
     */
    public function selstatu()
    {
//         $url="test.yusj.vip?s=User.User.Tokenverify&token=".$this->token."";
//         $isverify=curls($url)["data"];
//         if($isverify!=false)
//         {
           $name=new Drealname();
           return $name->selstatu($this->type, $this->order, $this->page);
//         }
//         else
//         {
//             exceptions("token不存在！", -100);
//         }
    }
    /**
     * 实名认证信息
     */
    public function selreal()
    {
//         if($this->uid!=null){
//             $uid=$this->uid;
//         }
//         elseif ($this->token!=null){
//             $url="test.yusj.vip?s=User.User.Tokenverify&token=".$this->token."";
//             $uid=curls($url)["data"];
//             if($uid==false){
//                 exceptions("token不存在！", -100);die;
//             }
//         }
//         else{
//             exceptions("请传入参数！", -200);die;
//         }
        $name=new Drealname();
        return $name->selreal($this->id);
    }
    /**
     * 实名认证信息审核
     */
    public function updreal()
    {
//         $url="test.yusj.vip?s=User.User.Tokenverify&token=".$this->token."";
//         $isverify=curls($url)["data"];
//         if($isverify!=false)
//         {
            $name=new Drealname();
            return $name->updreal($this->id,$this->status);
//         }
//         else
//         {
//             exceptions("token不存在！", -100);
//         }
    }
}