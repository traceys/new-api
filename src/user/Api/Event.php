<?php
namespace User\Api;
use PhalApi\Api;
use User\Domain\Event as Devent;

class Event extends Api
{
    public function getRules()
    {
        return array(
            'eventadd' => array(
                'event_zhname' => array('name' => 'event_zhname','source' => 'get'),
                'event_name' => array('name' => 'event_name','source' => 'get'),
                'event_value' => array('name' => 'event_value','require' => true,'source' =>'get'),
             ),
            'eventlogadd' => array(
                'event' => array('name' => 'event','require' => true,'source' => 'get'),
                'on_statu' => array('name' => 'on_statu','require' => true,'source' => 'get'),
            ),
            'eventselect' => array(
                'event_name' => array('name' => 'event_name','source' => 'get'),
            ),
            'eventupd' => array(
                'event_name' => array('name' => 'event_name','require' => true,'source' => 'get'),
                'event_value' => array('name' => 'event_value','require' => true,'source' => 'get'),
            ),
         );
    }
    /**
     *添加事件
     */
    public function eventadd()
    {
        $event=new Devent();
        $info=$event->eventselect($this->event_name);
        if($info!=null){
            exceptions("事件key已存在！", 10);
        }
        else
        {
          $event->eventadd($this->event_zhname,$this->event_name, $this->event_value);
        }
    }
    /**
     * 事件记录
     */
    public function eventlogadd(){
        $event=new Devent();
        return $event->eventlogadd($this->event, $this->on_statu);
    
    }
    /**
     * 查询url
     */
    public function eventselect()
    {
        $event=new Devent();
        return $event->eventselect($this->event_name);
    }
    /***
     * 修改
     */
    public function eventupd()
    {
        $event=new Devent();
        return $event->updevent($this->event_name,$this->event_value);
    }
}