<?php
namespace User\Api;
use PhalApi\Api;
use User\Domain\Mechanism as Dmchanism;
use User\Domain\User as Duser;

class Mechanism extends Api
{
    public function getRules()
    {
        return array(
            'addauth' => array(
                'token' => array('name' => 'token','source' => 'get','desc' =>'用户token'),
                'name' => array('name' => 'name','require' => true,'source' =>'get','desc' =>'机构名'),
                'username' => array('name' => 'username','require' => true,'source' =>'get','desc' =>'机构联系人'),
                'user_phone' => array('name' => 'user_phone','require' => true,'source' =>'get','desc' =>'联系人电话'),
                'img' => array('name' => 'img','source' =>'get','desc' => '以数组形式存放的机构认证图'),
                'contract_num' => array('name' => 'contract_num','require' => true,'source' =>'get','desc' => '合同编号'),
                'contract_doc' => array('name' => 'contract_doc','require' => true,'source' =>'get','desc' => '合同文件'),
                'auth_dir' => array('name' => 'auth_dir','require' => true,'source' =>'get','desc' => '授权目录'),
                'pro' => array('name' => 'pro','require' => true,'source' =>'get','desc' => '省'),
                'city' => array('name' => 'city','require' => true,'source' =>'get','desc' => '市'),
                'county' => array('name' => 'county','require' => true,'source' =>'get','desc' => '区'),
            ),
            'selstatu' => array(
                'token' => array('name' => 'token','require' => true,'source' => 'get','desc' => '用户token'),
                'type' => array('name' => 'type','require' => true,'source' => 'get','desc' => '状态类型'),
                'order' => array('name' => 'order','require' => true,'source' =>'get','desc' => '排序 如 time'),
                'page' => array('name' => 'page','require' => true,'source' =>'get','desc' => '分页数'),
            ),
            'selauth' => array(
                'id' => array('name' => 'id','source' => 'get','desc' => '用户Id'),
                'token' => array('name' => 'token','source' => 'get','desc' => '用户token'),
            ),
            'updauth' => array(
                'token' => array('name' => 'token','require' => true,'source' => 'get','desc' => '用户token'),
                'id' => array('name' => 'id','require' => true,'source' => 'get','desc' => '机构信息的ID'),
                'status' => array('name' => 'status','require' => true,'source' => 'get','desc' => '机构信息审核结果'),
                'beizhu' => array('name' => 'beizhu','source' => 'get','desc' => '备注'),
            ),
            'plupdauth' => array(
                'token' => array('name' => 'token','require' => true,'source' => 'get','desc' => '用户token'),
                'ids' => array('name' => 'ids','require' => true,'source' => 'get','desc' => '机构信息的IDs'),
                'status' => array('name' => 'status','require' => true,'source' => 'get','desc' => '机构信息审核结果'),
                'beizhu' => array('name' => 'beizhu','source' => 'get','desc' => '备注'),
            ),
            'melist' => array(
                'rec_status' => array('name' => 'rec_status','source' => 'get','desc' => '机构认证状态'),
                'star_time' => array('name' => 'star_time','source' => 'get','desc' => '到期开始时间'),
                'end_time' => array('name' => 'end_time','source' => 'get','desc' => '到期结束时间'),
                'page' => array('name' => 'page','source' => 'get','desc' => '分页'),
                'name' => array('name' => 'name','source' => 'get','desc' => '机构名字'),
                'username' => array('name' => 'username','source' => 'get','desc' => '机构邀请码'),
                'code' => array('name' => 'code','source' => 'get','desc' => '机构邀请码'),
                'use_state' => array('name' => 'use_state','default'=>'use','source' => 'get','desc' => '邀请码使用状态'),
                'uid' => array('name' => 'uid','source' => 'get','desc' => '用户ID'),
                'iscode' => array('name' => 'iscode','default'=>1,'source' => 'get','desc' => '邀请码是否存在'),
                'user_phone' => array('name' => 'user_phone','source' => 'get','desc' => '手机号'),
            ),
            'updateauth' => array(
                'token' => array('name' => 'token','source' => 'get','desc' =>'用户token'),
                'id' => array('name' => 'id','source' =>'get','desc' =>'Id'),
                'name' => array('name' => 'name','source' =>'get','desc' =>'机构名'),
                'username' => array('name' => 'username','source' =>'get','desc' =>'机构联系人'),
                'user_phone' => array('name' => 'user_phone','source' =>'get','desc' =>'联系人电话'),
                'contract_num' => array('name' => 'contract_num','source' =>'get','desc' => '合同编号'),
                'contract_doc' => array('name' => 'contract_doc','source' =>'get','desc' => '合同文件'),
                'auth_dir' => array('name' => 'auth_dir','source' =>'get','desc' => '授权目录'),
                'pro' => array('name' => 'pro','source' =>'get','desc' => '省'),
                'city' => array('name' => 'city','source' =>'get','desc' => '市'),
                'county' => array('name' => 'county','source' =>'get','desc' => '区'),
            ),
            'updatecode' => array(
                'token' => array('name' => 'token','source' => 'get','desc' =>'用户token'),
                'id' => array('name' => 'id','source' =>'get','desc' =>'Id'),
                'code' => array('name' => 'code','source' =>'get','desc' =>'机构邀请码'),
                'generate_time' => array('name' => 'generate_time','source' =>'get','desc' =>'生成时间'),
                'end_time' => array('name' => 'end_time','source' =>'get','desc' =>'到期时间'),
                'subaccount_num' => array('name' => 'subaccount_num','source' =>'get','desc' => '子账号个数'),
                'use_state' => array('name' => 'use_state','source' =>'get','desc' => '使用状态'),
            ),
            'selcode' => array(
                'token' => array('name' => 'token','require' => true,'source' => 'get','desc' => '用户token'),
                'code' => array('name' => 'code','require' => true,'source' => 'get','desc' => '机构邀请码'),
            ),
            'selstatus' => array(
                'token' => array('name' => 'token','require' => true,'source' => 'get','desc' => '用户token'),
            ),
            'selall' => array(
                'token' => array('name' => 'token','require' => true,'source' => 'get','desc' => '用户token'),
                'name' => array('name' => 'name','source' => 'get','desc' => '园长姓名'),
                'telphone' => array('name' => 'telphone','source' => 'get','desc' => '手机号'),
                'page' => array('name' => 'page','source' => 'get','desc' => '分页'),
            ),
            'selallcount' => array(
                'token' => array('name' => 'token','require' => true,'source' => 'get','desc' => '用户token'),
                'rec_status' => array('name' => 'rec_status','source' => 'get','desc' => '认证状态'),
                'name' => array('name' => 'name','source' => 'get','desc' => '园长姓名'),
                'telphone' => array('name' => 'telphone','source' => 'get','desc' => '手机号'),
                'end_time' => array('name' => 'end_time','source' => 'get','desc' => '过期时间'),
            ),
            'selhiscode' => array(
                'fromid' => array('name' => 'fromid','require' => true,'source' => 'get'),
            ),
            'updatetj' => array(
                'token' => array('name' => 'token','source' => 'get','desc' =>'用户token'),
                'id' => array('name' => 'id','source' =>'get','desc' =>'Id'),
                'img' => array('name' => 'img','source' =>'get','desc' => '以数组形式存放的机构认证图'),
            ),
            'jycode' => array(
                'id' => array('name' => 'id','require' => true,'source' => 'get'),
                'on_statu' => array('name' => 'on_statu','require' => true,'source' => 'get'),
            ),
            'txicode' => array(
                'token' => array('name' => 'token','source' => 'get','desc' => '用户token'),
                'id' => array('name' => 'id','source' => 'get','desc' => '用户token'),
            ),
            'istxicode' => array(
                'id' => array('name' => 'id','source' => 'get'),
            ),
            'selmechall' => array(
                'rec_status' => array('name' => 'rec_status','source' => 'get','desc' => '机构认证状态'),
                'star_time' => array('name' => 'star_time','source' => 'get','desc' => '到期开始时间'),
                'end_time' => array('name' => 'end_time','source' => 'get','desc' => '到期结束时间'),
                'page' => array('name' => 'page','default'=>1,'source' => 'get','desc' => '分页'),
                'name' => array('name' => 'name','source' => 'get','desc' => '机构名字'),
                'username' => array('name' => 'username','source' => 'get','desc' => '机构邀请码'),
                'code' => array('name' => 'code','source' => 'get','desc' => '机构邀请码'),
                'use_state' => array('name' => 'use_state','default'=>'use','source' => 'get','desc' => '邀请码使用状态'),
                'uid' => array('name' => 'uid','source' => 'get','desc' => '用户ID'),
                'iscode' => array('name' => 'iscode','default'=>1,'source' => 'get','desc' => '邀请码是否存在'),
                'user_phone' => array('name' => 'user_phone','source' => 'get','desc' => '手机号'),
            ),
            'selhisone' => array(
                'id' => array('name' => 'id','source' => 'get','desc' => '用户token'),
            ),
            'teammech' => array(
                'tid' => array('name' => 'tid','source' => 'get','desc' => '用户token'),
            ),
            'import' => array(
                'name' => array('name' => 'name','require' => true,'source' =>'get','desc' =>'机构名'),
                'username' => array('name' => 'username','require' => true,'source' =>'get','desc' =>'机构联系人'),
                'user_phone' => array('name' => 'user_phone','require' => true,'source' =>'get','desc' =>'联系人电话'),
                'contract_num' => array('name' => 'contract_num','require' => true,'source' =>'get','desc' => '合同编号'),
                'auth_dir' => array('name' => 'auth_dir','require' => true,'source' =>'get','desc' => '授权目录'),
                'pro' => array('name' => 'pro','require' => true,'source' =>'get','desc' => '省'),
                'city' => array('name' => 'city','require' => true,'source' =>'get','desc' => '市'),
                'county' => array('name' => 'county','require' => true,'source' =>'get','desc' => '区'),
                'subaccount_num' => array('name' => 'subaccount_num','require' => true,'source' =>'get','desc' => '子账号个数'),
                'end_time' => array('name' => 'end_time','require' => true,'source' =>'get','desc' => '到期时间'),
//              'contract_end' => array('name' => 'contract_end','require' => true,'source' =>'get','desc' => '合同终止状态'),
            ),
            'teamtid' => array(
                'tid' => array('name' => 'tid','source' => 'get','desc' => '用户token'),
            ),
         );
    }
    /**
     * 机构认证
     */
    public function addauth()
    {
//         $url="test.yusj.vip?s=User.User.Tokenverify&token=".$this->token."";
//         $isverify=curls($url)["data"];
//         if($isverify!=false)
//         {
            $name=new Dmchanism();
            return  $name->addauth($this->name,$this->username,$this->user_phone,$this->img,$this->contract_num,$this->contract_doc,$this->auth_dir,$this->pro,$this->city,$this->county);
//         }
//         else
//         {
//             exceptions("token不存在！", -100);
//         }
    }
    /**
     * 机构认证状态查询
     */
    public function selstatu()
    {
//         $url="test.yusj.vip?s=User.User.Tokenverify&token=".$this->token."";
//         $isverify=curls($url)["data"];
//         if($isverify!=false)
//         {
           $name=new Dmchanism();
           return $name->selstatu($this->type, $this->order, $this->page);
//         }
//         else
//         {
//             exceptions("token不存在！", -100);
//         }
    }
    
     /**
     * 机构认证信息
     */
    public function selauth()
    {
//         if($this->uid!=null){
//           $uid=$this->uid;     
//         }
//         elseif ($this->token!=null){
//             $url="test.yusj.vip?s=User.User.Tokenverify&token=".$this->token."";
//             $uid=curls($url)["data"];
//             if($uid==false){
//                  exceptions("token不存在！", -100);die;
//             }
//         }
//         else{
//             exceptions("请传入参数！", -200);die;
//         }
         $name=new Dmchanism();
         return $name->selauth($this->id);
    }
    /**
     * 机构认证信息审核
     */
    public function updauth()
    {
        $user=new Duser();
        $uid=$user->selecttoken($this->token);
        $name=new Dmchanism();
        $info=$name->selauth($this->id);
        if($info["rec_status"]==0)
        {
            if($this->status==1)
            {
               apiGet("Team.User.Add",array('tid'=>317,'uid'=>$info["uid"],'type'=>'user','end_time'=>$info["end_time"]));
               apiGet("Team.User.Add",array('tid'=>$info["tid"],'uid'=>$info["uid"],'type'=>'master','end_time'=>$info["end_time"]));
               $use_state=1;
               $content="恭喜您，您的机构会员资料审核通过，开始享受您的会员身份权益吧。详询400-991-4418。育视界，让教育无处不在。";
               $types="园所服务";
               
            }
            if($this->status==-1)
            {
                $num=$name->selnum($this->id, $info["code"]);
    //             if($num[0]["num"]==2){
    //               $use_state=1;  
    //             }
    //             else{
    //                 $use_state=0;
    //             }
                $use_state=1;
                $content="很抱歉，您提交的机构会员认证资料审核未通过，原因：".$this->beizhu."，请核实后重新提交。详询400-991-4418。育视界，让教育无处不在。";
                $types="园所服务";
            }
            userMessage($info["uid"],$types,$content);
          //apiGet("Push.Push.Tsfq",array("uid"=>$info["uid"],"type"=>"短信","code"=>$info["user_phone"],"value"=>$content,'searchToken'=>$this->token));
            
           $name->updsh($this->id,$use_state,$this->status,$this->beizhu,$uid["Id"]);
           $infos=$name->selauth($this->id);
           $name->addhistory($infos["uid"],$infos["name"],$infos["code"],$infos["username"],$infos["user_phone"],$infos["img"],$infos["rec_status"],$infos["time"],$infos["end_time"],$infos["contract_num"],$infos["contract_doc"],$infos["auth_dir"],$infos["statu"],$infos["generate_time"] ,$infos["subaccount_num"],$infos["tid"],$infos["use_state"],$infos["beizhu"],$infos["province"],$infos["city"],$infos["county"],$infos["prouid"],$this->id,$infos["on_statu"],$infos["region"]);
           return true;
        }
    }
//     /**
//      * 批量审核
//      */
//     public function plupdauth()
//     {
//         $name=new Dmchanism();
//         $ids=explode(",", $this->ids);
//         if(count($ids)>0)
//         {
//             foreach ($ids as $val)
//             {
//                 if($this->status==1)
//                 {
//                     $info=$name->selauth($this->id);
//                     apiGet("Team.User.Add",array('tid'=>317,'uid'=>$info["uid"],'type'=>'companyvip','end_time'=>$info["end_time"]));
//                     apiGet("Team.User.Add",array('tid'=>$info["tid"],'uid'=>$info["uid"],'type'=>'companyvip','end_time'=>$info["end_time"]));
//                 }
//                 $name->updateauth($this->id,null,null, null, null, null, null,$this->beizhu);
//                 $name->updauth($this->id,$this->status);
//             }
//             return true;
//         }
//         else{
//             exceptions("请传入相关参数！", 8);
//         }
//     }
    /**
     * 后台机构查询
     */
    public function melist()
    {
        $name=new Dmchanism();
        return $name->melist($this->rec_status, $this->star_time, $this->end_time, $this->page,$this->name,$this->username,$this->code,$this->use_state,$this->uid,$this->iscode,$this->user_phone);
    }
    /***
     *机构信息修改
     */
    public function updateauth()
    {
        $name=new Dmchanism();
        $infos=$name->selauth($this->id);
        if($infos["rec_status"]==1)
        {
            $data=explode(",",$infos["auth_dir"]);
            $datas=explode(",",$this->auth_dir);
            foreach ($data as $val)
            {
                if(!in_array($val, $datas))
                {
                    apiGet("Auth.Resource.Set", array("fid"=>$infos["tid"],"ftype"=>'team',"res_type"=>'videoClassLevel',"res_id"=>$val."_9","server"=>"read","value"=>0));
                }
            }
            foreach ($datas as $val)
            {
                if(!in_array($val, $data))
                {
                    apiGet("Auth.Resource.Set", array("fid"=>$infos["tid"],"ftype"=>'team',"res_type"=>'videoClassLevel',"res_id"=>$val."_9","server"=>"read","value"=>1));
                }
            }
            foreach ($data as $val)
            {
                if(!in_array($val, $datas))
                {
                    apiGet("Auth.Resource.Set", array("fid"=>$infos["tid"],"ftype"=>'team',"res_type"=>'videoClassLevel',"res_id"=>$val."_10","server"=>"read","value"=>0));
                }
            }
            foreach ($datas as $val)
            {
                if(!in_array($val, $data))
                {
                    apiGet("Auth.Resource.Set", array("fid"=>$infos["tid"],"ftype"=>'team',"res_type"=>'videoClassLevel',"res_id"=>$val."_10","server"=>"read","value"=>1));
                }
            }
        }
        $name->updateauth($this->id, $this->name, $this->username, $this->user_phone, $this->contract_num, $this->contract_doc, $this->auth_dir,$this->pro,$this->city,$this->county);
        if($infos["tid"]!=null|| $infos["tid"]!=0)
        {
           $info=apiGet("Team.Team.Info",array("id"=>$infos["tid"])); 
           apiGet("Team.Team.Update",array("id"=>$infos["tid"],"name"=>$this->name,"user_name"=>$info["user_name"],"desc"=>$info["desc"],"on_status"=>$info["on_status"],"statu"=>$info["statu"],"type"=>$info["type"]));
        }
        return true;
    }
    /**
     *机构信息提交认证 
     */
    public function updatetj(){
        
        $user=new Duser();
        $uid=$user->tokenverify($this->token);
        if($uid==false){
            exceptions("token不存在！", -100);die;
        }
        $name=new Dmchanism();
//         $infouser=$name->selmec($uid);
//         if($infouser!=null){
//             exceptions("该账户已绑定机构！", 10);die;
//         }
        $info=$name->updatetj($this->id, $this->img, $uid);
        $ls=$name->selxy($this->id);
        if($ls!=null)
        {
            $content="您申请续约机构会员，资料已经提交成功，审核大概需要1-2个工作日，请耐心等待…… 详询400-991-4418。育视界，让教育无处不在。";
            userMessage($uid,"园所服务",$content);
        }
        else{
        $content="您申请激活机构会员，资料已经提交成功，审核大概需要1-2个工作日，请耐心等待…… 详询400-991-4418。育视界，让教育无处不在。";
        userMessage($uid,"园所服务",$content,1);
        }
        if($info==408){
            exceptions("请传入相关参数！", 8);
        }
        else
        {
            return true;
        }
    }
    /**
     * 生成邀请码
     */
    public function updatecode()
    {
        $name=new Dmchanism();
        $infos=$name->selauth($this->id);
        if($infos["code"]!=null)
        {
          $name->addhistory($infos["uid"],$infos["name"],$infos["code"],$infos["username"],$infos["user_phone"],$infos["img"],$infos["rec_status"],$infos["time"],$infos["end_time"],$infos["contract_num"],$infos["contract_doc"],$infos["auth_dir"],$infos["statu"],$infos["generate_time"] ,$infos["subaccount_num"],$infos["tid"],$infos["use_state"],$infos["beizhu"],$infos["province"],$infos["city"],$infos["county"],$infos["prouid"],$this->id,$infos["on_statu"],$infos["region"]);
          if($infos["uid"]!=null&&$infos["use_state"]==1&&$infos["rec_status"]==1)
          {
              $name->updatecode($this->id, $this->code, date('Y-m-d H:i:s',time()), $this->end_time, $this->subaccount_num+1,1);
              $meinfo=$name->selauth($this->id);
              apiGet("Team.User.Add",array('tid'=>317,'uid'=>$meinfo["uid"],'type'=>'user','end_time'=>$meinfo["end_time"]));
              apiGet("Team.User.Add",array('tid'=>$meinfo["tid"],'uid'=>$meinfo["uid"],'type'=>'master','end_time'=>$meinfo["end_time"]));
              $list=apiGet("Team.User._List", array("tid"=>$meinfo["tid"]));
              foreach($list as $val)
              {
                  if($val["uid"]!=$meinfo["uid"])
                  {
                     apiGet("Team.User.Add",array('tid'=>$meinfo["tid"],'uid'=>$val["uid"],'type'=>'user','end_time'=>$meinfo["end_time"]));
                  }
              }
              if(count($list)-$meinfo["subaccount_num"]>0)
              {
                  $num=count($list)-$meinfo["subaccount_num"];
                  for ($j=0;$j<count($list);$j++)
                  {
                      $volume[$j]  = $list[$j]['id'];
                  }
                  array_multisort($volume, SORT_DESC, $list);
                  for ($i=0;$i<$num;$i++)
                  {
                      apiGet("Team.User.Del",array('tid'=>$meinfo["tid"],'uid'=>$list[$i]["uid"]));
                  }
              }
              $content="".$meinfo["username"]."园长您好，您在育视界的机构会员身份已续约成功，到期时间".$this->end_time."。育视界，让教育无处不在。";
              userMessage($meinfo["uid"],"园所服务",$content);
          }
          else
          {
              $info=$name->updatecode($this->id, $this->code, $this->generate_time, $this->end_time, $this->subaccount_num+1,$this->use_state);
              $meinfo=$name->selauth($this->id);
              $tid="";
              if($meinfo["tid"]==null){
                  $tid=apiGet("Team.Team.Add", array('name'=>$meinfo["name"],'user_name'=>"子账号",'type'=>'subaccount','desc'=>"账号"));
              }
              else{
                  $tid=$meinfo["tid"];
              }
              $name->updtidone($this->id, $tid);
              $data=explode(",", $meinfo["auth_dir"]);
              foreach ($data as $val){
                  apiGet("Auth.Resource.Set", array("fid"=>$tid,"ftype"=>'team',"res_type"=>'videoClassLevel',"res_id"=>$val."_9","server"=>"read","value"=>1));
              }
              foreach ($data as $val){
                  apiGet("Auth.Resource.Set", array("fid"=>$tid,"ftype"=>'team',"res_type"=>'videoClassLevel',"res_id"=>$val."_10","server"=>"read","value"=>1));
              }
              $content="".$meinfo["username"]."园长您好，您在育视界的机构会员身份激活码为".$this->code."，到期时间".$this->end_time."，请尽快前往激活哦。详询400-991-4418。育视界，让教育无处不在";
              userMessage(-1,"园所服务",$content,2,$meinfo["user_phone"]);
          }
        }
        else{
            $info=$name->updatecode($this->id, $this->code, $this->generate_time, $this->end_time, $this->subaccount_num+1,$this->use_state);
            $meinfo=$name->selauth($this->id);
            $tid="";
            if($meinfo["tid"]==null){
                $tid=apiGet("Team.Team.Add", array('name'=>$meinfo["name"],'user_name'=>"子账号",'type'=>'subaccount','desc'=>"账号"));
            }
            else{
                $tid=$meinfo["tid"];
            }
            $name->updtidone($this->id, $tid);
            $data=explode(",", $meinfo["auth_dir"]);
            foreach ($data as $val){
                apiGet("Auth.Resource.Set", array("fid"=>$tid,"ftype"=>'team',"res_type"=>'videoClassLevel',"res_id"=>$val."_9","server"=>"read","value"=>1));
            }
            foreach ($data as $val){
                apiGet("Auth.Resource.Set", array("fid"=>$tid,"ftype"=>'team',"res_type"=>'videoClassLevel',"res_id"=>$val."_10","server"=>"read","value"=>1));
            }
            $content="".$meinfo["username"]."园长您好，您在育视界的机构会员身份激活码为".$this->code."，到期时间".$this->end_time."，请尽快前往激活哦。详询400-991-4418。育视界，让教育无处不在";
            userMessage(-1,"园所服务",$content,2,$meinfo["user_phone"]);
        }
         return true;
    }
    /**
     *  前台机构邀请码获取机构信息
     */
    public function selcode()
    {
        $user=new Duser();
        $uid=$user->tokenverify($this->token);
        if($uid!=null)
        {
            $userinfo=$user->userone($uid);
            $mec=new Dmchanism();
            $codeinfo=$mec->selcode($this->code);
            if($codeinfo!=null)
            {
                if($userinfo["telphone"]==$codeinfo["user_phone"])
                {
                    if(time()<strtotime($codeinfo["end_time"])){
                        
                        if($codeinfo["use_state"]==0)
                        {
                            if($codeinfo["on_statu"]==0)
                            {
                                return $codeinfo;
                            }
                            else
                            {
                                exceptions("该邀请码已禁用！", 10);
                            }
                        }
                        else{
                            exceptions("该邀请码已使用！", 5);
                        }
                    }
                    else{
                        exceptions("该邀请码已过期！", -5);
                    }
                }
                else{
                    exceptions("该邀请码与账号信息不符！", -10);
                }
            }
            else{
                exceptions("邀请码不存在！", -50);
            }
        }
        else{
            exceptions("token不存在！", -100);
        }
    }
    /**
     * 查询机构审核状态
     * 
     */
    public function selstatus()
    {    
         $user=new Duser();
         $uid=$user->tokenverify($this->token);
         if($uid!=null)
         {
             $mec=new Dmchanism();
             return $mec->selstatus($uid);
         }
         else{
             exceptions("token不存在！", -100);
         }
    }
    /**
     * 
     * 查询该会员下面分会的机构
     * 
     */
    public function selall()
    {
        $user=new Duser();
        $uid=$user->selecttoken($this->token);
        if($uid!=null)
        {
            $mech=new Dmchanism();
            $branch=apiGet("Configure.Configure.seluser",array("uid"=>$uid["Id"]));
            $data="";
            if($branch!=null)
            {
                $value=json_decode($branch[0]["value"],true)["manageData"];
                if($value!=null){
                    $region=json_decode($value,true);
                    for($j=0;$j<count($region);$j++){
                        $dz=$region[$j][0].$region[$j][1].$region[$j][2];
                        $data.="'$dz',";
                    }
                }
                $data=rtrim($data,",");
            }
            if($data!=null){
                return  $mech->seladress($data, $this->name, $this->telphone, $this->page,null,null);
            }
            else{
                return [];
            }
        }
        else
        {
            exceptions("token不存在！", -100);
        }
    }
    /**
     *查询该会员分会机构总数,激活数,未激活,到期 总数
     */
    public function selallcount()
    {
        $user=new Duser();
        $uid=$user->selecttoken($this->token);
        if($uid!=null)
        {
            $mech=new Dmchanism();
            $branch=apiGet("Configure.Configure.seluser",array("uid"=>$uid["Id"]));
            $data="";
            if($branch!=null)
            {
                $value=json_decode($branch[0]["value"],true)["manageData"];
                if($value!=null){
                    $region=json_decode($value,true);
                    for($j=0;$j<count($region);$j++){
                        $dz=$region[$j][0].$region[$j][1].$region[$j][2];
                        $data.="'$dz',";
                    }
                }
                $data=rtrim($data,",");
            }
            if($data!=null){
                $info=$mech->seladress($data,$this->name,$this->telphone,-1,$this->rec_status,$this->end_time);
                return count($info);
            }
            else{
                return 0;
            }
        }
        else
        {
            exceptions("token不存在！", -100);
        }
    }
    /**
     * 提醒验证邀请码
     */
    public function txicode()
    {
        $is=cache([$this->id,date('Y-m-d',time())]);
        if($is==null)
        {
            $today = strtotime(date('Y-m-d',time()));
            $end = $today + 24 * 60 * 60;
            $now=$end-time();
            cache([$this->id,date('Y-m-d',time())],1,$now);
            $mec=new Dmchanism();
            $meinfo=$mec->selauth($this->id);
            if($meinfo["rec_status"]==0)
            {
              $content="".$meinfo["username"]."园长您好，您在育视界的机构会员身份还未激活,激活码".$meinfo["code"]."，到期时间".$meinfo["end_time"]."，请尽快前往激活哦。详询400-991-4418。育视界，让教育无处不在";
              userMessage(-1,"园所服务",$content,2,$meinfo["user_phone"]);
            }
            if(strtotime($meinfo["end_time"])<time())
            {
              $content="".$meinfo["username"]."园长您好，您育视界账号".$meinfo["name"]."的机构会员身份已于".$meinfo["end_time"]."到期，请尽快联系区域总监进行续约。详询400-991-4418。育视界，让教育无处不在。";
              userMessage($meinfo["uid"],"园所服务",$content);
            }
            if((strtotime($meinfo["end_time"])-time())/24*3600<=30)
            {
              $content="".$meinfo["username"]."园长您好，您育视界账号".$meinfo["name"]."的机构会员身份即将到期，到期时间".$meinfo["end_time"]."，为避免影响账号功能的使用，请尽快联系区域总监进行续约。详询400-991-4418。育视界，让教育无处不在。";
              userMessage($meinfo["uid"],"园所服务",$content);
            }
            return true;
        }
        else{
            exceptions("今天已经提醒过！", 80);
        }
    }
    /**
     *提醒显示提醒机构信息
     */
    public function istxicode()
    {
        $is=cache([$this->id,date('Y-m-d',time())]);
        if($is==null)
        {
            exceptions("今天没提醒过！", 40);
        }  
        else{
            exceptions("今天已经提醒过！", 80);
        }
    }
    /**
     * 提醒机构到期提醒
     */
    public function txdqicode()
    {
        $mec=new Dmchanism();
        $meinfo=$mec->selauth($this->id);
        $content="".$meinfo["username"]."园长您好，您育视界账号".$meinfo["name"]."的机构会员身份已于".$meinfo["end_time"]."到期，请尽快联系区域总监进行续约。详询400-991-4418。育视界，让教育无处不在。";
        userMessage($meinfo["uid"],"园所服务",$content);
        return true;
    }
    /**
     * 预先提醒机构到期提醒
     */
    public function txydqicode()
    {
        $mec=new Dmchanism();
        $meinfo=$mec->selauth($this->id);
        $content="".$meinfo["username"]."园长您好，您育视界账号".$meinfo["name"]."的机构会员身份即将到期，到期时间".$meinfo["end_time"]."，为避免影响账号功能的使用，请尽快联系区域总监进行续约。详询400-991-4418。育视界，让教育无处不在。";
        userMessage($meinfo["uid"],"园所服务",$content);
        return true;
    }
    /**
     * 查询历史邀请码
     */
    public function selhiscode()
    {
       $his=new Dmchanism();
       return $his->selhiscode($this->fromid);
    }
    /**
     * 邀请码过期
     */
    public function codeoverdue()
    {
        $mec=new Dmchanism();
        $info=$mec->selall();
        for($i=0;$i<count($info);$i++)
        {
            if(strtotime($info[$i]["end_time"])<time())
            {
               $mec->upduser($info[$i]["id"],-1);
               $infos=$mec->selauth($info[$i]["id"]);
               $mec->addhistory($infos["uid"],$infos["name"],$infos["code"],$infos["username"],$infos["user_phone"],$infos["img"],$infos["rec_status"],$infos["time"],$infos["end_time"],$infos["contract_num"],$infos["contract_doc"],$infos["auth_dir"],$infos["statu"],$infos["generate_time"] ,$infos["subaccount_num"],$infos["tid"],$infos["use_state"],$infos["beizhu"],$infos["province"],$infos["city"],$infos["county"],$infos["prouid"],$this->id,$infos["on_statu"],$infos["region"]);
            }
            if((time()-strtotime($info[$i]["end_time"]))/(24*3600)<=7)
            {
                $meinfo=$mec->selauth($info[$i]["id"]);
                $content="".$meinfo["username"]."园长您好，您在育视界的机构会员身份还未激活,激活码".$meinfo["code"]."，到期时间".$meinfo["end_time"]."，请尽快前往激活哦。详询400-991-4418。育视界，让教育无处不在";
                userMessage($meinfo["uid"],"园所服务",$content,2,$meinfo["user_phone"]);
                //apiGet("Push.Push.Tsfq",array("uid"=>$meinfo["uid"],"type"=>"短信","code"=>$meinfo["user_phone"],"value"=>$content,'searchToken'=>'123456789'));
            }
        }
    }
    /**
     * 禁用邀请码
     * 
     */
    public function jycode()
    {
        $mec=new Dmchanism();
        return $mec->jycode($this->id, $this->on_statu);
    }
    /**
     * 查询机构全部信息
     */
    public function selmechall(){
        $mec=new Dmchanism();
        return $mec->selmecall($this->rec_status, $this->star_time, $this->end_time, $this->page, $this->name, $this->username, $this->code, $this->use_state, $this->uid, $this->iscode, $this->user_phone);
    }
    /**
     * 查询历史详情信息
     */
    public function selhisone()
    {
        $mec=new Dmchanism();
        return $mec->selhisone($this->id);
    }
    /**
     * 查询团队机构信息
     */
    public function teammech()
    {
        $mec=new Dmchanism();
        return $mec->teammech($this->tid);
        
    }
    /**
     * 导入园所数据
     */
    public function import()
    {   
        $name=new Dmchanism();
        $tel=$name->telmech($this->user_phone);
//         if($this->contract_end==1)
//         {
//             $id=$tel[0]["id"];
//             $infos=$name->selauth($id);
//             if($infos["tid"]!=null){
//                 $list=apiGet("Team.User._List", array("tid"=>$infos["tid"]));
//                 apiGet("Team.User.Add",array('tid'=>$infos["tid"],'uid'=>$infos["uid"],'type'=>'master','end_time'=>date('Y-m-d H:s:i',time())));
//                 foreach($list as $val)
//                 {
//                     if($val["uid"]!=$infos["uid"])
//                     {
//                         apiGet("Team.User.Add",array('tid'=>$infos["tid"],'uid'=>$val["uid"],'type'=>'user','end_time'=>date('Y-m-d H:s:i',time())));
//                     }
//                     apiGet("Team.User.Del",array('tid'=>$infos["tid"],'uid'=>$val["uid"]));
//                 }
//             }
//             $name->updatecode($id, $infos["code"], date('Y-m-d H:i:s',time()), date('Y-m-d H:i:s',time()), $infos["subaccount_num"],$infos["use_state"]);
//             die;
//         }
        if($this->end_time!=null)
        {
            $code=getRandomStringNum(8);
            if($tel!=null)
            {
                $id=$tel[0]["id"];
                $infos=$name->selauth($id);
                if($infos["code"]!=null)
                {
                   $name->addhistory($infos["uid"],$infos["name"],$infos["code"],$infos["username"],$infos["user_phone"],$infos["img"],$infos["rec_status"],$infos["time"],$infos["end_time"],$infos["contract_num"],$infos["contract_doc"],$infos["auth_dir"],$infos["statu"],$infos["generate_time"] ,$infos["subaccount_num"],$infos["tid"],$infos["use_state"],$infos["beizhu"],$infos["province"],$infos["city"],$infos["county"],$infos["prouid"],$id,$infos["on_statu"],$infos["region"]);
                   $name->updateimport($id,$this->name, $this->username, $this->user_phone, $this->contract_num, $this->auth_dir, $this->pro, $this->city, $this->county);
                   if($infos["uid"]!=null&&$infos["use_state"]==1&&$infos["rec_status"]==1)
                   {
                       $data=explode(",",$infos["auth_dir"]);
                       $datas=explode(",",$this->auth_dir);
                       foreach ($data as $val)
                       {
                           if(!in_array($val, $datas))
                           {
                               apiGet("Auth.Resource.Set", array("fid"=>$infos["tid"],"ftype"=>'team',"res_type"=>'videoClassLevel',"res_id"=>$val."_9","server"=>"read","value"=>0));
                           }
                       }
                       foreach ($datas as $val)
                       {
                           if(!in_array($val, $data))
                           {
                               apiGet("Auth.Resource.Set", array("fid"=>$infos["tid"],"ftype"=>'team',"res_type"=>'videoClassLevel',"res_id"=>$val."_9","server"=>"read","value"=>1));
                           }
                       }
                       foreach ($data as $val)
                       {
                           if(!in_array($val, $datas))
                           {
                               apiGet("Auth.Resource.Set", array("fid"=>$infos["tid"],"ftype"=>'team',"res_type"=>'videoClassLevel',"res_id"=>$val."_10","server"=>"read","value"=>0));
                           }
                       }
                       foreach ($datas as $val)
                       {
                           if(!in_array($val, $data))
                           {
                               apiGet("Auth.Resource.Set", array("fid"=>$infos["tid"],"ftype"=>'team',"res_type"=>'videoClassLevel',"res_id"=>$val."_10","server"=>"read","value"=>1));
                           }
                       }
                       
                       $name->updatecode($id, $code, date('Y-m-d H:i:s',time()), $this->end_time, $this->subaccount_num+1,1);
                       $meinfo=$name->selauth($id);
                       apiGet("Team.User.Add",array('tid'=>317,'uid'=>$meinfo["uid"],'type'=>'user','end_time'=>$meinfo["end_time"]));
                       apiGet("Team.User.Add",array('tid'=>$meinfo["tid"],'uid'=>$meinfo["uid"],'type'=>'master','end_time'=>$meinfo["end_time"]));
                       $list=apiGet("Team.User._List", array("tid"=>$meinfo["tid"]));
                       foreach($list as $val)
                       {
                           if($val["uid"]!=$meinfo["uid"])
                           {
                               apiGet("Team.User.Add",array('tid'=>$meinfo["tid"],'uid'=>$val["uid"],'type'=>'user','end_time'=>$meinfo["end_time"]));
                           }
                       }
                       if(count($list)-$meinfo["subaccount_num"]>0)
                       {
                           $num=count($list)-$meinfo["subaccount_num"];
                           for ($j=0;$j<count($list);$j++)
                           {
                               $volume[$j]  = $list[$j]['id'];
                           }
                           array_multisort($volume, SORT_DESC, $list);
                           for ($i=0;$i<$num;$i++)
                           {
                               apiGet("Team.User.Del",array('tid'=>$meinfo["tid"],'uid'=>$list[$i]["uid"]));
                           }
                       }
                       $content="".$meinfo["username"]."园长您好，您在育视界的机构会员身份已续约成功，到期时间".$this->end_time."。育视界，让教育无处不在。";
                       userMessage($meinfo["uid"],"园所服务",$content);
                   }
                   else
                   {
                       $info=$name->updateimportcode($id,$code,date('Y-m-d H:i:s',time()), $this->end_time, $this->subaccount_num+1, 0);
                       $meinfo=$name->selauth($id);
                       $tid="";
                       if($meinfo["tid"]==null){
                           $tid=apiGet("Team.Team.Add", array('name'=>$meinfo["name"],'user_name'=>"子账号",'type'=>'subaccount','desc'=>"账号"));
                       }
                       else{
                           $tid=$meinfo["tid"];
                       }
                       $name->updtidone($id, $tid);
                       $data=explode(",", $meinfo["auth_dir"]);
                       foreach ($data as $val){
                           apiGet("Auth.Resource.Set", array("fid"=>$tid,"ftype"=>'team',"res_type"=>'videoClassLevel',"res_id"=>$val."_9","server"=>"read","value"=>1));
                       }
                       foreach ($data as $val){
                           apiGet("Auth.Resource.Set", array("fid"=>$tid,"ftype"=>'team',"res_type"=>'videoClassLevel',"res_id"=>$val."_10","server"=>"read","value"=>1));
                       }
                       $content="".$meinfo["username"]."园长您好，您在育视界的机构会员身份激活码为".$code."，到期时间".$this->end_time."，请尽快前往激活哦。详询400-991-4418。育视界，让教育无处不在";
                       userMessage(-1,"园所服务",$content,2,$meinfo["user_phone"]);
                   }
                }
             }
            else
            {
                $id=$name->addimport($this->name, $this->username, $this->user_phone, $this->contract_num, $this->auth_dir, $this->pro, $this->city, $this->county);
                $info=$name->updateimportcode($id,$code,date('Y-m-d H:i:s',time()), $this->end_time, $this->subaccount_num+1, 0);
                $meinfo=$name->selauth($id);
                $tid="";
                if($meinfo["tid"]==null){
                    $tid=apiGet("Team.Team.Add", array('name'=>$meinfo["name"],'user_name'=>"子账号",'type'=>'subaccount','desc'=>"账号"));
                }
                else{
                    $tid=$meinfo["tid"];
                }
                $name->updtidone($id, $tid);
                $data=explode(",", $meinfo["auth_dir"]);
                foreach ($data as $val){
                    apiGet("Auth.Resource.Set", array("fid"=>$tid,"ftype"=>'team',"res_type"=>'videoClassLevel',"res_id"=>$val."_9","server"=>"read","value"=>1));
                }
                foreach ($data as $val){
                    apiGet("Auth.Resource.Set", array("fid"=>$tid,"ftype"=>'team',"res_type"=>'videoClassLevel',"res_id"=>$val."_10","server"=>"read","value"=>1));
                }
                $content="".$meinfo["username"]."园长您好，您在育视界的机构会员身份激活码为".$code."，到期时间".$this->end_time."，请尽快前往激活哦。详询400-991-4418。育视界，让教育无处不在";
                userMessage(-1,"园所服务",$content,2,$meinfo["user_phone"]);
            }
        }
        else
        {
            if($tel!=null)
            {
                $id=$tel[0]["id"];
                $name->updateimport($id,$this->name, $this->username, $this->user_phone, $this->contract_num, $this->auth_dir, $this->pro, $this->city, $this->county);
            }
            else
            {
                $name->addimport($this->name, $this->username, $this->user_phone, $this->contract_num, $this->auth_dir, $this->pro, $this->city, $this->county);
            }
        }
        return true;
    }
    ///tid查询团队机构信息
    public function teamtid(){
        $mec=new Dmchanism();
        return $mec->teamtid($this->tid);
    }
}