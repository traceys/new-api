<?php
namespace User\Api;
use PhalApi\Api;
use User\Domain\Certificate as Certificates;

class Certificate extends Api{

    public function getRules()
    {
        return array(
            'applyFor'=>array(
                'token'=> array('name' => 'token','source' => 'get','require' =>true,'desc' =>'用户token'),
                'id'=>array('name' => 'id','source' => 'get','default' =>0,'type'=>'int','desc' =>'ID'),
                'territory'=> array('name' => 'territory','source' => 'get','default' =>NULL,'desc' =>'区域'),
                'province'=> array('name' => 'province','source' => 'get','default' =>NULL,'desc' =>'省份'),
                'unit'=> array('name' => 'unit','source' => 'get','default' =>NULL,'desc' =>'单位'),
                'jobnumber'=> array('name' => 'jobnumber','source' => 'get','default' =>NULL,'desc' =>'工号'),
                'identityUrl'=> array('name' => 'identityUrl','source' => 'get','default' =>NULL,'desc' =>'身份照'),
                'stopTime'=> array('name' => 'stopTime','source' => 'get','default' =>'','desc' =>'截止日期'),
                'linkman'=> array('name' => 'linkman','source' => 'get','default' =>NULL,'desc' =>'联系人'),
                'tell'=> array('name' => 'tell','source' => 'get','default' =>NULL,'desc' =>'手机号'),
                'uid'=> array('name' => 'uid','source' => 'get','default' =>'','desc' =>'手机号'),
                'spell' => array('name' => 'spell','source' => 'get','default' =>NULL,'desc' =>'姓名拼音'),
                'applyTime' => array('name' => 'applyTime','source' => 'get','default' =>null,'desc' =>'姓名拼音'),
                'message' => array('name' => 'message','source' => 'get','default' =>'','desc' =>'个人简介'),
                'grade' => array('name' => 'grade','source' => 'get','default' =>NULL,'desc' =>'证书等级'),
                'stageName' => array('name' => 'stageName','source' => 'get','default' =>'','desc' =>'艺名'),
                'licenceNum' => array('name' => 'licenceNum','source' => 'get','default' =>'','desc' =>'证书编号'),
                'dateBirth' => array('name' => 'dateBirth','source' => 'get','default' =>'','desc' =>'出生日期'),
                'state' => array('name' => 'state','source' => 'get','default' =>'','desc' =>'入职状态'),
                'videoProducing' => array('name' => 'videoProducing','source' => 'get','default' =>'','desc' =>'视频制作'),
                'certificates' => array('name' => 'certificates','source' => 'get','default' =>'','desc' =>'其他证书地址'),
                'personalAlbum' => array('name' => 'personalAlbum','source' => 'get','default' =>'','desc' =>'个人相册地址'),
                'showHome' => array('name' => 'showHome','source' => 'get','default' =>'','desc' =>'是否展示在首页'),
                'oldCer' => array('name' => 'oldCer','source' => 'get','default' =>'','desc' =>'前证书'),
                'entryTime' => array('name' => 'entryTime','source' => 'get','default' =>'','desc' =>'入职时间'),
                'applicationRestult' => array('name' => 'applicationRestult','source' => 'get','default' =>'','desc' =>'申请结果'),
                'pay_statu' => array('name' => 'pay_statu','source' => 'get','default' =>'','desc' =>'申请状态'),
                'statu' => array('name' => 'statu','source' => 'get','default' =>'','desc' =>'数据是否有效'),
            ),
            'finds'=>array(
                'token' => array('name' => 'token','source' => 'get','default'=>false,'desc' =>'用户token'),
                'id' => array('name' => 'id','source' => 'get','default' =>false,'desc' =>'是否检查到数据'),
                'pay_statu' => array('name' => 'pay_statu','source' => 'get','default' =>1,'desc' =>'查找数据状态'),
                ),
           'certificateList'=>array(
               'pay_statu' => array('name' => 'pay_statu','source' => 'get','default' =>null,'desc' =>'电子证书状态'),
               'grade' => array('name' => 'grade','source' => 'get','default' =>null,'desc' =>'证书等级'),
               'videoProducing' => array('name' => 'videoProducing','source' => 'get','default' =>null,'desc' =>'视频制作'),
               'serach' => array('name' => 'serach','source' => 'get','default' =>null,'desc' =>'搜索的值'),
               'like' => array('name' => 'like','source' => 'get','default' =>null,'desc' =>'匹配的字段'),
               'page' => array('name' => 'page','source' => 'get','default' =>1,'desc' =>'当前页数'),
               'state' => array('name' => 'state','source' => 'get','default' =>null,'desc' =>'当前页数'),
               'number' => array('name' => 'number','source' => 'get','default' =>40,'desc' =>'当前页数'),
               'on_statu' => array('name' => 'on_statu','source' => 'get','default'=>null,'desc' =>'状态'),
               ),
            'seluserteacher'=>array(
                'uid' => array('name' => 'uid','source' => 'get'),
            ),
            'selteacher'=>array(
                'uid' => array('name' => 'uid'),
                'tid' => array('name' => 'tid'),
            ),
            'selsupteacher'=>array(
                'uid' => array('name' => 'uid'),
                'tid' => array('name' => 'tid'),
            ),
        );

    }

    public function applyFor(){
        $arr = [
            'token'=>$this->token,
            'id'=>$this->id,
            'territory'=>$this->territory,
            'province'=>$this->province,
            'unit'=>$this->unit,
            'jobnumber'=>$this->jobnumber,
            'identityUrl'=>$this->identityUrl,
            'stopTime'=>$this->stopTime,
            'linkman'=>$this->linkman,
            'tell'=>$this->tell,
            'spell'=>$this->spell,
            'message'=>$this->message,
            'licenceNum'=>$this->licenceNum,
            'grade'=>$this->grade,
            'stageName'=>$this->stageName,
            'applyTime'=>$this->applyTime,
            'dateBirth'=>$this->dateBirth,
            'state'=>$this->state,
            'videoProducing'=>$this->videoProducing,
            'certificates'=>$this->certificates,
            'personalAlbum'=>$this->personalAlbum,
            'showHome'=>$this->showHome,
            'oldCer'=>$this->oldCer,
            'entryTime'=>$this->entryTime,
            'uid'=>$this->uid,
            'applicationRestult'=>$this->applicationRestult,
            'pay_statu'=>$this->pay_statu,
            'statu'=>$this->statu
        ];

        $Dmchanism = new Certificates();

        return $Dmchanism->ApplyCreate($arr);
    }

    public function finds(){
        $Dmchanism = new Certificates();

        $arr = [
            'id'=>$this->id,
            'token'=>$this->token,
            'pay_statu'=>$this->pay_statu
        ];

        return $Dmchanism->selectdata($arr);

    }



    public function certificateList(){

        $arr = [
            'pay_statu'=>$this->pay_statu,
            'grade'=>$this->grade,
            'videoProducing'=>$this->videoProducing,
            'serach'=>$this->serach,
            'like'=>$this->like,
            'page'=>$this->page,
            'state'=>$this->state,
            'number'=>$this->number,
            'on_statu'=>$this->on_statu
        ];

        $Dmchanism = new Certificates();

        return $Dmchanism->showList($arr);

    }

public function dataStatistics(){
    $Dmchanism = new Certificates();
    return $Dmchanism->statistice();

}
 ///根据uid查询教师身份
public function seluserteacher()
{
    $mcert=new Certificates();
    return $mcert->seluserteacher($this->uid);
}
//根据uid,tid查询教师身份
public function selteacher()
{
    $mcert=new Certificates();
    return $mcert->selteacher($this->uid, $this->tid);
}
//根据uid,tid查询上级教师身份
public function selsupteacher()
{
    $uid=apiGet("Team.User.getSuperior",array("uid"=>$this->uid,"tid"=>$this->tid));
    if($uid!=null)
    {
      $mcert=new Certificates();
      return $mcert->selteacher($uid, $this->tid);
    }
    else{
      return null;
    }
}
/**
 * Certificate of expiration
 * 检测到期证书
 */
//public function checkNndtime(){
//    $Dmchanism = new Dmchanism();
//
//}


}