<?php
namespace User\Api;
use PhalApi\Api;
use User\Domain\User as Douser;
use User\Domain\Funuser as Dfunuser;
use User\Domain\Mechanism as Dmechanism;
use User\Domain\Event as Devent;

class User extends Api
{
	public function getRules() 
	{
        return array(
            'login' => array(
                'telphone' => array('name' => 'telphone','require' => true,'source' => 'get'),
                'code' => array('name' => 'code','require' => true,'source' =>'get'),
            ),
            'sendcode' => array(
                'telphone' => array('name' => 'telphone','require' => true,'source' => 'get'),
            ),
            'logincode' => array(
                'code' => array('name' => 'code','require' => true,'source' => 'get'),
                'encryptData' => array('name' => 'encryptData','require' => true,'source' => 'get'),
                'iv' => array('name' => 'iv','require' => true,'source' => 'get'),
            ),
            'jxopenid' => array(
                'code' => array('name' => 'code','require' => true,'source' => 'get'),
                'encryptData' => array('name' => 'encryptData','require' => true,'source' => 'get'),
                'iv' => array('name' => 'iv','require' => true,'source' => 'get'),
            ),
            'loginmall' => array(
                'code' => array('name' => 'code','require' => true,'source' => 'get'),
                'telphone' => array('name' => 'telphone','require' => true,'source' => 'get'),
                'recode' => array('name' => 'recode','require' => true,'source' => 'get'),
            ),
            'loginregister' => array(
                'code' => array('name' => 'code','require' => true,'source' => 'get'),
                'telphone' => array('name' => 'telphone','require' => true,'source' => 'get'),
                'recode' => array('name' => 'recode','require' => true,'source' => 'get'),
            ),
            'returnwx' => array(
                'code' => array('name' => 'code','require' => true,'source' => 'get'),
            ),
            'wb_callback' => array(
                'code' => array('name' => 'code','require' => true,'source' => 'get'),
            ),
            'qq_callback' => array(
                'code' => array('name' => 'code','require' => true,'source' => 'get'),
            ),
            'outlogin' => array(
                'token' => array('name' => 'token','require' => true,'source' => 'get'),
            ),
            'selecttoken' => array(
                'token' => array('name' => 'token','require' => true),
                'searchToken' => array('name' => 'searchToken','require' => true),
            ),
            'deluser' => array(
                'token' => array('name' => 'token','require' => true,'source' => 'get'),
                'uid' => array('name' => 'uid','require' => true,'source' => 'get'),
            ),
            'updateuser' => array(
                'token' => array('name' => 'token','source' => 'get'),
                'uid' => array('name' => 'uid','require' => true,'source' => 'get'),
                'name' => array('name' => 'name','source' => 'get'),
                'header_img' => array('name' => 'header_img','source' => 'get'),
//                 'password' => array('name' => 'uid','source' => 'get'),
                'telphone' => array('name' => 'telphone','source' => 'get'),
                'user_status' => array('name' => 'user_status','source' => 'get'),
//                 'ver_code' => array('name' => 'uid','source' => 'get'),
//                 'tel_ver_code' => array('name' => 'uid','source' => 'get'),
            ),
            'updatequser' => array(
                'token' => array('name' => 'token','source' => 'get'),
                'name' => array('name' => 'name','source' => 'get'),
                'header_img' => array('name' => 'header_img','source' => 'get'),
                'telphone' => array('name' => 'telphone','source' => 'get'),
                'user_status' => array('name' => 'user_status','source' => 'get'),
            ),
            'updatepwd' => array(
                'password' => array('name' => 'password','require' => true,'source' => 'get'),
                'telphone' => array('name' => 'telphone','require' => true,'source' => 'get'),
                'recode' => array('name' => 'recode','require' => true,'source' => 'get'),
            ),
            'tokenverify' => array(
                'token' => array('name' => 'token','require' => true,'source' => 'get'),
            ),
            'adminreg' => array(
                'username' => array('name' => 'username','require' => true,'source' => 'get'),
                'telphone' => array('name' => 'telphone','require' => true,'source' => 'get'),
                'password' => array('name' => 'password','require' => true,'source' => 'get'),
            ),
            'updateadmin' => array(
                'uid' => array('name' => 'uid','require' => true,'source' => 'get'),
                'username' => array('name' => 'username','source' => 'get'),
                'telphone' => array('name' => 'telphone','source' => 'get'),
                'password' => array('name' => 'password','source' => 'get'),
            ),
            'userlist' => array(
                'id' => array('name' => 'id','source' => 'get'),
                'name' => array('name' => 'name','source' => 'get'),
                'telphone' => array('name' => 'telphone','source' => 'get'),
                'regstr_time' => array('name' => 'regstr_time','source' => 'get'),
                'regend_time' => array('name' => 'regend_time','source' => 'get'),
                'daoqstr_time' => array('name' => 'daoqstr_time','source' => 'get'),
                'daoqend_time' => array('name' => 'daoqend_time','source' => 'get'),
                'issming' => array('name' => 'issming','source' => 'get'),
                'usertype' => array('name' => 'usertype','source' => 'get'),
                'page' => array('name' => 'page','source' => 'get'),
                'phone_status' => array('name' => 'phone_status','source' => 'get'),
            ),
            'usercount' => array(
                'id' => array('name' => 'id','source' => 'get'),
                'name' => array('name' => 'name','source' => 'get'),
                'telphone' => array('name' => 'telphone','source' => 'get'),
                'regstr_time' => array('name' => 'regstr_time','source' => 'get'),
                'regend_time' => array('name' => 'regend_time','source' => 'get'),
                'daoqstr_time' => array('name' => 'daoqstr_time','source' => 'get'),
                'daoqend_time' => array('name' => 'daoqend_time','source' => 'get'),
                'issming' => array('name' => 'issming','source' => 'get'),
                'usertype' => array('name' => 'usertype','source' => 'get'),
                'phone_status' => array('name' => 'phone_status','source' => 'get'),
            ),
            'userone' => array(
                'uid' => array('name' => 'uid','require' => true,'source' => 'get'),
            ),
            'userin' => array(
                'uids' => array('name' => 'uids','require' => true,'source' => 'get'),
            ),
            'addhistory' => array(
                'uid' => array('name' => 'uid','require' => true,'source' => 'get'),
                'act' => array('name' => 'act','require' => true,'source' => 'get'),
                'value' => array('name' => 'value','require' => true,'source' => 'get'),
                'to_id' => array('name' => 'to_id','require' => true,'source' => 'get'),
                'type' => array('name' => 'type','require' => true,'source' => 'get'),
            ),
            'selhistory' => array(
                'uid' => array('name' => 'uid','source' => 'get'),
                'act' => array('name' => 'act','source' => 'get'),
                'value' => array('name' => 'value','source' => 'get'),
                'to_id' => array('name' => 'to_id','source' => 'get'),
                'type' => array('name' => 'type','source' => 'get'),
                'page' => array('name' => 'page','source' => 'get'),
            ),
            'userpwd' => array(
                'telphone' => array('name' => 'telphone','require' => true,'source' => 'get'),
                'pwd' => array('name' => 'pwd','require' => true,'source' => 'get'),
                'type' => array('name' => 'type','source' => 'get'),
            ),
            'telpwdreg' => array(
                'telphone' => array('name' => 'telphone','require' => true,'source' => 'get'),
                'pwd' => array('name' => 'pwd','source' => 'get'),
                'code' => array('name' => 'code','require' => true,'source' => 'get'),
            ),
            'userupdpwd' => array(
                'token' => array('name' => 'token','require' => true,'source' => 'get'),
                'oldpwd' => array('name' => 'oldpwd','require' => true,'source' => 'get'),
                'newpwd' => array('name' => 'newpwd','require' => true,'source' => 'get'),
            ),
            'bdtel' => array(
                'token' => array('name' => 'token','require' => true,'source' => 'get'),
                'telphone' => array('name' => 'telphone','require' => true,'source' => 'get'),
                'code' => array('name' => 'code','require' => true,'source' => 'get'),
            ),
            'watermark' => array(
                'level' => array('name' => 'level','require' => true),
                'name' => array('name' => 'name','require' => true),
                'pyname' => array('name' => 'pyname','require' => true),
                'idnum' => array('name' => 'idnum','require' => true),
                'cernum' => array('name' => 'cernum','require' => true),
                'pushtime' => array('name' => 'pushtime','require' => true),
                'yxtime' => array('name' => 'yxtime','require' => true),
                'photo' => array('name' => 'photo','require' => true),
                'islizhi' => array('name' => 'islizhi','require' => true),
            ),
            'bdnewvertel' => array(
                'token' => array('name' => 'token','require' => true,'source' => 'get'),
                'telphone' => array('name' => 'telphone','require' => true,'source' => 'get'),
                'code' => array('name' => 'code','require' => true,'source' => 'get'),
            ),
            'bdnewtel' => array(
                'token' => array('name' => 'token','require' => true,'source' => 'get'),
                'telphone' => array('name' => 'telphone','require' => true,'source' => 'get'),
                'code' => array('name' => 'code','require' => true,'source' => 'get'),
            ),
            'addsubaccount' => array(
                'token' => array('name' => 'token','require' => true,'source' => 'get'),
                'telphone' => array('name' => 'telphone','require' => true,'source' => 'get'),
            ),
            'delsubaccount' => array(
                'token' => array('name' => 'token','require' => true,'source' => 'get'),
                'telphone' => array('name' => 'telphone','require' => true,'source' => 'get'),
            ),
            'selsubaccount' => array(
                'token' => array('name' => 'token','require' => true,'source' => 'get'),
            ),
            'istel' => array(
                'telphone' => array('name' => 'telphone','require' => true,'source' => 'get'),
            ),
            'signin' => array(
                'token' => array('name' => 'token','require' => true,'source' => 'get'),
            ),
            'selsignin' => array(
                'token' => array('name' => 'token','require' => true,'source' => 'get'),
            ),
            'updnickname' => array(
                'token' => array('name' => 'token','require' => true,'source' => 'get'),
                'nickname' => array('name' => 'nickname','require' => true,'source' => 'get'),
            ),
            'selenter' => array(
                'token' => array('name' => 'token','require' => true,'source' => 'get'),
                'page' => array('name' => 'page','source' => 'get'),
            ),
            'zhpwd' => array(
                'telphone' => array('name' => 'telphone','source' => 'get'),
                'pwd' => array('name' => 'pwd','source' => 'get'),
                'code' => array('name' => 'code','source' => 'get'),
            ),
            'adminselenter' => array(
                'uid' => array('name' => 'uid','require' => true,'source' => 'get'),
            ),
            'adminselsubaccount' => array(
                'uid' => array('name' => 'uid','require' => true,'source' => 'get'),
            ),
            'userallcount' => array(
                'token' => array('name' => 'token','require' => true,'source' => 'get'),
            ),
            'uservipcount' => array(
                'token' => array('name' => 'token','require' => true,'source' => 'get'),
            ),
            'usermechcount' => array(
                'token' => array('name' => 'token','require' => true,'source' => 'get'),
            ),
            'admindelsubaccount' => array(
                'uid' => array('name' => 'uid','require' => true,'source' => 'get'),
                'telphone' => array('name' => 'telphone','require' => true,'source' => 'get'),
            ),
            'hlogin' => array(
                'unionId' => array('name' => 'unionId','require' => true,'source' => 'get'),
            ),
            'seluninonid' => array(
                'token' => array('name' => 'token','require' => true,'source' => 'get'),
            ),
            'updateadminpwd' => array(
                'uid' => array('name' => 'uid','require' => true,'source' => 'get'),
                'password' => array('name' => 'password','require' => true,'source' => 'get'),
            ),
             'selrel_name' => array(
                'rel_name' => array('name' => 'rel_name','source' => 'get'),
            ),
            'vercode' => array(
                'telphone' => array('name' => 'telphone','source' => 'get'),
                'recode' => array('name' => 'recode','source' => 'get'),
            ),
        );
	}
	/**
	 * 手机号登录接口服务
	 * @return user 用户个人信息
	 */
	public function login()
	{
	    $mall=$this->telphone;
	    $num=$this->code;
	    $code=cache([$mall]);
	    //\PhalApi\DI()->redis->get_time(''.$mall.'_'.$num.'');
	    if($code==$this->code)
	    {
	        clearnCache([$mall]);
	        //\PhalApi\DI()->redis->del(''.$mall.'_'.$num.'');
	        $user=new Douser();
	        $info=$user->Login($mall);
	        if($info===450)
	        {
	            exceptions("该账号已失效！", 50);
	        }
	        else{
	            return $info;
	        }
	    }
	    else
	    {
	        exceptions("验证码错误！", 6);
	    }
	}
	/**
	 * 微信code授权登录
	 * @return user 用户个人信息
	 */
	public function logincode()
	{
	    $user=new Douser();
	    $info= $user->Logincode($this->code, $this->encryptData, $this->iv);
	    if($info===450){
	        exceptions("该账号已失效！", 50);
	    }
	    elseif ($info===411){
	        exceptions("code已失效！", 11);
	    }
	    else{
	        return $info;
	    }
	}
	/**
	 * 微信解openid
	 * 
	 */
	public function jxopenid()
	{
	    $fun=new Dfunuser();
	    $sessionkey=json_decode($fun->getsession_key($this->code),true);
	    $openid=json_decode($fun->unionid($sessionkey["session_key"], $this->encryptData, $this->iv),true);
	    return $openid["openId"];
	}
	/**
	 * 微信手机绑定
	 * @return user 用户个人信息
	 */
	public function loginmall()
	{
	    $mall=$this->telphone;
	    $num=$this->recode;
	    $code=cache([$mall]);
	    //\PhalApi\DI()->redis->get_time(''.$mall.'_'.$num.'');
	    if($code==$this->recode)
	    {
	        clearnCache([$mall]);
	        //\PhalApi\DI()->redis->del(''.$mall.'_'.$num.'');
	        $user=new Douser();
	        $info=$user->Loginmall($this->code, $this->telphone);
	        if($info===405)
	        {
	            exceptions("手机号已绑定！",5);
	        }
	        elseif ($info===450){
	            exceptions("该手机号已失效！", 50);
	        }
	        else{
	            return $info;
	        }
	    }
	    if($mall=='13350848580')
	    {
	        $user=new Douser();
	        $info=$user->Loginmall($this->code, $this->telphone);
	        if($info===405)
	        {
	            exceptions("手机号已绑定！",5);
	        }
	        else{
	            return $info;
	        }
	    }
	    else
	    {
	        exceptions("验证码错误！",6);
	    }
	}
// 	/**
// 	 * 微信登录注册授权
// 	 * @return user 用户个人信息
// 	 */
// 	public function loginregister()
// 	{
// 	    $mall=$this->telphone;
// 	    $num=$this->recode;
// 	    $code=\PhalApi\DI()->cache->get(''.$mall.'_'.$num.'');
// 	    if($code==$this->recode)
// 	    {
// 	        \PhalApi\DI()->cache->delete(''.$mall.'_'.$num.'');
// 	        $user=new Douser();
// 	        $info=$user->Loginregister($this->telphone,$this->code);
// 	        return  $info;
// 	    }
// 	    else
// 	    {
// 	        exceptions("验证码错误！", -100);
// 	    } 
// 	}
	/**
	 * 微信第三方登录
	 */
	public function  loginwxurl()
	{
	    $appid = 'wx4e7a68bd98e080a0';
	    $url = "https://open.weixin.qq.com/connect/qrconnect?appid=$appid&redirect_uri=https://www.yusj.vip/wxIndex.html&response_type=code&scope=snsapi_login&state=1&connect_redirect=1#wechat_redirect";
	    return $url;
	} 
	/**
	 * 微信第三方登录回调
	 */
	public function returnwx()
	{
        //生成一个唯一且不重复的unionid,
	    $fun=new Dfunuser();
        $wx = json_decode($fun->weixin_open($this->code),true);
        $unionid = $wx['unionid'];
        $user=new Douser();
        $info=$user->Loginwx($unionid);
        if($info===450){
            exceptions("该账号已失效！", 50);
        }
        else{
            return $info;
        }
        
	}
	/**
	 * qq第三方登录
	 */
	public function qqlogin()
	{
	    $fun=new Dfunuser();
	    return $fun->qq_login();
	}
	/**
	 * qq第三方回调
	 * @return user 用户个人信息
	 */
	public function qq_callback()
	{
	    $fun=new Dfunuser();
	    $open =$fun->qq_callback();
	    $user=new Douser();
	    $info= $user->Loginqq($open); 
	    if($info===450){
	        exceptions("该账号已失效！", 50);
	    }
	    else{
	        return $info;
	    }
	    
	}
	/**
	 * 新浪微博第三方登录
	 */
	public function wblogin()
	{
	    $fun=new Dfunuser();
	    return $fun->wb_login();
	}
	/**
	 * 新浪微博第三方回调
	 * @return user 用户个人信息
	 */
	public function wb_callback()
	{
	    $fun=new Dfunuser();
	    $open =$fun->wb_callback($this->code)['uid'];
	    $user=new Douser();
	    $info=$user->Loginxs($open);
	    if($info===450){
	        exceptions("该账号已失效！", 50);
	    }
	    else{
	        return $info;
	    }
	}
	/**
	 * 快捷登录
	 * @return user 用户个人信息
	 */
	public function loginKj()
	{
	    $code =getRandomString(16);
	    $user=new Douser();
	    return $user->LoginKj($code);
	}
	/**
	 * 用户退出登录
	 */
	public function outlogin()
	{
	    clearnCache(["token",$this->token]);
	    //\PhalApi\DI()->redis->del('token_'.$this->token);
	    return '成功！';
	}
	/**
	 * token有效查询
	 * @return user 用户个人信息
	 */
	public function selecttoken()
	{
	   $user=new Douser();
	   $searchTokenuid=cache(["token",$this->searchToken]);
	   //\PhalApi\DI()->redis->get_time('token_'.$this->searchToken);
	   if($searchTokenuid==null){
	       $searchTokenuid=$user->tokenverify($this->searchToken);
	   }
	   $tokenuid=cache(["token",$this->token]);
	   //\PhalApi\DI()->redis->get_time('token_'.$this->token);
	   if($tokenuid==null){
	       $tokenuid=$user->tokenverify($this->token);
	   }
	   if($searchTokenuid!=false)
	   {
	      return $user->seltoken($searchTokenuid);
	   }
	   if($tokenuid!=false){
	       
	       return $user->seltoken($tokenuid);
	   }
       if($this->searchToken==Admin_token)
       {
           return array('Id'=>0);
       }else
       {
           return array('Id'=>-1);
       }
	}
	/**
	 * 验证token
	 */
	public function tokenverify()
	{
        $user=new Douser();
        return $user->tokenverify($this->token);
	}
	/**
	 * 后台修改用户信息
	 */
	public function updateuser()
	{
	     //checkAuth();
	     $user=new Douser();
	     $info=$user->updateuser($this->name, $this->header_img, $this->telphone, $this->uid,$this->user_status);
	     if($info===408){
	         exceptions("请传入相关参数！", 8);
	     }
	     elseif($info===409){
	         exceptions("手机号已存在！", 9);
	     }
	     else
	     {
	        return true;
	     }
	}
	/**
	 * 后台修改用户密码
	 */
	public function updateadminpwd(){
	    $user=new Douser();
	    return $user->updateadminpwd($this->uid, $this->password);
	}
	/**
	 * 前台修改用户信息
	 */
	public function updatequser()
	{
	    //checkAuth();
	    $user=new Douser();
	    $uid=$user->tokenverify($this->token);
    	    if($uid!=false){
    	    $info=$user->updateuser($this->name, $this->header_img, $this->telphone, $uid,$this->user_status);
    	    if($info===408){
    	        exceptions("请传入相关参数！", 8);
    	    }
    	    else
    	    {
    	        return true;
    	    }
	    }
	    else{
	        exceptions("token不存在！", -100);
	    }
	}
	/**
	 * 账号删除
	 */
	public function deluser()
	{
	    //$token=\PhalApi\DI()->cache->get('token_'.$this->token);
	    checkAuth();
	    if($this->token==Admin_token)
	    {
	        $user=new Douser();
	        $del=$user->userdel($this->uid);
	        if($del)
	        {
	            clearnCache(["token",$this->token]);
	            //\PhalApi\DI()->redis->del('token_'.$this->token);
	            return '成功！';
	        }
	        else{
	        exceptions("删除失败！", -100);
	       }
	    }
	    else{
	       exceptions("token无效！", -100);
	    }
	}
	/**
	 * 发送验证码
	 */
	public function sendcode()
	{
	    $mall=$this->telphone;
	    $num=rand(100000,999999);
	    $content="你的验证码是:".$num;
	    cache([$mall],$num,600);
	    ///\PhalApi\DI()->redis->set_time(''.$mall.'_'.$num.'',$num,600);
        apiGet("Push.Push.Tsfq",array("uid"=>-1,"type"=>"短信","code"=>$mall,"value"=>$content,'searchToken'=>'123456789'));
	    return "成功！";
	}
	/**
	 * 验证验证码
	 */
	public function vercode()
	{
	    $code= cache([$this->telphone]);
	    if($code==$this->recode)
	    {
	      clearnCache([$this->telphone]);
	      exceptions("验证码正确！", -6);
	    }
	    else{
	      exceptions("验证码错误！", 6);
	    }
	}
	/**
	 * 用户修改密码
	 * 
	 */
	public function updatepwd()
	{
	    $mall=$this->telphone;
	    $num=$this->recode;
	    $code= cache([$mall]);
	    //\PhalApi\DI()->redis->get_time(''.$mall.'_'.$num.'');
	    if($code==$this->recode)
	    {
	        clearnCache([$mall]);
	        //\PhalApi\DI()->redis->del(''.$mall.'_'.$num.'');
	        $user=new Douser();
	        $info=$user->updatepwd($this->telphone,$this->password);
	        if($info===407)
	        {
	          exceptions("手机号不存在！", 7);
	        }
	        else
	        {
	          return  $info;
	        }
	    }
	    else
	    {
	        exceptions("验证码错误！", 6);
	    }
	}
	/**
	 * 后台注册
	 */
	public function adminreg()
	{
	    checkAuth();
	    $user=new Douser();
	    $info=$user->adminreg($this->username,$this->telphone,md5($this->password));
	    if($info===405){
	        exceptions("手机号已存在！", 5);
	    }
	    else{
	        return $info;
	    }
	}
	/**
	 * 后台用户信息修改
	 */
	public function updateadmin()
	{
	    checkAuth();
	    $user=new Douser();
	    $password=null;
	    if($this->password!=null)
	    {
	        $password=md5($this->password);
	    }
	    $info=$user->updateadmin($this->telphone,$password, $this->uid,$this->username);
	    if($info===408){
	        exceptions("请传入相关参数！", 8);
	    }
	    elseif ($info===409){
	        exceptions("手机号与该会员不符合！", 9);
	    }
	    else
	    {
	       return true;
	    }
	}
	/**
	 * 条件查询会员列表
	 * 
	 */
	public function userlist()
	{
	    if($this->daoqstr_time!=null || $this->daoqend_time!=null||$this->usertype!=null)
	    {
	        $info=apiGet("Team.User._list",array('tid'=>$this->usertype,'page'=>-1,'startTime'=>$this->daoqstr_time,'endTime'=>$this->daoqend_time));
	        $uid="";
	        if($info!=null)
	        {
    	        foreach ($info as $v)
    	        {
    	           $uid.=$v["uid"].","; 
    	        }
	        }
	        $uid=rtrim($uid,",");
	    }
	    else{
	        $uid=null;
	    }
	    $user=new Douser();
	    return $user->userlist($this->id,$this->telphone, $this->name, $this->regstr_time, $this->regend_time,$this->issming, $uid, $this->page,$this->phone_status);
	}
	/**
	 *会员总数 
	 */
	public function usercount()
	{
	    if($this->daoqstr_time!=null || $this->daoqend_time!=null||$this->usertype!=null)
	    {
	       $info=apiGet("Team.User._list",array('tid'=>$this->usertype,'page'=>-1,'startTime'=>$this->daoqstr_time,'endTime'=>$this->daoqend_time));
    	    $uid="";
	        if($info!=null)
	        {
    	        foreach ($info as $v)
    	        {
    	           $uid.=$v["uid"].","; 
    	        }
	        }
	        $uid=rtrim($uid,",");
	    }
	    else{
	        $uid=null;
	    }
       $user=new Douser();
       $info=$user->userlist($this->id,$this->telphone, $this->name, $this->regstr_time, $this->regend_time,$this->issming, $uid, -1,$this->phone_status);
       return count($info);
	}
	
	/***
	 * 系统用户详情信息查询
	 * 
	 */
	public function userone()
	{
	    $user=new Douser();
	    $info= $user->userone($this->uid);
	    if($info==null)
	    {
	        exceptions("该uid无会员信息！", 5);
	    }
	    else{
	        return $info;
	    }
	}
	/***
	 *  查询指定用户列表
	 *
	 */
	public function userin()
	{
	    $user=new Douser();
	    $info= $user->userin($this->uids);
	    if($info==null)
	    {
	        exceptions("该uids无会员信息！", 5);
	    }
	    else{
	        return $info;
	    }
	}
	/**
	 * 记录用户操作记录
	 */
	public function addhistory()
	{
	    $user=new Douser();
	    return $user->addhistory($this->uid, $this->act, $this->value, $this->to_id, $this->type);
	}
	/**
	 * 查询用户操作历史记录
	 */
	public function selhistory()
	{
	    $user=new Douser();
	    return $user->selhistory($this->uid, $this->act, $this->value, $this->to_id, $this->type,$this->page);
	}
	/**
	 * 用户和密码登录
	 */
	public function userpwd()
	{
	    $user=new Douser();
	    $info=$user->userpwd($this->telphone, $this->pwd,$this->type);
	    if($info===405){
	        exceptions("手机号或密码错误！", 5);
	    }
	    elseif ($info===450){
	        exceptions("该账号已失效！", 50);
	    }
	    else{
	        return $info;
	    }
	}
	/**
	 * 检查电话号码是否存在
	 */
	public function istel()
	{
	  $user=new Douser();
	  $info=$user->seltel($this->telphone);
	  if($info!=null){
	      exceptions("电话号码已存在！", 5);
	  }
	  elseif ($info===450){
	      exceptions("该账号已失效！", 50);
	  }
	  else{
	      return true;
	  }
	}
	/**
	 * 用户和密码注册
	 */
    public function telpwdreg()
    {
        $mall=$this->telphone;
        $num=$this->code;
        $code=cache([$mall]);
        //\PhalApi\DI()->redis->get_time(''.$mall.'_'.$num.'');
        if($code==$this->code)
        {
            if($this->telphone!=null&$this->pwd!=null)
            {
                clearnCache([$mall]);
                //\PhalApi\DI()->redis->del(''.$mall.'_'.$num.'');
                $user=new Douser();
                $info=$user->telpwdreg($this->telphone, $this->pwd);
                if($info===405){
                    exceptions("手机号已存在！", 5);
                }
                else{
                    return $info;
                }
            }
            else{
                return true;
            }
        }
        else{
            exceptions("验证码错误！！", 6);
        }
    }
    /**
     * 用户个人中心修改密码
     */
    public function userupdpwd()
    {    
        $user=new Douser();
        $uid=$user->tokenverify($this->token);
        if($uid!=false)
        {
            $info=$user->userupdpwd($uid, $this->oldpwd, $this->newpwd);
            if($info===409)
            {
                exceptions("旧密码错误！！", 9);
            }
            else{
                return true;
            }
        }
        else{
            exceptions("token不存在！！", -100);
        } 
    }
    /**
     * 找回密码
     */
    public function zhpwd()
    {
        $mall=$this->telphone;
        $num=$this->code;
        $code=cache([$mall]);
        //\PhalApi\DI()->redis->get_time(''.$mall.'_'.$num.'');
        if($code==$this->code)
        {
            if($this->pwd!=null)
            { 
                clearnCache([$mall]);
                $user=new Douser();
                //\PhalApi\DI()->redis->del(''.$mall.'_'.$num.'');
                $info=$user->zhpwd($this->telphone, $this->pwd);
                if($info===409)
                {
                    exceptions("电话号码不存在！！", 9);
                }
                else{
                    return true;
                }
            }
            else{
                return true;
            }
        }
        else{
            exceptions("验证码错误！！", 6);
        }
    }
    /**
     * 绑定手机号
     */
    public function bdtel()
    {
        $mall=$this->telphone;
        $num=$this->code;
        $code=cache([$mall]);
        //\PhalApi\DI()->redis->get_time(''.$mall.'_'.$num.'');
        if($code==$this->code)
        {
            clearnCache([$mall]);
            //\PhalApi\DI()->redis->del(''.$mall.'_'.$num.'');
            $user=new Douser();
            $uid=$user->tokenverify($this->token);
            if($uid!=false)
            {
                $info=$user->bdtel($uid, $this->telphone);
                if($info===410)
                {
                    $infos=$user->seltoken($uid);
                    $data=$user->token($uid,1);
                    $users=array_merge($infos,$data);
                    return $users;
                }
                else{
                    apiGet("Comment.Comment.Upduid",array('uid'=>$uid,'nuid'=>$info["Id"]));
                    $infos=$user->seltoken($info["Id"]);
                    $data=$user->token($info["Id"],1);
                    $users=array_merge($infos,$data);
                    return $users;
                   //return "手机号绑定成功";
                }
                
            }
            else{
                exceptions("token不存在！！", -100);
            }
        }
        else{
            exceptions("验证码错误！！", 6);
        }
    }
    /**
     * 绑定更改手机号
     */
    public function bdnewvertel()
    {
        $mall=$this->telphone;
        $num=$this->code;
        $code=cache([$mall]);
        //\PhalApi\DI()->redis->get_time(''.$mall.'_'.$num.'');
        if($code==$this->code)
        {
            clearnCache([$mall]);
            //\PhalApi\DI()->redis->del(''.$mall.'_'.$num.'');
            $user=new Douser();
            $uid=$user->tokenverify($this->token);
            if($uid!=false)
            {
                $info=$user->bdnewvertel($uid, $this->telphone);
                if($info===410)
                {
                    exceptions("手机号码不正确！！", 10);
                }
                else{
                    return true;
                }
            }
            else{
                exceptions("token不存在！！", -100);
            }
        }
        else{
            exceptions("验证码错误！！", 6);
        }
    }
    /**
     * 
     * 更改绑定手机号
     */
    public function bdnewtel()
    {
        $mall=$this->telphone;
        $num=$this->code;
          $code=cache([$mall]);
        //\PhalApi\DI()->redis->get_time(''.$mall.'_'.$num.'');
        if($code==$this->code)
        {
            clearnCache([$mall]);
            //\PhalApi\DI()->redis->del(''.$mall.'_'.$num.'');
            $user=new Douser();
            $uid=$user->tokenverify($this->token);
            if($uid!=false)
            {
                $info=$user->bdnewtel($uid, $this->telphone);
                if($info===411)
                {
                    exceptions("手机号已存在！！", 11);
                }
                else
                {
                    return "手机号更改成功";
                }
            }
            else{
                exceptions("token不存在！！", -100);
            }
        }
        else{
            exceptions("验证码错误！！", 6);
        }
    }
    /**
     * 水印
     */
    public function watermark()
    {
        $fun=new Dfunuser();
        $name=$fun->trun($this->name);
        //return   $fun->thimg(3,"王洁","Wangjie","510921199101161412","ytl41245","20180712","20180712-19700101","http://file.yusj.vip/r/1534835351782img1.jpg",1);
        return  $fun->thimg($this->level, $name, $this->pyname, $this->idnum, $this->cernum, $this->pushtime, $this->yxtime, $this->photo, $this->islizhi);
    }
    /**
     * 添加子账号
     * 
     */
    public function addsubaccount()
    { 
        $user=new Douser();
        $uid=$user->tokenverify($this->token);
        if($uid!=false)
        {
            $mec=new Dmechanism();
            $mech=$mec->selmec($uid);
            
//           $user=new Douser();
//           $tel=$user->userone($uid);
//           if($tel["telphone"]==$this->telphone)
//           {
//            exceptions("子账号与主账号手机号相同！", 2);die;
//           }
            $teaminfo=apiGet("Team.User.Info", array("uid"=>$uid,"tid"=>317));
            if(intval($mech["subaccount_num"])>0)
            {
                if($mech["tid"]!=null)
                {
                    $list=apiGet("Team.User._List", array("token"=>$this->token,"tid"=>$mech["tid"]));
                    $count=count($list);
                    if(intval($mech["subaccount_num"])>=$count)
                    {
                        $info=$user->seltel($this->telphone);
                        if($info!=null){
                            $aid=$info["Id"];
                            $teamlist= apiGet("Team.User.TeamList", array("token"=>$this->token,"uid"=>$aid)); 
                            for($i=0;$i<count($teamlist);$i++){
                                if($teamlist[$i]["type"]=="subaccount"){
                                    exceptions("该账号已是其他团队子账号！", 4);
                                    break;
                                }
                            }
                            $data=explode(",", $mech["auth_dir"]);
                            $classname="";
                            foreach ($data as $val)
                            {
                                $class= apiGet("Video.drama.findClass", array("id"=>$val));
                                $classname.=$class["class"].",";
                            }
                            $contents="您已成功添加".$this->telphone."为机构会员子账号，该账号已成功获得".$classname."机构视频点播权限。详询400-991-4418。育视界，让教育无处不在。";
                            userMessage($uid,"园所服务",$contents,1);
                            
                            $content="您被".$mech["name"]."添加为机构会员子账号，成功获得".$classname."机构视频点播权限。详询400-991-4418。育视界，让教育无处不在";
                            userMessage($aid,"用户服务",$content);
                        }
                        else{
                            exceptions("该账号未注册！", 3);
                        }
                        apiGet("Team.User.Add", array("token"=>$this->token,"tid"=>$mech["tid"],"uid"=>$aid,'type'=>'user','end_time'=>$teaminfo["end_time"]));
                        return "添加子账号成功!";
                    }
                    else{
                        exceptions("子账号个数是已满！", 2);
                    }
                }
                else
                {
                   $tid=apiGet("Team.Team.Add", array("token"=>$this->token,'name'=>$mech["name"],'user_name'=>"子账号",'type'=>'subaccount','desc'=>"账号"));
                   $info=$user->seltel($this->telphone);
                   if($info!=null){
                       $aid=$info["Id"];
                       $teamlist= apiGet("Team.User.TeamList", array("token"=>$this->token,"uid"=>$aid));
                       for($i=0;$i<count($teamlist);$i++){
                           if($teamlist[$i]["type"]=="subaccount"){
                               exceptions("该账号已是其他团队子账号！", 4);
                               break;
                           }
                       }
                       $data=explode(",", $mech["auth_dir"]);
                       $classname="";
                       foreach ($data as $val)
                       {
                           $class= apiGet("Video.drama.findClass", array("id"=>$val));
                           $classname.=$class["class"].",";
                       }
                        $contents="您已成功添加".$this->telphone."为机构会员子账号，该账号已成功获得".$classname."机构视频点播权限。详询400-991-4418。育视界，让教育无处不在。";
                        userMessage($uid,"园所服务",$contents,1);
                            
                        $content="您被".$mech["name"]."添加为机构会员子账号，成功获得".$classname."机构视频点播权限。详询400-991-4418。育视界，让教育无处不在";
                        userMessage($aid,"用户服务",$content);
                   }
                   else{
                        exceptions("该账号未注册！", 3);
                   }
                   apiGet("Team.User.Add", array("token"=>$this->token,"tid"=>$tid,"uid"=>$aid,'type'=>'user','end_time'=>$teaminfo["end_time"]));
                   $mec->updtid($uid, $tid);
                   return "添加子账号成功!";
                }
            }
            else{
                exceptions("子账号个数是0！", 1);
            }
        }
        else
        {
            exceptions("token不存在！", -100);
        }
    }
    ///查询子账号
    public function selsubaccount()
    {  
        $listinfo=new Douser();
        $uid=$listinfo->tokenverify($this->token);
        if($uid!=false)
        {
            $mec=new Dmechanism();
            $mech=$mec->selmec($uid);
            $tid=$mech["tid"];
            if($tid!=null)
            {
                $list=apiGet("Team.User._List", array("token"=>$this->token,"tid"=>$tid));
                if($list!=null)
                {
                    $id="";
                    foreach ($list as $val)
                    {
                        $id.=$val["uid"].",";
                    }
                    $id=rtrim($id,",");
                    
                    $userlist=$listinfo->userin($id);
                    if($userlist!=null){
                        for ($i=0;$i<count($list);$i++)
                        {
                            for ($j=0;$j<count($userlist);$j++)
                            {
                                if($list[$i]["uid"]==$userlist[$j]["Id"])
                                {
                                    $list[$i]["telphone"]=$userlist[$j]["telphone"];
                                } 
                            }
                        } 
                        return $list;
                    }
                    else
                    {
                       exceptions("没有添加子账号！", 1);
                     }
                   
                }
                else{
                    exceptions("没有添加子账号！", 1);
                }
            }
            else{
                exceptions("没有添加子账号！", 1);
            }
        }
        else
        {
            exceptions("token不存在！", -100);
        }
    }
    ///查询子账号
    public function adminselsubaccount()
    {
        $listinfo=new Douser();
        $mec=new Dmechanism();
        $mech=$mec->selmec($this->uid);
        $tid=$mech["tid"];
        if($tid!=null)
        {
            $list=apiGet("Team.User._List", array("tid"=>$tid));
            if($list!=null)
            {
                $id="";
                foreach ($list as $val)
                {
                    $id.=$val["uid"].",";
                }
                $id=rtrim($id,",");

                $userlist=$listinfo->userin($id);
                if($userlist!=null){
                    for ($i=0;$i<count($list);$i++)
                    {
                        for ($j=0;$j<count($userlist);$j++)
                        {
                            if($list[$i]["uid"]==$userlist[$j]["Id"])
                            {
                                $list[$i]["telphone"]=$userlist[$j]["telphone"];
                            }
                        }
                    }
                    return $list;
                }
                else
                {
                    exceptions("没有添加子账号！", 1);
                }
                 
            }
            else{
                exceptions("没有添加子账号！", 1);
            }
        }
        else{
            exceptions("没有添加子账号！", 1);
        }
    }
    ///删除子账号
    public function delsubaccount()
    {
        $user=new Douser();
        $uid=$user->tokenverify($this->token);
        if($uid!=false)
        {
            $mec=new Dmechanism();
            $mech=$mec->selmec($uid);
            $tid=$mech["tid"];
            $info=$user->seltel($this->telphone);
            $uid=$info["Id"];
            apiGet("Team.User.Del", array("token"=>$this->token,"tid"=>$mech["tid"],"uid"=>$uid));
            $contents="您已成功删除子账号".$this->telphone."，该账号已被限制机构视频点播权限。详询400-991-4418。育视界，让教育无处不在。";
            userMessage($mech["uid"],"园所服务",$contents,1);
            
            $content="您已被".$mech["name"]."删除机构会员子账号身份，机构视频点播权限被取消。详询400-991-4418。育视界，让教育无处不在。";
            userMessage($uid,"用户服务",$content);
            return "删除成功！";
        }
        else
        {
            exceptions("token不存在！", -100);
        }
    }
    ///后台删除子账号
    public function admindelsubaccount()
    {
        $user=new Douser();
        $mec=new Dmechanism();
        $mech=$mec->selmec($this->uid);
        $tid=$mech["tid"];
        $info=$user->seltel($this->telphone);
        $uid=$info["Id"];
        apiGet("Team.User.Del", array("tid"=>$mech["tid"],"uid"=>$uid));
        return "删除成功！";
    }
    ///导出vip会员execl数据
    public function createexecl()
    {
        $uids="";
        $teamlist=apiGet("Team.User._List",array("tid"=>318));
        if($teamlist!=null)
        {
           foreach ($teamlist as $val)
           {
               $uids.=$val["uid"].",";
           }
            $uids=rtrim($uids,",");
        }
        $user=new Douser();
        $userlist=$user->userin($uids);
        $codelist=apiGet("Currency/receive/finddiscounts",array("uid"=>$uids,"type"=>'PromotionCode'));
        for ($i=0;$i<count($userlist);$i++)
        {
            if($teamlist!=null){
                for ($j=0;$j<count($teamlist);$j++){
                    if($userlist[$i]["Id"]==$teamlist[$j]["uid"]){
                        $userlist[$i]["endtime"]=$teamlist[$j]["end_time"];
                        break;
                    }
                    else{
                        $userlist[$i]["endtime"]=null;
                    }
                }
            }
            else{
                $userlist[$i]["endtime"]=null;
            }
          
            if($codelist!=null)
            {
                for ($h=0;$h<count($codelist);$h++){
                    if($userlist[$i]["Id"]==$codelist[$h]["uid"]){
                        $userlist[$i]["code"]=$codelist[$h]["key"];
                        $userlist[$i]["num"]=$codelist[$h]["nums"];
                        break;
                    }
                    else{
                        $userlist[$i]["code"]=null;
                        $userlist[$i]["num"]=null;
                    }
                }
            }
            else{
                $userlist[$i]["code"]=null;
                $userlist[$i]["num"]=null;
            }
           
        }
        header("Content-type:application/vnd.ms-excel");
        header("Content-Disposition:filename=vip会员信息.xls");
        $strexport="会员ID\t用户名\t绑定手机号\t注册时间\t是否实名\t到期时间\t推广码\t收益比例\t推广人次\r";
        foreach ($userlist as $list){
            $strexport.="".$list["Id"]."\t";
            $strexport.="".$list["username"]."\t";
            $strexport.="".$list["telphone"]."\t";
            $strexport.="".$list["reg_time"]."\t";
            if($list["rel_status"]==1)
            {
             $strexport.="已实名\t";
            }
           else{
              $strexport.="未实名\t";
          }
          $strexport.="".$list["endtime"]."\t";
          $strexport.="".$list["code"]."\t";
          $bilist=apiGet("Configure.Configure.Selone",array("key"=>$list["Id"],"type"=>'userGetMoneyFromSpread'));
          if($bilist!=null){
              $strexport.="".$bilist["value"]."\t";
          }
          else{
              $strexport.="\t";
          }
          $strexport.="".$list["num"]."\r";
        }
        $strexport=iconv('UTF-8',"GB2312//IGNORE",$strexport);
        exit($strexport);
    }
    /**
     * 签到
     */
    public function signin()
    {
           $user=new Douser();
           $uid=$user->tokenverify($this->token);
            if($uid!=false)
            {
              $signin= cache(["user.signin.qd",$uid]);
              //\PhalApi\DI()->redis->get_time('user_signin_qd_'.$uid.'');
              if($signin==null)
              {
                  $today = strtotime(date('Y-m-d',time()));
                  $end = $today + 24 * 60 * 60;
                  $now=$end-time();
                  cache(["user.signin.qd",$uid],$uid,$now);
                // \PhalApi\DI()->redis->set_time('user_signin_qd_'.$uid.'',$uid,$now);
                 ///加积分
                 addjifen("dayqd",$uid);
                 return "签到成功！";
              }
              else
              {
                exceptions("你已经签过到了！", -10);
             }
        }
        else{
            exceptions("token不存在！", -100);
        }
    }
    /**
     * 查询签到
     */
    public function selsignin()
    {
        $user=new Douser();
        $uid=$user->tokenverify($this->token);
        if($uid!=null)
        {
            $signin=cache(["user.signin.qd",$uid]);
            //\PhalApi\DI()->redis->get_time('user_signin_qd_'.$uid.'');
            if($signin==null)
            {
                exceptions("你还没有签到！", -5);
            }
            else{
                exceptions("你已经签过到了！", -10);
            }
        }
        else{
            exceptions("token不存在！", -100);
        }
    }
    /**
     * 获取签到积分
     */
    public function seldayqd()
    {
        $event=new Devent();
        $eventinfo=$event->eventselect("dayqd");
        return $eventinfo["event_value"];
    }
    /**
     * 修改昵称
     */
    public function updnickname()
    {
        $user=new Douser();
        $uid=$user->tokenverify($this->token);
        if($uid!=false)
        {
            return $user->updnickname($uid, $this->nickname);
        }
        else{
            exceptions("token不存在！", -100);
        }
    }
    /**
     * 登录日志
     */
    public function selenter()
    {
        $user=new Douser();
        $uid=$user->tokenverify($this->token);
        if($uid!=false)
        {
            return $user->selhistory($uid,"登录","","","",$this->page,15,100);
            
        }
        else{
            exceptions("token不存在！", -100);
        }
    }
    /**
     * 登录日志
     */
    public function adminselenter()
    {
       $user=new Douser();
       return $user->selhistory($this->uid,"登录","","","",1);
    }
    /**
     * 分会下注册会员总数
     */
    public function userallcount()
    {
        $user=new Douser();
        $uid=$user->selecttoken($this->token);
        $branch=apiGet("Configure.Configure.seluser",array("uid"=>$uid["Id"]));
        $data="";
        if($branch!=null)
        {
            $value=json_decode($branch[0]["value"],true)["manageData"];
            if($value!=null){
                $region=json_decode($value,true);
                for($j=0;$j<count($region);$j++){
                    $dz="中国".$region[$j][0].$region[$j][1];
                    $data.="'$dz',";
                }
                for($i=0;$i<count($region);$i++){
                    $dz="中国".$region[$i][0].$region[$i][1].$region[$i][2];
                    $data.="'$dz',";
                }
            }
            $data=rtrim($data,",");
        }
        if($data!=null){
            $info=$user->regcount($data,null);
            return $info[0]["num"];
        }
        else{
            return 0;
        }
    }
    /**
     * 分会下vip注册总数
     */
    public function uservipcount()
    {
        $user=new Douser();
        $uid=$user->selecttoken($this->token);
        $branch=apiGet("Configure.Configure.seluser",array("uid"=>$uid["Id"]));
        $data="";
        if($branch!=null)
        {
            $value=json_decode($branch[0]["value"],true)["manageData"];
            if($value!=null){
                $region=json_decode($value,true);
                for($j=0;$j<count($region);$j++){
                    $dz="中国".$region[$j][0].$region[$j][1];
                    $data.="'$dz',";
                }
                for($i=0;$i<count($region);$i++){
                    $dz="中国".$region[$i][0].$region[$i][1].$region[$i][2];
                    $data.="'$dz',";
                }
            }
            $data=rtrim($data,",");
        }
        $vip=apiGet("Team.User._List",array('tid'=>318));
        $uid="";
        if($vip!=null){
            for ($i=0;$i<count($vip);$i++){
                $uid.=$vip[$i]["uid"].",";
            }
           $uid=rtrim($uid,",");
        }
       if($data!=null && $uid!=null)
        {
            $info=$user->regcount($data,$uid);
            if($info!=null){
                return $info[0]["num"];
            }
            else{
                return 0;
            }
           
        }
        else{
            return 0;
        }
    }
    /**
     * 分会下机构注册总数
     */
    public function usermechcount()
    {
        $user=new Douser();
        $uid=$user->selecttoken($this->token);
        $branch=apiGet("Configure.Configure.seluser",array("uid"=>$uid["Id"]));
        $data="";
        if($branch!=null)
        {
            $value=json_decode($branch[0]["value"],true)["manageData"];
            if($value!=null){
                $region=json_decode($value,true);
                for($j=0;$j<count($region);$j++){
                    $dz="中国".$region[$j][0].$region[$j][1];
                    $data.="'$dz',";
                }
                for($i=0;$i<count($region);$i++){
                    $dz="中国".$region[$i][0].$region[$i][1].$region[$i][2];
                    $data.="'$dz',";
                }
            }
            $data=rtrim($data,",");
        }
        $mech=apiGet("Team.User._List",array('tid'=>317));
        $uid="";
        if($mech!=null){
            for ($i=0;$i<count($mech);$i++){
                $uid.=$mech[$i]["uid"].",";
            }
           $uid=rtrim($uid,",");
        }
        if($data!=null && $uid!=null)
        {
            $info=$user->regcount($data,$uid);
            if($info!=null)
            {
                return $info[0]["num"];
            }
            else{
                return 0;
            }
           
        }
        else{
            return 0;
        }
    }
//     /**
//      * 分会下会的会员消费金额
//      */
//     public function userallmoney()
//     {
//         $user=new Douser();
//         $uid=$user->selecttoken($this->token);
//         $branch=apiGet("Configure.Configure.seluser",array("uid"=>$uid["Id"]));
//         $data="";
//         if($branch!=null)
//         {
//             $value=json_decode($branch[0]["value"],true)["manageData"];
//             if($value!=null){
//                 $region=json_decode($value,true);
//                 for($j=0;$j<count($region);$j++){
//                     $dz="中国".$region[$j][0].$region[$j][1];
//                     $data.="'$dz',";
//                 }
//                 for($i=0;$i<count($region);$i++){
//                     $dz="中国".$region[$i][0].$region[$i][1].$region[$i][2];
//                     $data.="'$dz',";
//                 }
//             }
//             $data=rtrim($data,",");
//         }
//         if($data!=null)
//         {
//             $info=$user->reguser($data);
//             $uid="";
//             if($info!=null)
//             {
//                 for($i=0;$i<count($info);$i++)
//                 {
//                    $uid.=$info[$i]["uid"].",";     
//                 }
//                $uid=rtrim($uid,",");
//                $strtime=json_decode($branch[0]["value"],true)["createTime"];
//                $endtime=date("Y-m-d H:i:s",time());
//             }
//             else{
//                 return 0;
//             }
//         }
//         else{
//             return 0;
//         }
//     }
     /**
      * 
      * h5登录
      */
      public function hlogin()
      {
          $user=new Douser();
          return $user->Hlogin($this->unionId);
      }
      /**
       *查询第三方绑定 
       * 
       */
      public function seluninonid()
      {
          $user=new Douser();
          $uid=$user->selecttoken($this->token);
          return $user->Seluninonid($uid);
      }
      //真实姓名模糊查询
      public function selrel_name(){
          $user=new Douser();
          return $user->selrel_name($this->rel_name);
      }
}
