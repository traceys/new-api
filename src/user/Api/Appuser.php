<?php
namespace User\Api;
use PhalApi\Api;
use User\Domain\Appuser as Doappuser;
use User\Domain\User as Douser;
use User\Domain\Funuser as Dfunuser;
use User\Domain\Company as Dcompany;
use User\Domain\School as Dschool;
use User\Domain\Promo as Dpromo;
use User\Domain\Relation as Drelation;
use User\Domain\Certificate as Dcertificate;
use User\Domain\Record as Drecord;
class Appuser extends Api
{
    public function getRules()
    {
        return array(
            'applogin' => array(
                'telphone' => array('name' => 'telphone','require' => true,'source' => 'get'),
                'code' => array('name' => 'code','require' => true,'source' =>'get'),
            ),
            'userpwd' => array(
                'telphone' => array('name' => 'telphone','require' => true,'source' => 'get'),
                'pwd' => array('name' => 'pwd','require' => true,'source' => 'get'),
            ),
            'telpwdreg' => array(
                'telphone' => array('name' => 'telphone','require' => true,'source' => 'get'),
                'pwd' => array('name' => 'pwd','source' => 'get'),
                'code' => array('name' => 'code','require' => true,'source' => 'get'),
                'tgm' => array('name' => 'tgm','source' => 'get'),
            ),
            'zhpwd' => array(
                'telphone' => array('name' => 'telphone','source' => 'get'),
                'pwd' => array('name' => 'pwd','source' => 'get'),
                'code' => array('name' => 'code','source' => 'get'),
            ),
            'userupdpwd' => array(
                'token' => array('name' => 'token','require' => true,'source' => 'get'),
                'oldpwd' => array('name' => 'oldpwd','require' => true,'source' => 'get'),
                'newpwd' => array('name' => 'newpwd','require' => true,'source' => 'get'),
            ),
            'returnwx' => array(
                'code' => array('name' => 'code','require' => true,'source' => 'get'),
            ),
            'wb_callback' => array(
                'code' => array('name' => 'code','require' => true,'source' => 'get'),
            ),
            'qq_callback' => array(
                'code' => array('name' => 'code','require' => true,'source' => 'get'),
            ),
            'pcreg'=> array(
                'telphone' => array('name' => 'telphone','source' => 'get'),
                'pwd' => array('name' => 'pwd','source' => 'get'),
                'code' => array('name' => 'code','source' => 'get'),
                'type' => array('name' => 'type','source' => 'get'),
                'name' => array('name' => 'name','source' => 'get'),
                'name_jc' => array('name' => 'name_jc','source' => 'get'),
                'pro' => array('name' => 'pro','source' => 'get'),
                'city' => array('name' => 'city','source' => 'get'),
                'region' => array('name' => 'region','source' => 'get'),
                'adress' => array('name' => 'adress','source' => 'get'),
                'gps' => array('name' => 'gps','source' => 'get'),
                'pwd_gl' => array('name' => 'pwd_gl','source' => 'get'),
                'user' => array('name' => 'user','source' => 'get'),
                'mall' => array('name' => 'mall','source' => 'get'),
            ),
            'pclogin' => array(
                'telphone' => array('name' => 'telphone','require' => true,'source' => 'get'),
                'pwd' => array('name' => 'pwd','require' => true,'source' => 'get'),
            ),
            'setpwd' => array(
                'telphone' => array('name' => 'telphone','require' => true,'source' => 'get'),
                'pwd' => array('name' => 'pwd','require' => true,'source' => 'get'),
                'pwd_gl' => array('name' => 'pwd_gl','require' => true,'source' => 'get'),
            ),
            'getcompanyschool' => array(
                'uid' => array('name' => 'uid','require' => true,'source' => 'get'),
            ),
            'selcompany' => array(
                'tid' => array('name' => 'tid','require' => true,'source' => 'get'),
            ),
            'selschool' => array(
                'tid' => array('name' => 'tid','require' => true,'source' => 'get'),
            ),
            'getuserlist' => array(
                'tid' => array('name' => 'tid','require' => true,'source' => 'get'),
                'page' => array('name' => 'page','require' => true,'source' => 'get'),
            ),
            'bdtel' => array(
                'token' => array('name' => 'token','require' => true,'source' => 'get'),
                'telphone' => array('name' => 'telphone','require' => true,'source' => 'get'),
                'code' => array('name' => 'code','require' => true,'source' => 'get'),
            ),
            'bdnewvertel' => array(
                'token' => array('name' => 'token','require' => true,'source' => 'get'),
                'telphone' => array('name' => 'telphone','require' => true,'source' => 'get'),
                'code' => array('name' => 'code','require' => true,'source' => 'get'),
            ),
            'bdnewtel' => array(
                'token' => array('name' => 'token','require' => true,'source' => 'get'),
                'telphone' => array('name' => 'telphone','require' => true,'source' => 'get'),
                'code' => array('name' => 'code','require' => true,'source' => 'get'),
            ),
            'seluser' => array(
                'token' => array('name' => 'token','source' => 'get'),
                'tid' => array('name' => 'tid','source' => 'get'),
            ),
            'selqtuser' => array(
                'uid' => array('name' => 'uid','source' => 'get'),
                'tid' => array('name' => 'tid','source' => 'get'),
            ),
            'selqtuser' => array(
                'uid' => array('name' => 'uid','source' => 'get'),
            ),
            'updateperson' => array(
                'token' => array('name' => 'token','source' => 'get'),
                'header_img' => array('name' => 'header_img','source' => 'get'),
                'name' => array('name' => 'name','source' => 'get'),
                'person_region' => array('name' => 'person_region','source' => 'get'),
                'person_jj' => array('name' => 'person_jj','source' => 'get'),
                'person_zs' => array('name' => 'person_zs','source' => 'get'),
            ),
            'getuserall' => array(
                'tid' => array('name' => 'tid','source' => 'get'),
                'telphone' => array('name' => 'telphone','source' => 'get'),
                'name' => array('name' => 'name','source' => 'get'),
            ),
            'selname' => array(
                'type' => array('name' => 'type'),
                'name' => array('name' => 'name'),
            ),
            'createqy'=> array(
                'uid' => array('name' => 'uid','source' => 'get'),
                'type' => array('name' => 'type','source' => 'get'),
                'name' => array('name' => 'name','source' => 'get'),
                'name_jc' => array('name' => 'name_jc','source' => 'get'),
                'pro' => array('name' => 'pro','source' => 'get'),
                'city' => array('name' => 'city','source' => 'get'),
                'region' => array('name' => 'region','source' => 'get'),
                'adress' => array('name' => 'adress','source' => 'get'),
                'gps' => array('name' => 'gps','source' => 'get'),
                'user' => array('name' => 'user','source' => 'get'),
                'mall' => array('name' => 'mall','source' => 'get'),
            ),
            'updateqy'=> array(
                'tid' => array('name' => 'tid','source' => 'get'),
                'type' => array('name' => 'type','source' => 'get'),
                'name' => array('name' => 'name','source' => 'get'),
                'logo' => array('name' => 'logo','source' => 'get'),
                'name_jc' => array('name' => 'name_jc','source' => 'get'),
                'pro' => array('name' => 'pro','source' => 'get'),
                'city' => array('name' => 'city','source' => 'get'),
                'region' => array('name' => 'region','source' => 'get'),
                'adress' => array('name' => 'adress','source' => 'get'),
                'gps' => array('name' => 'gps','source' => 'get'),
                'user' => array('name' => 'user','source' => 'get'),
                'mall' => array('name' => 'mall','source' => 'get'),
                'jianje' => array('name' => 'jianje','source' => 'get'),
                'xiangce' => array('name' => 'xiangce','source' => 'get'),
                'rzzl' => array('name' => 'rzzl','source' => 'get'),
            ),
         );
    }
    /**
     * app手机验证码登录
     */
    public function applogin()
    {
        $mall=$this->telphone;
        $num=$this->code;
        $code=cache([$mall]);
        if($code==$this->code)
        {
            clearnCache([$mall]);
            $user=new Doappuser();
            $info=$user->Login($mall);
            if($info===450)
            {
                exceptions("该账号已失效！", 50);
            }
            else{
                ///生成推广码
                $tgm=getRandomStringNum(8);
                $pro=new Dpromo();
                $pro->add($info["Id"], $tgm,3);
                return $info;
            }
        }
        else
        {
            exceptions("验证码错误！", 6);
        }
    }
    /**
     * app用户和密码登录
     */
    public function userpwd()
    {
        $user=new Doappuser();
        $info=$user->userpwd($this->telphone, $this->pwd);
        if($info===405){
            exceptions("手机号或密码错误！", 5);
        }
        elseif ($info===450){
            exceptions("该账号已失效！", 50);
        }
        else{
            return $info;
        }
    }
    /**
     * app用户和密码注册
     */
    public function telpwdreg()
    {
        $mall=$this->telphone;
        $num=$this->code;
        $code=cache([$mall]);
        if($code==$this->code)
        {
            if($this->telphone!=null&$this->pwd!=null)
            {
                clearnCache([$mall]);
                $user=new Doappuser();
                $info=$user->appreg($this->telphone, $this->pwd);
                if($info===405){
                    exceptions("手机号已存在！", 5);
                }
                else{
                    ///生成推广码
                    $tgm=getRandomStringNum(8);
                    $pro=new Dpromo();
                    $pro->add($info["Id"], $tgm,3);
                    ///绑定用户关系
                    if($this->tgm!=null)
                    {
                       $tgm=$pro->seltgm($tgm);
                       if($tgm!=null)
                       {
                           if($tgm["promo_jh"]==2)
                           {
                               exceptions("推广码未激活！！", 7);
                           }
                           else
                           {
                               $rel=new Drelation();
                               if($tgm["promo_type"]==1){
                                   $type=4;
                               }
                               if($tgm["promo_type"]==2){
                                   $type=8;
                               }
                               if($tgm["promo_type"]==3){
                                   $type=5;
                               }
                               $rel->add($tgm["promo_tid"], $info["Id"], $type, 1);
                           }
                       }
                       else{
                           exceptions("推广码不存在！！", 8);
                       }
                    }
                    return $info;
                }
            }
            else{
                return true;
            }
        }
        else{
            exceptions("验证码错误！！", 6);
        }
    }
    /**
     * APP找回密码
     */
    public function zhpwd()
    {
        $mall=$this->telphone;
        $num=$this->code;
        $code=cache([$mall]);
        if($code==$this->code)
        {
            if($this->pwd!=null)
            {
                clearnCache([$mall]);
                $user=new Doappuser();
                $info=$user->zhpwd($this->telphone, $this->pwd);
                if($info===409)
                {
                    exceptions("电话号码不存在！！", 9);
                }
                if($info===450)
                {
                    exceptions("该账户已失效，无法找回密码！",50);
                }
                else{
                    return true;
                }
            }
            else{
                return true;
            }
        }
        else{
            exceptions("验证码错误！！", 6);
        }
    }
    /**
     *app系统设置修改密码
     */
    public function userupdpwd()
    {
        $user=new Douser();
        $uid=$user->tokenverify($this->token);
        if($uid!=false)
        {
            $info=$user->userupdpwd($uid, $this->oldpwd, $this->newpwd);
            if($info===409)
            {
                exceptions("旧密码错误！！", 9);
            }
            else{
                return true;
            }
        }
        else{
            exceptions("token不存在！！", -100);
        }
    }
    /**
     *app 微信第三方登录回调
     */
    public function returnwx()
    {
        //生成一个唯一且不重复的unionid,
        $fun=new Dfunuser();
        $wx = json_decode($fun->weixin_open($this->code),true);
        $unionid = $wx['unionid'];
        $user=new Douser();
        $info=$user->Loginwx($unionid);
        if($info===450){
            exceptions("该账号已失效！", 50);
        }
        else{
            return $info;
        }
    
    }
    /**
     *app qq第三方回调
     * @return user 用户个人信息
     */
    public function qq_callback()
    {
        $fun=new Dfunuser();
        $open =$fun->qq_callback();
        $user=new Douser();
        $info= $user->Loginqq($open);
        if($info===450){
            exceptions("该账号已失效！", 50);
        }
        else{
            return $info;
        }
         
    }
    /**
     *app 新浪微博第三方回调
     * @return user 用户个人信息
     */
    public function wb_callback()
    {
        $fun=new Dfunuser();
        $open =$fun->wb_callback($this->code)['uid'];
        $user=new Douser();
        $info=$user->Loginxs($open);
        if($info===450){
            exceptions("该账号已失效！", 50);
        }
        else{
            return $info;
        }
    }
    /**
     * 绑定手机号
     */
    public function bdtel()
    {
        $mall=$this->telphone;
        $num=$this->code;
        $code=cache([$mall]);
        if($code==$this->code)
        {
            clearnCache([$mall]);
            $user=new Douser();
            $uid=$user->tokenverify($this->token);
            if($uid!=false)
            {
                $appuser=new Doappuser();
                $info=$appuser->bdtel($uid, $this->telphone);
                if($info===410)
                {
                    $infos=$user->seltoken($uid);
                    $data=$user->token($uid,1);
                    $users=array_merge($infos,$data);
                    return $users;
                }
                else{
                    $infos=$user->seltoken($info["Id"]);
                    $data=$user->token($info["Id"],1);
                    $users=array_merge($infos,$data);
                    return $users;
                }
    
            }
            else{
                exceptions("token不存在！！", -100);
            }
        }
        else{
            exceptions("验证码错误！！", 6);
        }
    }
    /**
     * 旧手机号验证
     */
    public function bdnewvertel()
    {
        $mall=$this->telphone;
        $num=$this->code;
        $code=cache([$mall]);
        if($code==$this->code)
        {
            clearnCache([$mall]);
            $user=new Douser();
            $uid=$user->tokenverify($this->token);
            if($uid!=false)
            {
                $info=$user->bdnewvertel($uid, $this->telphone);
                if($info===410)
                {
                    exceptions("手机号码不正确！！", 10);
                }
                else{
                    return true;
                }
            }
            else{
                exceptions("token不存在！！", -100);
            }
        }
        else{
            exceptions("验证码错误！！", 6);
        }
    }
    /**
     *
     * 更改绑定手机号
     */
    public function bdnewtel()
    {
        $mall=$this->telphone;
        $num=$this->code;
        $code=cache([$mall]);
        if($code==$this->code)
        {
            clearnCache([$mall]);
            $user=new Douser();
            $uid=$user->tokenverify($this->token);
            if($uid!=false)
            {
                $info=$user->bdnewtel($uid, $this->telphone);
                if($info===411)
                {
                    exceptions("手机号已存在！！", 11);
                }
                else
                {
                    return "手机号更改成功";
                }
            }
            else{
                exceptions("token不存在！！", -100);
            }
        }
        else{
            exceptions("验证码错误！！", 6);
        }
    }
    /**测试**/
    public function getcity(){
        $adress="成都市金牛区";
        $city=null;
        $region=null;
        getcity_region($adress, $city,$region);
        $pro=apiGet("Configure.configure.getpro",array("city"=>$city));
        return $pro."___".$city."---".$region;
    }
    /**
     * 机构园所后台pc端注册
     */
    public function pcreg()
    {
        $mall=$this->telphone;
        $num=$this->code;
        $code=cache([$mall]);
        if($code==$this->code)
        {
            if($this->telphone!=null&$this->pwd!=null)
            {
                clearnCache([$mall]);
                $user=new Doappuser();
                $info=$user->appreg($this->telphone, $this->pwd);
                if($info===405){
                    exceptions("手机号已存在！", 5);
                }
                else{
                    ///个人用户生成推广码
                    $tgm=getRandomStringNum(8);
                    $pro=new Dpromo();
                    $pro->add($info["Id"], $tgm,3);
                    
                    $city=null;
                    $region=null;
                    getcity_region($this->adress, $city,$region);
                    $pro=apiGet("Configure.configure.getpro",array("city"=>$city));
                    ///企业推广码
                    $tgm=getRandomStringNum(8);
                    if($this->type==0)
                    {
                     $tid=apiGet("Team.Team.Add", array('name'=>$this->name,'user_name'=>"执教机构",'type'=>'root','desc'=>"执教机构"));
                     $com=new Dcompany();
                     $com->add("执教机构", $this->name, $this->name_jc, $this->gps, $pro, $city, $region, $this->adress, null, $this->user, $this->mall, null, null, $tid, $info["Id"],md5($this->pwd_gl),$tgm);
                     apiGet("Team.User.Add", array("tid"=>$tid,"uid"=>$info["Id"],'type'=>'master'));
                     apiGet("Team.Tree.Add", array("pid"=>333,"cid"=>$tid));
                     $info["tid"]=$tid;
                     $pro=new Dpromo();
                     $pro->add($tid, $tgm, 1);
                     return $info;
                    }
                    if($this->type==1)
                    {
                     $tid=apiGet("Team.Team.Add", array('name'=>$this->name,'user_name'=>"幼儿园",'type'=>'root','desc'=>"幼儿园"));
                     $sch=new Dschool();
                     $sch->add("幼儿园", $this->name, $this->name_jc, $this->adress,$pro, $city,$region, $this->gps, $this->user, $this->mall, null, null, null, null, null, $tid, $info["Id"], md5($this->pwd_gl), $tgm);
                     apiGet("Team.User.Add", array("tid"=>$tid,"uid"=>$info["Id"],'type'=>'master'));
                     apiGet("Team.Tree.Add", array("pid"=>333,"cid"=>$tid));
                     $info["tid"]=$tid;
                     $pro=new Dpromo();
                     $pro->add($tid, $tgm,2);
                     return $info;
                    }
                }
            }
            else{
                return true;
            }
        }
        else{
            exceptions("验证码错误！！", 6);
        }
    }
    /**
     * 机构园所后台pc端登录
     */
    public function pclogin()
    {
        $user=new Doappuser();
        $info=$user->userpwd($this->telphone, $this->pwd);
        
        if($info===405){
            exceptions("手机号或密码错误！", 5);
        }
        elseif ($info===450){
            exceptions("该账号已失效！", 50);
        }
        else
        {   $teamlist=apiGet("Team.user.teamlist",array("uid"=>$info["Id"],"had"=>true));
            foreach ($teamlist as $key=>$value)
            {
              if($value["utype"]=="master"){
                  return $info;
              }
            }
            exceptions("你不是机构或园所管理员！", 7);
        }
    }
    /**
     * 会员有哪些机构或园所
     */
    public function getcompanyschool()
    {
        $user=new Douser();
        $uid=$user->tokenverify($this->token);
        if($uid!=false)
        {
           $team=[];
           $teamlist=apiGet("Team.user.teamlist",array("uid"=>$this->uid,"had"=>true));
           foreach ($teamlist as $key=>$value)
           {
               $tid=apiGet("Team.tree.gettid",array("tid"=>$value["id"]));
               if($tid!=false)
               {
                $teaminfo=apiGet("Team.Team.Info",array("id"=>$tid));
                $team[$key]["tid"]=$tid;
                $team[$key]["name"]=$teaminfo["name"];
                $com=new Dcompany();
                $infocom=$com->seluid($tid);
                if($infocom!=null){
                    $type=0;
                }
                $sch=new Dschool();
                $infosch=$com->$sch($tid);
                if($infosch!=null){
                    $type=1;
                }
                $team[$key]["type"]=$type;
                $team[$key]["utid"]=$value["id"];
               }
           }
           return $team;
        }
        else
        {
            exceptions("token不存在！", -100);
        }
    }
    /**
     * 显示部门成员
     */
    public function getuserlist()
    {
        $listteam=apiGet("Team.User._List",array("tid"=>$this->tid));
        $uid=null;
        if($listteam!=null)
        {
            foreach ($listteam as $v)
            {
                $uid.=$v["uid"].",";
            }
            $uid=rtrim($uid,",");
        }
        $user=new Douser();
        $listuser=$user->userlist(null,null,null, null, null,null, $uid, -1,null);
        $data=[];
        $h=0;
        for($i=0;$i<count($listteam);$i++)
        {
           for ($j=0;$j<count($listuser);$j++)
           {
               if($listteam[$i]["uid"]==$listuser[$j]["Id"])
               {
                   $data[$h]["tid"]=$listteam[$i]["tid"];
                   $data[$h]["name"]=$listuser[$j]["rel_name"];
                   $data[$h]["uid"]=$listteam[$i]["uid"];
                   $data[$h]["work_num"]=$listteam[$i]["work_num"];
                   $data[$h]["mall"]=$listuser[$j]["telphone"];
                   $h++;
                   break;
               }
           }
        }
        $count=count($data);
        outputArgs(["num"=>$count,"pagenum"=>40,"page"=>$this->page]);
        $data = array_slice($data, ($this->page - 1) * 40, 40);
        return $data;
    }
    /**查询机构或园所的会员信息**/
    public function getuserall()
    {
        $listteam=apiGet("Team.Tree.tree",array("pid"=>$this->tid));
        $tid="";
        $data=[]; 
        \User\getdert($listteam, $tid);
        if($tid!=null)
        {
            $tid=rtrim($tid,",");
            $listteam=apiGet("Team.User.listin",array("tid"=>$tid));
            $uid=null;
            if($listteam!=null)
            {
                foreach ($listteam as $v)
                {
                    $uid.=$v["uid"].",";
                }
                $uid=rtrim($uid,",");
            }
            $user=new Douser();
            $listuser=$user->userlist(null,$this->telphone, $this->name, null, null,null, $uid, -1,null);
            $h=0;
            for($i=0;$i<count($listteam);$i++)
            {
                for ($j=0;$j<count($listuser);$j++)
                {
                    if($listteam[$i]["uid"]==$listuser[$j]["Id"])
                    {
                        $data[$h]["tid"]=$listteam[$i]["tid"];
                        $data[$h]["name"]=$listuser[$j]["rel_name"];
                        $data[$h]["uid"]=$listteam[$i]["uid"];
                        $data[$h]["work_num"]=$listteam[$i]["work_num"];
                        $data[$h]["mall"]=$listuser[$j]["telphone"];
                        $teamlist=apiGet("Team.Tree.getfuinfo",array("uid"=>$listteam[$i]["uid"],"tid"=>$this->tid));
                        $data[$h]["bumen"]=$teamlist;
                        $h++;
                        break;
                    }
                }
            }
        }
        return $data;
    }
    /**
     *app个人查看获取个人信息 
     */
    public function seluser()
    {
         $user=new Douser();
         $uid=$user->tokenverify($this->token);
         if($uid!=false)
         {
              $user=new Douser();
              $info= $user->userin($uid);
              if($info==null)
              {
                exceptions("该uids无会员信息！", 5);
              }
              else{
                ///基本信息
                if($info[0]["rel_code"]!=null)
                {
                    $info[0]["Birthday"]=substr($info[0]["rel_code"],6,8);
                    if(substr($info[0]["rel_code"],16,-1)%2==0){
                        $sex="女";
                    }
                    else{
                        $sex="男";
                    }
                    $info[0]["sex"]=$sex;
                }
                else{
                    $info[0]["Birthday"]="未知";
                    $info[0]["sex"]="未知";
                }
                //教师信息
                $teach=new Dcertificate();
                $teachinfo=$teach->selteacher($uid, $this->tid);
                if($teachinfo!=null)
                {
                    if($teachinfo["grade"]==1)
                    {
                        $grade="国际注册幼儿体智能初级教练";
                    }
                    if($teachinfo["grade"]==2)
                    {
                        $grade="国际注册幼儿体智能中级教练";
                    }
                    if($teachinfo["grade"]==3)
                    {
                        $grade="国际注册幼儿体智能高级教练";
                    }
                    $info[0]["stageName"]=$teachinfo["stageName"];;
                    $info[0]["grade"]=$grade;
                    $info[0]["mycer"]=$teachinfo["mycer"];
                }
                else{
                    $info[0]["stageName"]=null;
                    $info[0]["grade"]=null;
                    $info[0]["mycer"]=null;
                }
                ///团队信息
                $teaminfo=apiGet("Team.Team.Info",array("id"=>$this->tid));
                if($teaminfo!=null)
                {
                 $info[0]["mygs"]=$teaminfo["name"];
                }
                else{
                 $info[0]["mygs"]=null;
                }
                $teamlist=apiGet("Team.Tree.getfuinfo",array("uid"=>$uid,"tid"=>$this->tid));
                $info[0]["bumen"]=$teamlist;
                return $info;
             }
         }
         else
         {
             exceptions("token不存在！", -100);
         }
    }
    /**
     *app同事查看获取个人信息
     */
    public function seltsuser()
    {
        $user=new Douser();
        $info= $user->userin($this->uid);
        if($info==null)
        {
            exceptions("该uids无会员信息！", 5);
        }
        else{
            ///基本信息
            if($info[0]["rel_code"]!=null)
            {
                $info[0]["Birthday"]=substr($info[0]["rel_code"],6,8);
                if(substr($info[0]["rel_code"],16,-1)%2==0){
                    $sex="女";
                }
                else{
                    $sex="男";
                }
                $info[0]["sex"]=$sex;
            }
            else{
                $info[0]["Birthday"]="未知";
                $info[0]["sex"]="未知";
            }
            //教师信息
            $teach=new Dcertificate();
            $teachinfo=$teach->selteacher($this->uid, $this->tid);
            if($teachinfo!=null)
            {
                if($teachinfo["grade"]==1)
                {
                    $grade="国际注册幼儿体智能初级教练";
                }
                if($teachinfo["grade"]==2)
                {
                    $grade="国际注册幼儿体智能中级教练";
                }
                if($teachinfo["grade"]==3)
                {
                    $grade="国际注册幼儿体智能高级教练";
                }
                $info[0]["stageName"]=$teachinfo["stageName"];;
                $info[0]["grade"]=$grade;
                $info[0]["mycer"]=$teachinfo["mycer"];
            }
            else{
                $info[0]["stageName"]=null;
                $info[0]["grade"]=null;
                $info[0]["mycer"]=null;
            }
            ///团队信息
            $teaminfo=apiGet("Team.Team.Info",array("id"=>$this->tid));
            if($teaminfo!=null)
            {
                $info[0]["mygs"]=$teaminfo["name"];
            }
            else{
                $info[0]["mygs"]=null;
            }
            $teamlist=apiGet("Team.Tree.getfuinfo",array("uid"=>$this->uid,"tid"=>$this->tid));
            $info[0]["bumen"]=$teamlist;
            return $info;
        }
     
    }
    /**
     *app其他查看获取个人信息
     */
    public function selqtuser()
    {
        $user=new Douser();
        $info= $user->userin($this->uid);
        if($info==null)
        {
            exceptions("该uids无会员信息！", 5);
        }
        else{
            ///基本信息
            if($info[0]["rel_code"]!=null)
            {
                $info[0]["Birthday"]=substr($info[0]["rel_code"],6,8);
                if(substr($info[0]["rel_code"],16,-1)%2==0){
                    $sex="女";
                }
                else{
                    $sex="男";
                }
                $info[0]["sex"]=$sex;
            }
            else{
                $info[0]["Birthday"]="未知";
                $info[0]["sex"]="未知";
            }
//             //教师信息
//             $teach=new Dcertificate();
//             $teachinfo=$teach->selteacher($this->uid, $this->tid);
//             if($teachinfo!=null)
//             {
//                 if($teachinfo["grade"]==1)
//                 {
//                     $grade="国际注册幼儿体智能初级教练";
//                 }
//                 if($teachinfo["grade"]==2)
//                 {
//                     $grade="国际注册幼儿体智能中级教练";
//                 }
//                 if($teachinfo["grade"]==3)
//                 {
//                     $grade="国际注册幼儿体智能高级教练";
//                 }
//                 $info[0]["stageName"]=$teachinfo["stageName"];;
//                 $info[0]["grade"]=$grade;
//                 $info[0]["mycer"]=$teachinfo["mycer"];
//             }
//             else{
//                 $info[0]["stageName"]=null;
//                 $info[0]["grade"]=null;
//                 $info[0]["mycer"]=null;
//             }
//             ///团队信息
//             $teaminfo=apiGet("Team.Team.Info",array("id"=>$this->tid));
//             if($teaminfo!=null)
//             {
//                 $info[0]["mygs"]=$teaminfo["name"];
//             }
//             else{
//                 $info[0]["mygs"]=null;
//             }
//             $teamlist=apiGet("Team.Tree.getfuinfo",array("uid"=>$this->uid,"tid"=>$this->tid));
//             $info[0]["bumen"]=$teamlist;
            return $info;
        }
         
    }
    /**
     * app修改个人信息
     */
    public function updateperson()
    {
        $user=new Douser();
        $uid=$user->tokenverify($this->token);
        if($uid!=false)
        {
           $user->updateperson($uid, $this->header_img, $this->name, $this->person_region, $this->person_jj, $this->person_zs);
           return "修改成功";
        }
        else
        {
            exceptions("token不存在！", -100);
        }
    }
    /**
     * tid查询机构信息
     */
    public function selcompany()
    {
        $com=new Dcompany();
        return $com->seluid($this->tid);
    }
    /**
     * tid查询园所信息
     */
    public function selschool()
    {
        $com=new Dschool();
        return $com->seluid($this->tid);
    }
    /**
     * APP(PC)查询机构或园所名称是否存在
     * 
     */
    public function selname()
    {
        $info=null;
        if($this->type==0){
            $com=new Dcompany();
            $info=$com->selname($this->name);
        }
        else{
            $com=new Dschool();
            $info=$com->selname($this->name);
        }
        if($info!=null){
            exceptions("该企业名称已存在！", 5);
        }
    }
    /**
     * APP创建企业
     */
    public function createqy()
    {
        if($this->pro!=null && $this->city!=null&&$this->region!=null)
        {
            $pro=$this->pro;
            $city=$this->city;
            $region=$this->region;
        }
        else
        {
            $city=null;
            $region=null;
            getcity_region($this->adress, $city,$region);
            $pro=apiGet("Configure.configure.getpro",array("city"=>$city));
        }
        $tgm=getRandomStringNum(8);
        if($this->type==0)
        {
         $tid=apiGet("Team.Team.Add", array('name'=>$this->name,'user_name'=>"执教机构",'type'=>'root','desc'=>"执教机构"));
         $com=new Dcompany();
         $com->add("执教机构", $this->name, $this->name_jc, $this->gps, $pro, $city, $region, $this->adress, null, $this->user, $this->mall, null, null, $tid, $this->uid,null,$tgm);
         apiGet("Team.User.Add", array("tid"=>$tid,"uid"=>$this->uid,'type'=>'master'));
         apiGet("Team.Tree.Add", array("pid"=>333,"cid"=>$tid));
         $pro=new Dpromo();
         $pro->add($tid, $tgm, 1);
         $rel=new Drelation();
         $rel->upre($this->uid, $tid, 4);
         $rel->upre($this->uid, $tid, 8);
        }
        if($this->type==1)
        {
         $tid=apiGet("Team.Team.Add", array('name'=>$this->name,'user_name'=>"幼儿园",'type'=>'root','desc'=>"幼儿园"));
         $sch=new Dschool();
         $sch->add("幼儿园", $this->name, $this->name_jc, $this->adress, $pro, $city, $region, $this->gps, $this->user, $this->mall, null, null, null, null, null, $tid, $this->uid, null, $tgm);
         apiGet("Team.User.Add", array("tid"=>$tid,"uid"=>$this->uid,'type'=>'master'));
         apiGet("Team.Tree.Add", array("pid"=>333,"cid"=>$tid));
         $pro=new Dpromo();
         $pro->add($tid, $tgm, 2);
         $rel->upre($this->uid, $tid, 4);
         $rel->upre($this->uid, $tid, 8);
        }
        return true;
    }
    /**
     *APP(PC) 企业信息修改
     */
    public function updateqy()
    {
        if($this->pro!=null && $this->city!=null&&$this->region!=null)
        {
            $pro=$this->pro;
            $city=$this->city;
            $region=$this->region;
        }
        else
        {
            $city=null;
            $region=null;
            getcity_region($this->adress, $city,$region);
            $pro=apiGet("Configure.configure.getpro",array("city"=>$city));
        }
        if($this->type==0)
        {
           $com=new Dcompany();
           $com->up($this->tid, $this->name, $this->logo, $this->name_jc, $this->gps, $pro, $city ,$region, $this->adress, $this->user, $this->mall, $this->jianje, $this->xiangce,$this->rzzl);
           $info=$com->seluid($this->tid);
           $record=new Drecord();
           $record->add($this->name, $this->name_jc, $this->adress, $this->gps, $this->user, $this->mall, $this->xiangce, $this->jianje, $this->tid,$info["uid"],$info["id"],0);
        }
        if($this->type==1)
        {
            $sch=new Dschool();
            $sch->up($this->tid, $this->name, $this->logo, $this->name_jc, $this->gps, $pro, $city ,$region, $this->adress, $this->user, $this->mall, $this->jianje, $this->xiangce,$this->rzzl);
            $info=$sch->seluid($this->tid);
            $record=new Drecord();
            $record->add($this->name, $this->name_jc, $this->adress, $this->gps, $this->user, $this->mall, $this->xiangce, $this->jianje, $this->tid,$info["uid"],$info["id"],1);
        }
        return true;
    }
}