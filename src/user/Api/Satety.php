<?php
namespace User\Api;
use PhalApi\Api;
use User\Domain\Satety as Dsatety;

class Satety extends Api
{
    public function getRules()
    {
        return array(
            'add' => array(
                'safety_uid' => array('name' => 'safety_uid','source' => 'get'),
                'safety_code' => array('name' => 'safety_code','source' => 'get'),
                'safety_open' => array('name' => 'safety_open','source' =>'get'),
            ),
            'upcode' => array(
                'safety_uid' => array('name' => 'safety_uid','source' => 'get'),
                'safety_code' => array('name' => 'safety_code','source' => 'get'),
            ),
            'upopen' => array(
                'safety_uid' => array('name' => 'safety_uid','source' => 'get'),
                'safety_open' => array('name' => 'safety_open','source' => 'get'),
            ),
            'upstatu' => array(
                'safety_uid' => array('name' => 'safety_uid','source' => 'get'),
            ),
            'selectopen' => array(
                'safety_uid' => array('name' => 'safety_uid','source' => 'get'),
            ),
            'versafecode' => array(
                'safety_uid' => array('name' => 'safety_uid','source' => 'get'),
                'safety_code' => array('name' => 'safety_code','source' => 'get'),
            ),
        );
    }
    /**
     * 添加会员安全码信息
     */
    public function add()
    {
        $safe=new Dsatety();
        $info=$safe->select($this->safety_uid);
        if($info==null)
        {
            return $safe->add($this->safety_uid, $this->safety_code, $this->safety_open);
        }
        else{
            exceptions("该会员已存在安全码，无需添加！", 10);
        }
    }
    /**
     * 查询是否开启安全码
     */
    public function selectopen()
    {
        $safe=new Dsatety();
        $info=$safe->select($this->safety_uid);
        if($info!=null){
            if($info["safety_open"]==1){
                exceptions("该会员开启的安全码！", 30);
            }
            if($info["safety_open"]==-1){
                exceptions("该会员关闭的安全码！", 60);
            }
        }
        else{
            exceptions("该会员还未设置安全码！", 20);
        }
    }
    /**
     * 验证安全码
     */
    public function versafecode()
    {
        $cs= cache(["user.safecode.ver",$this->safety_uid]);
        if($cs>=3){
            exceptions("验证错误失败次数超过3次！", 10);
        }
        $safe=new Dsatety();
        $info=$safe->select($this->safety_uid);
        if($info!=null){
            if($info["safety_open"]==1){
                if($info["safety_code"]==$this->safety_code){
                    exceptions("安全码正确！", 40);
                }
                else{
                    $num=$cs+1;
                    $today = strtotime(date('Y-m-d', time()));
                    $end = $today + 24 * 60 * 60;
                    $now=$end-time();
                    cache(["user.safecode.ver",$this->safety_uid],$num,$now);
                    exceptions("安全码错误！", 50);
                }
            }
            if($info["safety_open"]==-1){
                exceptions("该会员关闭的安全码！", 60);
            }
        }
        else{
            exceptions("该会员还未设置安全码！", 20);
        }
    }
    /**
     * 修改安全码
     */
    public function upcode()
    {
        $safe=new Dsatety();
        return $safe->upcode($this->safety_uid, $this->safety_code);
    }
    /**
     * 是否开启安全码
     */
    public function upopen()
    {
        $safe=new Dsatety();
        return $safe->upopen($this->safety_uid, $this->safety_open);
    }
    /**
     * 删除安全码
     */
    public function upstatu()
    {
        $safe=new Dsatety();
        return $safe->upstatu($this->safety_uid);
    }
}