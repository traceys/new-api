<?php
namespace User;

//检测出生日期
function  datebirth($idcard){
    if(empty($idcard)) return null;
    $bir = substr($idcard, 6, 8);
    $year = (int) substr($bir, 0, 4);
    $month = (int) substr($bir, 4, 2);
    $day = (int) substr($bir, 6, 2);
    return $year . "-" . $month . "-" . $day;
}
//加载所有部门
function getdert($data,&$tid)
{
    if(isset($data["children"])){
        foreach ($data["children"] as $key=>$value)
        {
            $tid.=$value["tid"].",";
            getdert($value, $tid);
        }
    }
}
