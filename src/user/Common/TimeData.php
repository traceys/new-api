<?php
namespace User\Common;
/**
 * timeData 取证书年月日
 */
class timeData
{
    public $licenceNum;//证书编号
    public $year;//年
    public $month;//月
    public $day; //日

    public function __construct($licenceNum)
    {
        $this->licenceNum = $licenceNum;
        $this->year = date("Y");
        $this->month = date("m");
        $this->day = date("d");
    }

    public function run(){
        $data = substr($this->licenceNum , 0 , 1);
        switch ($data) {
            case 'C':
                $this->primary();
                break;
            case 'Z':
                $this->intermediate();
                break;
            case 'G':
                $this->advanced();
                break;
        }
        $this->month = strlen($this->month)>1?$this->month:'0'.$this->month;

        $reslut = $this->year.$this->month.'18';

        return $reslut;

    }


    //初级处理
    private function primary(){
        $mon = $this->month % 2;
        switch ($mon) {
            case 1:
                $this->monthData();
        }

        // $res = $this->year.'-'.$this->month.'-18';
        // return $res;
    }

    //中级处理
    private function intermediate(){
        $arr = [];
        $arr['2'] = [1,2,3,4];
        $arr['8'] = [6,7,8,9,10,11,12];
        switch ($this->month) {
            case 5:
                $this->monthData();
        }
        foreach ($arr as $key => $value) {
            if (in_array($this->month, $value)) {
                $this->month = $key;
            }
        }
        // return $this->month;

    }

    //高级处理
    private function advanced(){
        $this->month = 8;
    }

    //特殊月份数据
    public function monthData(){
        if ($this->day >15) {
            $this->month = $this->month+1;
        }else{
            $this->month = $this->month-1;
        }

    }

}