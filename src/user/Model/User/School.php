<?php
namespace User\Model\User;
use PhalApi\Model\NotORMModel as Model;

class School extends Model
{
    ///添加
    public function add($company_type,$school_name,$school_jc,$school_adress,$pro,$city,$region,$adress_gps,$school_xm,$school_phone,$school_my,$school_xc,$school_jj,$school_zj,$school_gh,$school_tid,$school_uid,$school_pwd,$school_tgm)
    {
       $data=array(
           'company_type'=>$company_type,
           'school_name'=>$school_name,
           'school_jc'=>$school_jc,
           'school_adress'=>$school_adress,
           'pro'=>$pro,
           'city'=>$city,
           'region'=>$region,
           'adress_gps'=>$adress_gps,
           'school_xm'=>$school_xm,
           'school_phone'=>$school_phone,
           'school_my'=>$school_my,
           'school_xc'=>$school_xc,
           'school_jj'=>$school_jj,
           'school_zj'=>$school_zj,
           'school_gh'=>$school_gh,
           'school_statu'=>0,
           'school_createtime'=>date('Y-m-d H:i:s',time()),
           'school_tid'=>$school_tid,
           'school_uid'=>$school_uid,
           'school_pwd'=>$school_pwd,
           'school_tgm'=>$school_tgm,
           'statu'=>1
       );   
       $this->getORM()->insert($data);
    }
    ///条件查询
    public function sellist($shool_name){
        $sql="select * from api_user_school where statu=1";
        if($shool_name!=null)
        {
          $sql.=" and school_name like '%".$shool_name."%'";    
        }
        return $this->getORM()->queryAll($sql);
    }
    ///查询
    public function seluid($tid)
    {
        return $this->getORM()->where(["school_tid"=>$tid])->fetchOne();
    }
    ///设置管理密码
    public function setpwd($tid,$pwd){
        $this->getORM()->where(["school_tid"=>$tid])->update(["school_pwd"=>$pwd]);
    }
    //修改
    public function up($tid,$data)
    {
        $this->getORM()->where(["tid"=>$tid])->update($data);
    }
    //删除
    public function del($id)
    {
        $data=array(
            'statu'=>-1
        );
        $this->getORM()->where(["id"=>$id])->update($data);
    }
    ///查询机构名称是否存在
    public function selname($shool_name)
    {
        return $this->getORM()->where(["school_name"=>$shool_name])->fetchAll();
    }
}