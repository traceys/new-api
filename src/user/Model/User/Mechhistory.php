<?php
namespace User\Model\User;
use PhalApi\Model\NotORMModel as Model;

class Mechhistory extends Model
{
    ///机构认证历史
    public function addhistory($uid,$name,$code,$username,$user_phone,$img,$rec_status,$time,$end_time,$contract_num,$contract_doc,$auth_dir,$statu,$generate_time,$subaccount_num,$tid,$use_state,$beizhu,$province,$city,$county,$prouid,$fromid,$on_statu,$region)
    {
        $data=array(
            'uid'=>$uid,
            'name'=>$name,
            'code'=>$code,
            'username'=>$username,
            'user_phone'=>$user_phone,
            'img'=>$img,
            'rec_status'=>$rec_status,
            'time'=>$time,
            'end_time'=>$end_time,
            'contract_num'=>$contract_num,
            'contract_doc'=>$contract_doc,
            'auth_dir'=>$auth_dir,
            'statu'=>$statu,
            'generate_time'=>$generate_time,
            'subaccount_num'=>$subaccount_num,
            'tid'=>$tid,
            'use_state'=>$use_state,
            'beizhu'=>$beizhu,
            'province'=>$province,
            'city'=>$city,
            'county'=>$county,
            'prouid'=>$prouid,
            'fromid'=>$fromid,
            'on_statu'=>$on_statu,
            'region'=>$region
        );
        return  $this->getORM()->insert($data);
    }
    ///查询历史邀请码
    public function selcode($fromid){
        $sql="select * from api_user_mechhistory where fromid='".$fromid."'  group by code order by ID DESC";
        return  $this->getORM()->queryAll($sql);
    }
    ///判断是否续约
    public function selxy($fromid){
        $sql="select * from api_user_mechhistory where fromid='".$fromid."' and rec_status=1";
        return  $this->getORM()->queryAll($sql);
    }
    ///邀请码使用次数
    public function selnum($fromid,$code){
        $sql="select count(*) as num from api_user_mechhistory where fromid='".$fromid."' and code='".$code."' and rec_status=-1";
        return $this->getORM()->queryAll($sql);
    }
    ///查询机构历史
    public function selmecall($rec_status,$star_time,$end_time,$name,$username,$code,$use_state,$uid,$iscode,$user_phone)
    {
         $sql="select m.*,u.`name` as proname from api_user_mechhistory as m left join api_user_user as u on m.prouid=u.Id where m.statu=1";
        if($rec_status!=null){
            $sql.=" and m.rec_status=".$rec_status."";
        }
        if($star_time!=null){
            $sql.=" and m.end_time >'".$star_time."'";
        }
        if($end_time!=null){
            $sql.=" and m.end_time <'".$end_time."'";
        }
        if($name!=null){
            $sql.=" and  m.name like '%".$name."%'";
        }
        if($username!=null){
            $sql.=" and  m.username like '%".$username."%'";
        }
        if($code!=null){
            $sql.=" and  m.code like '%".$code."%'";
        } 
        if($use_state!="use"){
            $sql.=" and m.use_state=".$use_state."";
        }
        if($use_state===0){
            $sql.=" and m.uid!=0";
        }
        if($uid!=null){
            $sql.=" and m.uid like '%".$uid."%'";
        }
        if($iscode===0){
            $sql.=" and m.code !=''";
        }
        if($user_phone!=null){
            $sql.=" and m.user_phone like '%".$user_phone."%'";
        }
        $sql.=" order by m.time DESC";
        return $this->getORM()->queryAll($sql);;
    }
    ///机构认证信息
    public function selhisone($id)
    {
        return $this->getORM()->where(["id"=>$id,"statu"=>1])->fetchOne();
    }
}