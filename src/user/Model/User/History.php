<?php
namespace User\Model\User;
use PhalApi\Model\NotORMModel as Model;

class History extends Model
{
    ///添加历史记录
    public function add($uid,$act,$value,$to_id,$type,$ip,$ipadress,$province)
    {
        $data=array(
        'uid'=>$uid,
        'act'=>$act,
        'value'=>$value,
        'to_id'=>$to_id,
        'type'=>$type,
        'time'=>date("Y-m-d H:i:s",time()),
        'statu'=>1,
        'ip'=>$ip,
        'ipadress'=>$ipadress,
         'province'=>$province
        );
        $this->getORM()->insert($data);
        clearnCache(["user.history.model"],true);
    }
    ///查询用户操作历史记录
    public function sel($uid,$act,$value,$to_id,$type,$page,$num,$xscount)
    {
        $count=$this->selcount($uid, $act, $value, $to_id, $type);
        $list=cache(["user.history.model.user.history.sel",$uid,$act,$value,$to_id,$type,$page,$num,$xscount]);
        if($xscount!=null){
            if($xscount<$count){
                $count=100;
            }
        }
        outputArgs(["num"=>$count,"pagenum"=>$num,"page"=>$page]);
        if($list!=null){
          return $list;  
        } 
        $sql="select * from api_user_history as h left join api_user_user as u on h.uid=u.Id where h.statu=1";
        if($uid!=null)
        {
            $sql.=" and h.uid=".$uid."";
        }
        if($act!=null)
        {
            $sql.=" and h.act like '%".$act."%'";
        }
        if($value!=null)
        {
            $sql.=" and h.value like '%".$value."%'";
        }
        if($to_id!=null)
        {
            $sql.=" and h.to_id like '%".$to_id."%'";
        }
        if($type!=null)
        {
            $sql.=" and h.type like '%".$type."%'";
        }
        $sql.=" order by time DESC";
        if($page>0)
        {
            if($xscount!=null)
            {
                $cpage=floor($xscount/$num);
                if($cpage+1==$page)
                {
                    $pre= ($page-1)*$num;
                    $next=$xscount-$cpage*$num;
                    $sql.=" limit ".$pre.",".$next."";
                }
                else
                {
                    $pre= ($page-1)*$num;
                    $next=$num;
                    $sql.=" limit ".$pre.",".$next."";
                }
            }
         else{
            $pre= ($page-1)*$num;
            $next=$num;
            $sql.=" limit ".$pre.",".$next."";
          }
        }
        $info=$this->getORM()->queryAll($sql);
        cache(["user.history.model.user.history.sel",$uid,$act,$value,$to_id,$type,$page,$num,$xscount],$info);
        return $info;
    }
    
    ///查询用户操作历史记录
    public function selcount($uid,$act,$value,$to_id,$type)
    {
        $count=cache(["user.history.model.user.history.selcount",$uid,$act,$value,$to_id,$type]);
        if($count!=null){
            return $count;
        }
      $sql="select count(*) as num from api_user_history as h left join api_user_user as u on h.uid=u.Id where h.statu=1";
        if($uid!=null)
        {
            $sql.=" and h.uid=".$uid."";
        }
        if($act!=null)
        {
            $sql.=" and h.act like '%".$act."%'";
        }
        if($value!=null)
        {
            $sql.=" and h.value like '%".$value."%'";
        }
        if($to_id!=null)
        {
            $sql.=" and h.to_id like '%".$to_id."%'";
        }
        if($type!=null)
        {
            $sql.=" and h.type like '%".$type."%'";
        }
        $info=$this->getORM()->queryAll($sql);
        cache(["user.history.model.user.history.selcount",$uid,$act,$value,$to_id,$type],$info[0]["num"]);
        return $info[0]["num"];
    }
    ///注册区域地址人数
    public function regcount($region,$uid)
    {
        $sql="select count(*) as num from api_user_history where statu=1  and value like '%注册%'";
        if($region!=null){
            $sql.="and ipadress in(".$region.")";
        }
        if($uid!=null){
            $sql.="and uid in(".$uid.")";
        }
        return $this->getORM()->queryAll($sql);
    }
    ///注册区域得找会员
    public function reguser($region)
    {
        $sql="select * from api_user_history where statu=1  and value like '%注册%'";
        if($region!=null){
            $sql.="and ipadress in(".$region.")";
        }
        return $this->getORM()->queryAll($sql);
    }
}