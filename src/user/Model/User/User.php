<?php
namespace User\Model\User;
use PhalApi\Model\NotORMModel as Model;

class User extends Model 
{
    
    ///用telphone查询用户信息
    public function selecttel($telphone)
    {
     $user=$this->getORM()->select('Id,level,username,telphone,rel_name,rel_code,phone_status,rel_status,name,header_img,user_status,status_time,statu,reg_time,person_region,person_jj,person_zs,person_xl')->where(['telphone'=>$telphone,'statu'=>1])->fetchOne();
     return $user;
    }
    ///用uid查询用户信息
    public function selectuid($uid)
    {
        $user=$this->getORM()->select('Id,level,username,telphone,rel_name,rel_code,phone_status,rel_status,name,header_img,user_status,status_time,statu,reg_time,person_region,person_jj,person_zs,person_xl')->where(['Id'=>$uid,'statu'=>1])->fetchOne();
        return $user;
    }
    ///用uid和密码查询用户信息
    public function selectuidpwd($uid,$password)
    {
        $user=$this->getORM()->select('Id,level,username,telphone,rel_name,rel_code,phone_status,rel_status,name,header_img,user_status,status_time,statu,reg_time,person_region,person_jj,person_zs,person_xl')->where(['Id'=>$uid,'password'=>$password,'statu'=>1])->fetchOne();
        return $user;
    }
    ///账户和密码登录
    public function userpwd($telphone,$pwd)
    {
        $user=$this->getORM()->select('Id,level,username,telphone,rel_name,rel_code,phone_status,rel_status,name,header_img,user_status,status_time,statu,reg_time,person_region,person_jj,person_zs,person_xl')->where(['telphone'=>$telphone,'password'=>$pwd,'statu'=>1])->fetchOne();
        return $user;
    }
    ///注册
    public function add($level,$username,$telphone,$rel_name,$rel_code,$phone_status,$rel_status,$reg_time,$password,$name=null,$header_img=null)
    {
        $data=array(
            'level'=>$level,
            'username'=>$username,
            'telphone'=>$telphone,
            'rel_name'=>$rel_name,
            'rel_code'=>$rel_code,
            'phone_status'=>$phone_status,
            'rel_status'=>$rel_status,
            'reg_time'=>$reg_time,
            'password'=>$password,
            'name'=>$name,
            'header_img'=>$header_img
        );
        $this->getORM()->insert($data);
        $id=$this->getORM()->insert_id();
        clearnCache(["user.user.model"],true);
        return $id;
    }
    ///绑定手机号
    public function updatetel($uid,$telphone)
    {
        $data=array(
            'level'=>1,
            'telphone'=>$telphone,
            'phone_status'=>1
        );
        $this->getORM()->where(['Id'=>$uid])->update($data);
        clearnCache(["user.user.model"],true);
    }
    ///更改绑定手机号
    public function updatenewtel($uid,$telphone)
    {
        $data=array(
            'telphone'=>$telphone
        );
        $this->getORM()->where(['Id'=>$uid])->update($data);
        clearnCache(["user.user.model"],true);
    }
    ///用户信息修改
    public function updateuser($data,$uid)
    {
        $this->getORM()->where(['Id'=>$uid])->update($data);
        clearnCache(["user.user.model"],true);
    }
    ///后台用户信息修改
    public function updateadmin($data,$uid)
    {
        $this->getORM()->where(['Id'=>$uid])->update($data);
        clearnCache(["user.user.model"],true);
    }
    ///账号删除
    public function updatedel($uid)
    {
        $data=array(
            'statu'=>-1
        );
        $this->getORM()->where('Id='.$uid.'')->update($data);
        clearnCache(["user.user.model"],true);
    }
    ///用户修改密码
    public function updatepwd($uid,$password)
    {
        $data=array(
            'password'=>$password
        );
        $this->getORM()->where(['Id'=>$uid])->update($data);
        clearnCache(["user.user.model"],true);
    }
    ///实名认证修改
    public function updaterel($uid,$rel_name,$rel_code,$rel_status)
    {
        $data=array(
            'rel_name'=>$rel_name,
            'rel_code'=>$rel_code,
            'rel_status'=>$rel_status
        );
        $this->getORM()->where(['Id'=>$uid])->update($data);
        clearnCache(["user.user.model"],true);
    }
    ///查询指定用户列表
    public function userin($uid)
    {
      $sql="select Id,level,username,telphone,rel_name,rel_code,phone_status,rel_status,name,header_img,user_status,status_time,statu,reg_time,person_region,person_jj,person_zs,person_xl from api_user_user where Id in($uid) and statu=1";
      return  $this->getORM()->queryAll($sql);
    }
    ///查询用户列表
    public function userlist($id,$tel,$name,$regstr_time,$regend_time,$issming,$uid,$page,$phone_status)
    {
        $count=$this->userlistcount($id,$tel,$name,$regstr_time,$regend_time,$issming,$uid,$phone_status);
        $list= cache(["user.user.model.user.userlist",$id,$tel,$name,$regstr_time,$regend_time,$issming,$uid,$page,$phone_status]);
        outputArgs(["num"=>$count,"pagenum"=>40,"page"=>$page]);
        if($list!=null){
            return $list;
        }
        if($uid==null)
        {
          $sql="select Id,level,username,telphone,rel_name,rel_code,phone_status,rel_status,name,header_img,user_status,status_time,statu,reg_time,person_region,person_jj,person_zs,person_xl from api_user_user where statu=1";
        }
        else
        {
         $sql="select Id,level,username,telphone,rel_name,rel_code,phone_status,rel_status,name,header_img,user_status,status_time,statu,reg_time,person_region,person_jj,person_zs,person_xl from api_user_user where Id in($uid) and statu=1";
        }
        if($id!=null){
            $sql.=" and Id like '%".$id."%'";
        }
        if($tel!=null){
            $sql.=" and telphone like '%".$tel."%'";
        }
        if($name!=null){
            $sql.=" and name like '%".$name."%'";
        }
        if($regstr_time!=null){
            $sql.=" and reg_time >'".$regstr_time."'";
        }
        if($regend_time!=null){
            $sql.=" and reg_time <'".$regend_time."'";
        }
        if($issming!=null){
            $sql.=" and rel_status =".$issming."";
        }
        if($phone_status!=null){
            $sql.=" and level =".$phone_status."";
        }
        $sql.=" order by Id DESC";
        if($page>0)
        {
            $pre= ($page-1)*40;
            $next=40;
            $sql.=" limit ".$pre.",".$next."";
        }
        $info=$this->getORM()->queryAll($sql);
        cache(["user.user.model.user.userlist",$id,$tel,$name,$regstr_time,$regend_time,$issming,$uid,$page,$phone_status],$info);
        return $info;
       
    }
    ///条件查询用户列表分页
    public function userlistcount($id,$tel,$name,$regstr_time,$regend_time,$issming,$uid,$phone_status)
    {
    
        $count= cache(["user.user.model.user.userlistcount",$id,$tel,$name,$regstr_time,$regend_time,$issming,$uid,$phone_status]);
         if($count!=null){
            return $count;
        }
        if($uid==null)
        {
            $sql="select count(*) as num from api_user_user where statu=1";
        }
        else
        {
            $sql="select count(*) as num from api_user_user where Id in($uid) and statu=1";
        }
        if($id!=null){
            $sql.=" and Id like '%".$id."%'";
        }
        if($tel!=null){
            $sql.=" and telphone like '%".$tel."%'";
        }
        if($name!=null){
            $sql.=" and name like '%".$name."%'";
        }
        if($regstr_time!=null){
            $sql.=" and reg_time >'".$regstr_time."'";
        }
        if($regend_time!=null){
            $sql.=" and reg_time <'".$regend_time."'";
        }
        if($issming!=null){
            $sql.=" and rel_status =".$issming."";
        }
        if($phone_status!=null){
            $sql.=" and level =".$phone_status."";
        }
        $info=$this->getORM()->queryAll($sql);
        cache(["user.user.model.user.userlistcount",$id,$tel,$name,$regstr_time,$regend_time,$issming,$uid,$phone_status],$info[0]["num"]);
        return $info[0]["num"];
    }
    ///修改昵称
    public function updnickname($uid,$nickname)
    {
        $data=array(
            'name'=>$nickname,
        );
        $this->getORM()->where(['Id'=>$uid])->update($data);
        clearnCache(["user.user.model"],true);
    }
    ///恢复账号
    public function huifustatus($uid)
    {
        $data=array(
            'user_status'=>1,
        );
        $this->getORM()->where(['Id'=>$uid])->update($data);
        clearnCache(["user.user.model"],true);
    }
    //真实姓名模糊查询
    public function selrel_names($rel_name){
        $sql="select * from api_user_user where rel_name like '%".$rel_name."%'";
        return $this->getORM()->queryAll($sql);
    }
    ///修改个人信息
    public function updateperson($id,$header_img,$name,$person_region,$person_jj,$person_zs)
    {
        $data=array(
            'person_region'=>$person_region,
            'person_jj'=>$person_jj,
            'person_zs'=>$person_zs,
            'name'=>$name,
            'header_img'=>$header_img
        );
        $this->getORM()->where(["Id"=>$id])->update($data);
        clearnCache(["user.user.model"],true);
    }
}