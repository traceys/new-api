<?php
namespace User\Model\User;
use PhalApi\Model\NotORMModel as Model;

class Relation extends Model
{
    //添加
    public function add($re_uid,$sh_uid,$type,$on_statu)
    {
      $data=array(
          "re_uid"=>$re_uid,
          "sh_uid"=>$sh_uid,
          "type"=>$type,
          "on_statu"=>$on_statu,
          "add_time"=>date("Y-m-d H:i:s",time()),
          "statu"=>1
      );   
      return $this->getORM()->insert($data);
    }
    ///修改
    public function up($re_uid,$sh_uid,$type,$on_statu)
    {
       return $this->getORM()->where(["re_uid"=>$re_uid,"sh_uid"=>$sh_uid,"type"=>$type])->update(["on_statu"=>$on_statu]);    
    }
    ///删除
    public function del($re_uid,$sh_uid,$type)
    {
       return $this->getORM()->where(["re_uid"=>$re_uid,"sh_uid"=>$sh_uid,"type"=>$type])->update(["statu"=>-1]);
    }
    //修改关系
    public function upre($sh_uid,$new_uid,$type)
    {
        return $this->getORM()->where(["sh_uid"=>$sh_uid,"type"=>$type])->update(["sh_uid"=>$new_uid]);
    }
    //查询机构合作单位
    public function sel($re_uid,$type){
        return $this->getORM()->where(["re_uid"=>$re_uid,"type"=>$type])->fetchAll();
    }
    //查询园所合作单位
    public function sels($sh_uid,$type){
        return $this->getORM()->where(["sh_uid"=>$sh_uid,"type"=>$type])->fetchAll();
    }
}