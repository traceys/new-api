<?php
namespace User\Model\User;
use PhalApi\Model\NotORMModel as Model;

class Certificate extends Model
{

    //创建信息
    public function create($val){
        $model = $this->getORM();
        $data =  $model->insert($val);
        clearnCache(['user.model.certificate'],true);
        return $data;
    }

    //修改信息
    public function updates($val,$id){
        $model = $this->getORM();
        $data =  $model->where(['id'=>$id])->update($val);
        clearnCache(['user.model.certificate'],true);
        return $data;

    }
    //根据用户修改信息
    public function updatesuid($val,$uid){
        $model = $this->getORM();
        $data =  $model->where(['uid'=>$uid])->update($val);
        clearnCache(['user.model.certificate'],true);
        return $data;

    }

    //查找单个信息
    public function find($id){
        $model = $this->getORM();

        return $model->where(['id'=>$id,'statu'=>1])->fetchOne();

    }

    //展示列表
    public function findAll($val,$type='data'){
        $arr = cache(['user.model.certificate.findAll',json_encode($val),$type]);

        if (!$arr){
            $model = $this->getORM();
                $sql = "select * from  (select * from (SELECT a.*,b.name,b.telphone,b.header_img,b.rel_name";

            $sql = $sql." FROM api_user_certificate AS a LEFT JOIN api_user_user AS b ON a.uid=b.Id WHERE a.statu=1";

            if($val['pay_statu'] !== null){
                $pay_statu = $val['pay_statu'];
                $sql = $sql." && a.pay_statu=$pay_statu";

            }
            if ($val['grade'] !== null){
                $grade = $val['grade'];
                $sql = $sql."  && a.grade=$grade";

            }
            if ($val['videoProducing'] !== null ){
                $videoProducing = $val['videoProducing'];
                $sql = $sql."  && a.videoProducing=$videoProducing";
            }
            if ($val['state'] !== null){
                $state = $val['state'];
                $sql = $sql."  && a.state=$state";

            }
            if ($val['on_statu'] !== null){
                $on_statu = $val['on_statu'];
                $sql = $sql."  && a.on_statu=$on_statu";

            }
            if ($val['serach'] !== null){
                $like = $val['like'];
                $serach = $val['serach'];
                $sql = $sql."  && $like LIKE '%$serach%'";
            }
            $sql = $sql." ORDER BY a.id DESC ) as d  GROUP BY uid) as ac order by id desc";
            if ($type == 'data') {
                if ($val['page']) {
                    $num = $val['number'];
                    $page = ($val['page'] - 1) * $num;

                    $sql = $sql ."  LIMIT $page,$num";
                }
            }
            $arr = $model->queryAll($sql, array());
            if ($type == 'num'){
                $arr = count($arr);
            }
            cache(['user.model.certificate.findAll',json_encode($val),$type],$arr);
        }
        return $arr;
    }
//查找指定用户的教师信息
    public function finduser($uid){
        $model = $this->getORM();
      return   $model->where(['uid'=>$uid,'statu'=>1])->fetchOne();
    }
//查找指定用户的单教师信息
    public function finduserAll($arr,$type=true){
        $model = $this->getORM();
        $data = $model->where($arr)->order('id DESC');
        $data = $data->fetchOne();
        return $data;
    }
/**
 * select certificate list
 */
    public function finduserAlls($arr){
        $model = $this->getORM();
        return   $model->where($arr)->fetchAll();

    }

    /**
     * set time
     */
    public function setTime($start,$end){
        $model = $this->getORM();
        return   $model->where('applyTime > ?',$start)->where('applyTime < ?',$end)->fetchAll();
    }

    /**
     * find all end time data
     */
    public function endTimeCertificate(){
        $model = $this->getORM();
        $data = $model->where('stopTime < ?',date("Y-m-d H:i:s",time()))->where(['statu'=>1])->fetchAll();
        return $data;
    }
  ///根据uid查询教师身份
  public function seluserteacher($uid)
  {
      $model = $this->getORM();
      $data = $model->where(['uid'=>$uid,'pay_statu'=>1,'statu'=>1,'on_statu'=>1])->fetchOne();
      return $data;
  }
  //根据uid,tid查询教师身份
  public function selteacher($uid,$tid)
  {
      $model = $this->getORM();
      $data = $model->where(['uid'=>$uid,'cert_tid'=>$tid,'pay_statu'=>1,'statu'=>1,'on_statu'=>1])->fetchOne();
      return $data;
  }
}