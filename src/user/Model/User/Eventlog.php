<?php
namespace User\Model\User;
use PhalApi\Model\NotORMModel as Model;

class Eventlog extends Model
{
    
    //添加
    public function add($event,$on_statu)
    {
        $data=array(
            'event'=>$event,
            'on_statu'=>$on_statu,
            'time'=>date('Y-m-d H:i:s',time())
        );
       return $this->getORM()->insert($data);
    }
}