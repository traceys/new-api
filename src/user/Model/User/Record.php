<?php
namespace User\Model\User;
use PhalApi\Model\NotORMModel as Model;

class Record extends Model
{
    //添加
    public function add($cs_name,$cs_jc,$cs_adress,$cs_gps,$cs_xm,$cs_phone,$cs_xc,$cs_jj,$cs_tid,$cs_uid,$cs_id,$cs_type){
        $data=array(
            'cs_name'=>$cs_name,
            'cs_js'=>$cs_jc,
            'cs_adress'=>$cs_adress,
            'cs_gps'=>$cs_gps,
            'cs_xm'=>$cs_xm,
            'cs_phone'=>$cs_phone,
            'cs_xc'=>$cs_xc,
            'cs_tid'=>$cs_tid,
            'cs_uid'=>$cs_uid,
            'cs_id'=>$cs_id,
            'cs_type'=>$cs_type,
            'cs_time'=>date('Y-m-d H:i:s',time()),
            'statu'=>1
        );
        return $this->getORM()->insert($data);
    }
}