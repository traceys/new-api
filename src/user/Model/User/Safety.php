<?php
namespace User\Model\User;
use PhalApi\Model\NotORMModel as Model;

class Safety extends Model
{
    //添加
    public function add($safety_uid,$safety_code,$safety_open)
    {
        $data=array(
            'safety_uid'=>$safety_uid,
            'safety_code'=>$safety_code,
            'safety_open'=>$safety_open,
            'statu'=>1
        );
        return $this->getORM()->insert($data);
    }
    ///查询
    public function select($safety_uid)
    {
       return $this->getORM()->where(["safety_uid"=>$safety_uid])->fetchOne();
    }
    ///修改安全码
    public function upcode($safety_uid,$safety_code)
    {
        $data=array(
            'safety_code'=>$safety_code
        );
        return $this->getORM()->where(["safety_uid"=>$safety_uid])->update($data);
    }
    ///是否开启
    public function upopen($safety_uid,$safety_open)
    {
        $data=array(
            'safety_open'=>$safety_open
        );
        return $this->getORM()->where(["safety_uid"=>$safety_uid])->update($data);
    }
    ///删除 
    public function upstatu($safety_uid)
    {
        $data=array(
            'statu'=>-1
        );
        return $this->getORM()->where(["statu"=>$safety_uid])->update($data);
    }
}