<?php
namespace User\Model\User;
use PhalApi\Model\NotORMModel as Model;

class Promo extends Model
{

    ///添加
    public function add($promo_tid,$promo_code,$promo_type){
        $data=array(
            'promo_tid'=>$promo_tid,
            'promo_code'=>$promo_code,
            'promo_type'=>$promo_type,
            'promo_jh'=>2,
            'statu'=>1
        );
       return $this->getORM()->insert($data);
    }
    ///激活
    public function jihou($promo_tid,$promo_type)
    {
       return $this->getORM()->where(["promo_tid"=>$promo_tid,"promo_type"=>$promo_type])->update(["promo_jh"=>1]);
    }
    ///推广码查询
    public function seltgm($tgm)
    {
       return $this->getORM()->where(["promo_code"=>$tgm])->fetchOne();    
    }
    ///删除
    public function del($id){
        
       return  $this->getORM()->where(["id"=>$id])->update(["statu"=>-1]);
    }
}