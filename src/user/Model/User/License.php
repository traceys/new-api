<?php
namespace User\Model\User;
use PhalApi\Model\NotORMModel as Model;

class License extends Model
{
    ///微信查询unionid
    public function Seluid($unionid,$type)
    {
       $user=$this->getORM()->select('*')->where(['code'=>$unionid,'type'=>$type,"statu"=>1])->fetchOne();
       return $user; 
    }
    ///查询
    public function Seluninonid($uid,$type)
    {
        $user=$this->getORM()->select('*')->where(['uid'=>$uid,"statu"=>1,'type'=>$type])->fetchOne();
        return $user;
    }
    ///查询
    public function Seltype($uid)
    {
        $user=$this->getORM()->select('*')->where(['uid'=>$uid,"statu"=>1])->fetchAll();
        return $user;
    }
    ///添加数据
    public function Addlicense($uid,$type,$code,$statu,$time){
        $data=array(
          'uid'=>$uid,
          'type'=>$type,
          'code'=>$code,
          'statu'=>$statu,
          'time'=>$time
        );
        $this->getORM()->insert($data);
    }
    ///修改uid
    public function upduid($uid,$nuid){
        $this->getORM()->where(["uid"=>$uid])->update(["uid"=>$nuid]);
    }
    ///
    public function updcode($code){
        $data=array(
            'statu'=>-1
        );
        $this->getORM()->where(['code'=>$code])->update($data);
    }
}