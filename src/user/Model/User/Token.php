<?php
namespace User\Model\User;
use PhalApi\Model\NotORMModel as Model;

class Token extends Model
{
    ///token查询uid信息
    public function seluid($token){
        $uid=$this->getORM()->select('*')->where(["token"=>$token,'statu'=>1])->fetchOne();
        return $uid;
    }
    //生成token
    public function Settoken($token,$uid,$time,$type)
    {
      $data=array(
          'token'=>$token,
          'uid'=>$uid,
          'time'=>$time,
          'statu'=>1,
          'type'=>$type
      );
     $sta=$this->getORM()->insert($data);
     if($sta!=null){
         return true;
     }
     else{
         return false;
     }
    }
    ///修改statu
    public function upduid($uid,$type){
        $data=array(
            'statu'=>-1
        );
        $this->getORM()->where(['uid'=>$uid,'type'=>$type])->update($data);
    }
    ///修改statu
    public function upduidone($uid){
        $data=array(
            'statu'=>-1
        );
        $this->getORM()->where(['uid'=>$uid])->update($data);
    }
    ///查询token
    public function seltokenone($uid)
    {
        $token=$this->getORM()->select('*')->where(["uid"=>$uid,'statu'=>1])->fetchOne();
        return $token;
    }
    ///查询token
    public function seltoken($uid,$type)
    {
        $token=$this->getORM()->select('*')->where(["uid"=>$uid,'statu'=>1,'type'=>$type])->fetchOne();
        return $token;
    }
}