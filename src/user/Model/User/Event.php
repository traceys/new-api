<?php
namespace User\Model\User;
use PhalApi\Model\NotORMModel as Model;

class Event extends Model
{
    
    //添加
    public function add($event_zhname,$event_name,$event_value)
    {
        $data=array(
            'event_zhname'=>$event_zhname,
            'event_name'=>$event_name,
            'event_value'=>$event_value,
            'time'=>date('Y-m-d H:i:s',time())
        );
       return $this->getORM()->insert($data);
    }
    ///查询
    public function select($event_name)
    {
        if($event_name!=null)
        {
           return $this->getORM()->where(["event_name"=>$event_name])->fetchOne();
        }
        else
        {
           return $this->getORM()->fetchAll();
        }
    }
    ///修改
    public function upd($event_name,$event_value)
    {
        $data=array(
            'event_value'=>$event_value
        );
        return $this->getORM()->where(["event_name"=>$event_name])->update($data);
    }
}