<?php
namespace User\Model\User;
use PhalApi\Model\NotORMModel as Model;

class Mechanism extends Model
{
    ///机构认证
    public function addauth($name,$username,$user_phone,$img,$contract_num,$contract_doc,$auth_dir,$province,$city,$county)
    {
        $data=array(
            'name'=>$name,
            'username'=>$username,
            'user_phone'=>$user_phone,
            'img'=>$img,
            'rec_status'=>0,
            'time'=>date('Y-m-d H:i:s',time()),
            'statu'=>1,
            'contract_num'=>$contract_num,
            'contract_doc'=>$contract_doc,
            'auth_dir'=>$auth_dir,
            'province'=>$province,
            'city'=>$city,
            'use_state'=>0,
            'county'=>$county,
            'region'=>$province.$city.$county
        );
           $this->getORM()->insert($data);
           clearnCache(["user.mechanism。model"],true);
    }
    ///机构认证
    public function updateauth($id,$name,$username,$user_phone,$contract_num,$contract_doc,$auth_dir,$pro,$city,$county)
    {
        $data=array(
            'name'=>$name,
            'username'=>$username,
            'user_phone'=>$user_phone,
            'time'=>date('Y-m-d H:i:s',time()),
            'contract_num'=>$contract_num,
            'contract_doc'=>$contract_doc,
            'auth_dir'=>$auth_dir,
            'province'=>$pro,
            'city'=>$city,
            'county'=>$county,
            'region'=>$pro.$city.$county
        );
         $this->getORM()->where(["id"=>$id])->update($data);
        clearnCache(["user.mechanism。model"],true);
    }
    ///机构信息查询
    public function selecteuser($uid)
    {
        return  $this->getORM()->where(["uid"=>$uid,'statu'=>1])->fetchOne();
    }
    ///生成邀请码
    public function updatecode($id,$data)
    {
         $this->getORM()->where(["id"=>$id])->update($data);
        clearnCache(["user.mechanism。model"],true);
    }
    ///机构认证状态查询
    public function selstatu($type,$order,$page)
    {
        $count=$this->selstatucount($type, $order);
        $list=cache(["user.mechanism。model.user.mechanism.selstatu",$type,$order,$page]);
        outputArgs(["num"=>$count,"pagenum"=>40,"page"=>$page]);
        if($list!=null){
            return $list;
        }
        $sql="select * from api_user_mechanism where rec_status=".$type." and statu=1 order by ".$order." DESC ";
        if($page>0)
        {
         $pre= ($page-1)*40;
         $next=$page*40;
         $sql.=" limit ".$pre.",".$next."";
        }
        $info=$this->getORM()->queryAll($sql);
        cache(["user.mechanism。model.user.mechanism.selstatu",$type,$order,$page],$info);
        return $info;
    }
    ///机构认证状态查询
    public function selstatucount($type,$order)
    {
        $count= cache(["user.mechanism。model.user.mechanism.selstatucount",$type,$order]);
        if($count!=null){
            return $count;
        }
        $sql="select count(*) as num from api_user_mechanism where rec_status=".$type." and statu=1 order by ".$order." DESC ";
        $info=$this->getORM()->queryAll($sql);
        cache(["user.mechanism。model.user.mechanism.selstatucount",$type,$order],$info[0]["num"]);
        return $info[0]["num"];
    }
    ///机构认证信息
    public function selauth($id)
    {
        return  $this->getORM()->where(["id"=>$id,"statu"=>1])->fetchOne();
    }
    ///机构认证信息审核
    public function updauth($id,$status)
    {
        $this->getORM()->where(["id"=>$id])->update(["rec_status"=>$status]);
        clearnCache(["user.mechanism。model"],true);
    }
    ///添加团队ID
    public function updtid($uid,$tid)
    {
        $this->getORM()->where(["uid"=>$uid])->update(["tid"=>$tid]);
        clearnCache(["user.mechanism。model"],true);
    }
    ///添加团队ID
    public function updtidone($id,$tid)
    {
        $this->getORM()->where(["id"=>$id])->update(["tid"=>$tid]);
        clearnCache(["user.mechanism。model"],true);
    }
    ///后台条条件查询
    public function melist($rec_status,$star_time,$end_time,$page,$name,$username,$code,$use_state,$uid,$iscode,$user_phone)
    {
        $count=$this->melistcount($rec_status,$star_time,$end_time,$name,$username,$code,$use_state,$uid,$iscode,$user_phone);
        $list=cache(["user.mechanism。model.user.mechanism.melist",$rec_status,$star_time,$end_time,$page,$name,$username,$code,$use_state,$uid,$iscode,$user_phone]);
        outputArgs(["num"=>$count,"pagenum"=>40,"page"=>$page]);
        if($list!=null){
            return $list;
        }
        $sql="select m.*,u.`name` as proname from api_user_mechanism as m left join api_user_user as u on m.prouid=u.Id where m.statu=1";
        if($rec_status!=null){
            $sql.=" and m.rec_status=".$rec_status."";
        }
        if($star_time!=null){
            $sql.=" and m.end_time >'".$star_time."'";
        }
        if($end_time!=null){
            $sql.=" and m.end_time <'".$end_time."'";
        }
        if($name!=null){
            $sql.=" and  m.name like '%".$name."%'";
        }
        if($username!=null){
            $sql.=" and  m.username like '%".$username."%'";
        }
        if($code!=null){
            $sql.=" and  m.code like '%".$code."%'";
        } 
        if($use_state!="use"){
            $sql.=" and m.use_state=".$use_state."";
        }
        if($use_state===1){
            $sql.=" and m.uid!=0";
        }
        if($uid!=null){
            $sql.=" and m.uid like '%".$uid."%'";
        }
        if($iscode===0){
            $sql.=" and m.code !=''";
        }
        if($user_phone!=null){
            $sql.=" and m.user_phone like '%".$user_phone."%'";
        }
        $sql.=" order by m.id DESC";
        if($page>0)
        {
            $pre= ($page-1)*40;
            $next=40;
            $sql.=" limit ".$pre.",".$next."";
        }
        $info=$this->getORM()->queryAll($sql);
        cache(["user.mechanism。model.user.mechanism.melist",$rec_status,$star_time,$end_time,$page,$name,$username,$code,$use_state,$uid,$iscode,$user_phone],$info);
        return $info;
    }
    ///后台条条件查询
    public function melistcount($rec_status,$star_time,$end_time,$name,$username,$code,$use_state,$uid,$iscode,$user_phone)
    {
        $count= cache(["user.mechanism。model.user.mechanism.melistcount",$rec_status,$star_time,$end_time,$name,$username,$code,$use_state,$uid,$iscode,$user_phone]);
        if($count!=null){
            return $count;
        }
        $sql="select count(*) as num from api_user_mechanism as m left join api_user_user as u on m.prouid=u.Id where m.statu=1";
        if($rec_status!=null){
            $sql.=" and m.rec_status=".$rec_status."";
        }
        if($star_time!=null){
            $sql.=" and m.end_time >'".$star_time."'";
        }
        if($end_time!=null){
            $sql.=" and m.end_time <'".$end_time."'";
        }
        if($name!=null){
            $sql.=" and  m.name like '%".$name."%'";
        }
        if($username!=null){
            $sql.=" and  m.username like '%".$username."%'";
        }
        if($code!=null){
            $sql.=" and  m.code like '%".$code."%'";
        }
        if($use_state!="use"){
            $sql.=" and m.use_state=".$use_state."";
        }
        if($use_state===1){
            $sql.=" and m.uid!=0";
        }
        if($uid!=null){
            $sql.=" and m.uid like '%".$uid."%'";
        }
        if($iscode==0){
            $sql.=" and m.code !=''";
        }
        if($user_phone!=null){
            $sql.=" and m.user_phone like '%".$user_phone."%'";
        }
        $info=$this->getORM()->queryAll($sql);
        cache(["user.mechanism。model.user.mechanism.melistcount",$rec_status,$star_time,$end_time,$name,$username,$code,$use_state,$uid,$iscode,$user_phone],$info[0]["num"]);
        return $info[0]["num"];
    }
    //前台通过机构邀请码查询
    public function selcode($code)
    {
        return  $this->getORM()->where(["code"=>$code,"statu"=>1])->fetchOne();
    }
    ///查询机构认证状态
    public function selstatus($uid)
    {
        return $this->getORM()->where(["uid"=>$uid,"statu"=>1])->fetchOne();
    }
    ///查询全部机构
    public function selall(){
        $sql="select * from api_user_mechanism where statu=1";
        return  $this->getORM()->queryAll($sql);
    }
    ///邀请码使用状态
    public function upduser($id,$use_state){
         $this->getORM()->where(["id"=>$id])->update(["use_state"=>$use_state]);
        clearnCache(["user.mechanism。model"],true);
    }
    ///机构审核
    public function updsh($id,$use_state,$status,$beizhu,$cluid){
        
        $this->getORM()->where(["id"=>$id])->update(["use_state"=>$use_state,"rec_status"=>$status,"beizhu"=>$beizhu,"prouid"=>$cluid]);
        clearnCache(["user.mechanism。model"],true);
    }
    ///禁用邀请码
    public function jycode($id,$on_statu)
    {
        $this->getORM()->where(["id"=>$id])->update(["on_statu"=>$on_statu]);
        clearnCache(["user.mechanism。model"],true);
    }
    ///查询分会下面的机构
    public function seladress($data,$name,$telphone,$page,$rec_status,$end_time)
    {
        $count=$this->seladresscount($data, $name, $telphone, $rec_status, $end_time);
        $list= cache(["user.mechanism。model.user.mechanism.seladress",$data,$name,$telphone,$page,$rec_status,$end_time]);
        outputArgs(["num"=>$count,"pagenum"=>8,"page"=>$page]);
        if($list!=null){
            return $list;
        }
        $sql=" select * from api_user_mechanism where region in(".$data.")";
        if($name!="")
        {
            $sql.=" and username like '%".$name."%'";
        }
        if($telphone!="")
        {
           $sql.=" and user_phone like '%".$telphone."%'";    
        }
        if($rec_status!="")
        {
            $sql.=" and rec_status=".$rec_status."";
        }
        if($end_time!="")
        {
            $sql.=" and end_time < '".date('Y-m-d H:i:s',time())."'";
        }
        $sql.=" order by time DESC";
        if($page>0)
        {
            $pre= ($page-1)*8;
            $next=8;
            $sql.=" limit ".$pre.",".$next."";
        }
        $info=$this->getORM()->queryAll($sql);
        cache(["user.mechanism。model.user.mechanism.seladress",$data,$name,$telphone,$page,$rec_status,$end_time],$info);
        return $info;
    }
    ///查询分会下面的机构
    public function seladresscount($data,$name,$telphone,$rec_status,$end_time)
    {
        $count= cache(["user.mechanism。model.user.mechanism.seladresscount",$data,$name,$telphone,$rec_status,$end_time]);
        if($count!=null){
            return $count;
        }
        $sql=" select count(*) as num from api_user_mechanism where region in(".$data.")";
        if($name!="")
        {
            $sql.=" and username like '%".$name."%'";
        }
        if($telphone!="")
        {
            $sql.=" and user_phone like '%".$telphone."%'";
        }
        if($rec_status!="")
        {
            $sql.=" and rec_status=".$rec_status."";
        }
        if($end_time!="")
        {
            $sql.=" and end_time < '".date('Y-m-d H:i:s',time())."'";
        }
        $info=$this->getORM()->queryAll($sql);
        cache(["user.mechanism。model.user.mechanism.seladresscount",$data,$name,$telphone,$rec_status,$end_time],$info[0]["num"]);
        return $info[0]["num"];
    }
    ///查询所有机构信息
    public function selmecall($rec_status,$star_time,$end_time,$name,$username,$code,$use_state,$uid,$iscode,$user_phone)
    {
        $sql="select m.*,u.`name` as proname from api_user_mechanism as m left join api_user_user as u on m.prouid=u.Id where m.statu=1";
        if($rec_status!=null){
            $sql.=" and m.rec_status=".$rec_status."";
        }
        if($star_time!=null){
            $sql.=" and m.end_time >'".$star_time."'";
        }
        if($end_time!=null){
            $sql.=" and m.end_time <'".$end_time."'";
        }
        if($name!=null){
            $sql.=" and  m.name like '%".$name."%'";
        }
        if($username!=null){
            $sql.=" and  m.username like '%".$username."%'";
        }
        if($code!=null){
            $sql.=" and  m.code like '%".$code."%'";
        } 
        if($use_state!="use"){
            $sql.=" and m.use_state=".$use_state."";
        }
        if($use_state===1){
            $sql.=" and m.uid!=0";
        }
        if($uid!=null){
            $sql.=" and m.uid like '%".$uid."%'";
        }
        if($iscode===0){
            $sql.=" and m.code !=''";
        }
        if($user_phone!=null){
            $sql.=" and m.user_phone like '%".$user_phone."%'";
        }
        $sql.=" order by m.time DESC";
        return $this->getORM()->queryAll($sql);;
    }
    ///uid查询团队机构信息
    public function teammech($tid)
    {
        $sql="select * from api_user_mechanism where uid='".$tid."'";
        return $this->getORM()->queryAll($sql);
    }
    ///tel查询团队机构
    public function telmech($tel)
    {
        $sql="select * from api_user_mechanism where user_phone='".$tel."'";
        return $this->getORM()->queryAll($sql);
    }
    ///机构认证
    public function addimport($name,$username,$user_phone,$contract_num,$auth_dir,$province,$city,$county)
    {
        $data=array(
            'name'=>$name,
            'username'=>$username,
            'user_phone'=>$user_phone,
            'rec_status'=>0,
            'time'=>date('Y-m-d H:i:s',time()),
            'statu'=>1,
            'contract_num'=>$contract_num,
            'auth_dir'=>$auth_dir,
            'province'=>$province,
            'city'=>$city,
            'use_state'=>0,
            'county'=>$county,
            'region'=>$province.$city.$county
        );
        $this->getORM()->insert($data);
        $id=$this->getORM()->insert_id();
        clearnCache(["user.mechanism。model"],true);
        return $id;
    }
    ///tid查询团队机构信息
    public function teamtid($tid)
    {
        $sql="select * from api_user_mechanism where tid='".$tid."'";
        return $this->getORM()->queryAll($sql);
    }
}