<?php
namespace User\Model\User;
use PhalApi\Model\NotORMModel as Model;

class Company extends Model
{
    
    //添加
    public function add($company_type,$company_name,$company_jc,$company_gps,$pro,$city,$reigon,$company_adress,$company_my,$company_user,$company_phone,$company_jj,$company_zj,$company_tid,$company_uid,$company_pwd,$company_tgm)
    {
//       $sql="insert into api_user_company(company_type,company_name,company_jc,company_gps,pro,city,region,company_adress,company_my,company_user,company_phone,company_jj,company_zj,company_tid,company_uid,company_pwd,company_tgm)";
//       $sql.=" values('".$company_type."','".$company_name."','".$company_jc."',GeomFromText('".$company_gps."'),'".$pro."','".$city."','".$reigon."','".$company_adress."','".$company_my."','".$company_user."','".$company_phone."','".$company_jj."','".$company_zj."','".$company_tid."','".$company_uid."','".$company_pwd."','".$company_tgm."')";
         $data=array(
          'company_type'=>$company_type,
          'company_name'=>$company_name,
          'company_jc'=>$company_jc,
          'company_gps'=>$company_gps,
          'pro'=>$pro,
          'city'=>$city,
          'region'=>$reigon,
          'company_adress'=>$company_adress,
          'company_my'=>$company_my,
          'company_user'=>$company_user,
          'company_phone'=>$company_phone,
          'company_jj'=>$company_jj,
          'company_zj'=>$company_zj,
          'company_tid'=>$company_tid,
          'company_uid'=>$company_uid,
          'company_pwd'=>$company_pwd,
          'company_tgm'=>$company_tgm,
          'company_statu'=>0,
          'statu'=>1,
          'company_createtime'=>date('Y-m-d H:i:s',time())
        );
         $this->getORM()->insert($data);
    }
    ///查询
    public function seluid($tid)
    {
      return $this->getORM()->where(["company_tid"=>$tid])->fetchOne();    
    }
    ///设置管理密码
    public function setpwd($tid,$pwd){
        $this->getORM()->where(["company_tid"=>$tid])->update(["company_pwd"=>$pwd]);
    }
    //修改
    public function up($tid,$data)
    {
        $this->getORM()->where(["tid"=>$tid])->update($data);
    }
    //删除
    public function del($id)
    {
        $data=array(
            'statu'=>-1
        );
        $this->getORM()->where(["id"=>$id])->update($data);
    }
    ///查询机构名称是否存在
    public function selname($company_name)
    {
       return $this->getORM()->where(["company_name"=>$company_name])->fetchAll();
    }
}