<?php
namespace User\Model\User;
use PhalApi\Model\NotORMModel as Model;

class Realname extends Model
{
    
    ///实名认证
    public function addreal($uid,$name,$code,$head_img,$back_img,$rec_status)
    {
       $data=array(
           'uid'=>$uid,
           'name'=>$name,
           'code'=>$code,
           'head_img'=>$head_img,
           'back_img'=>$back_img,
           'rec_status'=>$rec_status,
           'time'=>date('Y-m-d H:i:s',time()),
           'statu'=>1
       );
       $this->getORM()->insert($data);
      clearnCache(["user.mechanism。model"],true);
    }
    ///实名认证状态查询
    public function selstatu($type,$order,$page)
    {
        $count=$this->selstatucount($type, $order);
        $list=cache(["user.realname.model.user.realname.selstatu",$type,$order,$page]);
        outputArgs(["num"=>$count,"pagenum"=>40,"page"=>$page]);
        if($list!=null){
            return $list;
        }
        $sql="select * from api_user_realname where rec_status=".$type." and statu=1 order by ".$order." DESC ";
        if($page>0)
        {
        $pre= ($page-1)*40;
        $next=40;
        $sql.=" limit ".$pre.",".$next."";
        }
        $info=$this->getORM()->queryAll($sql);
        cache(["user.realname.model.user.realname.selstatu",$type,$order,$page],$info);
        return $info;
    }
    ///实名认证状态查询
    public function selstatucount($type,$order)
    {
        $count=cache(["user.realname.model.user.realname.selstatucount",$type,$order]);
        if($count!=null){
            return $count;
        }
        $sql="select count(*) as num from api_user_realname where rec_status=".$type." and statu=1 order by ".$order." DESC ";
        $info=$this->getORM()->queryAll($sql);
        cache(["user.realname.model.user.realname.selstatucount",$type,$order],$info[0]["num"]);
        return $info[0]["num"];
    }
    ///实名认证信息
    public function selreal($id)
    {
       return  $this->getORM()->where(["id"=>$id,"statu"=>1])->fetchOne();
    }
    ///实名认证信息审核
    public function updreal($id,$status)
    {
        $this->getORM()->where(["id"=>$id])->update(["rec_status"=>$status]);
        clearnCache(["user.realname.model"],true);
        
    }
}