<?php
namespace User\Domain;
use User\Model\User\Relation as Mrelation;
class Relation
{
    //添加
    public function add($re_uid,$sh_uid,$type,$on_statu)
    {
       $rel=new Mrelation();
       return $rel->add($re_uid, $sh_uid, $type, $on_statu);
    }
    ///修改
    public function up($re_uid,$sh_uid,$type,$on_statu)
    {
        $rel=new Mrelation();
        return $rel->up($re_uid, $sh_uid, $type, $on_statu);
    }
    ///删除
    public function del($re_uid,$sh_uid,$type)
    {
        $rel=new Mrelation();
        return $rel->del($re_uid, $sh_uid, $type);
    }
    //修改关系
    public function upre($sh_uid,$new_uid,$type)
    {
        $rel=new Mrelation();
        return $rel->upre($sh_uid, $new_uid, $type);
    }
    //查询机构合作单位
    public function sel($re_uid,$type)
    {
       $rel=new Mrelation();
       return $rel->sel($re_uid, $type);
    }
    //查询园所合作单位
    public function sels($sh_uid,$type)
    {
        $rel=new Mrelation();
        return $rel->sels($sh_uid, $type);
    }
}