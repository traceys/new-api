<?php
namespace User\Domain;
require 'Exit/hanzi/Hanzi.php';
require 'Exit/Mda/Connect2.1/API/qqConnectAPI.php';
include_once("Exit/Unionid/wxBizDataCrypt.php");
include_once('Exit/Mda/libweibo-master/config.php');
include_once('Exit/Mda/libweibo-master/saetv2.ex.class.php');

class Funuser{
    //实名认证
    function myname($name,$myid){
    
        $host = "http://idcard.market.alicloudapi.com";
        $path = "/lianzhuo/idcard";
        $method = "GET";
        $appcode = "958d68720db8421e8df4230c4d264e26";
        $headers = array();
        array_push($headers, "Authorization:APPCODE " . $appcode);
        $querys = 'cardno='.$myid.'&name='.$name.'';
        $bodys = "";
        $url = $host . $path . "?" . $querys;
    
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_FAILONERROR, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, true);
        if (1 == strpos("$".$host, "https://"))
        {
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        }
        $val =  curl_exec($curl);
        $val = '{'.substr($val,strpos($val,'{')+1);
    
        $res = json_decode($val,true);
        curl_close($curl);
        if ($res['resp']['code'] == 0){
            return true;
        }else{
            return false;
        }
    }
    ///获取session_key
    function getsession_key($code)
    {
        $appid='wxce85d94e890bf650';
        $secret='bd1e940fa1ae4561f0838bc685ed71a1';
        $url='https://api.weixin.qq.com/sns/jscode2session?appid='.$appid.'&secret='.$secret.'&js_code='.$code.'&grant_type=authorization_code';
        $ch=curl_init($url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch,CURLOPT_BINARYTRANSFER,true);
        $output=curl_exec($ch);
        curl_close($ch);
        return $output;
    }
    ///解析unionid
    function unionid($sessinokey,$encypte,$iv){
    
        $appid = 'wxce85d94e890bf650';
        $sessionKey = $sessinokey;
    
        $encryptedData=$encypte;
        $pc = new \WXBizDataCrypt($appid, $sessionKey);
        $errCode = $pc->decryptData($encryptedData, $iv, $data );
    
        if ($errCode == 0) {
            return $data;
        } else {
            return false;
        }
    }
    //微信第三方解析接口
    function weixin_open($code){
        $appid = 'wx4e7a68bd98e080a0';
        $secret = '08088e91703fad7c9245240bf67bb002';
        $url ='https://api.weixin.qq.com/sns/oauth2/access_token?appid='.$appid.'&secret='.$secret.'&code='.$code.'&grant_type=authorization_code';
        $ch=curl_init($url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch,CURLOPT_BINARYTRANSFER,true);
        $output=curl_exec($ch);
        curl_close($ch);
        return $output;
    }
    ///qq发起登录
    function qq_login()
    {
        $qc = new \QC();
        return $qc->qq_login();
    }
    //QQ回调接收参数
    function qq_callback(){
        $qc = new \QC();
        return $qc->qq_callback();
    }
    //新浪微博登陆
    function wb_login()
    {
        $o = new \SaeTOAuthV2( WB_AKEY , WB_SKEY );
        $code_url = $o->getAuthorizeURL( WB_CALLBACK_URL );
        return $code_url;
    }
    //登陆后获取微博的基本信息
    function wb_callback($code)
    {
        $o = new \SaeTOAuthV2( WB_AKEY , WB_SKEY );
        $keys = array();
        $keys['code'] = $code;
        $keys['redirect_uri'] = WB_CALLBACK_URL;
        $token = $o->getAccessToken( 'code', $keys ) ;
        return $token;
    }
    ///添加文字水印
    function textwatermark($level,$name,$pyname,$idnum,$cernum,$pushtime,$yxtime)
    {
        if($level==1)
        {
            $dst_path = 'Exit/mark/1.jpg';
            $left=950;
        }
        if($level==2)
        {
            $dst_path = 'Exit/mark/2.jpg';
            $left=900;
        }
        if($level==3)
        {
            $dst_path = 'Exit/mark/3.jpg';
            $left=950;
        }
        //创建图片的实例
        $dst = imagecreatefromstring(file_get_contents($dst_path));
    
        //打上文字
        $font1 = 'Exit/mark/QisiKAITI.ttf';//字体
        $font2 = 'Exit/mark/Annabelle.ttf';//字体
        $font3 = 'Exit/mark/tempofont.ttf';//字体
    
        $black = imagecolorallocate($dst, 0x00, 0x00, 0x00);//字体颜色
        imagefttext($dst, 90, 0, 2100,900, $black, $font1, $name);
        imagefttext($dst, 50, 0, 2050,1000, $black, $font2,$pyname);
        imagefttext($dst, 35, 0, $left,780, $black, $font3, $idnum);
    
        if($level==1)
        {
            imagefttext($dst, 35, 0, $left,860, $black, $font1, '技術初级');
            imagefttext($dst, 35, 0, 1150,860, $black, $font1, '(');
            imagefttext($dst, 35, 0, 1170,860, $black, $font2, 'Primary Technology');
            imagefttext($dst, 35, 0, 1580,860, $black, $font1, ')');
        }
        if($level==2)
        {
            imagefttext($dst, 35, 0, $left,860, $black, $font1, '技術中级');
            imagefttext($dst, 35, 0, 1120,860, $black, $font1, '(');
            imagefttext($dst, 35, 0, 1140,860, $black, $font2, 'Intermediate Technology');
            imagefttext($dst, 35, 0, 1625,860, $black, $font1, ')');
        }
        if($level==3)
        {
            imagefttext($dst, 35, 0, $left,860, $black, $font1, '技術高级');
            imagefttext($dst, 35, 0, 1150,860, $black, $font1, '(');
            imagefttext($dst, 35, 0, 1170,860, $black, $font2, 'Advanced Technology');
            imagefttext($dst, 35, 0, 1613,860, $black, $font1, ')');
        }
    
        imagefttext($dst, 35, 0, $left,940 ,$black, $font3, $cernum);
        imagefttext($dst, 35, 0, $left,1010 ,$black, $font3, $pushtime);
        imagefttext($dst, 35, 0, $left,1080 ,$black, $font3, $yxtime);
        //输出图片
        list($dst_w, $dst_h, $dst_type) = getimagesize($dst_path);
        $url=getRandomString(16);
        switch ($dst_type) {
            case 1://GIF
                $Route="Exit/cert/".$url.".gif";
                header('Content-Type: image/gif');
                imagegif($dst,$Route);
    
                break;
            case 2://JPG
                $Route="Exit/cert/".$url.".jpg";
                header('Content-Type: image/jpeg');
                imagejpeg($dst,$Route);
                break;
            case 3://PNG
                $Route="Exit/cert/".$url.".png";
                header('Content-Type: image/png');
                imagepng($dst,$Route);
                break;
            default:
                break;
        }
        imagedestroy($dst);
        return $Route;
    }
    ///生成缩略图
    function thimg($level,$name,$pyname,$idnum,$cernum,$pushtime,$yxtime,$photo,$islizhi)
    {
        $route=$this->textwatermark($level,$name,$pyname,$idnum,$cernum,$pushtime,$yxtime);
        $w =409;
        $h =570;
        $filename = "Exit/cert/".getRandomString(16).".jpg";//输出到的位置
        $loc_img=$photo;//寸照
        $this->image_resize($loc_img,$filename, $w, $h);
    
        $dst_path =$route;
        $src_path = $filename;
        $zhang='Exit/mark/shui.png';
        if($islizhi==false)
        {
            $lizhi="Exit/mark/lizhi.png";
        }
        //创建图片的实例
        $dst = imagecreatefromstring(file_get_contents($dst_path));
        $src = imagecreatefromstring(file_get_contents($src_path));
        $yz = imagecreatefromstring(file_get_contents($zhang));
        if($islizhi==false)
        {
            $liz= imagecreatefromstring(file_get_contents($lizhi));
        }
        //获取水印图片的宽高
        list($src_w, $src_h) = getimagesize($src_path);
        list($yz_w, $yz_h) = getimagesize($zhang);
        if($islizhi==false)
        {
            list($li_w, $li_h) = getimagesize($lizhi);
        }
        //将水印图片复制到目标图片上，最后个参数50是设置透明度，这里实现半透明效果
        imagecopymerge($dst, $src, 2725, 590, 0, 0, $src_w, $src_h, 100);
        //如果水印图片本身带透明色，则使用imagecopy方法
        imagecopy($dst, $yz, 2905, 1000, 0, 0, $yz_w, $yz_h);
        if($islizhi==false)
        {
            imagecopy($dst, $liz, 1200, 900, 0, 0, $li_w, $li_h);
        }
        //输出图片
        list($dst_w, $dst_h, $dst_type) = getimagesize($dst_path);
        switch ($dst_type) {
            case 1://GIF
                header('Content-Type: image/gif');
                imagegif($dst,$route);
                break;
            case 2://JPG
                header('Content-Type: image/jpeg');
                imagejpeg($dst,$route);
                break;
            case 3://PNG
                header('Content-Type: image/png');
                imagepng($dst,$route);
                break;
            default:
                break;
        }
        unlink($filename);
        imagedestroy($dst);
        imagedestroy($src);
        return "http://test.yusj.vip/".$route;
    }
    
    // 按指定大小生成缩略图，而且不变形，缩略图函数
    function image_resize($f, $t, $tw, $th){
        $temp = array(1=>'gif', 2=>'jpeg', 3=>'png');
        list($fw, $fh, $tmp) = getimagesize($f);
        if(!$temp[$tmp]){
            return false;
        }
        $tmp = $temp[$tmp];
        $infunc = "imagecreatefrom$tmp";
        $outfunc = "image$tmp";
    
        $fimg = $infunc($f);
        //      $fw = 10;
        //      $fh = 4;
        //      $tw = 4;
        //      $th = 2;
        // 把图片铺满要缩放的区域
        if($fw/$tw > $fh/$th){
            $zh = $th;
            $zw = $zh*($fw/$fh);
            $_zw = ($zw-$tw)/2;
            $_zh=0;
        }else{
            $zw = $tw;
            $zh = $zw*($fh/$fw);
            $_zh = ($zh-$th)/2;
            $_zw=0;
        }
        //        echo $zw."<br>";
        //        echo $zh."<br>";
        //        echo $_zw."<br>";
        //        echo $_zh."<br>";
        //        exit;
        $zimg = imagecreatetruecolor($zw, $zh);
        // 先把图像放满区域
        imagecopyresampled($zimg, $fimg, 0,0, 0,0, $zw,$zh, $fw,$fh);
    
        // 再截取到指定的宽高度
        $timg = imagecreatetruecolor($tw, $th);
        imagecopyresampled($timg, $zimg,0,0, 0+$_zw,0+$_zh, $tw,$th, $zw-$_zw*2,$zh-$_zh*2);
        //
        if($outfunc($timg, $t)){
            return true;
        }else{
            return false;
        }
    }
    ///获取IP地址
    function ip_getaddress($ip)
    {
        $url="http://ip.taobao.com/service/getIpInfo.php?ip=".$ip;
        //    print_r(file_get_contents($url));
        $ipinfo=json_decode(file_get_contents($url));
        if($ipinfo->code=='1'){
            return false;
        }
        $city = $ipinfo->data->region.$ipinfo->data->city;
        //    $city = $city->data->region->city;
        return $city;
    }
    ///简体转繁体
    function trun($hanzi)
    {
       return \Hanzi::turn($hanzi);     
    }
    //导出execl
    function createtable(){
        header("Content-type:application/vnd.ms-excel");
        header("Content-Disposition:filename=测试.xls");
    
        $strexport="编号\t姓名\t性别\t年龄\r";
            $strexport.="1\t";
            $strexport.="2\t";
            $strexport.="3\t";
            $strexport.="4\r";
    
        $strexport=iconv('UTF-8',"GB2312//IGNORE",$strexport);
        exit($strexport);
    }
}