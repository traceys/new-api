<?php
namespace User\Domain;
use User\Model\User\Token as Mtoken;
use User\Model\User\History as Mhistory;
use User\Model\User\License as Mlicense;
use User\Model\User\User as Muser;
class User
{
    ///生成token
    public function token($uid,$type){
        ///生成token
        $token=new Mtoken();
        $ken=$token->seltoken($uid, $type);
        $token->upduid($uid, $type);
         clearnCache(["token",$ken["token"]]);
        //\PhalApi\DI()->redis->del('token_'.$ken["token"].'');
       
        $time=date("Y-m-d h:i:s",strtotime("+1 months", time()));
        $tokens=getRandomString(16);
        cache(["token",$tokens],$uid,3600*24*30);
       // \PhalApi\DI()->redis->set_time('token_'.$tokens.'',$uid,3600*24*30);
        $token->Settoken($tokens, $uid, $time,$type);
        $data=array(
            'token'=>$tokens
        );
        return $data;
    }
    ///注册
    public function reg($level,$mall,$names=null,$header_imgs=null,$phone_statu=0,$pwd=null)
    {
        ///注册信息
        $reg=new Muser();
        if($mall=="")
        {
           $username='tel_'.getRandomString(12);
        }
        else
        {
           $username='tel_'.$mall;
        }
        $telphone=$mall;
        $rel_name='';
        $rel_code='';
        $phone_status=$level;
        $rel_status=0;
        $reg_time=date("Y-m-d H:i:s",time());
        if($pwd!=null)
        {
            $password=md5($pwd);
        }
        else{
            $password=null;
        }
        
        $name=$names;
        $header_img=$header_imgs;
        $uids=$reg->add($level,$username, $telphone, $rel_name, $rel_code, $phone_status, $rel_status, $reg_time, $password,$name,$header_img);
        ///站内信初始化
         mesgpos($uids);
        //加积分
          addjifen("register",$uids);
        if($mall!=null)
        {
          addjifen("bdtel",$uids);
        }
        $message="欢迎您加入育视界，快去开启全新的幼儿律动点播体验吧。育视界，让教育无处不在。";
        userMessage($uids,"用户服务",$message);
        return $uids;
    }
    ///历史记录
    public  function history($uid,$cat){
        ///生成历史记录
        $history=new Mhistory();
        $act=$cat;
        $value=$cat;
        $to_id=$uid;
        $type="用户";
        $ip=$_SERVER['REMOTE_ADDR'];
        $ips=IPaddress($ip);
        $ipadress=$ips["country"].$ips["region"].$ips["city"].$ips["area"];
        $province=$ips["region"];
        $history->add($uid, $act, $value, $to_id, $type,$ip,$ipadress,$province);
    }
    ///微信绑定账号
    private function license($uid,$type,$code){
        ///绑定uninonid
        $license=new Mlicense();
        $code=cache(["unionid",$code]);
        //\PhalApi\DI()->redis->get_forever('unionid_'.$code.'');
        $license->updcode($code);
        $statu=1;
        $time=date('Y-m-d H:i:s',time());
        $license->Addlicense($uid,$type,$code,$statu,$time);
    }
    ///第三方登录绑定账号
    private function dsflicense($uid,$type,$code){
        ///绑定uninonid
        $license=new Mlicense();
        $statu=1;
        $time=date('Y-m-d H:i:s',time());
        $license->Addlicense($uid,$type,$code,$statu,$time);
    }
    ///登录验证
    public function enterverify($info)
    {
        $user=new Muser();
        if($info["user_status"]==-1)
        {
            $token=new Mtoken();
            $ken=$token->seltokenone($info["Id"]);
            clearnCache(["token",$ken["token"]]);
            //\PhalApi\DI()->redis->del('token_'.$ken["token"].'');
            $token->upduidone($info["Id"]);
            return 450;
//             if(strtotime($info["status_time"])<time())
//             {
//                 $user->huifustatus($info["Id"]);
//                 return 410;
//             }
//             if(strtotime($info["status_time"])>time()){
//                 return 450;
//             }
        }
        else{
            return 410;
        }
       
    }
    ///h5登录
    public function Hlogin($unionId)
    {
        $paty=new Mlicense();
        $uids=$paty->Seluid($unionId,1);
        if($uids)
        {
            ///生成token
            $user=new Muser();
            $info=$user->selectuid($uids["uid"]);
            $ver=$this->enterverify($info);
            if($ver==450){
                return 450;die;
            }
            $data=$this->token($info["Id"],4);
            $users=array_merge($info,$data);
            ///添加历史记录
            $this->history($users["Id"],"H5授权登录");
            return $users;
        }
        else{
            $names=getRandomString(12);
            ///注册信息
            $uid= $this->reg(0,'',$names,'');
            $this->dsflicense($uid,1,$unionId);
            ///生成token
            $user=new Muser();
            $infos=$user->selectuid($uid);
            $data=$this->token($uid,4);
            $users=array_merge($infos,$data);
            $this->history($uid,"H50级用户注册登录");
            return $users;
        }
    }
   ///手机号登录 
    public function Login($mall)
    {
        $user=new Muser();
        $info=$user->selecttel($mall);
        if($info!=null){
            $ver=$this->enterverify($info);
            if($ver==450){
                return 450;die;
            }
            $data=$this->token($info["Id"],1);
            $users=array_merge($info,$data);
            $this->history($users["Id"],"手机号登录");
        }
        else{
            $pwd=getRandomString(6);
            $uid= $this->reg(1, $mall,$mall,null,null,$pwd);
            userMessage($uid,"系统","你的初始密码为:".$pwd."",2,$mall);
            ///生成token
            $infos=$user->selecttel($mall);
            $data=$this->token($uid,1);
            $users=array_merge($infos,$data);
            $this->history($users["Id"],"手机号注册登录");
        }
        
        return $users;
    }
    ///微信code授权登录
    public function Logincode($code,$encryptData,$iv)
    {
        $fun=new Funuser();
        $sessionkey=json_decode($fun->getsession_key($code),true);
        if($sessionkey==null){
            return 411;
        }
        $unionid=json_decode($fun->unionid($sessionkey["session_key"], $encryptData, $iv),true);
        cache(["unionid",$code],$unionid["unionId"],3600*24);
       // \PhalApi\DI()->redis->set_time('unionid_'.$code.'',$unionid["unionId"],3600*24);
        $paty=new Mlicense();
        $uids=$paty->Seluid($unionid["unionId"],1);
        if($uids)
        {
            ///生成token
            $user=new Muser();
            $info=$user->selectuid($uids["uid"]);
            $ver=$this->enterverify($info);
            if($ver==450){
                return 450;die;
            }
            $data=$this->token($info["Id"],2);
            $users=array_merge($info,$data);
            ///添加历史记录
            $this->history($users["Id"],"微信code授权登录");
            return $users;
        }
        else{
            ///注册信息
            $uid= $this->reg(0,'',$unionid["nickName"],$unionid["avatarUrl"]);
            $this->dsflicense($uid,1,$unionid["unionId"]);
            ///生成token
            $user=new Muser();
            $infos=$user->selectuid($uid);
            $data=$this->token($uid,2);
            $users=array_merge($infos,$data);
            $this->history($uid,"微信code授权0级用户注册登录");
            return $users;
        }
    }
    ///微信手机绑定登录
    public function Loginmall($code,$mall)
    {
        $user=new Muser();
        $info=$user->selecttel($mall);
        if($info!=null)
        {
            $ver=$this->enterverify($info);
            if($ver==450){
                return 450;die;
            }
            $license=new Mlicense();
            $party=$license->Seluninonid($info["Id"],1); 
            if($party)
            {
               return 405;die;
            }
            $user->updatetel($info["Id"], $mall);
            ///绑定uninonid
            
            $codes=cache(["unionid",$code]);
            //\PhalApi\DI()->redis->get_time('unionid_'.$code.'');
            
            $lic=new Mlicense();
            $uids=$lic->Seluid($codes,1);
            $lic->upduid($uids["uid"], $info["Id"]);
            
            apiGet("Comment.Comment.Upduid",array('uid'=>$uids["uid"],'nuid'=>$info["Id"]));
            //$this->license($info["Id"],1,$code);
            ///生成token
            $data=$this->token($info["Id"],2);
            $users=array_merge($info,$data);
            ///添加历史记录
            $this->history($users["Id"],"微信手机绑定登录");
            
            ///加积分
            addjifen("bdtel",$info["Id"]);
            return $users;
        }
        else{
            ///注册信息
            $uid= $this->reg(1, $mall,$mall,null,1);
            ///生成token
            $user=new Muser();
            $info=$user->selecttel($mall);
            $data=$this->token($info["Id"],2);
            $users=array_merge($info,$data);
            
             ///绑定uninonid
            $codes=cache(["unionid",$code]);
            //\PhalApi\DI()->redis->get_time('unionid_'.$code.'');
            
            $lic=new Mlicense();
            $uids=$lic->Seluid($codes,1);
            $lic->upduid($uids["uid"], $uid);
            
            apiGet("Comment.Comment.Upduid",array('uid'=>$uids["uid"],'nuid'=>$uid));
            //$this->license($uid,1,$code);
            ///添加历史记录
            
            $this->history($users["Id"],"微信注册登录");
            
            ///加积分
            addjifen("bdtel",$uid);
            return $users;
        }
    }
//     ///微信注册登录
//     public function Loginregister($mall,$code)
//     {
//         ///注册信息
//         $uid= $this->reg(1, $mall);
//         ///生成token
//         $user=new Muser();
//         $info=$user->selecttel($mall);
//         $data=$this->token($info["Id"]);
//         $users=array_merge($info,$data);
//          ///绑定uninonid
//         $this->license($uid,1,$code);
//         ///添加历史记录
//         $this->history($users["Id"],"微信注册登录");
//         return $users;
//     }
    ///微信第三方登录
    public function Loginwx($unionid)
    {  
        $paty=new Mlicense();
        $uids=$paty->Seluid($unionid,1);
        if($uids)
        {
            ///生成token
            $user=new Muser();
            $info=$user->selectuid($uids["uid"]);
            $ver=$this->enterverify($info);
            if($ver==450){
                return 450;die;
            }
            $data=$this->token($uids["uid"],1);
            $users=array_merge($info,$data);
            ///添加历史记录
            $this->history($users["Id"],"微信第三方登录");
        }
        else{
            $names=getRandomString(12);
             $uid= $this->reg(0, '',$names);
            ///绑定uninonid
              $this->dsflicense($uid,1,$unionid);
            ///生成token
              $user=new Muser();
            $infos=$user->selectuid($uid);
            $data=$this->token($uid,1);
            $users=array_merge($infos,$data);  
            ///添加历史记录
            $this->history($users["Id"],"微信第三方注册登录");
        }
        return $users;
    }
    ///qq第三方登录
    public function Loginqq($openid)
    {
        $paty=new Mlicense();
        $uids=$paty->Seluid($openid,2);
        if($uids)
        {
            ///生成token
            $user=new Muser();
            $info=$user->selectuid($uids["uid"]);
         $ver=$this->enterverify($info);
            if($ver==450){
                return 450;die;
            }
            $data=$this->token($uids["uid"],1);
            $users=array_merge($info,$data);
            ///添加历史记录
            $this->history($users["Id"],"qq第三方登录");
        }
        else{
            $names=getRandomString(12);
            ///注册信息
            $uid= $this->reg(0, '',$names);
            ///绑定uninonid
              $this->dsflicense($uid,2,$openid);
            ///生成token
              $user=new Muser();
            $infos=$user->selectuid($uid);
            $data=$this->token($uid,1);
            $users=array_merge($infos,$data);
            ///添加历史记录
            $this->history($users["Id"],"qq第三方注册登录");
        }
        
        return $users;
    }
    ///新浪微博第三方登录
    public function Loginxs($openid)
    {
        $paty=new Mlicense();
        $uids=$paty->Seluid($openid,3);
        if($uids)
        {
            ///生成token
            $user=new Muser();
            $info=$user->selectuid($uids["uid"]);
         $ver=$this->enterverify($info);
            if($ver==450){
                return 450;die;
            }
            $data=$this->token($uids["uid"],1);
            $users=array_merge($info,$data);
            ///添加历史记录
            $this->history($users["Id"],"新浪微博第三方登录");
        }
        else{
            $names=getRandomString(12);
            ///注册信息
            $uid= $this->reg(0,'',$names);
            ///绑定uninonid
            $this->dsflicense($uid,3,$openid);
            ///生成token
            $user=new Muser();
            $infos=$user->selectuid($uid);
            $data=$this->token($uid,1);
            $users=array_merge($infos,$data);
            ///添加历史记录
            $this->history($users["Id"],"新浪微博第三方注册登录");
        }
        return $users;
    }
    ///快捷登录
    public function LoginKj($code)
    {
        $paty=new Mlicense();
        $uids=$paty->Seluid($code,0);
        if($uids)
        {
            ///生成token
            $user=new Muser();
            $info=$user->selectuid($uids["uid"]);
            $data=$this->token($uids["uid"],1);
            $users=array_merge($info,$data);
            ///添加历史记录
            $this->history($users["Id"],"快捷登录");
        }
        else{
            $names='tel_'.getRandomString(16);
            ///注册信息
            $uid= $this->reg(0,'',$names);
            ///绑定uninonid
            $this->dsflicense($uid,0,$code);
            ///生成token
            $user=new Muser();
            $infos=$user->selectuid($uid);
            $data=$this->token($uid,1);
            $users=array_merge($infos,$data);
            ///添加历史记录
            $this->history($users["Id"],"快捷注册登录");
        }
        return $users;
    }
    ///根据uid查询会员信息
    public function seltoken($uid)
    {
       $selu=new Muser();
       $user=$selu->selectuid($uid);
       return $user;
    }
    ///账号删除
    public function userdel($uid)
    {
        $del=new Muser();
        $del->updatedel($uid);
        return true;
    }
    ///用户信息修改
    public function updateuser($name,$header_img,$telphone,$uid,$user_status)
    {
        $user=new Muser();
        if($telphone!=null)
        {
            $tuid=$user->selecttel($telphone);
            if($tuid!=null)
            {
                if($tuid["Id"]!=$uid){
                    return 409;die;
                }
            }
        }
        $code=['name','header_img','telphone','user_status'];
        $rcode=[$name,$header_img,$telphone,$user_status];
        $data=guolu($code, $rcode);
        if($data!=null){
            return $user->updateuser($data,$uid);
        }
        else{
            return 408;
        }
    }
    ///用户修改密码
    public function updatepwd($tel,$password)
    {
        $user=new Muser();
        $us= $user->selecttel($tel);
        if($us!=null)
        {
          $user->updatepwd($us["Id"],md5($password));
          $message="您重置了登录密码，勿告诉他人，妥善保管，确保账号使用安全。育视界，让教育无处不在。";
          userMessage($us["Id"],"重置密码",$message);
           return true;
        }
        else{
           return 407;
        }
    }
    ///
    public function updateadminpwd($uid,$password)
    {
        $user=new Muser();
        $user->updatepwd($uid,md5($password));
        $message="您重置了登录密码，勿告诉他人，妥善保管，确保账号使用安全。育视界，让教育无处不在。";
        userMessage($uid,"重置密码",$message);
        return true;
    }
    ///验证token
    public function tokenverify($token)
    {
        $uid= cache(["token",$token]);
        //\PhalApi\DI()->redis->get_time('token_'.$token);
        if($uid!=null){
            return $uid;
        }
        else
        {
            $seltoken=new Mtoken();
            $tokeninfo=$seltoken->seluid($token);
            if($tokeninfo!=null)
            {
                if(strtotime($tokeninfo["time"])>time())
                {
                  cache(["token",$token],$tokeninfo["uid"],3600*24*30);
                  //\PhalApi\DI()->redis->set_time('token_'.$token.'',$tokeninfo["uid"],3600*24*30);
                  return $tokeninfo["uid"]; 
                }
                else{
                    return false;
                }
            }
            else{
               return false;
            }
        }
    }
    ///后台注册
    public function adminreg($username,$mall,$password)
    {
        $user=new Muser();
        $info=$user->selecttel($mall);
        if($info==null)
        {
            ///注册信息
            $reg=new Muser();
            $telphone=$mall;
            $rel_name='';
            $rel_code='';
            $phone_status=1;
            $rel_status=0;
            $reg_time=date("Y-m-d H:i:s",time());
            $names=getRandomString(12);
            $uids=$reg->add(1,$username, $telphone, $rel_name, $rel_code, $phone_status, $rel_status, $reg_time, $password,$names);
            mesgpos($uids);
            $this->history($uids,"后台注册");
            addjifen("register",$uids);
            addjifen("bdtel",$uids);
            $message="欢迎您加入育视界，快去开启全新的幼儿律动点播体验吧。育视界，让教育无处不在。";
            userMessage($uids,"用户服务",$message);
            return $uids;
        }
        else
        {
            return 405;
        }
    }
    ///用户修改密码
    public function updateadmin($telphone,$password,$uid,$username)
    {
        $user=new Muser();
        if($telphone!=null){
            $tuid=$user->selecttel($telphone);
            if($tuid["Id"]!=$uid){
                return 409;die;
            }
        }
        $code=['username','password','telphone'];
        $rcode=[$username,$password,$telphone];
        $data=guolu($code, $rcode);
        if($data!=null){
            return $user->updateadmin($data,$uid);
        }
        else{
            return 408;
        }
    }
    ///查询用户列表
    public function userlist($id,$tel,$username,$regstr_time,$regend_time,$issming,$uid,$page,$phone_status)
    {
        $user=new Muser();
        return  $user->userlist($id,$tel, $username, $regstr_time, $regend_time, $issming, $uid, $page,$phone_status);
    }
    ///系统用户详情信息查询
    public function userone($uid)
    {
//         $info=\PhalApi\DI()->cache->get('Users_'.$uid.'');
//         if($info!=null)
//         {
//             return $info;
//         }
//         else
//         {
            $user=new Muser();
            $info=$user->selectuid($uid);
            return $info;
//             if($info!=null)
//             {
//              \PhalApi\DI()->cache->set('Users_'.$uid.'',$info,3600*24*30);
//              return $info;
//             }
//             else{
//                 return null;
//             }
//         }
    }
    ///操作历史记录
    public function addhistory($uid, $act, $value, $to_id, $type)
    {
        $user=new  Mhistory();
        $user->add($uid, $act, $value, $to_id, $type);
        return true;
    }
    ///查询用户操作历史记录
    public function selhistory($uid,$act,$value,$to_id,$type,$page,$num=40,$count=null)
    {
        $user=new Mhistory();
        return  $user->sel($uid, $act, $value, $to_id, $type, $page,$num,$count);
    }
    ///账户和密码登录
    public function userpwd($telphone,$pwd,$type){
        $user=new Muser();
        $tel=$user->selecttel($telphone);
        if($tel==null)
        {
            return 405;
        }
        $info=$user->userpwd($telphone,md5($pwd));
        $infos=$user->userpwd($telphone,md5($pwd+strtotime($tel["reg_time"])));
        if($info==null && $infos==null){
            return 405;
        }
        else{
            if($info!=null){
            $ver=$this->enterverify($info);
            if($ver==450){
                return 450;die;
            }
            if($type==3){
                 $data=$this->token($info["Id"],3);
            }
            else{
                $data=$this->token($info["Id"],1);
            }
            $users=array_merge($info,$data);
            $this->history($info["Id"],"账号密码登录");
            return $users;
            }
            if($infos!=null){
            $ver=$this->enterverify($infos);
            if($ver==450){
                return 450;die;
            }
            if($type==3){
                $data=$this->token($infos["Id"],3);
            }
            else{
                $data=$this->token($infos["Id"],1);
            }
            $users=array_merge($infos,$data);
            $this->history($infos["Id"],"账号密码登录");
            return $users;
            }
        }
    }
    ///手机号和密码注册
    public function telpwdreg($tel,$pwd)
    {
        $user=new Muser();
        $info=$user->selecttel($tel);
        if($info==null)
        {
            ///注册信息
            $reg=new Muser();
            $username='tel_'.$tel;
            $telphone=$tel;
            $rel_name='';
            $rel_code='';
            $phone_status=1;
            $rel_status=0;
            $reg_time=date("Y-m-d H:i:s",time());
            $password=md5($pwd);
            $names=getRandomString(12);
            $uids=$reg->add(1,$username, $telphone, $rel_name, $rel_code, $phone_status, $rel_status, $reg_time, $password,$names);
            mesgpos($uids);
            $infos=$user->selecttel($tel);
            $data=$this->token($uids,1);
            $users=array_merge($infos,$data);
            $this->history($uids,"手机号和密码注册登录");
            addjifen("register",$uids);
            addjifen("bdtel",$uids);
            $message="欢迎您加入育视界，快去开启全新的幼儿律动点播体验吧。育视界，让教育无处不在。";
            userMessage($uids,"用户服务",$message);
            return $users;
        }
        else
        {
            return 405;
        }
    }
    ///用户修改密码
    public function userupdpwd($uid,$oldpwd,$newpwd)
    {
        $user=new Muser();
        $info=$user->selectuid($uid);
        $old=$user->selectuidpwd($uid,md5($oldpwd));
        $olds=$user->selectuidpwd($uid,md5($oldpwd+strtotime($info["reg_time"])));
        if($old!=null || $olds!=null){
            $user->updatepwd($uid, md5($newpwd));
            $message="您修改了登录密码，勿告诉他人，妥善保管，确保账号使用安全。育视界，让教育无处不在。";
            userMessage($uid,"修改密码",$message);
            return true;
        }
        else{
            return 409;
        }
    }
    
    ///找回密码
    public function zhpwd($telphone,$pwd)
    {
        $user=new Muser();
        $info=$user->selecttel($telphone);
        if($info!=null)
        {
         $ver=$this->enterverify($info);
            if($ver==450){
                return 450;die;
            }
            $user->updatepwd($info["Id"],md5($pwd));
            $message="您重置了登录密码，勿告诉他人，妥善保管，确保账号使用安全。育视界，让教育无处不在。";
            userMessage($info["Id"],"重置密码",$message);
            return true;
        }
        else{
            return 409;
        }
    }
    
    ///绑定手机号
    public function bdtel($uid,$tel)
    {
        $user=new Muser();
        $info=$user->selecttel($tel);
        if($info!=null)
        {
            $lic=new Mlicense();
            $user->updatetel($info["Id"],$tel);
            $lic->upduid($uid, $info["Id"]);
            addjifen("bdtel",$info["Id"]);
            return $info;
        }
        else
        {
            $user->updatetel($uid,$tel);
            addjifen("bdtel",$uid);
            return 410;
        }
    }
    ///更改绑定手机号验证
    public function bdnewvertel($uid,$tel)
    {
        $user=new Muser();
        $info=$user->selectuid($uid);
        if($info["telphone"]==$tel){
            return true;
        }
        else
        {
            return 410;
        }
    }
    ///更改绑定手机号
    public function bdnewtel($uid,$telphone)
    {
        $user=new Muser();
        $info=$user->selecttel($telphone);
        if($info!=null){
            return 411;
        }
        else
        {
         $user->updatenewtel($uid, $telphone);
         return true;
        }
    }
    ///实名认证修改
    public function updaterel($uid,$rel_name,$rel_code,$rel_status)
    {
        $user=new Muser();
        $user->updaterel($uid, $rel_name, $rel_code,$rel_status);
        return true;
    }
    ///电话查询会员信息
    public function seltel($telphone)
    {
        $user=new Muser();
        $info=$user->selecttel($telphone);
     $ver=$this->enterverify($info);
            if($ver==450){
                return 450;die;
            }
        return $info;
    }
    ///查询指定用户列表
    public function userin($uid)
    {
        $user=new Muser();
        $info=$user->userin($uid);
        return $info;
    }
    ///修改昵称
    public function updnickname($uid,$nickname)
    {
        $user=new Muser();
        $user->updnickname($uid,$nickname);
        return true;
    }
    ///token有效查询
    public function selecttoken($searchtoken)
	{
	    $searchTokenuid=cache(["token",$searchtoken]);
	    //\PhalApi\DI()->redis->get_time('token_'.$searchtoken);
	    if($searchTokenuid==null){
	        $searchTokenuid=$this->tokenverify($searchtoken);
	    }
	    if($searchTokenuid!=false)
	    {
	        return $this->seltoken($searchTokenuid);
	    }
	    if($searchtoken==Admin_token)
	    {
	        return array('Id'=>0);
	    }else
	    {
	        return array('Id'=>-1);
	    }
	}
	///注册区域地址人数
	public function regcount($region,$uid){
	    $his=new Mhistory();
	    return $his->regcount($region,$uid);
	}
	///注册区域得找会员
	public function reguser($region)
	{
	    $his=new Mhistory();
	    return $his->reguser($region);
	}
	///查询第三方绑定
	public function Seluninonid($uid)
	{
	    $lic=new Mlicense();
	    return $lic->Seltype($uid);
	}
	//真实姓名模糊查询
	public function selrel_name($rel_name){
	    $user=new Muser();
	    return $user->selrel_names($rel_name);
	}
	///修改个人信息
	public function updateperson($id,$header_img,$name,$person_region,$person_jj,$person_zs)
	{
	    $person=new Muser();
	    return $person->updateperson($id, $header_img, $name, $person_region, $person_jj, $person_zs);
	}
}