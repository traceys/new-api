<?php
namespace User\Domain;
use User\Model\User\Company as Mcompany;
class Company
{
    ///添加
    public function add($company_type, $company_name, $company_jc, $company_gps, $pro, $city, $reigon, $company_adress, $company_my, $company_user, $company_phone, $company_jj, $company_zj, $company_tid, $company_uid, $company_pwd, $company_tgm)
    {
        $com=new Mcompany();
        return $com->add($company_type, $company_name, $company_jc, $company_gps, $pro, $city, $reigon, $company_adress, $company_my, $company_user, $company_phone, $company_jj, $company_zj, $company_tid, $company_uid, $company_pwd, $company_tgm);
    }
    //修改
    public function up($tid, $company_name,$company_logo,$company_jc,$company_gps,$pro,$city,$region,$company_adress,$company_user,$company_phone,$company_jj,$company_xc,$company_zj){
        $com=new Mcompany();
        $code=['company_name','company_logo','company_jc','company_gps','pro','city','region','company_adress','company_user','company_phone','company_jj','company_xc','company_zj'];
        $rcode=[$company_name,$company_logo,$company_jc,$company_gps,$pro,$city,$region,$company_adress,$company_user,$company_phone,$company_jj,$company_xc,$company_zj];
        $data=guolu($code, $rcode);
        if($data!=null)
        {
            return $com->up($tid, $data);
        }
    }
    //删除
    public function del($id){
        $com=new Mcompany();
        return $com->del($id);
    }
    ///查询
    public function seluid($tid)
    {
        $com=new Mcompany();
        return $com->seluid($tid);
    }
    ///设置管理密码
    public function setpwd($tid,$pwd){
        $com=new Mcompany();
        return $com->setpwd($tid, $pwd);
    }
    ///查询机构名称是否存在
    public function selname($company_name)
    {
        $com=new Mcompany();
        return $com->selname($company_name);
    }
}