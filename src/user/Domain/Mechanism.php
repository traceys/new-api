<?php
namespace User\Domain;
use User\Model\User\Mechanism as Mmechanism;
use User\Model\User\Mechhistory as Mmechhistory;
class Mechanism
{
    ///机构认证
    public function addauth($name,$username,$user_phone,$img,$contract_num,$contract_doc,$auth_dir,$pro,$city,$county)
    {
        $names=new Mmechanism();
        $names->addauth($name,$username,$user_phone,$img,$contract_num,$contract_doc,$auth_dir,$pro,$city,$county);
        return true;
    }
    ///机构认证状态查询
    public function selstatu($type,$order,$page){
        $name=new Mmechanism();
        return $name->selstatu($type, $order, $page);
    }
    ///机构认证信息
    public function selauth($id)
    {
        $name=new Mmechanism();
        return  $name->selauth($id);
    }
    ///机构认证信息审核
    public function updauth($id,$status)
    {
        $name=new Mmechanism();
        return $name->updauth($id,$status);
    }
    ///后台条条件查询
    public function melist($rec_status,$star_time,$end_time,$page,$names,$username,$code,$use_state,$uid,$iscode,$user_phone)
    {
        $name=new Mmechanism();
        return $name->melist($rec_status, $star_time, $end_time, $page,$names,$username,$code,$use_state,$uid,$iscode,$user_phone);
    }
    ///生成邀请码
    public function updatecode($id,$codes,$generate_time,$end_time,$subaccount_num,$use_state)
    {
        $mech=new Mmechanism();
        $data=array(
            'code'=>$codes,
            'generate_time'=>$generate_time,
            'end_time'=>$end_time,
            'subaccount_num'=>$subaccount_num,
            'use_state'=>$use_state,
            'beizhu'=>null
        );
        if($data!=null){
            return $mech->updatecode($id,$data);
        }
        else{
            return 408;
        }
    }
    ///导入生成邀请码
    public function updateimportcode($id,$codes,$generate_time,$end_time,$subaccount_num,$use_state)
    {
        $mech=new Mmechanism();
        $data=array(
            'code'=>$codes,
            'generate_time'=>$generate_time,
            'end_time'=>$end_time,
            'subaccount_num'=>$subaccount_num,
            'use_state'=>$use_state
        );
        if($data!=null){
            return $mech->updatecode($id,$data);
        }
        else{
            return 408;
        }
    }
    ///机构信息修改
    public function updateauth($id,$name,$username,$user_phone,$contract_num,$contract_doc,$auth_dir,$pro,$city,$county)
    {
        $mech=new Mmechanism();
//         $code=['name','username','user_phone','contract_num','contract_doc','auth_dir','time','province','city','county','region'];
//         $rcode=[$name,$username,$user_phone,$contract_num,$contract_doc,$auth_dir,date('Y-m-d H:i:s',time()),$pro,$city,$county,$pro.$city.$county];
//         $data=guolu($code, $rcode);
//         if($data!=null){
        return $mech->updateauth($id,$name,$username,$user_phone,$contract_num,$contract_doc,$auth_dir,$pro,$city,$county);
//         }
//         else{
//             return 408;
//         }
    }
    ///机构信息提交
    public function updatetj($id,$img,$uid){
        
        $mech=new Mmechanism();
        $data=array(
            'img'=>$img,
            'time'=>date('Y-m-d H:i:s',time()),
            'uid'=>$uid,
            'rec_status'=>0,
            'use_state'=>1
        );
//         $code=['img','time','uid','rec_status'];
//         $rcode=[$img,date('Y-m-d H:i:s',time()),$uid,0];
//         $data=guolu($code, $rcode);
        if($data!=null){
            return $mech->updatecode($id,$data);
        }
        else{
            return 408;
        }
    }
    ///查询会员机构信息
    public function selmec($uid)
    {
       $mech=new Mmechanism();
       $info=$mech->selecteuser($uid);
       return $info;
    }
    ///添加团队ID
    public function updtid($uid,$tid)
    {
        $mech=new Mmechanism();
        $mech->updtid($uid, $tid);
        return true;
    }
    //前台通过机构邀请码查询
    public function selcode($code)
    {
        $mech=new Mmechanism();
        return  $mech->selcode($code);
    }
    ///查询机构认证状态
    public function selstatus($uid)
    {
        $mech=new Mmechanism();
        return  $mech->selstatus($uid);
    }
    ///查询全部机构
    public function selall(){
        $mech=new Mmechanism();
        return $mech->selall();
    }
    ///添加团队ID
    public function updtidone($id,$tid)
    {
        $mech=new Mmechanism();
        $mech->updtidone($id, $tid);
        return true;
    }
    ///添加机构历史
    public function addhistory($uid, $name, $code, $username, $user_phone, $img, $rec_status, $time, $end_time, $contract_num, $contract_doc, $auth_dir, $statu, $generate_time, $subaccount_num, $tid, $use_state, $beizhu, $province, $city, $county,$prouid,$fromid,$on_statu,$region){
        $his=new Mmechhistory();
        $his->addhistory($uid, $name, $code, $username, $user_phone, $img, $rec_status, $time, $end_time, $contract_num, $contract_doc, $auth_dir, $statu, $generate_time, $subaccount_num, $tid, $use_state, $beizhu, $province, $city, $county,$prouid,$fromid,$on_statu,$region);
    }
    ///查询历史邀请码
    public function selhiscode($fromid){
        $his=new Mmechhistory();
        return $his->selcode($fromid);
    }
    ///邀请码使用状态
    public function upduser($id,$use_state){
        $mec=new Mmechanism();
        return $mec->upduser($id, $use_state);
    }
    ///邀请码使用次数
    public function selnum($fromid,$code){
        $his=new Mmechhistory();
        return $his->selnum($fromid,$code);
    }
    ///机构审核
    public function updsh($id,$use_state,$status,$beizhu,$cluid)
    {
        $mec=new Mmechanism();
        return $mec->updsh($id, $use_state, $status, $beizhu,$cluid);
    }
    ///禁用邀请码
    public function jycode($id,$on_statu)
    {
        $mec=new Mmechanism();
        return $mec->jycode($id,$on_statu);
    }
    ///查询分会下面的机构
    public function seladress($data,$name,$telphone,$page,$rec_status,$end_time)
    {
        $mec=new Mmechanism();
        return $mec->seladress($data, $name, $telphone, $page,$rec_status,$end_time);
    }
   ///查询机构信息
   public function selmecall($rec_status,$star_time,$end_time,$page,$name,$username,$code,$use_state,$uid,$iscode,$user_phone)
   {
       $list= cache(["user.mechanism。model.user.mechanism.selmecall",$rec_status,$star_time,$end_time,$page,$name,$username,$code,$use_state,$uid,$iscode,$user_phone]);
       if($list!=null){
           return $list;
       }
       $data=[];
       $h=0;
       $mec=new Mmechanism();
       $info=$mec->selmecall($rec_status, $star_time, $end_time, $name, $username, $code, $use_state, $uid, $iscode, $user_phone);
       $his=new Mmechhistory();
       $infos=$his->selmecall($rec_status, $star_time, $end_time, $name, $username, $code, $use_state, $uid, $iscode, $user_phone);
       for($i=0;$i<count($info);$i++)
       {
           $data[$h]=$info[$i];
           $data[$h]["type"]="zb";
           $h++;
       }
       for($j=0;$j<count($infos);$j++)
       {
            $data[$h]=$infos[$j];
            $data[$h]["type"]="ls";
            $h++;
       }
       foreach ($data as $key => $row)
       {
           $volume[$key]  = $row['time'];
       }
       array_multisort($volume, SORT_DESC, $data);
       $code="";
       $datas=[];
       for($k=0;$k<count($data);$k++)
       {
           if($code!=$data[$k]["code"])
           {
             $datas[$k]=$data[$k];
           }
           $code=$data[$k]["code"];
       }
       $count=count($data);
       outputArgs(["num"=>$count,"pagenum"=>40,"page"=>$page]);
       $data = array_slice($datas, ($page - 1) * 40, 40);
       
       cache(["user.mechanism。model.user.mechanism.selmecall",$rec_status,$star_time,$end_time,$page,$name,$username,$code,$use_state,$uid,$iscode,$user_phone],$data);
       return $data;
   }
   ///机构认证信息
   public function selhisone($id)
   {
       $his=new Mmechhistory();
       return $his->selhisone($id);
   }
   ///查询团队机构信息
   public function teammech($tid)
   {
       $mec=new Mmechanism();
       return $mec->teammech($tid);
   }
   ///判断是否续约
   public function selxy($fromid)
   {
       $his=new Mmechhistory();
       return $his->selxy($fromid);
   }
   ///tel查询团队机构
   public function telmech($tel)
   {
       $tels=new Mmechanism();
       return $tels->telmech($tel);
   }
   ///机构认证
   public function addimport($name,$username,$user_phone,$contract_num,$auth_dir,$province,$city,$county)
   {
       $import=new Mmechanism();
       return $import->addimport($name, $username, $user_phone, $contract_num, $auth_dir, $province, $city, $county);
   }
   ///机构信息修改
   public function updateimport($id,$name,$username,$user_phone,$contract_num,$auth_dir,$pro,$city,$county)
   {
       $mech=new Mmechanism();
       $code=['name','username','user_phone','contract_num','auth_dir','time','province','city','county','region',"use_state"];
       $rcode=[$name,$username,$user_phone,$contract_num,$auth_dir,date('Y-m-d H:i:s',time()),$pro,$city,$county,$pro.$city.$county,0];
       $data=guolu($code, $rcode);
       if($data!=null){
           return $mech->updatecode($id,$data);
       }
       else{
           return 408;
       }
   }
   ///tid查询团队机构信息
   public function teamtid($tid){
       $tels=new Mmechanism();
       return $tels->teamtid($tid);
   }
}