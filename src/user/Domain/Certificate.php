<?php
namespace User\Domain;
use PhalApi\Exception;
use User\Model\User\Certificate as Certificates;
use User\Domain\User;
use PhalApi\Api;
use PhalApi\Exception\BadRequestException;
use User\Common\TimeData;
class Certificate{


public function ApplyCreate($val){
    $model = new Certificates();

//    数据过滤
    foreach( $val as $k=>$v){
        if( $v === '' )
            unset( $val[$k] );
    }

    //update
    if ($val['id']){
        $id = $val['id'];

        $res = $model->find($id);
        if (!$res){
            throw new BadRequestException('没有找到您需要的证书',4);
        }

        //证书升级
        if ($val['grade']){
            //清空发证日期
            $val['entryTime'] = '';
        }

//        if (isset($val['uid'])){
//            $uid = $val['uid'];
//        }else{
//            $uid = apiGet('User.user.Selecttoken', array('searchToken'=>$val['token']))['Id'];
//        }

        if (isset($val['pay_statu']) && $val['pay_statu'] == 1 || isset($val['state'])){
            $uid = $res['uid'];
            $url = 'User.User.Userone&uid='.$uid;
            $rename =  apiGet($url);
            $stopTime = $res['stopTime'];
            if (isset($val['pay_statu']) && $val['pay_statu'] == 1){
                $stopTime = $val['stopTime'];
                $timedata = [
                    'tid'=>315,
                    'uid'=>$uid,
                    'end_time'=>$stopTime
                ];
                apiGet('Team.User.Add',$timedata);
            }
            $cernum = isset($val['licenceNum'])?$val['licenceNum']:$res['licenceNum'];
            //如果上传了证书编号更新发证时间
            if (isset($val['licenceNum'])){
                $timedata = new TimeData($val['licenceNum']);
                $applyTime = $timedata->run();
                $val['applyTime'] = $applyTime;
            }else{
                $applyTime = date("Ymd",strtotime($res['applyTime']));
            }
            $data = [
                'level'=>$res['grade'],
                'name'=>$rename['rel_name'],
                'pyname'=>$res['spell'],
                'idnum'=>$rename['rel_code'],
                'cernum'=>$cernum,
                'pushtime'=>$applyTime,
                'yxtime'=>$applyTime.'-'.date('Ymd',strtotime($stopTime)),
                'photo'=>$res['identityUrl'],
                'islizhi'=>$res['state']
            ];
            //是否离职顶替证书
            if (isset($val['state'])){
                $data['islizhi'] =  $val['state'];
            }
            $url = 'User.User.Watermark';
            $val['mycer'] = apiGet($url,$data);
        }
        $val = array_remove($val,'id');
        $val = array_remove($val,'token');
//    数据过滤
        foreach( $val as $k=>$v){
            if( $v === null )
                unset( $val[$k] );
        }
            return $model->updates($val,$id);


    }
    //create
    if (!$val['id']){
        $user = new User();
        if ( !isset($val['uid'])){
            $val['uid'] = $user->tokenverify($val['token']);
        }
        //重复认证
        $data = $this->findcertificate(false,false,$val['uid']);
        if ($data){
            //若教师离职不予新证书发放
            if (!$data['state']){
                throw new BadRequestException('该教师已离职',3);
            }

            if (!isset($val['stageName']) || !$val['stageName'] ){
                $val['stageName'] = $data['stageName'];
            }
            if (!isset($val['videoProducing']) || !$val['videoProducing'] ){
                $val['videoProducing'] = $data['videoProducing'];
            }

            $id =$val['uid'];
            $va = ['on_statu'=>-1];
            $model->updatesuid($va,$id);
        }

        // check for-rename
        $url = 'User.User.Userone&uid='.$val['uid'];
        $username = apiGet($url);
          if (!$username['rel_status']){

              throw new BadRequestException('请实名认证后再来申请',3);

          }

       $val['dateBirth'] = \User\datebirth($username['rel_code']);
          if ($val['territory']){
              $sitedata = explode(',',$val['territory']);
              $site = json_encode($sitedata,JSON_UNESCAPED_UNICODE);
              $val['unit']= apiGet('Configure.Configure.region',array('region'=>$site))['0']['key'];
          }
        $val['create_time'] = date("Y-m-d H:i:s",time());
        $val = array_remove($val,'id');
        $val = array_remove($val,'token');
        try{
            return $model->create($val);
        }catch (\Exception $e){
            throw new BadRequestException('传入数据有误,请检您的输入',3);
        }
    }

}

//find
public function findcertificate($id,$token=false,$uid=false){

    $model = new Certificates();
if ($id){
    return $model->find($id);
}

if ($token){
    $user = new User();
    $uid = $user->tokenverify($token);
    return $model->finduser($uid);

}
if ($uid){
    return $model->finduser($uid);
}


}

//finds
public function selectdata($val){
    $model = new Certificates();
    if ($val['id']){
        return $model->find($val['id']);
    }
    $user = new User();
    $uid = $user->tokenverify($val['token']);
    $arr = [
        'uid'=>$uid,
    ];
    if ($val['pay_statu'] != 'null'){
        $arr['pay_statu'] = $val['pay_statu'];
    }
    $data =  $model->finduserAll($arr);
    return $data;
}

//Show list
public function showList($val){
    $model = new Certificates();

    $data =  $model->findAll($val);
    $num = $model->findAll($val,'num');
    outputArgs(array('num'=>$num,'pagenum'=>$val['number'],'age'=>$val['page']));
    return $data;
}

/**
 * Certificate resource statistice
 */
public function statistice(){

    $model = new Certificates();
    $where = [
        'statu'=>1
    ];
          //sum
          $data['a'] = count($model->finduserAlls($where));
          $start = date("Y-m-d",time());
          $end = date('Y-m-d',strtotime('+1 day'));
          //today
          $data['b'] = count($model->setTime($start,$end));
          $start = date('Y-m-d',strtotime('-1 day'));
          $end = date("Y-m-d",time());;
            //yesterday
          $data['c'] = count($model->setTime($start,$end));

          return $data;
}

/**
 * check endtime certificate
 */
//public function checkcertificate(){
//        $model = new Certificates();
//        $res = $model->endTimeCertificate();
//        if ($res){
//            foreach ($res as $key => $value){
//                $this->
//
//            }
//        }
//
//
//}

    /**
     * @param $uid
     * @return select uid data
     */
public function seluserteacher($uid)
{
    $mcert=new Certificates();
    $data =  $mcert->seluserteacher($uid);
    return $data;
}
//根据uid,tid查询教师身份
public function selteacher($uid,$tid)
{
    $mcert=new Certificates();
    $data =  $mcert->selteacher($uid, $tid);
    return $data;
}
}