<?php
namespace User\Domain;
use User\Model\User\Realname as Mrealname;
class Realname
{
    ///实名认证
    public function addreal($uid,$name,$code,$head_img,$back_img,$rec_status)
    {
        $names=new Mrealname();
        $names->addreal($uid, $name, $code, $head_img, $back_img,$rec_status);
        return true;
    }
    ///实名认证状态查询
    public function selstatu($type,$order,$page){
        $name=new Mrealname();
        return $name->selstatu($type, $order, $page);
    }
    ///实名认证信息
    public function selreal($id)
    {
        $name=new Mrealname();
        return  $name->selreal($id);
    }
    ///实名认证信息审核
    public function updreal($id,$status)
    {
        $name=new Mrealname();
        return $name->updreal($id,$status);
    }
}