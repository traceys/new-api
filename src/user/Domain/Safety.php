<?php
namespace User\Domain;
use User\Model\User\Safety as Msafety;
class Satety
{
    //添加
    public function add($safety_uid,$safety_code,$safety_open)
    {
        $safe=new Msafety();
        return $safe->add($safety_uid, $safety_code, $safety_open);
    }
    ///查询验证安全码
    public function select($safety_uid)
    {
        $safe=new Msafety();
        return $safe->select($safety_uid);
    }
    ///修改安全码
    public function upcode($safety_uid,$safety_code)
    {
        $safe=new Msafety();
        return $safe->upcode($safety_uid, $safety_code);
    }
    ///是否开启
    public function upopen($safety_uid,$safety_open)
    {
        $safe=new Msafety();
        return $safe->upopen($safety_uid, $safety_open);
    }
    ///删除 
    public function upstatu($safety_uid)
    {
        $safe=new Msafety();
        return $safe->upstatu($safety_uid);
    }
}