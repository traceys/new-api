<?php
namespace User\Domain;
use User\Model\User\Event as Uevent;
use User\Model\User\Eventlog as Eventlog;
class Event
{
    //添加记录事件
    public function eventadd($event_zhname,$event_name, $event_value){
        $event=new Uevent();
        return $event->add($event_zhname,$event_name, $event_value);
        
    }
    ///添加记录事件log
    public function eventlogadd($event, $on_statu){
        $events=new Eventlog();
        return $events->add($event, $on_statu);
    
    }
    ///查询url
    public function eventselect($event_name){
        $event=new Uevent();
        return $event->select($event_name);
    }
    ///修改
    public function updevent($event_name,$event_value){
        $event=new Uevent();
        return $event->upd($event_name,$event_value);
    }
}