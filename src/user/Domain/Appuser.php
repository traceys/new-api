<?php
namespace User\Domain;
use User\Model\User\Token as Mtoken;
use User\Model\User\History as Mhistory;
use User\Model\User\License as Mlicense;
use User\Model\User\User as Muser;
class Appuser
{
   
    ///app手机号验证码登录
    public function Login($mall)
    {
        $user=new Muser();
        $info=$user->selecttel($mall);
        if($info!=null)
        {
            $ver=$this->enterverify($info);
            if($ver==450)
            {
                return 450;
            }
            $data=$this->token($info["Id"],4);
            $users=array_merge($info,$data);
            $this->history($users["Id"],"APP手机号和密码注册登录");
        }
        else{
            $pwd=getRandomString(6);
            $uid= $this->appreg($mall);
            userMessage($uid["Id"],"系统","你的初始密码为:".$pwd."",2,$mall);
        }
        return $users;
    }
    ///app账户和密码登录
    public function userpwd($telphone,$pwd){
        $user=new Muser();
        $tel=$user->selecttel($telphone);
        if($tel==null)
        {
            return 405;
        }
        $info=$user->userpwd($telphone,md5($pwd));
        $infos=$user->userpwd($telphone,md5($pwd+strtotime($tel["reg_time"])));
        if($info==null && $infos==null){
            return 405;
        }
        else
        {
            if($info!=null){
                $ver=$this->enterverify($info);
                if($ver==450){
                    return 450;
                }
                $data=$this->token($info["Id"],4);
                $users=array_merge($info,$data);
                $this->history($info["Id"],"APP账号密码登录");
                return $users;
            }
            if($infos!=null){
                $ver=$this->enterverify($infos);
                if($ver==450){
                    return 450;
                }
                $data=$this->token($infos["Id"],4);
                $users=array_merge($infos,$data);
                $this->history($infos["Id"],"APP账号密码登录");
                return $users;
            }
        }
    }
    ///注册
    public function appreg($tel,$pwd)
    {
        $user=new Muser();
        $info=$user->selecttel($tel);
        if($info==null)
        {
            ///注册信息
            $reg=new Muser();
            $username='tel_'.$tel;
            $telphone=$tel;
            $rel_name='';
            $rel_code='';
            $phone_status=1;
            $rel_status=0;
            $reg_time=date("Y-m-d H:i:s",time());
            $password=md5($pwd);
            $names=getRandomString(12);
            $uids=$reg->add(1,$username, $telphone, $rel_name, $rel_code, $phone_status, $rel_status, $reg_time, $password,$names);
            mesgpos($uids);
            $user=new Muser();
            $infos=$user->selecttel($tel);
            $data=$this->token($uids,4);
            $users=array_merge($infos,$data);
            $this->history($uids,"APP手机号和密码注册登录");
            return $users;
        }
        else
        {
            return 405;
        }
    }
    ///找回密码
    public function zhpwd($telphone,$pwd)
    {
        $user=new Muser();
        $info=$user->selecttel($telphone);
        if($info!=null)
        {
            $ver=$this->enterverify($info);
            if($ver==450){
                return 450;
            }
            $user->updatepwd($info["Id"],md5($pwd));
            return true;
        }
        else{
            return 409;
        }
    }
    ///用户修改密码
    public function userupdpwd($uid,$oldpwd,$newpwd)
    {
        $user=new Muser();
        $info=$user->selectuid($uid);
        $old=$user->selectuidpwd($uid,md5($oldpwd));
        $olds=$user->selectuidpwd($uid,md5($oldpwd+strtotime($info["reg_time"])));
        if($old!=null || $olds!=null){
            $user->updatepwd($uid, md5($newpwd));
            return true;
        }
        else{
            return 409;
        }
    }
    ///微信第三方登录
    public function Loginwx($unionid)
    {
        $paty=new Mlicense();
        $uids=$paty->Seluid($unionid,1);
        if($uids)
        {
            ///生成token
            $user=new Muser();
            $info=$user->selectuid($uids["uid"]);
            $ver=$this->enterverify($info);
            if($ver==450){
                return 450;
            }
            $data=$this->token($uids["uid"],1);
            $users=array_merge($info,$data);
            ///添加历史记录
            $this->history($users["Id"],"app微信第三方登录");
        }
        else{
            $names=getRandomString(12);
            $uid= $this->reg(0, '',$names);
            ///绑定uninonid
            $this->dsflicense($uid,1,$unionid);
            ///生成token
            $user=new Muser();
            $infos=$user->selectuid($uid);
            $data=$this->token($uid,1);
            $users=array_merge($infos,$data);
            ///添加历史记录
            $this->history($users["Id"],"app微信第三方注册登录");
        }
        return $users;
    }
    ///qq第三方登录
    public function Loginqq($openid)
    {
        $paty=new Mlicense();
        $uids=$paty->Seluid($openid,2);
        if($uids)
        {
            ///生成token
            $user=new Muser();
            $info=$user->selectuid($uids["uid"]);
            $ver=$this->enterverify($info);
            if($ver==450){
                return 450;
            }
            $data=$this->token($uids["uid"],1);
            $users=array_merge($info,$data);
            ///添加历史记录
            $this->history($users["Id"],"appqq第三方登录");
        }
        else{
            $names=getRandomString(12);
            ///注册信息
            $uid= $this->reg(0, '',$names);
            ///绑定uninonid
            $this->dsflicense($uid,2,$openid);
            ///生成token
            $user=new Muser();
            $infos=$user->selectuid($uid);
            $data=$this->token($uid,1);
            $users=array_merge($infos,$data);
            ///添加历史记录
            $this->history($users["Id"],"appqq第三方注册登录");
        }
    
        return $users;
    }
    ///新浪微博第三方登录
    public function Loginxs($openid)
    {
        $paty=new Mlicense();
        $uids=$paty->Seluid($openid,3);
        if($uids)
        {
            ///生成token
            $user=new Muser();
            $info=$user->selectuid($uids["uid"]);
            $ver=$this->enterverify($info);
            if($ver==450){
                return 450;
            }
            $data=$this->token($uids["uid"],1);
            $users=array_merge($info,$data);
            ///添加历史记录
            $this->history($users["Id"],"app新浪微博第三方登录");
        }
        else{
            $names=getRandomString(12);
            ///注册信息
            $uid= $this->reg(0,'',$names);
            ///绑定uninonid
            $this->dsflicense($uid,3,$openid);
            ///生成token
            $user=new Muser();
            $infos=$user->selectuid($uid);
            $data=$this->token($uid,1);
            $users=array_merge($infos,$data);
            ///添加历史记录
            $this->history($users["Id"],"app新浪微博第三方注册登录");
        }
        return $users;
    }
    ///注册
    public function reg($level,$mall,$names=null,$header_imgs=null,$phone_statu=0,$pwd=null)
    {
        ///注册信息
        $reg=new Muser();
        if($mall=="")
        {
            $username='tel_'.getRandomString(12);
        }
        else
        {
            $username='tel_'.$mall;
        }
        $telphone=$mall;
        $rel_name='';
        $rel_code='';
        $phone_status=$level;
        $rel_status=0;
        $reg_time=date("Y-m-d H:i:s",time());
        $password=md5($pwd);
        $name=$names;
        $header_img=$header_imgs;
        $uids=$reg->add($level,$username, $telphone, $rel_name, $rel_code, $phone_status, $rel_status, $reg_time, $password,$name,$header_img);
        ///站内信初始化
        mesgpos($uids);
        return $uids;
    }
    ///生成token
    public function token($uid,$type){
        ///生成token
        $token=new Mtoken();
        $ken=$token->seltoken($uid, $type);
        $token->upduid($uid, $type);
        clearnCache(["token",$ken["token"]]);
        $time=date("Y-m-d h:i:s",strtotime("+1 months", time()));
        $tokens=getRandomString(16);
        cache(["token",$tokens],$uid,3600*24*30);
        $token->Settoken($tokens, $uid, $time,$type);
        $data=array(
            'token'=>$tokens
        );
        return $data;
    }
    ///历史记录
    public  function history($uid,$cat){
        ///生成历史记录
        $history=new Mhistory();
        $act=$cat;
        $value=$cat;
        $to_id=$uid;
        $type="用户";
        $ip=$_SERVER['REMOTE_ADDR'];
        $ips=IPaddress($ip);
        $ipadress=$ips["country"].$ips["region"].$ips["city"].$ips["area"];
        $province=$ips["region"];
        $history->add($uid, $act, $value, $to_id, $type,$ip,$ipadress,$province);
    }
    ///第三方登录绑定账号
    private function dsflicense($uid,$type,$code){
        ///绑定uninonid
        $license=new Mlicense();
        $statu=1;
        $time=date('Y-m-d H:i:s',time());
        $license->Addlicense($uid,$type,$code,$statu,$time);
    }
    ///登录验证,是否冻结
    public function enterverify($info)
    {
        $user=new Muser();
        if($info["user_status"]==-1)
        {
            $token=new Mtoken();
            $ken=$token->seltokenone($info["Id"]);
            clearnCache(["token",$ken["token"]]);
            $token->upduidone($info["Id"]);
            return 450;
        }
        else{
            return 410;
        }
    }
    ///绑定手机号
    public function bdtel($uid,$tel)
    {
        $user=new Muser();
        $info=$user->selecttel($tel);
        if($info!=null)
        {
            $lic=new Mlicense();
            $user->updatetel($info["Id"],$tel);
            $lic->upduid($uid, $info["Id"]);
            return $info;
        }
        else
        {
            $user->updatetel($uid,$tel);
            return 410;
        }
    }

}