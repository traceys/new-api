<?php
namespace User\Domain;
use User\Model\User\Promo as Mpromo;
class Promo
{
    ///添加
    public function add($promo_tid,$promo_code,$promo_type){
        $pro=new Mpromo();
        return $pro->add($promo_tid, $promo_code, $promo_type);
    }
    ///激活
    public function jihou($promo_tid,$promo_type)
    {
        $pro=new Mpromo();
        return $pro->jihou($promo_tid, $promo_type);
    }
    ///删除
    public function del($id)
    {
        $pro=new Mpromo();
        return  $pro->del($id);
    }
    ///推广码查询
    public function seltgm($tgm)
    {
        $tgm=new Mpromo();
        return $tgm->seltgm($tgm);
    }
}