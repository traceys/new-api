<?php
namespace User\Domain;
use User\Model\User\School as Mschool;
class School
{
    ///添加
    public function add($company_type, $school_name, $school_jc, $school_adress, $pro, $city, $region, $adress_gps, $school_xm, $school_phone, $school_my, $school_xc, $school_jj, $school_zj, $school_gh, $school_tid, $school_uid, $school_pwd, $school_tgm)
    {
        $com=new Mschool();
        return $com->add($company_type, $school_name, $school_jc, $school_adress, $pro, $city, $region, $adress_gps, $school_xm, $school_phone, $school_my, $school_xc, $school_jj, $school_zj, $school_gh, $school_tid, $school_uid, $school_pwd, $school_tgm);
    }
    //修改
    public function up($tid, $company_name,$school_logo,$school_jc,$adress_gps,$pro,$city,$region,$school_adress,$school_xm,$school_phone,$school_jj,$school_xc,$school_zj)
    {
        $com=new Mschool();
        $code=['company_name','school_logo','school_jc','adress_gps','pro','city','region','school_adress','school_xm','school_phone','school_jj','school_xc','school_zj'];
        $rcode=[$company_name,$school_logo,$school_jc,$adress_gps,$pro,$city,$region,$school_adress,$school_xm,$school_phone,$school_jj,$school_xc,$school_zj];
        $data=guolu($code, $rcode);
        if($data!=null)
        {
            return $com->up($tid, $data);
        }
    }
    //删除
    public function del($id){
        $com=new Mschool();
        return $com->del($id);
    }
    ///查询
    public function seluid($tid)
    {
        $com=new Mschool();
        return $com->seluid($tid);
    }
    ///设置管理密码
    public function setpwd($tid,$pwd){
        $com=new Mschool();
        return $com->setpwd($tid, $pwd);
    }
    ///条件查询
    public function sellist($shool_name)
    {
        $com=new Mschool();
        return $com->sellist($shool_name);
    }
    ///查询机构名称是否存在
    public function selname($shool_name)
    {
        $com=new Mschool();
        return $com->selname($shool_name);
    }
}