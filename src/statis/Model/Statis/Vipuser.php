<?php
namespace Statis\Model\Statis;
use PhalApi\Model\NotORMModel as Model;

class Vipuser extends Model
{
    
    ///查询vipuser记录日志
    public function vipuser_log($act,$strtime,$endtime)
    {
        $sql="select count(*) as num,DATE_FORMAT(time,'%Y-%m-%d') as dtime from api_team_user_log where act='".$act."' and time>'".$strtime."' and time<'".$endtime."' group by dtime";
        return $this->getORM()->queryAll($sql);
    }
    ///添加
    public function add($num,$time,$type)
    {
        $data=array(
            'num'=>$num,
            'time'=>$time,
            'type'=>$type
        );
        $this->getORM()->insert($data);
    }
    ///是否统计
    public function is_sel($type)
    {
        $sql="select max(time) as time from api_statis_vipuser where type='".$type."'";
        return $this->getORM()->queryAll($sql);
    }
    ////查询vipuser推广人数
    public function vipusernum($strtime,$endtime)
    {
        $sql="select count(*) num,DATE_FORMAT(end_time,'%Y-%m-%d') as dtime from api_order_order where code_type='PromotionCode' and end_time>'".$strtime."' and end_time<'".$endtime."' and order_status='200' group by dtime";
        return $this->getORM()->queryAll($sql);
    }
    ////统计vipuser
    public function vipusertongji($type,$strtime,$endtime){
        $sql="select * from api_statis_vipuser where time>'".$strtime."' and time<'".$endtime."' and type='".$type."'  order by time asc";
        return $this->getORM()->queryAll($sql);
    }
}