<?php
namespace Statis\Model\Statis;
use PhalApi\Model\NotORMModel as Model;
class order extends Model
{
    /**
     * 查询消费金额
     */
    public function findMoneyAll(){
        $model = $this->getORM();
        $sql = "SELECT sum(money) as money FROM api_order_order WHERE order_status=200";
        $data = $model->queryAll($sql);
        return $data;

    }
    /**
     * 查询消费总人次
     */
    public function findUserAll(){
        $model = $this->getORM();
        $sql = "SELECT *  FROM api_order_order WHERE order_status=200 GROUP BY `uid`";
        $data = $model->queryAll($sql);
        return count($data);
    }
    /**
     * 展示支付成功的列表
     */
    public function orderAlls(){
        $model = $this->getORM();
        $sql = "SELECT * FROM api_order_order WHERE order_status=200 ORDER BY end_time DESC LIMIT 20";
        $data = $model->queryAll($sql);
        return $data;
    }
}