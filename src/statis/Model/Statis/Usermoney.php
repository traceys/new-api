<?php
namespace Statis\Model\Statis;
use PhalApi\Model\NotORMModel as Model;

class Usermoney extends Model
{
    ///查询该会员的分会
    public function seluser($uid=null)
    {
        $sql="select * from api_configure_watch where type='branch' and on_status=1 and statu=1";
        if($uid!=null){
            $sql.=" and value like '%\"mid\":\"".$uid."\"%' ";
        }
        return  $this->getORM()->queryAll($sql);
    }
    ///注册区域得找会员
    public function reguser($region)
    {
        $sql="select * from api_user_history where statu=1  and value like '%注册%' and ipadress in($region)";
        return $this->getORM()->queryAll($sql);
    }
    ///查询订单统计
    public function orderstatis($region,$strtime,$endtime)
    {
        $sql="select sum(money) as money,DATE_FORMAT(end_time,'%Y-%m-%d') as dtime from api_order_order where `add` in(".$region.") and  end_time>'".$strtime."' and end_time<'".$endtime."' and order_status=200 group by dtime";
        return $this->getORM()->queryAll($sql);
    }
    ///添加统计记录
    public function add($name,$type,$key,$conmoney,$time)
    {
        $data=array(
            'name'=>$name,
            'type'=>$type,
            'key'=>$key,
            'conmoney'=>$conmoney,
            'time'=>$time
        );  
        return $this->getORM()->insert($data);
    }
    ///查询统计记录
    public function sel($name,$type,$key,$strtime,$endtime)
    {
        $sql="select sum(conmoney) as conmoney from api_statis_usermoney where name='".$name."' and type='".$type."' and `key`='".$key."' and time>'".$strtime."' and time<'".$endtime."'";
        return $this->getORM()->queryAll($sql);
    }
    ///判断会员是否统计
    public function isstatis($name,$type,$key)
    {
        $sql="select max(time) as time from api_statis_usermoney where  name='".$name."' and type='".$type."' and `key`='".$key."'";
        return $this->getORM()->queryAll($sql);
    }
    ///机构会员已激活人数
    public function mechjhcount()
    {
        $sql="select count(*) as num from api_user_mechanism where rec_status=1";   
        return $this->getORM()->queryAll($sql);
    }
    ///机构会员待激活人数
    public function mechdjhcount()
    {
        $sql="select count(*) as num from api_user_mechanism where rec_status=0 and code!=''";
        return $this->getORM()->queryAll($sql);
    }
    ///机构会员激活人数续约
    public function mechxjhcount()
    {
        $sql="select count(*) as num from api_user_mechanism where rec_status=1 and end_time<'".date('Y-m-d H:i:s',time())."'";
        return $this->getORM()->queryAll($sql);
    }
    ///会员注册地区排行
    public function regcount()
    {
        $sql="select count(*) as num,province from api_user_history where act like '%注册%' group by province order by num desc";
        return $this->getORM()->queryAll($sql); 
    }
 
    ///会员登录人数
    public function entercount($strtime,$endtime)
    {
        $sql="select distinct uid from api_user_history where act like '%登录%' and time>'".$strtime."'  and time<'".$endtime."'";
        return $this->getORM()->queryAll($sql);
    }
}