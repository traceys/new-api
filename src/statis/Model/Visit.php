<?php
namespace Statis\Model;
use PhalApi\Model\NotORMModel as Model;
class Visit extends Model
{
    /**
     * 导入访问历史
     */
    public function _insert($data){
        // $model = $this->getORM();
        $rs=\PhalApi\DI()->notorm->visit_api_log
            ->insert_multi($data);
        // $sql = "SELECT sum(money) as money FROM api_order_order WHERE order_status=200";
        // $data = $model->queryAll($sql);
        return $rs;

    }
    /**
     * 统计方法的最后统计时间
     */
    public function lastTime($name,$type,$numType){
        $data = \PhalApi\DI()->notorm->visit_count
            ->select("time")
            ->where(["name"=>$name,"type"=>$type,"numType"=>$numType])
            ->order("time desc")
            ->fetchOne();
        return $data?$data["time"]:false;
    }
    /**
     * 统计用户每小时的接口访问数
     */
    public function countVistByTimeFromUser($startTime="1970-01-01"){
        $startTime=$this->lastTime("userVisit","userVisit","visit");
        if($startTime==false){
            $startTime="1970-01-01";
        }
        $model = $this->getORM();
        $sql = "select id,DATE_FORMAT(time,'%Y-%m-%d %H:59:59')  as Htime,count(id) as num from api_visit_api_log 
        where uid!=0 and time>? and time <?
        group by Htime";
        $data = $model->queryAll($sql,[$startTime,$this->Htime()]);
        for($n=0;$n<count($data);$n++){
            \PhalApi\DI()->notorm->visit_count->insert(["name"=>"userVisit","type"=>"userVisit","numType"=>"visit","time"=>$data[$n]["Htime"],"num"=>$data[$n]["num"]]);
        }
        return $data;
    }

    /**
     * 统计每小时的用户IP访问数
     */
    public function countVistByTimeFromIP($startTime="1970-01-01"){
        $startTime=$this->lastTime("userVisit","userVisit","IP");
        if($startTime==false){
            $startTime="1970-01-01";
        }
        $model = $this->getORM();
        $sql = "select id,DATE_FORMAT(time,'%Y-%m-%d %H:59:59')  as Htime,count(distinct IP) as IPnum,count(id) as num from api_visit_api_log where uid!=0 and time>? and time <? group by Htime";
        $data = $model->queryAll($sql,[$startTime,$this->Htime()]);
        for($n=0;$n<count($data);$n++){
            \PhalApi\DI()->notorm->visit_count->insert(["name"=>"userVisit","type"=>"userVisit","numType"=>"IP","time"=>$data[$n]["Htime"],"num"=>$data[$n]["IPnum"]]);
        }
        return $data;
    }

    /**
     * 统计每小时的用户地址访问数
     */
    public function countVistByTimeFromAddress($startTime="1970-01-01"){
        $startTime=$this->lastTime("countTime","regionVisit","IP");
        if($startTime==false){
            $startTime="1970-01-01";
        }
        $hadLength=0;
        $hadData=[];
        $model = $this->getORM();
        $sql = "select id,DATE_FORMAT(time,'%Y-%m-%d %H:59:59')  as Htime,count(distinct IP) as IPnum,count(id) as num,region from api_visit_api_log where uid!=0 and time>? and time <? group by Htime,region";
        $data = $model->queryAll($sql,[$startTime,$this->Htime()]);
        $hadLength+=count($data);
        for($n=0;$n<count($data);$n++){
            $hadData[]=["name"=>$data[$n]["region"],"type"=>"regionVisit","numType"=>"IP","time"=>$data[$n]["Htime"],"num"=>$data[$n]["IPnum"]];
        }
        for($n=0;$n<count($data);$n++){
            $hadData[]=["name"=>$data[$n]["region"],"type"=>"regionVisit","numType"=>"visit","time"=>$data[$n]["Htime"],"num"=>$data[$n]["num"]];
        }
        //市级地址统计
        $model = $this->getORM();
        $sql = "select id,DATE_FORMAT(time,'%Y-%m-%d %H:59:59')  as Htime,count(distinct IP) as IPnum,count(id) as num,region,city from api_visit_api_log where uid!=0 and time>? and time <? group by Htime,region,city";
        $data = $model->queryAll($sql,[$startTime,$this->Htime()]);
        $hadLength+=count($data);
        for($n=0;$n<count($data);$n++){
            $hadData[]=["name"=>$data[$n]["region"].$data[$n]["city"],"type"=>"regionVisit","numType"=>"IP","time"=>$data[$n]["Htime"],"num"=>$data[$n]["IPnum"]];
        }
        for($n=0;$n<count($data);$n++){
            $hadData[]=["name"=>$data[$n]["region"].$data[$n]["city"],"type"=>"regionVisit","numType"=>"visit","time"=>$data[$n]["Htime"],"num"=>$data[$n]["num"]];
        }

        //县级地址统计
        // $model = $this->getORM();
        // $sql = "select id,DATE_FORMAT(time,'%Y-%m-%d %H:59:59')  as Htime,count(distinct IP) as IPnum,count(id) as num,region,city,area from api_visit_api_log where uid!=0 and time>? and time <? group by Htime,region,city,area";
        // $data = $model->queryAll($sql,[$startTime,$this->Htime()]);
        // $hadLength+=count($data);
        // for($n=0;$n<count($data);$n++){
        //     \PhalApi\DI()->notorm->visit_count->insert(["name"=>$data[$n]["region"].$data[$n]["city"].$data[$n]["area"],"type"=>"regionVisit","numType"=>"IP","time"=>$data[$n]["Htime"],"num"=>$data[$n]["IPnum"]]);
        // }
        // for($n=0;$n<count($data);$n++){
        //     \PhalApi\DI()->notorm->visit_count->insert(["name"=>$data[$n]["region"].$data[$n]["city"].$data[$n]["area"],"type"=>"regionVisit","numType"=>"visit","time"=>$data[$n]["Htime"],"num"=>$data[$n]["num"]]);
        // }
        if($hadLength>0){
            \PhalApi\DI()->notorm->visit_count
            ->insert_multi($hadData);
            $times=time();
            $times=intval($times/(3600))*3600-1;
            $date=dateTime($times);
            \PhalApi\DI()->notorm->visit_count->insert(["name"=>"countTime","type"=>"regionVisit","numType"=>"IP","time"=>$date,"num"=>1]);
        }
        
        return $hadLength;
    }

    /**
     * 统计每小时的接口访问量
     */
    public function countVistByTimeFromApi($startTime="1970-01-01"){
        $startTime=$this->lastTime("countTime","apiVisit","IP");
        if($startTime==false){
            $startTime="1970-01-01";
        }
        $hadLength=0;
        $hadData=[];
        $model = $this->getORM();
        $sql = "select id,service,DATE_FORMAT(time,'%Y-%m-%d %H:59:59')  as Htime,avg(useTime) as useTime,count(id) as num from api_visit_api_log where uid!=0 and time>? and time <? group by Htime,service";
        $data = $model->queryAll($sql,[$startTime,$this->Htime()]);
        $hadLength+=count($data);
        for($n=0;$n<count($data);$n++){
            $hadData[]=["name"=>$data[$n]["service"],"type"=>"apiVisit","numType"=>"useTime","time"=>$data[$n]["Htime"],"num"=>$data[$n]["useTime"]];
        }
        for($n=0;$n<count($data);$n++){
            $hadData[]=["name"=>$data[$n]["service"],"type"=>"apiVisit","numType"=>"visit","time"=>$data[$n]["Htime"],"num"=>$data[$n]["num"]];
        }
        

        
        if($hadLength>0){
            \PhalApi\DI()->notorm->visit_count
            ->insert_multi($hadData);
            $times=time();
            $times=intval($times/(3600))*3600-1;
            $date=dateTime($times);
            \PhalApi\DI()->notorm->visit_count->insert(["name"=>"countTime","type"=>"apiVisit","numType"=>"IP","time"=>$date,"num"=>1]);
        }
        
        return $hadLength;
    }
    /**
     * 对外统计抽象接口
     * H 小时为单位分组
     * D 天为单位分组
     * A 全部数据的统计
     */
    public function countVisit($ctype,$count,$type="H",$startTime="1970-01-01",$endTime=false){
        $endTime=$endTime?$endTime:dateTime();
        $rs=[];
        switch($type){
            case "H":
            $rs=[];
                break;
            case "D":
            $rs=[];
                break;
            case "A":
                $rs=$this->countVisitA($ctype,$count,$startTime,$endTime);
                break;
        }
        return $rs;
    }

    public function countVisitA($ctype,$count,$startTime,$endTime){
        $data = \PhalApi\DI()->notorm->visit_count
            ->select("sum(num) as num")
            ->where(["type"=>$ctype,"numType"=>$count])
            ->where(" time>? and time<?",[$startTime,$endTime])
            ->order("time desc")
            ->fetchOne();
        return $data["num"]!=null?$data["num"]:0;
    }
    /**
     * 获取当前时间的小时标准时间格式 如 21:05 取 21点
     */
    public function Htime(){
        $times=time();
        $times=intval($times/(3600))*3600;
        $date=dateTime($times);
        return $date;
    }
}