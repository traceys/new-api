<?php
namespace Statis\Domain;
use Statis\Model\Statis\order as order;
class Orderdomain
{
    //数据统计
    public function orderstatic(){
        $model = new order();
        $data['money'] = $model->findMoneyAll()['0']['money'];
        $data['usernumber'] = $model->findUserAll();
        return $data;

    }
    //展示支付数据
    public function orderAll(){
        $model = new order();
        $data = $model->orderAlls();
        foreach ( $data as $key => $value){
            $data[$key]['timeLength'] = format_date(strtotime($value['end_time']));
            if ($value['end_time'] == '0000-00-00 00:00:00'){
                $data[$key]['timeLength']  = '未知';
            }
        }
        return $data;

    }


}