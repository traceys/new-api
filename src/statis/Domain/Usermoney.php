<?php
namespace Statis\Domain;
use Statis\Model\Statis\Usermoney as Musermoney;
use Statis\Model\Statis\Vipuser;
class Usermoney
{
    ///注册区域得找会员
    public function reguser($region)
    {
        $money=new Musermoney();
        return $money->reguser($region);
    }
    ///会员消费，分成总金额统计记录
    public function statislog()
    {
         $money=new Musermoney();
         $branch=$money->seluser();
         if($branch!=null)
         {
            for ($i=0;$i<count($branch);$i++)
            {
                $data="";
                $value=json_decode($branch[$i]["value"],true)["manageData"];
                if($value!=null)
                {
                    $region=json_decode($value,true);
                    for($j=0;$j<count($region);$j++)
                    {
                        $dz="中国".$region[$j][0].$region[$j][1];
                        $dzs="中国".$region[$j][0].$region[$j][1].$region[$j][2];
                        $data.="'$dz','$dzs',";
                    }
                    $data=rtrim($data,",");
                    $isstatis=$money->isstatis($branch[$i]["key"], "fenhui", "xiaofei");
                    if($isstatis[0]["time"]!=null)
                    {
                       $strtime=$isstatis[0]["time"];
                       $endtime=date('Y-m-d',time()); 
                    }
                    else
                    {
                        $strtime=json_decode($branch[$i]["value"],true)["createTime"];
                        $endtime=date('Y-m-d',time());
                    }
                    $order=$money->orderstatis($data, $strtime, $endtime);
                    if($order)
                    {
                         for ($k=0;$k<count($order);$k++)
                         {
                           $money->add($branch[$i]["key"], "fenhui", "xiaofei",$order[$k]["money"],$order[$k]["dtime"]); 
                         }
                    }
                }
            }
        }
    }
    ///查询会员消费统计
    public function selstatis($uid,$strtime,$endtime)
    {
        $money=new Musermoney();
        $branch=$money->seluser($uid);
        if($branch!=null)
        {
//             $strtime=json_decode($branch[0]["value"],true)["createTime"];
//             $endtime=date('Y-m-d H:i:s',time());
            return $money->sel($branch[0]["key"],"fenhui","xiaofei",$strtime,$endtime);
        }
    }
    ///机构会员已激活，待激活，待续约人数统计;
    public function mechcount()
    {
        $money=new Musermoney();
        $jh=$money->mechjhcount()[0]["num"];
        $djh=$money->mechdjhcount()[0]["num"];
        $xjh=$money->mechxjhcount()[0]["num"];
        $data["jh"]=$jh;
        $data["djh"]=$djh;
        $data["xjh"]=$xjh;
        return $data;
    }
    ///会员注册地区排行
    public function regcount()
    {
        $money=new Musermoney();
         return $money->regcount();
    }
    ///会员登录人数
    public function entercount($strtime,$endtime)
    {
        $money=new Musermoney();
        return count($money->entercount($strtime,$endtime));
    }
}