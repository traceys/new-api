<?php
namespace Statis\Domain;
use Statis\Model\Visit as ModelVisit;
class Visit
{
    ///查询vip会员新增，到期，续费，推广人数
    public function loadVisitHis()
    {
        $model=new ModelVisit();
        $data=getVisitLog();
        $rs=$model->_insert($data);
        return $rs;
    }
    
    public function countVisit(){
        $model=new ModelVisit();
        // $data=getVisitLog();
        $rs=$model->countVistByTimeFromApi();
        $model->countVistByTimeFromUser();
        $model->countVistByTimeFromIP();
        $model->countVistByTimeFromAddress();
        // $model->countVistByTimeFromUser();
        // $model->countVistByTimeFromUser();
        return $rs;
    }

    public function countVisitApi($ctype,$count,$type="H",$startTime="1970-01-01",$endTime=false){
        $model=new ModelVisit();
        // $data=getVisitLog();
        $rs=$model->countVisit($ctype,$count,$type,$startTime,$endTime);
        return $rs;
    }
}