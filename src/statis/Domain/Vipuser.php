<?php
namespace Statis\Domain;
use Statis\Model\Statis\Vipuser as Mvipuser;
class Vipuser
{
    ///查询vip会员新增，到期，续费，推广人数
    public function sel_vipuser($type)
    {
        $strtime=date('Y-m-d',strtotime(date('Y-m-d',time()))-30*24*60*60);
        $endtime=date('Y-m-d',time());
        $money=new Mvipuser();
        $data=$money->vipusertongji($type,$strtime, $endtime);
        return $data;
    }
    ///添加进入记录日志
    public function add_vipuser()
    {
        $money=new Mvipuser();
        $xztime=$money->is_sel('xz');
        if($xztime[0]["time"]!=null)
        {
            $xzstrtime=$xztime[0]["time"];
            $xzendtime=date('Y-m-d',time());
        }
        else
        {
            $xzstrtime=date('Y-m-d',0000-00-00);
            $xzendtime=date('Y-m-d',time());
        }
        $money=new Mvipuser();
        $xz=$money->vipuser_log("create",$xzstrtime,$xzendtime);
        $dqtime=$money->is_sel('dq');
        if($dqtime[0]["time"]!=null)
        {
            $dqstrtime=$dqtime[0]["time"];
            $dqendtime=date('Y-m-d',time());
        }
        else
        {
            $dqstrtime=date('Y-m-d',0000-00-00);
            $dqendtime=date('Y-m-d',time());
        }
        $dq=$money->vipuser_log("timeout",$dqstrtime,$dqendtime);
        $xftime=$money->is_sel('xf');
        if($xftime[0]["time"]!=null)
        {
            $xfstrtime=$xftime[0]["time"];
            $xfendtime=date('Y-m-d',time());
        }
        else
        {
            $xfstrtime=date('Y-m-d',0000-00-00);
            $xfendtime=date('Y-m-d',time());
        }
        $xf=$money->vipuser_log("update",$xfstrtime,$xfendtime);
        $tgtime=$money->is_sel('tg');
        if($tgtime[0]["time"]!=null)
        {
            $tgstrtime=$tgtime[0]["time"];
            $tgendtime=date('Y-m-d',time());
        }
        else
        {
            $tgstrtime=date('Y-m-d',0000-00-00);
            $tgendtime=date('Y-m-d',time());
        }
        $tg=$money->vipusernum($tgstrtime, $tgendtime);
        $data["xz"]=$xz;
        $data["dq"]=$dq;
        $data["xf"]=$xf;
        $data["tg"]=$tg;
        for ($i=0;$i<count($data["xz"]);$i++)
        {
            $money->add($data["xz"][$i]["num"], $data["xz"][$i]["dtime"], "xz");    
        }
        for ($j=0;$j<count($data["dq"]);$j++)
        {
            $money->add($data["dq"][$j]["num"], $data["dq"][$j]["dtime"], "dq");
        }
        for ($h=0;$h<count($data["xf"]);$h++)
        {
            $money->add($data["xf"][$h]["num"], $data["xf"][$h]["dtime"], "xf");
        }
        for ($k=0;$k<count($data["tg"]);$k++)
        {
            $money->add($data["tg"][$k]["num"], $data["tg"][$k]["dtime"], "tg");
        }
    }
}