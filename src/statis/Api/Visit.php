<?php
namespace Statis\Api;
use PhalApi\Api;
use Statis\Domain\Visit as DomainVisit;
class Visit extends Api{

    public function getRules()
    {
        return array(
            '*' => array(
                'token' => array('name' => 'token','default' =>'','source' =>'get','desc'=>'用户token'),
            ),
            'countVisitApi' => array(
                'ctype' => array('name' => 'ctype','source' =>'get','desc'=>'用户token'),
                'count' => array('name' => 'count','source' =>'get','desc'=>'用户token'),
                'type' => array('name' => 'type','source' =>'get','desc'=>'用户token'),
                'startTime' => array('name' => 'startTime','default' =>'1970-01-01','source' =>'get','desc'=>'用户token'),
                'endTime' => array('name' => 'endTime','source' =>'get','desc'=>'用户token'),
            ),
        );
    }

    /**
     * 销售总金额和消费人次
     */
    public function load(){

        $Orderdomain = new DomainVisit();
        return $Orderdomain->loadVisitHis();

    }

    /**
     * 消费数据展示
     */
    public function countVisit(){
        $Orderdomain = new DomainVisit();
        return $Orderdomain->countVisit();
    }

    public function countVisitApi(){
        $Orderdomain = new DomainVisit();
        // $data=getVisitLog();
        $rs=$Orderdomain->countVisitApi($this->ctype,$this->count,$this->type,$this->startTime,$this->endTime);
        return $rs;
    }
}