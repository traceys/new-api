<?php
namespace Statis\Api;
use PhalApi\Api;
use Statis\Domain\Orderdomain as Orderdomain;
class Orderlist extends Api{

    public function getRules()
    {
        return array(
            '*' => array(
                'token' => array('name' => 'token','default' =>'','source' =>'get','desc'=>'用户token'),
            ),
        );
    }

    /**
     * 销售总金额和消费人次
     */
    public function selectOrderlist(){

        $Orderdomain = new Orderdomain();
        return $Orderdomain->orderstatic();

    }

    /**
     * 消费数据展示
     */
    public function orderList(){
        $Orderdomain = new Orderdomain();
        return $Orderdomain->orderAll();
    }
}