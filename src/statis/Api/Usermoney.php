<?php
namespace Statis\Api;
use PhalApi\Api;
use Statis\Domain\Usermoney as Dusermoney;
use Statis\Domain\Vipuser;
class Usermoney extends Api
{
    
    public function getRules()
    {
        return array(
            'selstatis' => array(
                'token' => array('name' => 'token','require' => true,'source' =>'get'),
                'stratime' => array('name' => 'stratime','require' => true,'source' =>'get'),
                'endtime' => array('name' => 'endtime','require' => true,'source' =>'get'),
            ),
            'selstatisone' => array(
                'uid' => array('name' => 'uid','require' => true,'source' =>'get'),
                'stratime' => array('name' => 'stratime','require' => true,'source' =>'get'),
                'endtime' => array('name' => 'endtime','require' => true,'source' =>'get'),
            ),
            'vipuser' => array(
                'type' => array('name' => 'type','require' => true,'source' =>'get'),
            ),
            'entercount' => array(
                'startime' => array('name' => 'startime','require' => true,'source' =>'get'),
                'endtime' => array('name' => 'endtime','require' => true,'source' =>'get'),
            ),
         );
    }
    /**
     * 会员消费金额统计
     */
    public function  statisusermoney()
    {
        $money=new Dusermoney();
       return  $money->statislog();
    }
    /**
     * 查询会员消费统计
     */
    public function selstatis()
    {
        $user=apiGet("User.User.Selecttoken",array("searchToken"=>$this->token));
        if($user!=null)
        {
            $money=new Dusermoney();
            return $money->selstatis($user["Id"],$this->stratime,$this->endtime);
        }
        else
        {
            exceptions("token不存在！", -100);
        }
    }
    /**
     * 查询会员消费统计
     */
    public function selstatisone()
    {
        $money=new Dusermoney();
        return $money->selstatis($this->uid,$this->stratime,$this->endtime);
    }
    /**
     *机构会员已激活，待激活，待续约人数统计
     */
    public function mechcount()
    {
        $money=new Dusermoney();
        return $money->mechcount();
    }
    /**
     * 会员注册地区排行
     */
    public function regcount()
    {
        $money=new Dusermoney();
        return $money->regcount();
    }
    /**
     * 查询vip会员新增，到期，续费，推广人数
     */
    public function vipuser()
    {
       $money=new Vipuser();
       return $money->sel_vipuser($this->type);
    }
    /**
     * 记录vip会员新增，到期，续费，推广人数
     */
    public function add_vipuser()
    {
        $money=new Vipuser();
        return $money->add_vipuser();
    }
    /**
     *会员登录人数
     */
    public function entercount()
    {
        $money=new Dusermoney();
        return $money->entercount($this->startime, $this->endtime);
    }
}