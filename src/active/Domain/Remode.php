<?php
namespace Active\Domain;
use PhalApi\Exception\BadRequestException;
use Active;
use  Active\Model\Active_active;
use  Active\Model\Active_group;
use Active\Model\Active_sign;
class Remode {



    public function index($val)
    {
        $model  = new Active_active();

        $id = $val['id'];
        $val = Active\array_remove($val,'id');
        if (!$id){
            $val['uid'] = Active\getCache($_GET['token'])['Id'];
            $val['cretTime'] = Active\timeout(time());
           $data = $model->issue($val);
        }
        if ($id){
           $res =  $model->find($id);
            if (!$res){
                throw new BadRequestException('未找到指定活动',4);
            }
            $val = Active\array_remove($val,'type');
            $data = $model->up($val,$id);
        }
        return $data;

    }

    //发布活动组别
    public function vc($arr,$type="insert"){

        $model = new Active_group();
        $ac = new Active_active();

        if ($type == "insert"){
            $arr['time'] = date("Y-m-d H:i:s",time());

            $money = $arr['money'];

            $curl = new \PhalApi\CUrl(2);

            $data=[
                'type'=>'active',
                'name'=>$ac->find($arr['aid'])['title'],
                'money_type'=>'money',
                'money'=>$arr['money'],
                'num'=>100000000,
                'on_status'=>1,
                'desc'=>json_encode('活动报名')
            ];


            $rs = apiGet('Commodity.commoditys.insertcom',$data);

            if ($rs ){

                $arr['commodity_id'] = $rs['id'];
            }else{
                throw new BadRequestException('商品系统错误',5);
            }
            $arr = Active\array_remove($arr,'money');
            $res = $model->issue($arr);
            return $res;

        }

        if ($type == "update"){
            $id = $arr['id'];

            $arr = Active\array_remove($arr,'id');

            $res = $model->up($arr,$id);
        }

        return $res;

    }
    //展示活动信息即删除
    public function dc($id,$type="find"){
        $model  = new Active_active();
        if ($type == 'find'){
            if(!$id){
                $res =$model->newactive();
            }else{
                $res =  $model->find($id);
            }

            if (!$res){
                throw new BadRequestException('未找到指定活动',5);
            }
        }
        if ($type == "delete"){

            $data = ['statu'=>-1];
            $res = $model->up($data,$id);
        }


        return $res;


    }
    //展示活动组及删除
    public function act($id,$type="find"){
        $model = new Active_group();
        if ($type=='find'){
            $res = $model->find($id);
            if (!$res){
                throw new BadRequestException('未找到指定活动组别信息',5);
            }
        }
        if ($type=='delete'){
            $data = ['statu'=>-1];
            $res = $model->up($data,$id);
        }

        return $res;

    }
    //活动列表
    public function listd($arr){

        $model = new Active_active();
        $sign = new Active_sign();
        $num = 1000;
        $page = ($arr['page']-1)*$num;
        $pages = $arr['page'];
        $arr= Active\array_remove($arr,'page');
        $data =  $model->pagelist($arr,$page,$num);
        //计算报名该活动有多少报名成功的
        foreach ($data as $key => $value){
            $data[$key]['applyNum'] = $sign->findSum($value['id']);
        }
        outputArgs(array('num'=>$model->pagelist($arr,$page,$num,'num'),'pagenum'=>$num,'age'=>$pages));
        return $data;
    }

    //活动报名
    public function applys($arr,$type="issue"){

        if ($type == 'issue'){

        //去掉商品系统查看金额
        $curl = new \PhalApi\CUrl(2);

        $rs = json_decode($curl->post('http://test.yusj.vip/?s=Commodity.commoditys.message',['id'=>$arr['com_id']], 3000));

        if ($rs['ret'] != 200){
            throw new BadRequestException('查看商品出错',8);
        }
        $commo = $rs['data'];

        //生成订单
        $orderdata = [
            'commodity_type'=>$commo['type'],
            'commodity_id'=>$commo['id'],
            'num'=>1
        ];
        $order =  json_decode($curl->post('http://test.yusj.vip/?s=Order.receive.ceroreders',$orderdata, 3000));

        return $order;
        }else if ($type="confirm"){
            //检查订单是否支付
            $id = $arr['order_id'];

//            $curl = new \PhalApi\CUrl(2);
//           $order =  json_decode($curl->post('http://test.yusj.vip/?s=Order.receive.orderfindmess',$id, 3000));
//            if ($order['ret'] != 200){
//                throw new BadRequestException('查看订单出错',6);
//            }
            $data = apiGet('Order.receive.orderfindmess',array('oid'=>$id));

//            if ($data['act_status'] == 2){

                $model = new Active_sign();

                $arr['uid'] = $data['uid'];

                $arr['reg_time'] = Active\timeout(time());

//                $arr = Active\array_remove($arr,'order_id');
                return $model->record($arr);

//            }



        }

    }
    //展示报名列表
    public function  applylist($val){

        $model = new Active_sign();

        $group = new Active_active();

        $data = $model->applyList($val);
        $num  = $model->applyList($val,'num')['0']['num'];

        foreach ($data as $key => $value){

            if (isset($value['uid'])){
                $data[$key]['user'] = apiGet('User.User.Userone',array('uid'=>$value['uid']))['username'];
            }
            $data[$key]['group_name'] = $group->find($value['activity_id'])['title'];
        }

        outputArgs(array('number'=>$num,'pagenum'=>40,'age'=>$val['page']));
        return $data;

    }

//退费审核列表
public function applyRefund($val){
    $model = new Active_sign();

    $data =  $model->premium($val);
    $num =  $model->premium($val,'num')['0']['num'];
    outputArgs(array('number'=>$num,'pagenum'=>40,'age'=>$val['page']));
    return $data;
}

//退费审核处理
public function auditProcess($val){

    $model = new Active_sign();

    $statu = $model->find($val['id']);

    if (!$statu ){
        throw new BadRequestException('未找到指定申请审核',4);
    }

    if ( $statu['audit'] != 0){

        throw new BadRequestException('该审核已经处理过了',3);

    }

    if ($val['audit'] == 2){
        $curl = new \PhalApi\CUrl();
        $orderdata = json_decode($curl->get('http://test.yusj.vip/?s=Order.receive.orderfindmess&oid='.$statu['order_id'], 3000),true)['data'];

        $uid = $statu['uid'];
        $money_type = $orderdata['money_type'];
        $money = $orderdata['money'];
        $rs = json_decode($curl->get('http://test.yusj.vip/index.php?s=Currency.receive.addcursd&uid='.$uid.'&money_type='.$money_type.'&num='.$money.'', 3000),true);

        if ($rs['ret'] != 200){

            throw new BadRequestException('货币退款失败',5);

        }
    }
        $id = $val['id'];

        $val = Active\array_remove($val,'id');

        $where = ['id'=>$id];

        return $model->updates($where,$val);



}

//活动审核是否通过
public function autidaps($val){

    $model = new Active_sign();

    $res =  $model->find($val['id']);

    $pay = $res['pay_status'];
    $arr = [
        '-1'=>'活动正在申请退款中,无法操作',
        '1'=>'该活动申请报名已拒绝请勿重复操作',
        '2'=>'该活动申请报名已成功请勿重复操作'
    ];
    if ($arr[$pay]){
      throw new BadRequestException($arr[$pay],3);
    }
    //拒绝
    if ($val['pay_status'] == 1){
        $curl = new \PhalApi\CUrl();
        $orderdata = json_decode($curl->get('http://test.yusj.vip/?s=Order.receive.orderfindmess&oid='.$res['order_id'], 3000),true)['data'];
        $uid  = $res['uid'];
        $money_type = $orderdata['money_type'];
        $money = $orderdata['money'];
        $rs = json_decode($curl->get('http://test.yusj.vip/index.php?s=Currency.receive.addcursd&uid='.$uid.'&money_type='.$money_type.'&num='.$money.'', 3000),true);

    }

    $id = $val['id'];

    $val = Active\array_remove($val,'id');

    $where = ['id'=>$id];

    return $model->updates($where,$val);

}

//活动下的活动组别
public function acgroup($aid){

    $model = new Active_group();

    return $model->findgroup($aid);

}









}




































?>