<?php
namespace Active\Model;
use PhalApi\Model\NotORMModel as NotORM;
class Active_active extends NotORM {



    //发布活动
    public function issue($arr){

        $model = $this->getORM();


        $data =  $model->insert($arr);
        clearnCache(['active.model.active'],true);
        return $data;
    }
    //修改活动
    public function up($arr,$id){
        $model = $this->getORM();

        $data =  $model->where(['id'=>$id])->update($arr);
        clearnCache(['active.model.active'],true);
        return $data;
    }

    //查找单个
    public function find($id){
        $model = $this->getORM();
        return $model->where(['id'=>$id,'statu'=>1])->fetchOne();

    }
    //分页显示条件活动
    public function pagelist($arr,$page,$num='20',$type='data'){
        $data =  cache(['active.model.active.pagelist',json_encode($arr),$page,$num,$type]);
        if (!$data){
            $model = $this->getORM();
            $data = $model->where(['statu'=>$arr['statu']]);
            if ($arr['type']){
                $data = $data->where(['type'=>$arr['type']]);
            }
            if ($arr['key']){
                $key = $arr['key'];
                $search = $arr['search'];
                $data = $data->where("$key like ?","'%$search%'");
            }
            if ($type=='data'){
                $data = $data->limit($page,$num)->order('id DESC')->fetchAll();
            }
            if ($type == 'num'){
                $data = $data->count('id');
            }
            cache(['active.model.active.pagelist',json_encode($arr),$page,$num,$type],$data);
        }

        return $data;

    }
    //查询最新活动
    public function newactive(){
        $model = $this->getORM();
        $data = $model->where(['statu'=>1])->order('id DESC')->fetchOne();
        return $data;

    }



}