<?php
namespace Active\Model;

use PhalApi\Model\NotORMModel as NotORM;

class Active_sign extends NotORM {



    //报名记录
    public function record($arr){
        $model = $this->getORM();

        $data =  $model->insert($arr);
        clearnCache(['active.model.sign'],true);
        return $data;
    }
    //查询指定活动有多少人报名
    public function findSum($active_id){
        $data = cache(['active.model.sign.findSum',$active_id]);
        if (!$data ){
            $model = $this->getORM();
            $data =  $model->where(['statu'=>1,'activity_id'=>$active_id])->count('id');
            cache(['active.model.sign.findSum',$active_id],$data);
        }
        return $data;

    }
    //展示活动报名列表
    public function applyList($arr,$type='data'){
        $data =  cache(['active.model.sign.applyList',json_encode($arr),$type]);
        if (!$data){
            $model = $this->getORM();
            if ($type == 'data'){
                $sql = "SELECT a.*,b.money,b.way,b.pay_status AS stusa,b.timeout";
            }
            if ($type == 'num'){
                $sql = "SELECT count(*) AS  num";
            }
            $sql =$sql." FROM api_active_sign AS a LEFT JOIN api_order_order AS b ON a.order_id=b.id WHERE a.statu=1 && a.pay_status!=-1";
            if ($arr['uid']){
                $uid = $arr['uid'];
                $sql = $sql." && a.uid=$uid";
            }
            if ($arr['activity_id']){
                $activity_id = $arr['activity_id'];
                $sql = $sql." && a.activity_id=$activity_id";
            }

            if ($arr['pay_status']){
                $pay_status = $arr['pay_status'];
                $sql = $sql." && a.pay_status=$pay_status";
            }

            if ($arr['audit']){
                $audit = $arr['audit'];
                $sql = $sql." && a.audit=$audit";
            }

            if ($arr['key']){
                $key = $arr['key'];
                $value = $arr['value'];
                $sql = $sql." && a.$key LIKE  '%$value%'";
            }

            if ($arr['way']){
                $way = $arr['way'];
                $sql = $sql." && b.way=$way";
            }
            if ($arr['order_id'] != 'false'){
                $order_id = $arr['order_id'];
                $sql = $sql." && a.order_id=$order_id";
            }

            if ($arr['manage'] != 'false'){
                $manage = $arr['manage'];
                $sql = $sql." && a.manage=$manage";
            }

            if (!$arr['order']){

                $sql = $sql." ORDER BY a.id DESC";
            }
            if ($type =='data'){
                $num = 40;
                $page = ($arr['page']-1)*$num;
                $sql = $sql." LIMIT $page,$num ";
            }
            $data = $model->queryAll($sql, array());
            cache(['active.model.sign.applyList',json_encode($arr),$type],$data);
        }
        return $data;

    }
    //展示退费管理
    public function premium($val,$type='data'){
        $arr = cache(['active.model.sign.premium',json_encode($val),$type]);
        if (!$arr){
            $model = $this->getORM();
            if ($type == 'data'){
                $sql = "SELECT a.*,b.way,b.timeout,b.money";
            }
            if ($type == 'num'){
                $sql = "SELECT count(*) AS num";
            }
            $sql = $sql." FROM api_active_sign AS a LEFT JOIN api_order_order AS b  ON a.order_id=b.id WHERE a.statu=1 && a.pay_status=-1";
            //支付方式
            if ($val['way']){
                $way = $val['way'];
                $sql = $sql." && b.way=$way";
            }
            //处理状态
            if ($val['audit']){
                $audit = $val['audit'];
                $sql = $sql." && a.audit=$audit";
            }
            //处理人
            if ($val['manage']){
                $menage = $val['manage'];
                $sql = $sql." && a.manage=$menage ";
            }
            //搜索
            if ($val['key']){
                $key = $val['key'];
                $search = $val['search'];
                $sql = $sql." && a.$key='$search'";
            }

            if (!$val['order']){

                $sql = $sql." ORDER BY a.id DESC";
            }
            if ($type == 'data'){
                $num = 40;
                $page = ($val['page']-1)*$num;
                $sql = $sql." LIMIT $page,$num";
            }
            $arr = $model->queryAll($sql, array());
            cache(['active.model.sign.premium',json_encode($val),$type],$arr);
        }

        return $arr;

    }

//查询指定报名信息
public function find($id){
    $model = $this->getORM();

   return $model->where(['id'=>$id,'statu'=>1])->fetchOne();

}
//update 指定信息
public function updates($where,$data){

    $model = $this->getORM();
    $data =  $model->where($where)->update($data);
    clearnCache(['active.model.sign'],true);
    return $data;

}

}