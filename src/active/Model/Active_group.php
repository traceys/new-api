<?php
namespace Active\Model;

use PhalApi\Model\NotORMModel as NotORM;

class Active_group extends NotORM {



    //发布活动别
    public function issue($arr){
        $model = $this->getORM();
        $data =  $model->insert($arr);
        clearnCache(['active.model.group'],true);
        return $data;

    }

    //查找单个活动组信息
    public function find($id){
        $model = $this->getORM();
        return $model->where(['id'=>$id,'statu'=>1])->fetchOne();
    }

    //修改活动组别
    public function up($arr,$id){
        $model = $this->getORM();
        $data =  $model->where(['id'=>$id])->update($arr);
        clearnCache(['active.model.group'],true);
        return $data;

    }
    //通过活动ID查找活动组信息
    public  function findgroup($aid){
        $data = cache(['active.model.group.findgroup',$aid]);
        if (!$data){
            $model = $this->getORM();
            $data =  $model->where(['aid'=>$aid,'statu'=>1])->fetchAll();
            cache(['active.model.group.findgroup',$aid],$data);
        }
        return $data;
    }




}