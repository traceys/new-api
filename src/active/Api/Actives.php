<?php
namespace Active\Api;
use PhalApi\Api;
use Active\Domain\Remode as Activesd;
use Active;
use PhalApi\Exception\BadRequestException;
class Actives extends Api{

    private static $data = null;

    public function __construct()
    {
//        return 123;
        self::getDomainAdd();
        self::getToken();
    }

    //实例化domin层
    static  private  function getDomainAdd(){
        if (self::$data === null){
            self::$data = new  Activesd();
        }

    }
    //得到token值
    static private  function getToken(){


        $token = isset($_GET['token'])?$_GET['token']:'';


        if (!Active\getCache($token)){

            $url = "test.yusj.vip?s=Demo.User.Selecttoken&searchToken=$token&token=$token";
            $res = Active\curls($url);

            if ($res['ret'] == 200 ){
                Active\setCache($token,$res['data']);
            }
        }

    }
    //记录信息验证
    public function getRules() {
        return array(
            '*' => array(
                'token' => array('name' => 'token','source'=>'get','desc' => '用户身份token'),
            ),
            'publish' => array(
                'id' => array('name' => 'id','source'=>'get','default'=>false,'desc' => 'ID'),
                'title' => array('name' => 'title','source'=>'get','require'=>true,'desc' => '名称'),
                'body' => array('name' => 'body','source'=>'get','require'=>true,'desc' => '内容'),
                'img' => array('name' => 'img','source'=>'get','require'=>true,'desc'=> '图片地址'),
                'type' => array('name' => 'type','source'=>'get','default'=>false,'desc'=> '类型'),
                'explain' => array('name' => 'explain','source'=>'get','default'=>false,'desc'=> '说明'),
            ),
            'publigrop' => array(
                'id' => array('name' => 'id','source'=>'get','type' => 'int','require'=>true,'desc' => '活动ID'),
                'reg_end_time' => array('name' => 'reg_end_time','source'=>'get','require'=>true,'desc' => '活动报名截止日期'),
                'start_time' => array('name' => 'start_time','source'=>'get','require'=>true,'desc' => '开始时间'),
                'end_time' => array('name' => 'end_time','source'=>'get','require'=>true,'desc' => '结束时间'),
                'address' => array('name' => 'address','source'=>'get','require'=>true,'desc' => '活动地址'),
                'money' => array('name' => 'money','source'=>'get','type'=>'int','require'=>true,'desc' => '价格'),
                'reg_file' => array('name' => 'reg_file','source'=>'get','require'=>true,'desc' => '报名文件'),
            ),
            'message' => array(
                'id' => array('name' => 'id','source'=>'get','require'=>0,'desc' => '活动ID'),
                'type' => array('name' => 'type','source'=>'get','default'=>'find','desc' => '类型'),
            ),
            'activegroup' => array(
                'id' => array('name' => 'id','source'=>'get','type' => 'int','require'=>true,'desc' => '活动组别ID'),
                'type' => array('name' => 'type','source'=>'get','default'=>'find','desc' => '类型'),
            ),
            'inset' => array(
                'title' => array('name' => 'title','source'=>'get','require'=>true,'desc' => '活动组别ID'),
                'body' => array('name' => 'body','source'=>'get','require'=>true,'desc' => '类型'),
                'img' => array('name' => 'img','source'=>'get','require'=>true,'desc' => '类型'),
                'type' => array('name' => 'type','source'=>'get','require'=>true,'desc' => '类型'),
                'id' => array('name' => 'id','source'=>'get','default'=>false,'desc' => 'ID'),
            ),
            'updategrop' => array(
                'id' => array('name' => 'id','source'=>'get','type' => 'int','require'=>true,'desc' => '活动ID'),
                'reg_end_time' => array('name' => 'reg_end_time','source'=>'get','require'=>true,'desc' => '活动报名截止日期'),
                'start_time' => array('name' => 'start_time','source'=>'get','require'=>true,'desc' => '开始时间'),
                'end_time' => array('name' => 'end_time','source'=>'get','require'=>true,'desc' => '结束时间'),
                'address' => array('name' => 'address','source'=>'get','require'=>true,'desc' => '活动地址'),
                'reg_file' => array('name' => 'reg_file','source'=>'get','require'=>true,'desc' => '报名文件'),
            ),
            'activelist' => array(
                'type' => array('name' => 'type','source'=>'get','default'=>false,'desc' => '活动类型'),
                'status' => array('name' => 'status','source'=>'get','default'=>1,'desc' => '活动状态'),
                'page' => array('name' => 'page','source'=>'get','require'=>true,'desc' => '活动页数'),
                'key' => array('name' => 'key','source'=>'get','default'=>false,'desc' => '搜索的key'),
                'search' => array('name' => 'search','source'=>'get','default'=>false,'desc' => '搜索的值'),
//                'pay_status' => array('name' => 'pay_status','source'=>'get','default'=>false,'desc' => '支付状态'),
//                'way' => array('name' => 'way','source'=>'get','default'=>false,'desc' => '支付方式'),
//                'audit' => array('name' => 'audit','source'=>'get','default'=>false,'desc' => '审核状态'),
            ),
            'addapply' => array(
                'com_id' => array('name' => 'type','source'=>'get','require'=>true,),
            ),
            'apply' => array(
                'activity_id' => array('name' => 'activity_id','source'=>'get','type' => 'int','require'=>true,'desc' => '活动ID'),
                'com_id' => array('name' => 'com_id','source'=>'get','require'=>true,'desc' => '活动下的商品ID'),
                'order_id' => array('name' => 'order_id','source'=>'get','require'=>true,'desc' => '订单ID'),
                'name' => array('name' => 'name','source'=>'get','require'=>true,'desc' => '名字'),
                'telphone' => array('name' => 'telphone','source'=>'get','require'=>true,'desc' => '手机号'),
                'com' => array('name' => 'com','source'=>'get','require'=>true,'desc' => '单位名'),
                'email' => array('name' => 'email','source'=>'get','require'=>true,'desc' => '邮箱地址'),
                'des' => array('name' => 'des','source'=>'get','require'=>true,'desc' => '备注'),
            ),
            'userList' => array(
                'uid' => array('name' => 'uid','source'=>'get','default'=>false,'desc' => '用户ID'),
                'activity_id' => array('name' => 'activity_id','source'=>'get','default'=>false,'desc' => '活动ID'),
                'pay_status' => array('name' => 'pay_status','source'=>'get','default'=>false,'desc' => '状态'),
                'order' => array('name' => 'order','source'=>'get','default'=>1,'desc' => '排序值'),
                'page' => array('name' => 'page','source'=>'get','default'=>1,'desc' => '当前页数'),
                'order_id' => array('name' => 'order_id','source'=>'get','default'=>'false','desc' => '审核状态'),
                'manage' => array('name' => 'manage','source'=>'get','default'=>'false','desc' => '管理者'),
                'way' => array('name' => 'way','source'=>'get','require'=>true,'default'=>false,'desc' => '支付方式'),
                'audit' => array('name' => 'audit','source'=>'get','default'=>false,'desc' => '审核状态'),
                'key' => array('name' => 'key','source'=>'get','default'=>'','desc' => '审核状态'),
                'value' => array('name' => 'value','source'=>'get','default'=>'','desc' => '审核状态'),
            ),
            'refundAudit' => array(
                'way' => array('name' => 'way','source'=>'get','default'=>false,'desc' => '支付方式'),
                'audit' => array('name' => 'audit','source'=>'get','default'=>false,'desc' => '审核状态'),
                'manage' => array('name' => 'manage','source'=>'get','default'=>false,'desc' => '处理人'),
                'order' => array('name' => 'order','source'=>'get','default'=>1,'desc' => '排序值'),
                'page' => array('name' => 'page','source'=>'get','default'=>1,'desc' => '当前页数'),
                'key' => array('name' => 'key','source'=>'get','default'=>null,'desc' => '搜索的key'),
                'search' => array('name' => 'search','source'=>'get','default'=>null,'desc' => '搜索的值'),
            ),
            'auditProcessing' => array(
                'audit' => array('name' => 'audit','source'=>'get','require'=>true,'desc' => '审核状态'),
                'manage' => array('name' => 'manage','source'=>'get','require'=>true,'desc' => '处理人'),
                'id' => array('name' => 'id','source'=>'get','require'=>true,'desc' => 'ID'),
            ),
            'auditApp' => array(
                'pay_status' => array('name' => 'pay_status','source'=>'get','require'=>true,'desc' => '状态'),
                'id' => array('name' => 'id','source'=>'get','require'=>true,'desc' => 'ID'),
                'des' => array('name' => 'des','source'=>'get','require'=>true,'desc' => '处理结果原因'),
            ),
            'group' => array(
                'aid' => array('name' => 'aid','source'=>'get','require'=>true,'desc' => 'ID'),
            ),
        );
    }

    //活动修改和发布
    public function publish()
    {

        $arr = [
            'title'=>$this->title,
            'id'=>$this->id,
            'img'=>$this->img,
            'body'=>$this->body,
            'type'=>$this->type,
            'explain'=>$this->explain
        ];

        return self::$data->index($arr);

    }
    //发布活动组别
    public function publigrop(){


        $arr = [
            'aid'=>$this->id,
            'reg_end_time'=>$this->reg_end_time,
            'start_time'=>$this->start_time,
            'end_time'=>$this->end_time,
            'address'=>$this->address,
            'money'=>$this->money,
            'reg_file'=>$this->reg_file,
        ];
        return self::$data->vc($arr);

    }
    //活动信息
    public function message(){


        return self::$data->dc($this->id,$this->type);
}
    //活动组别信息
    public function activegroup(){

        return self::$data->act($this->id,$this->type);


    }
    //添加新的活动
    public function inset(){

        $arr = [
            'title'=>$this->title,
            'body'=>$this->body,
            'img'=>$this->img,
            'type'=>$this->type,
        ];
        return self::$data->index($arr);

    }
    //修改活动组别
    public function updategrop(){

        $arr = [
            'id'=>$this->id,
            'reg_end_time'=>$this->reg_end_time,
            'start_time'=>$this->start_time,
            'end_time'=>$this->end_time,
            'address'=>$this->address,
            'reg_file'=>$this->reg_file,
        ];
        return self::$data->vc($arr,$type='update');
    }

//活动列表
public function activelist(){

    $arr = [
        'type'=>$this->type,
        'statu'=>$this->status,
        'page'=>$this->page,
        'key'=>$this->key,
        'search'=>$this->search,
    ];
    return self::$data->listd($arr);

}

//活动报名
public function addapply(){

    $arr = [
        'com_id'=>$this->com_id,
    ];

    return self::$data->applys($arr);

}
//报名成功
public function apply(){
    $arr = [
        'activity_id'=>$this->activity_id,
        'com_id'=>$this->com_id,
        'order_id'=>$this->order_id,
        'name'=>$this->name,
        'telphone'=>$this->telphone,
        'com'=>$this->com,
        'email'=>$this->email,
        'des'=>$this->des,
    ];
    return self::$data->applys($arr,'confirm');

}

//报名列表
public  function  userList(){

        $arr = [
            'uid'=>$this->uid,
            'activity_id'=>$this->activity_id,
            'pay_status'=>$this->pay_status,
            'order'=>$this->order,
            'page'=>$this->page,
            'manage'=>$this->manage,
            'way'=>$this->way,
            'audit'=>$this->audit,
            'order_id'=>$this->order_id,
            'key'=>$this->key,
            'value'=>$this->value
        ];

    return self::$data->applylist($arr);

}


//退费审核列表
public function refundAudit(){
        $arr = [
            'way'=>$this->way,
            'audit'=>$this->audit,
            'manage'=>$this->manage,
            'page'=>$this->page,
            'order'=>$this->order,
            'key'=>$this->key,
            'search'=>$this->search
        ];

    return self::$data->applyRefund($arr);

}
//退费审核处理
public function auditProcessing(){

        $arr =[
            'id'=>$this->id,
            'audit'=>$this->audit,
            'manage'=>$this->manage,
        ];

    return self::$data->auditProcess($arr);

}

//活动审核
public function auditApp(){

    $arr = [
        'pay_status'=>$this->pay_status,
        'id'=>$this->id,
        'des'=>$this->des
    ];

    return self::$data->autidaps($arr);

}

//活动下的活动组别
public function group(){

    return self::$data->acgroup($this->aid);

}



}