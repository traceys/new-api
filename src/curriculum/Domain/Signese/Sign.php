<?php
namespace Curriculum\Domain\Signese;
use Curriculum\Model\Sign as Signs;
use Curriculum;
use PhalApi\Exception;

class Sign{
    /**
     * @param $arr
     * @return bool
     * 发起合约
     */
    public function sponsor($arr){
        $uid = Curriculum\selectUser($arr['token'])['Id'];
        if (!$uid){
            outError('token错误',3);
        }
        $data['tid'] = $arr['tid'];
        $data['oid'] = $arr['oid'];
        $data['sid'] = $arr['sid'];
        $data['mealName'] = $arr['mealName']; //套餐名称
        $data['initiator'] = $uid;
        $data['number'] = $arr['number'];
        $data['file'] = $arr['file'];
        $data['startTime'] = $arr['startTime'];
        $data['createTime'] = timeout(time());
        $data['endTime'] = $arr['endTime'];
        $data['on_statu'] = 1;    //已签约状态
        if (strtotime($data['createTime']) <time()){
            $data['on_statu'] = 2; //服务中状态
        }
        $model = new Signs();
        try{
            $res = $model->add($data);
            return true;
        }catch (\Exception $e){
            return false;
        }
    }

    /**
     * @param $arr
     * @return bool
     * 终止合约
     */
    public function termination($arr){
            $model = new Signs();
            $where = [
                'id'=>$arr['id']
            ];
            $data['applyTime'] = timeout(time());
            $data['makeTime'] = $arr['makeTime'];
            $data['reslutFor'] = $arr['reslutFor'];
            $data['people'] = Curriculum\selectUser($arr['token'])['Id'];//用户ID
            $data['on_statu'] = 4;
            try{
                $res = $model->updata($where,$data);
                return true;
            }catch (Exception $e){
                return false;
            }
    }

    /**
     * @param $arr
     * @param $datatype
     * @param $page
     * @param $num
     * @return mixed
     * 查询数据
     */
    public function finds($arr,$datatype,$page,$num){
        $school_name = $arr['school_name']; //园所名称
        $arr = array_remove($arr,'school_name'); //去除园所名称
        $arr = Curriculum\undatas($arr,null); //进行简单过滤
        if ($page){
            $page = ($page-1)*$num;
        }
        $model = new Signs();
        $res = $model->find($arr,$datatype);
        if (count($res) == count($res, 1)) {    //判断数据是一维还是二维
            $res['school_name'] = apiPost('User.Appuser.selschool',array('tid'=>$res['tid'])); //园所名称
        } else {
                $i = 0;
                foreach ($res as $key => $value){
                    $school_names  = apiPost('User.Appuser.selschool',array('tid'=>$value['tid'])); //园所名称
                    $res[$key]['school_name'] = $school_names;
                    if ($school_name && strstr($school_names,$school_name)){
                        $res = array();
                        $res[$i] = $value;
                        $res[$i]['school_name'] = $school_names;
                        $i++;
                    }
                }
                $res  = array_slice($res,$page,$num);
        }
        return $res;
    }




}
