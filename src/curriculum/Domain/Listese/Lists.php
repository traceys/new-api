<?php
namespace Curriculum\Domain\Listese;
use Curriculum;
use PhalApi\Exception;
use Curriculum\Model\SchCess as SchCess;

class Lists{
    /**
     * @param $arr
     * @return bool
     * create flow path
     */
        public function create($arr){
            $model = new  SchCess();
            $token = $arr['token'];  //用户token
            $arr = array_remove($arr,'token');
            $uid = Curriculum\selectUser($token)['Id'];
            if (!$uid){
                outError('token传入错误',3);
            }
            $arr['uid'] = $uid;
            $arr['handTime'] = timeout(time());
            try{
                $res = $model->add($arr);
                return true;
            }catch (Exception $e){
                return false;
            }
        }

}