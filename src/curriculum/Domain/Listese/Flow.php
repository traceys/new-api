<?php
namespace Curriculum\Domain\Listese;
use Curriculum\Model\ClassHead as ClassHead;
use Curriculum;
use PhalApi\Exception;
use Curriculum\Domain\Mealese\Plan as Plan;
use Curriculum\Model\Packplan as Packplan;
use Curriculum\Model\Sign as Sign;
use Curriculum\Domain\Hourese\Hour as Hour;
class Flow{
    /**
     * @param $arr
     * @param $datatype
     * @param $page
     * @param $num
     * @return \PhalApi\Model\
     * query class schedule the data
     */
    public function finds($arr,$datatype,$page,$num){
        $arr = Curriculum\undatas($arr,null);
        $model = new ClassHead();
        $Sign = new Sign();
        if ($page){
            $page = ($page-1)*$num;
        }
        $res = $model->find($arr,$datatype,$page,$num);

        if (count($res) != count($res, 1) && $res) {

               foreach ($res as $key => $value){
                   $oid = $Sign->find(array('id'=>$value['signId']),'one')['oid'];  //机构ID
                   $tecData = apiPost('User.certificate.selteacher',array('tid'=>$oid,'uid'=>$value['tecId']));
                    $res[$key]['tecName'] = $tecData['linkman'];                 //教师名称未完成
                    $res[$key]['stageName'] = $tecData['stageName'];               //教师别名未完成
               }
        }elseif($res){
            $Packplan = new Packplan();   //套餐和教案关联模型
            $Plan = new Plan();
            $oid = $Sign->find(array('id'=>$arr['signId']),'one')['oid'];  //机构ID
            $pid =  $Packplan->find(array('sid'=>$res['sid']),'one')['pid'];
            $pName =  $Plan->finds(array('id'=>$pid),'one',0,0)['name'];
            $tecData = apiPost('User.certificate.selteacher',array('tid'=>$oid,'uid'=>$res['tecId']));   //教师信息
            $kind = apiPost('User.Appuser.selschool',array('tid'=>$res['tid']));
            $res['pName'] = $pName;      //教案名称
            $res['kindName'] = $kind['school_name'];       //幼儿园名称
            $res['kindAdd'] = $kind['school_adress'];        //幼儿园地址
            $res['tecName'] = $tecData['linkman']; //教师名称未完成
            $res['stageName'] = $tecData['stageName'];                //教师别名未完成
            $res['cerName'] = $tecData['cert_name'];                  //证书名称
        }
        return $res;
    }

    /**
     * @param $arr
     * @return bool
     * add class schedule
     */
    public function crate($arr){
        $model = new ClassHead();
        $Sign = new Sign();
        $oid = $Sign->find(array('id'=>$arr['signId']),'one')['oid'];  //机构ID
        $zid = apiPost('User.certificate.selsupteacher',array('tid'=>$oid,'tecId'=>$arr['tecId']))['uid'];
        $arr['zid'] = $zid;    //执教主管ID 未完成
        $arr['tid'] = $this->finds(array('signId'=>$arr['signId']),'one',0,0)['tid'];
        $arr['on_statu'] = 0;
        try{
            $res = $model->add($arr);
            return true;
        }catch (Exception $e){
            return false;
        }
    }

    /**
     * @param $arr
     * @return bool
     * update class schedule state
     */
    public function upstate($arr){
        $model = new ClassHead();

        $data = $model->find(array('id'=>$arr['id']),'one',0,0);
        if (!$data){
            outError('未找到指定数据',3);
        }
        switch ($arr['on_statu']){
            case -1:
                if ($data['on_statu'] != 0){
                    outError('该课表已无法被撤回');
                }
                break;
            case 2:
                  $hour = new Hour();      // 未完成
                  $hourdata = [
                      'cid'=>$data['id'],
                      'sid'=>$data['sid'],
                      'type'=>$data['type'],
                      'time'=>$data['starTime'],
                      'hourNum'=>$data['hourNum'],
                      'lessonsnum'=>$data['lessonsNum'],
                      'sigintime'=>$data['sigintime'],
                      'timeQuantum'=>$data['timeQuantum'],
                      'tecId'=>$data['tecId'],
                      'grade'=>$data['gradeClass'],
                  ];
                  $hours =  $hour->add($hourdata);     //生成课时,生成课节回调
                  if (!$hours){
                      outError('生成课时课节失败',100);
                  }
                break;
            case 0:
                        //未完成
                        //执教机构回退课表
                break;
            default:
                outError('不允许出现此类操作',3);
        }
        try{
            $res = $model->updata(array('id'=>$arr['id']),$arr);
            return true;
        }catch (Exception $e){
            return false;
        }
    }

    /**
     * @param $arr
     * @return bool
     * update class schedule
     */
    public function up($arr){
        $model = new ClassHead();
        $where = ['id'=>$arr['id']];

        $data = $model->find($where,'one',0,0);
        if (!$data) {
            outError('未找到指定数据', 3);
        }
        if (isset($arr['on_statu']) && $arr['on_statu'] == 1){

                if ($data['on_statu'] !== 0){ //园长审核时判断状态园长是否可以审核
                    outError('非法请求',3);
                }
        }
        try{
            $model->updata($where,$arr);
            $res =  true;
        }catch (Exception $e){
            $res =  false;
        }
        return $res;
    }

}