<?php
namespace Curriculum\Domain\Lessonese;
use Curriculum\Domain\Hourese\Hour;
use Curriculum\Model\Lessons;
//use Curriculum\Model\packplan as packplan;
use Curriculum;
use PhalApi\Exception;

class Lesson{
    /**
     * @param $arr
     * @return bool
     * add class grade
     */
    public function create($arr){
        $model = new Lessons();
        if (isset($arr['attendance'])){         //发起课评时二次添加的接口
            $data = $model->find(array('id'=>$arr['lid'],'type'=>0),'one');
            $arr = array_remove($arr,'lid');
            $arr['index'] = $data['index'];
            $arr['hid'] = $data['hid'];
            $arr['type'] = 1;
            try{
                $res = $model->add($arr);
                return true;
            }catch (Exception $e){
                outError('系统错误,请联系客服',100);
            }
        }else{
            $hour = new Hour();
            $data = $hour->finds(array('id'=>$arr['hid']),'one',0,12);
            if (!$data){
                outError('系统错误,请稍后再试',3);
            }
            $grade = json_decode($arr['grade'],true);
            $arr = array_remove($arr,'grade');
            for ($i = 1;$i<=$data['lessonsnum'];$i++){  //循环生成课节
                $gradeClass = array();
                $terId = array();
                foreach ($grade[$i] as $key => $value){ //取出对应课节的班级及人数
                    $gradeClass[$key] = $value['gradeClass']; //班级
                    $terId[$key] = $value['terId'];          //配班老师
                    $person = 0;                           //人数
                    $person += $value['peopleNum'];
                }
                    $arr['index'] = $i;
                    $arr['type'] = 0;   //预设课节
                    $arr['gradeClass'] = json_encode($gradeClass);
                    $arr['terId'] = json_encode($terId);
                    $arr['person'] = $person;
                    try{
                        $res = $model->add($arr);
                    }catch (Exception $e){
                        outError('系统错误，请联系管理员',100);
                    }
            }
            return true;
        }
    }

    /**
     * @param $arr
     * @return bool
     * update state
     */
    public function upState($arr){
        $model = new Lessons();
        try{
            $res = $model->updata(array('id'=>$arr['id']),$arr);
            return true;
        }catch (Exception $e){
            outError('update state fail',100);
        }
    }
    public function find($arr,$datatype,$page,$num){
        $model = new Lessons();
        if ($page){
            $page = ($page-1)*$num;
        }
        $res = $model->find(array('id'=>$arr['id']),$datatype,$page,$num);
        return $res;
    }

}