<?php
namespace Curriculum\Domain\Lessonese;
use Curriculum\Model\ClassReview;
use Curriculum;
use PhalApi\Exception;
use Curriculum\Domain\Lessonese\Lesson as Lesson;
class Evaluate{
    /**
     * @param $arr
     * @return bool
     * a class review
     */
    public function create($arr){
        $Lesson = new Lesson();
        $Lesson->create($arr); //二次生成课节数据
        $model = new ClassReview();
        $attendanceClass = json_decode($arr['attendanceClass'],true); //班级
        $terId = json_decode($arr['terId'],true); //老师uid
        $data['lid'] = $arr['lid'];
        $data['createTime'] = timeout(time());
        $data['peopleNumber'] = $arr['attendance'];//到课人数
        foreach ($attendanceClass as $key => $value){  //对应班级
            $data['bid'] = $value;
            $data['classTeacher'] = $terId[$key];
            try{
                $res = $model->add($data);

            }catch (Exception $e){
                outError('系统错误，请联系客服',100);
            }
        }
        return true;
    }

    /**
     * @param $arr
     * @param $token
     * @return bool
     * techers first time review
     *
     */
    public function up($arr,$token){
        $model = new ClassReview();
        $uid = Curriculum\selectUser($token)['Id'];
        $data = $model->find(array('lid'=>$arr['lid'],'uid'=>$uid),'one');
        $review = $model->find(array('lid'=>$arr['lid'],'on_statu'=>1),'one');
        if ($review){
            outError('该课已无法再重新评论',3);
        }
        if (!$data){
            outError('未找到发起的课评',3);
        }
        $newarray['signUrl'] = $arr['signUrl'];
        $newarray['on_statu'] = 1;
        $arr = array_remove($arr,'signUrl');
        try{
            $model->updata(array('lid'=>$arr['lid'],'on_statu'=>0),$arr);//对课节进行统一修改
            $model->updata(array('lid'=>$arr['lid'],'uid'=>$uid),$newarray);//针对班级老师签字
            return true;
        }catch (Exception $e){
            outError('系统错误，请联系客服');
        }

    }

    /**
     * @param $arr
     * @return bool
     * techers second time review
     */
    public function review($arr){
        $model = new ClassReview();
        $data = $model->find(array('lid'=>$arr['lid']),'all');
        if ($data && $data['0']['on_statu'] == -1){
            outError('该课已经重新发起过评论了',3);
        }

        $model->updata(array('lid'=>$arr['lid']),array('on_statu'=>-1)); //将原评价失效
        for ($i = 0; $i<count($data);$i++){
            $nearray = [
                'lid'=>$data[$i]['lid'],
                'bid'=>$data[$i]['bid'],
                'createTime'=>$data[$i]['createTime'],
                'peopleNumber'=>$data[$i]['peopleNumber'],
                'classTeacher'=>$data[$i]['classTeacher']
            ];
            try{
                $res = $model->add($nearray);
            }catch (Exception $e){
                outError('系统错误,请联系客服',3);
            }
        }
        return true;
    }
}