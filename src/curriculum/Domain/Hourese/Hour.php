<?php
namespace Curriculum\Domain\Hourese;
use Curriculum\Model\Hour as Hours;
use Curriculum;
use PhalApi\Exception;
use Curriculum\Domain\Mealese\Plan as Plan;
use Curriculum\Domain\Lessonese\lesson as lesson;
class Hour{
    /**
     * @param $arr
     * @return bool
     * add hour
     */
    public function add($arr){
        $model = new Hours(); //课时
        //查看套餐下的教案
        $Plan = new Plan();     //教案
        $lesson = new lesson(); //课节
        $pdata = $Plan->findMealPlan($arr['sid']);
        $pnum  = count($pdata);
        $data['cid'] = $arr['cid']; //课目id
        $data['tecId'] = $arr['tecId']; //教师ID
        $data['timeQuantum'] = $arr['timeQuantum'];//时段
        $data['sigintime'] = $arr['sigintime']; //签到时间
        $data['lessonsnum'] = $arr['lessonsnum'];//课节数量
        $j = 0;
        for ($i = 0;$i<$arr['hourNum'];$i++){  //生成指定数量的课时
            if ($j > $pnum){            //当长度到截止长度则重新计算
                $j = 0;
            }
            $data['pid'] = $pdata[$j]['pid'];   //教案ID
            $data['lesson'] = $pdata[$j]['name']; //教案名称
            $data['time'] += Curriculum\get_weeks($arr['time'],$arr['type'],$i); //日期
            $data['scheduleId'] = ''; //未完成 日程id的回调
            $res  = $model->add($data); //生成课时
            $lesson->create(array('hid'=>$res['id'],'grade'=>$arr['grade']));  // 生成课节
            $j++;
        }
        return true;
    }

    /**
     * @param $arr
     * @return bool
     * update state
     */
    public function upState($arr){
        $arr = Curriculum\undatas($arr,null);
        $model = new Hours();
        $id = $arr['id'];
        $data = $model->find(array('id'=>$id),'one');
        if (!$data){
            outError('未找到指定数据',3);
        }

        try{
            $res = $model->updata(array('id'=>$id),$arr);
            return true;
        }catch (Exception $e){
            return false;
        }

    }

    /**
     * @param $arr
     * @param $datatype
     * @param $page
     * @param $num
     * @return mixed
     * simple query data
     */
    public function finds($arr,$datatype,$page,$num){
        $model = new Hours();
        if ($page){
            $page = ($page-1)*$num;
        }
        $res = $model->find($arr,$datatype,$page,$num);
        return $res;
    }

}