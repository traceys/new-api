<?php
namespace Curriculum\Domain\Mealese;
use Curriculum\Model\Subjeck as Subjeck;
use Curriculum\Model\Packplan as Packplan;
use Curriculum;
class Subject{
    /**
     * @param $data
     * @return bool
     * 添加科目
     */
    public function create($data){
        $model = new Subjeck();
        $where = array('name'=>$data['name'],'tid'=>$data['tid']);
        $check = $model->find($where,'one'); //验证科目名称是否重复
        if ($check){
            outError('科目名称重复',3);
        }
        $res = $model->add($data);
        return true;
    }

    /**
     * 查询数据
     * where 条件
     * datatype 数据类型
     */
    public function finds($where,$datatype,$page,$num){
        $where = Curriculum\undatas($where,null); //数据初始过滤
        $model = new Subjeck();
        if ($page){
            $page = ($page-1)*$num;
        }
        $res = $model->find($where,$datatype,$page,$num);
        return $res;
    }

    /**
     * @param $where
     * @param $data
     * @return bool
     */
    public function up($where,$data){
        $model = new Subjeck();
        try{
            $res = $model->updata($where,$data);
            return true;
        }catch (\Exception $e){
            return false;
        }
    }

}