<?php
namespace Curriculum\Domain\Mealese;
use Curriculum\Model\setmeal as setmeal;
use Curriculum\Model\packplan as packplan;
use Curriculum;
class Meal{
    /**
     * @param $data
     * @return bool
     * 添加套餐
     */
    public function create($data){
        $model = new setmeal();
        $where = array('name'=>$data['name'],'tid'=>$data['tid']);
        $check = $model->find($where,'one'); //验证套餐名称是否重复
        if ($check){
            outError('科目名称重复',3);
        }
        $pid = $data['pid'];
        $data = array_remove($data,'pid');
        $res = $model->add($data);  //添加套餐信息
        $planData = [
            'sid'=>$res['id']
        ];
        foreach ($pid as $key => $value){
            $packplan = new packplan();
            $planData['pid'] = $value;
            $packplan->add($planData); //将套餐和教案关联起来
        }
        return true;
    }

    /**
     * @param $where 条件
     * @param $datatype 数据类型
     * @param $page 当前页数
     * @param $num  当前数量
     * @return mixed
     */
    public function finds($where,$datatype,$page,$num){
        $where = Curriculum\undatas($where,null); //数据初始过滤
        $model = new setmeal();
        if ($page){
            $page = ($page-1)*$num;
        }
        $res = $model->find($where,$datatype,$page,$num); //查询套餐信息
        $packplan = new packplan();
        if (count($res) == count($res, 1)) {
           $res['sum'] = count($packplan->find(array('sid'=>$res['id']),'all'));
        } else {
            foreach ($res as $key => $value){
                $res['$key']['sum'] = count($packplan->find(array('sid'=>$value['id']),'all'));
            }
        }
        return $res;
    }

    /**
     * @param $where
     * @param $data
     * @return bool
     */
    public function up($where,$data){
        $model = new setmeal();
        if (isset($data['pid']) && $data['pid']){
            $packplan = new packplan();
            $wheres = array('sid'=>$data['id'],'statu'=>1); //清除条件
            $datas = array('statu'=>-1);
            $addData = array('sid'=>$data['id']);
            foreach ($data['pid'] as $key => $value){
                $packplan->updata($wheres,$datas);  //清除数据
                $addData['pid'] = $value;
                $packplan->add($addData); //重新加入套餐
            }
            $data = array_remove($data,'pid');
        }

        try{
            $res = $model->updata($where,$data);
            return true;
        }catch (\Exception $e){
            return false;
        }
    }



}