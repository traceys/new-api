<?php
namespace Curriculum\Domain\Mealese;
use Curriculum\Model\Teachingplan as Teachingplan;
use Curriculum;
class Plan{
    /**
     * @param $data
     * @return bool
     * add plan
     */
    public function create($data){
        $model = new Teachingplan();
        $where = array('name'=>$data['name'],'tid'=>$data['tid']);
        $check = $model->find($where,'one'); //验证教案名称是否重复
        if ($check){
            outError('教案名称重复',3);
        }
        $res = $model->add($data);
        return true;

    }

    /**
     * @param $where
     * @param $datatype
     * @return mixed
     * find plan
     */
    public function finds($where,$datatype,$page,$num){
        $where = Curriculum\undatas($where,null); //数据初始过滤
        $model = new Teachingplan();

        if ($page){
            $page = ($page-1)*$num;
        }
        $res = $model->find($where,$datatype,$page,$num);
        return $res;
    }

    /**
     * @param $where
     * @param $data
     * @return bool
     * update plan
     */
    public function up($where,$data){
        $model = new Teachingplan();
        try{
            $res = $model->updata($where,$data);
            return true;
        }catch (\Exception $e){
            return false;
        }

    }

    /**
     * @param $sid
     * @return mixed
     * check the meal plan
     */
    public function findMealPlan($sid){
        $model = new packplan();
        $plan = new Plan();
        $res  = $model->find(array('sid'=>$sid),'all');
        if ($res){
            foreach ($res as $key => $value){
                $res['name'] = $plan->finds(array('id'=>$value['pid']),'all',0,0);
            }
        }
        return $res;
    }

}