<?php
namespace Curriculum\Model;

use PhalApi\Model\NotORMModel as NotORM;

class Subjeck extends NotORM {

    protected function getTableName($id) {
        return 'curriculum_subjeck';
    }

    /**
     * 添加
     * data 添加数组
     */
    public  function add($arr){
        $data = $this->getORM();
        $data = $data->insert($arr);
        return $data;
    }
    /**
     * 查询
     * where 条件
     * datatype 数据类型
     */
    public function find($where,$datatype,$page = 0,$num = 12){
        $data = $this->getORM()->where('statu',1);
        if ($where){
            $data = $data->where($where);
        }
        switch ($datatype){
            case 'one':
                $data = $data->fetchOne();
                break;
            case 'all':
                if ($page){
                    $data = $data->limit($page,$num);
                }
                $data = $data->fetchAll();
        }
        return $data;

    }

    /**
     * @param $where
     * @param $arr
     * @return \PhalApi\Model\
     */
    public function updata($where,$arr){
        $data = $this->getORM();
        $data = $data->where($where)->update($arr);
        return $data;
    }


}