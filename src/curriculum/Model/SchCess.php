<?php
namespace Curriculum\Model;

use PhalApi\Model\NotORMModel as NotORM;

class SchCess extends NotORM {

    protected function getTableName($id) {
        return 'curriculum_schCess';
    }

    /**
     * @param $arr
     * @return \PhalApi\Model\
     * add
     */
    public function add($arr){
        $data = $this->getORM();
        $data = $data->insert($arr);
        return $data;
    }

}