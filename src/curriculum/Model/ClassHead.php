<?php
namespace Curriculum\Model;

use PhalApi\Model\NotORMModel as NotORM;

class ClassHead extends NotORM {

    protected  function getTableName($id)
    {
        return 'curriculum_classHead';
    }

    /**
     * @param $where
     * @return \PhalApi\Model\
     * find data
     */
    public function find($where,$datatype = 'all',$page = 0,$num = 12){
        $data = $this->getORM();
        $sql = "SELECT a.*,b.mealName,b.sid FROM api_curriculum_classHead AS a LEFT JOIN
                api_curriculum_sign AS b ON 
                a.signId=b.id WHERE a.statu = 1";

        if (isset($where['oid'])){
            $oid = $where['oid'];
            $sql .= "&& b.oid = $oid" ;
        }
        if (isset($where['id'])){
            $id = $where['id'];
            $sql .= " && a.id=$id ";
        }
        if (isset($where['year'])){
            $year = $where['year'];
            $sql .= "  && a.year = $year";
        }
        if (isset($where['semester'])){
            $semester = $where['semester'];
            $sql .= "  && a.semester = '$semester'";
        }
        if (isset($where['weeks'])){
            $weeks = $where['weeks'];
            $sql .= " && a.weeks = $weeks";
        }
        if (isset($where['timeQuantum'])){
            $timeQuantum = $where['timeQuantum'];
            $sql .= " && a.timeQuantum = '$timeQuantum'";
        }
        if (isset($where['signId'])){
            $signId = $where['signId'];
            $sql .= "&& a.signId = $signId";
        }

        if ($page){
            $sql .= " LIMIT $page,$num"; //分页
        }

        $data = $data->queryAll($sql,array());


        if ($datatype == 'one' && $data){
            foreach ($data as $key => $value){
                $data = $value;
            }
        }

        return $data;
    }

    /**
     * @param $arr
     * @return \PhalApi\Model\NotORM_Result
     * add data
     */
    public function add($arr){
        $data = $this->getORM();
        $data = $data->insert($arr);
        return $data;
    }

    /**
     * @param $where
     * @param $arr
     * @return \PhalApi\Model\
     * update data
     */
    public function updata($where,$arr){
        $data = $this->getORM();
        $data = $data->where($where)->update($arr);
        return $data;
    }

}