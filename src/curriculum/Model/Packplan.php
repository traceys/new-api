<?php
namespace Curriculum\Model;

use PhalApi\Model\NotORMModel as NotORM;

class Packplan extends NotORM {

    protected function getTableName($id) {
        return 'curriculum_packplan';
    }

    /**
     * @param $data
     * @return mixed
     * 添加数据
     */
    public function add($data){
        $model = $this->getORM();
        $data = $model->insert($data);
        return $data;
    }
    /**
     * 查询
     * where 条件
     * datatype 数据类型
     */
    public function find($where,$datatype){
        $data = $this->getORM()->where('statu',1);
        if ($where){
            $data = $data->where($where);
        }
        switch ($datatype){
            case 'one':
                $data = $data->fetchOne();
                break;
            case 'all':
                $data = $data->fetchAll();
        }
        return $data;

    }
    /**
     * @param $where
     * @param $arr
     * @return \PhalApi\Model\
     */
    public function updata($where,$arr){
        $data = $this->getORM();
        $data = $data->where($where)->update($arr);
        return $data;
    }


}