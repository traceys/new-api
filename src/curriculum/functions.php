<?php
namespace Curriculum;
//数据过滤
function undatas($val,$data){
    foreach( $val as $k=>$v){
        if( $v === $data || $v === ''){
            unset( $val[$k] );
        }else{
            $val[$k] = trim($v); //空格过滤
        }
    }

    return $val;
}
//用户信息
function selectUser($token){
    $val = apiPost('User.User.Selecttoken',array('searchToken'=>$token)); //用户信息
    return $val;
}
//增加指定日期一个星期
function get_weeks($date,$type,$page){
    switch ($type) {
        case 1:
            $data =  date("Y-m-d",strtotime("+ $page week",strtotime($date)));
            break;
    }
    return $data;

}