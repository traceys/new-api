<?php
namespace Curriculum\Api;
use PhalApi\Api;
use Curriculum;
use Curriculum\Domain\Hourese\Hour as Hour;
use PhalApi\Exception\BadRequestException;

class Hourese extends Api{

    //数据过滤
    public function getRules()
    {
        return array(
            '*'=>array(
                'token' => array('name' => 'token','source'=>'get','require'=>true,'desc' => '用户身份token'),
            ),
            'Create'=>array(
                'id' => array('name' => 'id','require'=>true,'desc' => '课时id'),
                'pay_statu' => array('name' => 'pay_statu','default'=>null,'desc' => '课时状态'),
                'on_statu' => array('name' => 'on_statu','default'=>null,'desc' => '调课状态'),
           ),
             );
    }

    /**
     * @return bool
     * update state
     */
    public function State(){
        $hour = new Hour();
            $arr = [
                'id'=>$this->id,
                'pay_statu'=>$this->pay_statu,
                'on_statu'=>$this->on_statu
            ];
        return $hour->upState($arr);
    }

}