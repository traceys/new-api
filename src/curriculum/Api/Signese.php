<?php
namespace Curriculum\Api;
use PhalApi\Api;
use Curriculum;
use Curriculum\Domain\Signese\Sign as Sign;
use PhalApi\Exception\BadRequestException;

class Signese extends Api{

    public function getRules(){
        return array(
            '*'=>array(
                'token' => array('name' => 'token','require'=>true,'desc' => '用户身份token'),
            ),
            'sponsor'=>array(
                'tid' => array('name' => 'tid','require'=>true,'desc' => '幼儿园ID'),
                'oid' => array('name' => 'oid','require'=>true,'desc' => '机构ID'),
                'number' => array('name' => 'number','require'=>true,'desc' => '合同编号'),
                'startTime' => array('name' => 'startTime','require'=>true,'desc' => '开始时间'),
                'endTime' => array('name' => 'endTime','require'=>true,'desc' => '结束时间'),
                'sid' => array('name' => 'sid','require'=>true,'desc' => '套餐ID'),
                'file' => array('name' => 'file','default'=>'','desc' => '合同文件地址'),
                'mealName' => array('name' => 'mealName','require'=>true,'desc' => '套餐名称'),
            ),
            'termination'=>array(
                'id' => array('name' => 'id','require'=>true,'desc' => 'id'),
                'makeTime' => array('name' => 'makeTime','require'=>true,'desc' => '终止时间'),
                'reslutFor' => array('name' => 'reslutFor','require'=>true,'desc' => '终止原因'),
            ),
            'findData'=>array(
                'id' => array('name' => 'id','default'=>null,'desc' => 'id'),
                'oid' => array('name' => 'oid','default'=>null,'desc' => '机构ID'),
                'school_name' => array('name' => 'school_name','default'=>null,'desc' => '园长名称'),
                'on_statu' => array('name' => 'on_statu','default'=>null,'desc' => '签约状态'),
                'datatype'=> array('name' => 'datatype','default'=>'all','desc' => '数据展示类型'),
                'page'=> array('name' => 'page','default'=>0,'desc' => '当前页数'),
                'num'=> array('name' => 'num','default'=>12,'desc' => '展示数量'),
            ),

        );
    }

    /**
     * @return bool
     * 发起合约
     */
    public function sponsor(){
        $Domain = new Sign();
        $arr =  [
            'token'=>$this->token,
            'tid'=>$this->tid,
            'oid'=>$this->oid,
            'number'=>$this->number,
            'startTime'=>$this->startTime,
            'endTime'=>$this->endTime,
            'sid'=>$this->sid,
            'file'=>$this->file,
            'mealName'=>$this->mealName
        ];
        return $Domain->sponsor($arr);
    }

    /**
     * @return bool
     * 合约终止
     */
    public function termination(){
        $Domain = new Sign();
        $arr = [
            'token'=>$this->token,
            'id'=>$this->id,
            'makeTime'=>$this->makeTime,
            'reslutFor'=>$this->reslutFor
        ];
        return $Domain->termination($arr);
    }

    /**
     * @return mixed
     * 查询合约
     */
    public function findData(){
        $Domain = new Sign();
        $arr = [
            'id' => $this->id,
            'oid'=> $this->oid,
            'school_name'=>$this->school_name,
            'on_statu'=>$this->on_statu
        ];
        return $Domain->finds($arr,$this->datatype,$this->page,$this->num);
    }



}