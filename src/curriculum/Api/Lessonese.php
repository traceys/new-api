<?php
namespace Curriculum\Api;
use PhalApi\Api;
use Curriculum;
use Curriculum\Domain\Lessonese\lesson as lesson;
use Curriculum\Domain\Lessonese\Evaluate as Evaluate;
use PhalApi\Exception\BadRequestException;

class Lessonese extends Api{

    public function getRules()
    {
        return array(
            '*' => array(
                'token' => array('name' => 'token', 'source' => 'get', 'desc' => '用户身份token'),
            ),
            'state' => array(
                'id' => array('name' => 'id', 'require' => true, 'desc' => '课节id'),
                'on_statu' => array('name' => 'on_statu', 'require' => true, 'desc' => '课节状态'),
            ),
            'sponsor' => array(
                'lid' => array('name' => 'lid', 'require' => true, 'desc' => '课节id'),
                'gradeClass' => array('name' => 'gradeClass', 'require' => true, 'desc' => '应到班级'),
                'attendanceClass' => array('name' => 'attendanceClass', 'require' => true, 'desc' => '实到班级'),
                'terId' => array('name' => 'terId', 'require' => true, 'desc' => '应到老师uid'),
                'person' => array('name' => 'person', 'require' => true, 'desc' => '应到人数'),
                'attendance' => array('name' => 'attendance', 'require' => true, 'desc' => '实到人数'),
            ),
            'review' => array(
                'lid' => array('name' => 'lid', 'require' => true, 'desc' => '课节id'),
                'attireScreen' => array('name' => 'attireScreen', 'require' => true, 'desc' => '老师着装评价'),
                'atmosphereScreen' => array('name' => 'atmosphereScreen', 'require' => true, 'desc' => '课堂评分'),
                'safetyScreen' => array('name' => 'safetyScreen', 'require' => true, 'desc' => '安全评分'),
                'signUrl' => array('name' => 'signUrl', 'require' => true, 'desc' => '老师签名url'),
                'describe' => array('name' => 'describe', 'require' => true, 'desc' => '详细描述'),
                'desc' => array('name' => 'desc', 'default' => '', 'desc' => '备注'),
            ),
            'startAgain' => array(
                'lid' => array('name' => 'lid', 'require' => true, 'desc' => '课节id'),
            ),
        );
    }

    /**
     * @return bool
     * update state value
     */
    public function state(){
        $Domain = new lesson();
        $arr = [
            'id'=>$this->id,
            'on_statu'=>$this->on_statu
        ];
        return $Domain->upState($arr);
    }

    /**
     * @return bool
     * sponsor class  review
     */
    public function sponsor(){
        $Domain = new Evaluate();
        $arr = [
            'lid'=>$this->lid,
            'gradeClass'=>$this->gradeClass,
            'attendanceClass'=>$this->attendanceClass,
            'terId'=>$this->terId,
            'person'=>$this->person,
            'attendance'=>$this->attendance
        ];
        return $Domain->create($arr);
    }

    /**
     * @return bool
     * teacher rewiew
     */
    public function review(){
        $Domain = new Evaluate();
        $arr = [
            'lid'=>$this->lid,
            'attireScreen'=>$this->attireScreen,
            'atmosphereScreen'=>$this->atmosphereScreen,
            'safetyScreen'=>$this->safetyScreen,
            'describe'=>$this->describe,
            'desc'=>$this->desc,
            'signUrl'=>$this->signUrl
        ];
        return $Domain->up($arr,$this->token);
    }

    /**
     * @return bool
     * teachers second time review
     */
    public function startAgain(){
        $Domain = new Evaluate();
        $arr = [
            'lid'=>$this->lid
        ];
        return $Domain->review($arr);
    }

}