<?php
namespace Curriculum\Api;
use PhalApi\Api;
use Curriculum;
use Curriculum\Domain\Listese\Flow as Flow;
use Curriculum\Domain\Listese\Lists as Lists;
use PhalApi\Exception\BadRequestException;

class Listese extends Api{
    public function getRules(){
        return array(
            '*'=>array(
                'token' => array('name' => 'token','source'=>'get','desc' => '用户身份token'),
            ),
            'findTeacher'=>array(
                'oid' => array('name' => 'oid','require'=>true,'desc' => '机构ID'),
                'year' => array('name' => 'year','require'=>true,'desc' => '年份'),
                'semester' => array('name' => 'semester','require'=>true,'desc' => '学期'),
                'weeks' => array('name' => 'weeks','require'=>true,'desc' => '周几'),
                'timeQuantum' => array('name' => 'timeQuantum','require'=>true,'desc' => '时段'),
            ),
            'addHead'=>array(
                'signId' => array('name' => 'signId','require'=>true,'desc' => '合约ID'),
                'year' => array('name' => 'year','require'=>true,'desc' => '年份'),
                'semester' => array('name' => 'semester','require'=>true,'desc' => '学期'),
                'hourNum' => array('name' => 'hourNum','require'=>true,'desc' => '课时数量'),
                'weeks' => array('name' => 'weeks','require'=>true,'desc' => '周几'),
                'lessonsNum' => array('name' => 'lessonsNum','require'=>true,'desc' => '课节数量'),
                'classNum' => array('name' => 'classNum','require'=>true,'desc' => '课节总量'),
                'timeQuantum' => array('name' => 'timeQuantum','require'=>true,'desc' => '时段'),
                'tecId' => array('name' => 'tecId','require'=>true,'desc' => '教师ID'),
                'startTime' => array('name' => 'startTime','require'=>true,'desc' => '首次开课日期'),
            ),
            'findList'=>array(
                'tid' => array('name' => 'tid','default'=>null,'desc' => '园所ID'),
                'id' => array('name' => 'id','default'=>null,'desc' => '机构ID'),
                'sid' => array('name' => 'sid','default'=>null,'desc' => '套餐ID'),
                'semester' => array('name' =>'semester','default'=>null,'desc' => '学期'),
                'on_statu' => array('name' => 'on_statu','default'=>null,'desc' => '课表状态'),
                'datatype' => array('name' => 'datatype','default'=>'all','desc' => '二维数据'),
                'page' => array('name' => 'page','default'=>0,'desc' => '当前页数'),
                'num' => array('name' => 'num','default'=>12,'desc' => '展示数量'),
            ),
            'State'=>array(
                'id' => array('name' => 'id','require'=>true,'desc' => '课表id'),
                'on_statu' => array('name' => 'on_statu','require'=>true,'desc' => '状态'),
            ),
            'confrim'=>array(
                'id' => array('name' => 'id','require'=>true,'desc' => '课表id'),
                'gradeClass' => array('name' => 'gradeClass','require'=>true,'desc' => '预设课节信息'),
                'sigintime' => array('name' => 'sigintime','require'=>true,'desc' => '上课时间'),
                'kid' => array('name' => 'kid','require'=>true,'desc' => '操作园长uid'),
            ),
            'flowPath'=>array(
                'cid' => array('name' => 'cid','require'=>true,'desc' => '课表id'),
                'type' => array('name' => 'type','require'=>true,'desc' => '类型'),
                'tid' => array('name' => 'tid','require'=>true,'desc' => '团队id'),
                'desc' => array('name' => 'desc','default'=>'','desc' => '操作备注'),
            ),
        );
    }

    /**
     * @return \PhalApi\Model\
     * screening of teachers
     */
    public function findTeacher(){
        $Domain = new Flow();
        $arr = [
            'oid'=>$this->oid,
            'year'=>$this->year,
            'semester'=>$this->semester,
            'weeks'=>$this->weeks,
            'timeQuantum'=>$this->timeQuantum
        ];
        return $Domain->finds($arr,'all',0,12);
    }
    /**
     * @return bool
     * add class schedule
     */
    public function addHead(){
        $Domain = new Flow();
        $arr = [
            'signId'=>$this->signId,
            'year'=>$this->year,
            'semester'=>$this->semester,
            'hourNum'=>$this->hourNum,
            'weeks'=>$this->weeks,
            'lessonsNum'=>$this->lessonsNum,
            'classNum'=>$this->classNum,
            'timeQuantum'=>$this->timeQuantum,
            'tecId'=>$this->tecId,
            'startTime'=>$this->startTime
        ];
        return $Domain->crate($arr);
    }

    /**
     * @return \PhalApi\Model\
     *screening schedule data
     */
    public function findList(){
        $Domain = new Flow();
        $arr = [
            'id'=>$this->id,
            'tid'=>$this->tid,
            'sid'=>$this->sid,
            'semester'=>$this->semester,
            'on_statu'=>$this->on_statu,
        ];
        return $Domain->finds($arr,$this->datatype,$this->page,$this->num);
    }

    /**
     * @return bool
     * update class schedule state API
     */
    public function State(){
        $Domain = new Flow();
        $arr = [
            'id'=>$this->id,
            'on_statu'=>$this->on_statu
        ];
        return $Domain->upstate($arr);
    }

    /**
     * @return bool
     * nursery school confirm
     */
    public function confrim(){
        $Domain = new Flow();
        $arr = [
            'id'=>$this->id,
            'gradeClass'=>$this->gradeClass,
            'sigintime'=>$this->sigintime,
            'kid'=>$this->kid,
            'on_statu'=>1
        ];
        return $Domain->up($arr);
    }

    /**
     * @return bool
     * create flow path
     */
    public function flowPath(){
        $Domain = new Lists();
        $arr = [
            'token'=>$this->token,
            'cid'=>$this->cid,
            'type'=>$this->type,
            'tid'=>$this->tid,
            'desc'=>$this->desc
        ];
        return $Domain->create($arr);
    }





}