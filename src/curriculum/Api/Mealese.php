<?php
namespace Curriculum\Api;
use PhalApi\Api;
use Curriculum;
use Curriculum\Domain\Mealese\Meal as Meal;
use Curriculum\Domain\Mealese\Subject as Subject;
use Curriculum\Domain\Mealese\Plan as Plan;
use PhalApi\Exception\BadRequestException;

class Mealese extends Api{


    //数据过滤
    public function getRules() {
        return array(
//            '*'=>array(
//                'token' => array('name' => 'token','source'=>'get','require'=>true,'desc' => '用户身份token'),
//            ),
            'up'=>array(
                'where'=> array('name' => 'where','type'=>'array','format' => 'json','require'=>true,'desc' => '查询条件'),
                'data'=> array('name' => 'data','type'=>'array','format' => 'json','require'=>true,'desc' => '修改数据'),
                'type'=> array('name' => 'type','require'=>true,'desc' => '修改什么类型数据'),
            ),
            'findPlan'=>array(
                'tid'=> array('name' => 'tid','require'=>null,'desc' => '机构ID'),
                'id'=> array('name' => 'id','default'=>null,'desc' => 'id'),
                'name'=> array('name' => 'name','default'=>null,'desc' => '机构名称'),
                'type'=> array('name' => 'type','default'=>null,'desc' => '机构类型'),
                'bid'=> array('name' => 'bid','default'=>null,'desc' => '科目ID'),
                'on_statu'=> array('name' => 'on_statu','default'=>null,'desc' => '数据状态'),
                'datatype'=> array('name' => 'datatype','default'=>'all','desc' => '数据展示类型'),
                'page'=> array('name' => 'page','default'=>0,'desc' => '当前页数'),
                'num'=> array('name' => 'num','default'=>12,'desc' => '展示数量'),
            ),
            'findSubject'=>array(
                'id'=> array('name' => 'id','default'=>null,'desc' => 'id'),
                'tid'=> array('name' => 'tid','require'=>null,'desc' => '机构ID'),
                'name'=> array('name' => 'name','require'=>null,'desc' => '科目名称'),
                'on_statu'=> array('name' => 'on_statu','default'=>null,'desc' => '数据状态'),
                'datatype'=> array('name' => 'datatype','default'=>'all','desc' => '数据展示类型'),
                'page'=> array('name' => 'page','default'=>0,'desc' => '当前页数'),
                'num'=> array('name' => 'num','default'=>12,'desc' => '展示数量'),
            ),
            'findMeal'=>array(
                'id'=> array('name' => 'id','default'=>null,'desc' => 'id'),
                'tid'=> array('name' => 'tid','require'=>null,'desc' => '机构ID'),
                'name'=> array('name' => 'name','require'=>null,'desc' => '套餐名称'),
                'on_statu'=> array('name' => 'on_statu','default'=>null,'desc' => '数据状态'),
                'datatype'=> array('name' => 'datatype','default'=>'all','desc' => '数据展示类型'),
                'page'=> array('name' => 'page','default'=>0,'desc' => '当前页数'),
                'num'=> array('name' => 'num','default'=>12,'desc' => '展示数量'),
            ),
            'addPlan'=>array(
                'name'=> array('name' => 'name','require'=>true,'desc' => '名称'),
                'type'=> array('name' => 'type','require'=>true,'desc' => '类型'),
                'bid'=> array('name' => 'bid','require'=>true,'desc' => '科目ID'),
                'tid'=> array('name' => 'tid','require'=>true,'desc' => '机构ID'),
            ),
            'addSubject'=>array(
                'name'=> array('name' => 'name','require'=>true,'desc' => '名称'),
                'tid'=> array('name' => 'tid','require'=>true,'desc' => '机构ID'),
            ),
            'addMeal'=>array(
                'name'=> array('name' => 'name','require'=>true,'desc' => '名称'),
                'tid'=> array('name' => 'tid','require'=>true,'desc' => '机构ID'),
                'pid'=> array('name' => 'pid','type'=>'array','format' => 'json','require'=>true,'desc' => '教案ID,一维数组'),
            ),
        );
    }

    /**
     * @return bool
     * 添加教案
     */
    public function addPlan(){
        $Domain = new Plan();
        $data = [
            'name'=>$this->name,
            'type'=>$this->type,
            'tid'=>$this->tid,
            'bid'=>$this->bid
        ];
        return $Domain->create($data);
    }
    /**
     * @return bool
     * 添加科目
     */
    public function addSubject(){
        $Domain = new Subject();
        $data = [
            'name'=>$this->name,
            'tid'=>$this->tid
        ];
        return $Domain->create($data);
    }
    /**
     * @return bool
     * 添加套餐
     */
    public function addMeal(){
        $Domain = new Meal();
        $data = [
            'name'=>$this->name,
            'tid'=>$this->tid,
            'pid'=>$this->pid
        ];
        return $Domain->create($data);
    }
    /**
     * @return mixed
     * 查询教案
     */
    public function findPlan(){
        $Domain = new Plan();
        $arr = [
            'id'=>$this->id,
            'tid'=>$this->tid,
            'type'=>$this->type,
            'bid'=>$this->bid,
            'name'=>$this->name
        ];
        return $Domain->finds($arr,$this->datatype,$this->page,$this->num);
    }
    /**
     * @return mixed
     * 查询科目
     */
    public function findSubject(){
        $Domain = new Subject();
        $arr = [
            'id'=>$this->id,
            'tid'=>$this->tid,
            'name'=>$this->name
        ];
        return $Domain->finds($arr,$this->datatype,$this->page,$this->num);
    }
    /**
     * @return mixed
     * 查询套餐
     */
    public function findMeal(){
        $Domain = new Meal();
        $arr = [
            'id'=>$this->id,
            'tid'=>$this->tid,
            'name'=>$this->name
        ];
        return $Domain->finds($arr,$this->datatype,$this->page,$this->num);
    }
    /**
     * @return bool
     * 修改教案、科目、套餐
     */
    public function up(){
        $Domain = $this->domains($this->type);
        return $Domain->updata($this->where,$this->data);
    }

    /**
     * @param $type
     * @return Meal|Plan|Subject
     * domain层封装
     */
    protected function domains($type){
        switch ($type){
            case 1:
                $Domain = new Plan(); //教案
                break;
            case 2:
                $Domain = new Subject();//科目
                break;
            case 3:
                $Domain = new Meal();//套餐
                break;
        }
        return $Domain;
    }

}