<?php
namespace Order\Api;
use PhalApi\Api;
use Order;
use Order\Domain\Controller as Controllers;
use PhalApi\Exception\BadRequestException;
class Receive extends Api{


    private static $data = null;

    public function __construct()
    {
        self::getDomainAdd();
        self::getToken();

    }

    //实例化Domin层
    static  private  function getDomainAdd(){
        if (self::$data === null){
            self::$data = new  Controllers();
        }
    }
//得到token值
    static private  function getToken(){

        $token = isset($_GET['token'])?$_GET['token']:'';
//        if (!$token){
//            throw new BadRequestException('缺少必要参数token',4);
//        }

        if (!Order\getCache($token)) {
            Order\setCache($token, apiGet('User.User.Selecttoken',array('searchToken'=>$token)));

        }

    }



    public function getRules() {
        return array(
//        '*' => array(
//                'token' => array('name' => 'token','source'=>'get','require'=>true,'desc' => '用户身份token'),
//            ),

            'ceroreders' => array(
                'commodity_id'=> array('name' => 'commodity_id','require'=>true, 'desc' => '商品ID'),
                'num'=> array('name' => 'num','require'=>true, 'desc' => '商品数量'),
                'code'=> array('name' => 'code','default'=>'', 'desc' => '优惠码'),
            ),
            'sponsor' => array(
                'oid'=> array('name' => 'oid','require'=>true, 'desc' => '订单ID'),
                'type'=> array('name' => 'type','require'=>true, 'desc' => '支付类型'),
                'agument'=> array('name' => 'agument','desc' => '支付预加参数'),
            ),
            'notifycurrencys' => array(
//                'data'=> array('name' => 'data','require'=>true, 'desc' => '订单状态'),
            ),
            'orderfindmess' => array(
                'oid'=> array('name' => 'oid','require'=>true, 'desc' => '订单ID'),
            ),
            'findcodes' => array(
                'code'=> array('name' => 'code','require'=>true, 'desc' => '推广码'),
            ),
            'overorderon_sta' => array(
                'oid'=> array('name' => 'oid','require'=>true, 'desc' => '订单ID'),
                'order_status'=> array('name' => 'order_status','require'=>true, 'desc' => '订单状态'),
                'statu'=> array('name' => 'statu','require'=>true, 'desc' => '订单状态'),
            ),
            'orderall' => array(
                'page'=> array('name' => 'page','default'=>1, 'desc' => '当前页数'),
                'select'=> array('name' => 'select','require'=>true,'defaut'=>'','desc' => '查询信息，json数据'),
                'order'=> array('name' => 'order','default'=>'id','desc' => '排序值'),
                'order_aes'=> array('name' => 'order_aes','default'=>'1','desc' => '默认正序还是倒序'),
                'money_code'=> array('name' => 'money_code','default'=>null,'desc' => '检测是否有使用优惠券'),
                'starttime'=> array('name' => 'starttime','default'=>'','desc' => '开始支付时间'),
                'endtime'=> array('name' => 'endtime','default'=>'','desc' => '结束支付时间'),
                'key'=> array('name' => 'key','default'=>'','desc' => 'key'),
                'value'=> array('name' => 'value','default'=>'','desc' => 'value'),
                'commodity_type'=> array('name' => 'commodity_type','default'=>'','desc' => '商品like'),
                'code_type'=> array('name' => 'code_type','default'=>'','desc' => '码的类型'),
                'number'=> array('name' => 'number','default'=>40,'desc' => '分页条数'),
            ),
            'orderdis' => array(
                'type'=> array('name' => 'type','require'=>true, 'desc' => '当前页数'),
//                'select'=> array('name' => 'select','require'=>true,'defaut'=>'','desc' => '查询信息，json数据'),
//                'order'=> array('name' => 'order','default'=>'id','desc' => '排序值'),
//                'order_aes'=> array('name' => 'order_aes','default'=>'1','desc' => '默认正序还是倒序'),
//                'money_code'=> array('name' => 'money_code','default'=>null,'desc' => '检测是否有使用优惠券'),
//                'starttime'=> array('name' => 'starttime','default'=>'','desc' => '开始支付时间'),
//                'endtime'=> array('name' => 'endtime','default'=>'','desc' => '结束支付时间'),
//                'key'=> array('name' => 'key','default'=>'','desc' => '结束支付时间'),
//                'value'=> array('name' => 'value','default'=>'','desc' => '结束支付时间'),
            ),

        );
    }


//创建订单
public function ceroreders(){
        $arr = [
            'commodity_id'=>$this->commodity_id,
            'num'=>$this->num,
            'code'=>$this->code
        ];
    return self::$data->cerOrder($arr);

}
//发起支付请求参数
public function sponsor(){
    $arr = [
        'oid'=>$this->oid,
        'type'=>$this->type,
        'agument'=>$this->agument
    ];

    return self::$data->sponsors($arr);
}

//支付回调货币系统
public function notifycurrencys(){
//         return $this->data;
    $data = $_POST['data'];

    if ($data['sign'] != '798bdd1ec31d173a'){
        throw new BadRequestException('签名验证失败',3);
    }
    $arr = [
        'pay_info'=>$data['pay_info'],
        'data'=>$data['data']
    ];
    return self::$data->payBack($arr);

}

//订单详情
public  function orderfindmess(){

    return self::$data->findorder($this->oid);

}
//取消订单
public  function overorderon_sta(){
    $arr = [
        'oid'=>$this->oid,
        'order_status'=>$this->order_status,
        'statu'=>$this->statu
    ];
    return self::$data->ordersta($arr);

}
//订单列表
public function orderall(){

    $arr = [
        'page'=>$this->page,
        'select'=>$this->select,
        'order'=>$this->order,
        'money_code'=>$this->money_code,
        'order_aes'=>$this->order_aes,
        'starttime'=>$this->starttime,
        'endtime'=>$this->endtime,
        'key'=>$this->key,
        'value'=>$this->value,
        'commodity_type'=>$this->commodity_type,
        'code_type'=>$this->code_type,
        'number'=>$this->number
    ];

    return self::$data->allorder($arr);

}
//推广码数据统计
public function disproAll(){

    return self::$data->allPro();

}
//指定用户的使用次数
public function findcodes(){

    return self::$data->findallcodes($this->code);
}

//查询指定码的使用
public function findcode(){

    return self::$data->findallcode();

}
//订单数据统计
public function orderdataAllsti(){

    return self::$data->allsta();

}
public  function orderdis(){
    $arr = [
        'type'=>$this->type
    ];

    return self::$data->disorderll($arr);
}

}