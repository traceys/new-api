<?php
namespace Order\Model;

use PhalApi\Model\NotORMModel as NotORM;

class currency_deal extends NotORM {


    //货币减少进入到指定状态
    public  function  subtractres($val){

        $model = $this->getORM();
        return $model->insert($val);

    }

    //查询指定贸易信息
    public function findu($uid,$type,$page){
        $model = $this->getORM();
       return $model->where(['uid'=>$uid,'money_type'=>$type])->limit($page,10);

    }
    //插入指定货币交易
    public function inser($val){
        $model = $this->getORM();
        $data = $model->insert($val);
        return $data;

    }
    //通过指定订单号查询指定账户信息
    public function findorder($order_id){
        $model = $this->getORM();
        $data = $model->where(['order_id'=>$order_id,'statu'=>1])->fetchOne();
        return $data;
    }
    //通过id修改指定账户记录
    public function upid($id,$data){
        $model = $this->getORM();
        $data = $model->where(['id'=>$id])->update($data);
        return $data;
    }

}

