<?php
namespace Order\Model;

use PhalApi\Model\NotORMModel as NotORM;

class currency_discounts extends NotORM {



    //添加指定优惠码
    public function  add($data){
        $model = $this->getORM();

       return $model->insert($data);

    }
    //修改指定优惠码信息
    public function updateCode($code,$val){
        $model = $this->getORM();

        return $model->where(['key'=>$code])->update($val);

    }
    //修改优惠码可使用次数
    public function upNum($val,$num,$calculate){
        $model = $this->getORM();

        return  $model->where(['key'=>$val])->update(array('had_num' => new \NotORM_Literal("had_num $calculate $num")));


    }
    //查询优惠码信息
    public function find($code){
        $model = $this->getORM();

        return $model->where(['`key`'=>$code])->fetchOne();


    }




}

