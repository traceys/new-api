<?php
namespace Order\Model;

use PhalApi\Model\NotORMModel as NotORM;

class order_order extends NotORM {


    //创建订单
    public function cert($val){
        $model = $this->getORM();
       return $model->insert($val);

    }
    //查询指定订单信息
    public  function find($id){
        $model = $this->getORM();
        return $model->where(['id'=>$id])->fetchOne();

    }

    //修改订单
    public function overOrder($id,$val){
        $model = $this->getORM();

        return $model->where(['id'=>$id])->update($val);


    }

//展示所有订单
public function findall($val,$type=false){
    $model = $this->getORM();
    $data = $model->where(['statu'=>1]);
    if (json_decode($val['select'])){
        $data = $data->where(json_decode($val['select'],true));
    }
    //时间
    if ($val['starttime']){
        $data = $data->where('end_time > ?',$val['starttime']);
    }
    if ($val['endtime']){
        $data = $data->where('end_time < ?',$val['endtime']);
    }
    if ($val['money_code'] !== null){

        if ($val['money_code']){
            $data = $data->where("money_code IS NOT ?",null);
        }
        if (!$val['money_code']){
            $data = $data->where('money_code = ?',null);
        }
    }
    if ($val['commodity_type']){
        $commodity_type = $val['commodity_type'];
        $data = $data->where("commodity_type like ?","$commodity_type%");
    }

    if ($val['key']){
        $key = $val['key'];
        $value = $val['value'];
        $data = $data->where("$key like ?","%$value%");

    }
    $act = $val['order'];
    if ($val['order_aes'] == '1'){
        $data = $data->order("$act ASC");
    }else{
        $data = $data->order("$act DESC");
    }
    if ($val['page'] != -1){
        $page =  ($val['page']-1)*$val['number'];
        $data = $data->limit($page,$val['number']);
    }
    if (!$type){
        $data = $data->fetchAll();
    }
    if ($type){
        $data = $data->count('id');
    }

return $data;


}
//查询指定码的使用次数
public function findcode(){
    $model =  $this->getORM();
    $data = $model->select('money_code,count(*) AS num')->where(['statu'=>1,'pay_status'=>1,])->where('money_code  IS NOT ?',null)->group('money_code,uid')->fetchAll();
    return $data;

}
//查询指定码的使用次数
    public function findcodes($code){
        $model =  $this->getORM();
        $data = $model->select('money_code,count(*) AS num')->where(['statu'=>1,'pay_status'=>1,'money_code'=>$code])->fetchAll();
        return $data;
    }

//指定类型码的订单
public function alldis($arr){
    $model =  $this->getORM();
    $sql = "SELECT * FROM api_order_order AS a 
                LEFT JOIN api_currency_discounts AS b ON a.money_code=b.key 
                WHERE a.money_code != ''";

    if ($arr['type']){
        $type = $arr['type'];
        $sql = $sql." && b.type='$type'";

    }
    if ($arr['order_status']){
        $order_status = $arr['order_status'];
        $sql = $sql." && a.order_status=$order_status";
    }


    $arr = $model->queryAll($sql, array());

    return $arr;


}

//查询指定推广码使用结果
public function proCode($start,$end){
    $model =  $this->getORM();
    $data = $model->where('create_time > ?',$start)->where('create_time < ?',$end)->fetchAll();
    return $data;
}
//查询指定用户是否使用过指定卷
public function UserFindCodes($where){
    $model =  $this->getORM();
    $data = $model->where($where)->fetchOne();
    return $data;

}



}