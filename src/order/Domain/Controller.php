<?php
namespace Order\Domain;
use Order\Model\currency_deal;
use Order\Model\currency_currency;
use Order\Model\currency_discounts;
use Order\Model\currency_list;
use Order\Model\currency_toup;
use Order\Model\order_order;
use Order\Model\commodity_commodity;
use Order\Model\order_log;
use Order\Model\commodity_record;
use PhalApi\Exception\BadRequestException;
use Order;
use Statis\Api\Orderlist;

class Controller {


    //删除用户货币
    public function  reduceCurrency($val){

        $deal = new currency_deal();

        $currency = new currency_currency();

        //减少金额
        $arr = [
            'uid'=>$val['uid'],
            'money_type'=>$val['money_type']
        ];

        $a =  $currency->find($arr);

        if ($a['statu'] == '-1'){
            throw new BadRequestException('用户货币失效不可减少',3);
        }


       $num = $a['money_num'];
        //判断用户货币是否足够
        if ($num < $val['num']){
            throw new BadRequestException('用户货币不足',1);
        }

        $dealarr = [
            'uid'=>$val['uid'],
            'to_uid'=>-1,
            'money_type'=>$val['money_type'],
            'money_num'=>$val['num'],
            'act'=>-1,
            'start_time'=>Order\timeout(time()),
            'end_time'=>Order\timeout(time()+1),
            'pay_status'=>1
        ];
        //记录状态
        $state = $deal->subtractres($dealarr);

        if ($state){


       $res =  $currency->rezyu($arr,$val['num']);

        return $res;

        }

    }


    //转账货币
    public function  accounts($val){
        $deal = new currency_deal();


        $currency = new currency_currency();

        //减少金额
        $arr = [
            'uid'=>Order\getCache($_GET['token'])['uid'],
            'money_type'=>$val['money_type']
        ];


        $a =  $currency->find($arr);

        if ($a['statu'] == '-1'){
            throw new BadRequestException('用户货币失效不可减少',3);
        }


        $num = $a['money_num'];
        //判断用户货币是否足够
        if ($num < $val['num']){
            throw new BadRequestException('用户货币不足',4);
        }



        //转账数据
        $dealarr = [
            'uid'=> Order\getCache($_GET['token'])['uid'],
            'to_uid	'=>$val['uid'],
            'money_type'=>$val['money_type'],
            'money_num'=>$val['num'],
            'act'=>1,
            'start_time	'=>Order\timeout(time()),
            'end_time'=>Order\timeout(time()+1),
            'pay_status'=>1
        ];
        //记录状态
        $state = $deal->subtractres($dealarr);

        if ($state){
                //减少该用户货币
              $currency->rezyu($arr,$val['num']);
                //增加指定用户货币

            $arr['uid'] = $val['uid'];

            $res = $currency->appendTO($arr,$val['num']);

            return $res;


    }


    }
    //用户提现
    public  function  withdrawDeposit($val){
        $deal = new currency_deal();

        $currency = new currency_currency();


        $dealarr = [
            'uid'=>Order\getCache($_GET['token'])['uid'],
            'to_uid	'=>-1,
            'money_type'=>$val['money_type'],
            'money_num'=>$val['num'],
            'act'=>0,
            'start_time	'=>Order\timeout(time()),
            'end_time'=>Order\timeout(time()+1),
            'pay_status'=>0
        ];
        $state = $deal->subtractres($dealarr);

        if($state){

            $arr = [
                'uid'=>Order\getCache($_GET['token'])['uid'],
                'money_type'=>$val['money_type']
            ];

            $res =  $currency->rezyu($arr,$val['num']);

            return $res;
        }

    }
    //增加优惠码
    public function appendD($val){
        $dis = new currency_discounts();

        $data = [
            'key'=>Order\getRandomString(13),
            'uid'=>$val['uid'],
            'type'=>$val['type'],
            'item_id'=>$val['item_id'],
            'reduce_num'=>$val['reduce_num'],
            'had_num'=>$val['num'],
            'time'=>$val['time']
        ];

        return $dis->add($data);

    }

    //优惠码修改
    public function upCode($val,$type='delete',$calculate = '+'){
        $dis = new currency_discounts();
        if ($type == 'delete'){
            $arr = [
                'statu'=> -1
            ];
            return $dis->updateCode($val,$arr);
            //修改优惠码次数
        }else if($type == 'update'){
            $code = $val['code'];
            $num = $val['num'];

            return $dis->upNum($code,$num,$calculate);


        }

    }
    //查询优惠码信息
    public function findCodes($val){

        $dis = new currency_discounts();


        return $dis->find($val);


    }
    //创建货币类型
    public function appendList($val){
        $list = new currency_list();

        return $list->inser($val);

    }

    //管理货币
    public function mldmone($val){
        $list = new currency_list();
        $currency = new currency_currency();
        if ($val['on_status'] == '-1'){
            $data = $list->find($val['id']);
         $one = $data['to_one'];
         $type = $data['money_type'];
         $currency->transition($type,$one);
        }
    return $list->upStatu($val['id'],$val['type']);


    }
    //查询个人货币系统
    public function findCuren(){
        $currency = new currency_currency();

        return $currency->finduid(Order\getCache($_GET['token'])['uid']);


    }
    //查询个人指定货币交易类型
    public function dealType($val){
        $deal = new currency_deal();
        return $deal->findu(Order\getCache($_GET['token'])['uid'],$val['money_type'],$val['page']);

    }
    //增加个人货币
    public function appendencs($val){
        $currency = new currency_currency();

       return $currency->addcur($val['uid'],$val['money_type'],$val['num']);
    }
    //创建订单
    public function cerOrder($val){
        $order = new order_order();
        $list = new currency_list();
        $deal = new  currency_deal();
        $Commodity = apiGet('Commodity.commoditys.message',array('id'=>$val['commodity_id']));

        $money = $Commodity['money'];
        $code = $this->findCodes($val['code']);
        $uid = Order\getCache($_GET['token'])['Id'];
        if ($uid == -1){
            throw new BadRequestException('您还未登录',3);
        }
        //优惠码信息
        if (!$code){
            $val['code'] = null;
        }else{
            //检测该优惠券用户是否使用过
            if ($code['type'] == 'discounts'){
                $where = [
                    'uid'=>$uid,
                    'order_status'=>200,
                    'money_code'=>$val['code'],
                    'statu'=>1
                ];
            $re = $order->UserFindCodes($where);
            if ($re){
                throw new BadRequestException('该优惠券已经使用过了',3);
            }
            }
            switch ($code['astype']){
                //如果为代金券的话
                case "moneynum":
                    $money =  round ($money-$code['reduce_num'],2);
                    if ($money <= 0){
                        $money = 0;
                    }

                    break;
                    //如果为折扣卷的话
                case 'discount':
                    $money = $money-$money*($code['reduce_num']/100);
                    $money = sprintf(ceil($money*100)/100);

                    break;
                    //如果为商品卷的话
                case 'shopping' :
                    $money = $code['reduce_num'];
                    break;
                    //货币卷
                case 'currency':
                    $money = $money-$code['reduce_num']<0?0:$money-$code['reduce_num'];
                    break;
            }
        }
        $add = IPaddress($_SERVER['REMOTE_ADDR']);
        $add = implode('',$add);
        $arr = [
            'uid'=>$uid,
            'commodity_type'=>$Commodity['type'],
            'commodity_id'=>$val['commodity_id'],
            'num'=>$val['num'],
            'money_type'=>$Commodity['money_type'],
            'money'=>$money,
            'money_code'=>$val['code'],
            'pay_status'=>0,
            'order_status'=>0,
            'create_time'=>Order\timeout(time()),
            'code_type'=>$code['type'],
            'add'=>$add
        ];

        $data =  $order->cert($arr);
        //货币支付记录
        $dealdata = [
            'id'=>order\createdeal_id(),
            'uid'=>$uid,
            'to_uid'=>-1,
            'money_type'=>$Commodity['money_type'],
            'money_num'=>$money,
            'act'=>2,
            'start_time'=>Order\timeout(time()),
            'pay_status'=>0,
            'order_id'=>$data['id'],
            'on_status'=>-1
        ];
        $deal->inser($dealdata);
        return $data;
    }
    //发起支付请求
    public  function  sponsors($val){
        $currency = new currency_currency();
        $order = new order_order();

        $res = $order->find($val['oid']);

        if ($res['act_status'] == 1){
            throw new BadRequestException('该订单已支付',3);
        }
        if ($res['money'] <= 0){
            //做回调请求
            //订单号
            //交易号
            //交易类型
            //备注
            $data = [
              'out_trade_no'=>$res['id'],
                'trade_no'=>time().rand(1000,9999),
                'type'=>'system',
                'desc'=>'支付金额等于0元，直接进入订单环节'
            ];

              $re =  $this->payBack(base64_encode(json_encode($data)));

                if ($re['value'] == 'success'){
                    return '购买成功';
                }else{
                    throw new BadRequestException('购买错误,查看订单日志',100);
                }

        }

        //检测是否需要调用支付接口
//        if ($res['money'] != 0){
//            $datawhere = [
//                'money_type'=>$res['money_type'],
//                'statu'=>1
//            ];
//
//            $curr =$currency->find($datawhere);
////            return $curr;
//            //如果没有该货币
//            if (!$curr){
//                throw new BadRequestException('未拥有该货币',103);
//            }
//            //如果该货币不足
//            if ($curr['money_num']<$res['num']){
//                throw new BadRequestException('货币不足',1);
//            }
//            $cudata = [
//                'uid'=>Order\getCache($_GET['token'])['Id'],
//                'money_type'=>$res['money_type'],
//                'num'=>$res['num']
//            ];
//            //删除货币结果
//            $delecur =  $this->reduceCurrency($cudata);
//
//            if (!$delecur){
//                throw new BadRequestException('货币无法删除',7);
//            }
//
//            //结束订单
//            $data = [
//                'out_trade_no'=>$res['id'],
//                'trade_no'=>time().rand(1000,9999),
//                'type'=>'system',
//                'desc'=>'支付金额等于0元，直接进入订单环节'
//            ];
//
//            $re =  $this->payBack(base64_encode(json_encode($data)));
//
//            if ($re['value'] == 'success'){
//                return '购买成功';
//            }else{
//                throw new BadRequestException('购买错误,查看订单日志',100);
//            }
//
//        }
        $title ='育视界-'.apiGet('Commodity.commoditys.message',array('id'=>$res['commodity_id']))['name'];
        $arr = ['title'=>$title,'order'=>$res['id'],'money'=>$res['money']];
        //如果请求为支付宝的话
        if ($val['type'] == 1){
            return $arr;
        }

        //检测其余请求方式
        switch ($val['type']){
            //微信二维码
            case 2:
                $url = "https://test.yusj.vip/Exit/Wxpaywb/example/native.php";
                break;
                //微信jsapi
            case 3:
                $url = "https://test.yusj.vip/Exit/Wxpayprocedure/example/jsapi.php";
                break;
            default:
                $url = null;
                break;
        }
            //检测是否有openid存在
            if ($val['agument']){
                $arr['openid'] = json_decode($val['agument'],true)['open_id'];
            }

            //sign验证
            $arr['sign'] = '798bdd1ec31d173a';

//            return $arr;
        try {
            // 实例化时也可指定失败重试次数，这里是2次，即最多会进行3次请求
            $curl = new \PhalApi\CUrl(2);
            // 第二个参数为待POST的数据；第三个参数表示超时时间，单位为毫秒
            $rs = $curl->post($url, $arr, 3000);
            // 一样的输出

        } catch (\PhalApi\Exception\InternalServerErrorException $ex) {
            throw new BadRequestException('支付请求失败',100);
        }
            return $rs;

    }

    //支付返回结果操作
    public function payBack($val){
        //回调数据
        $data = json_decode(base64_decode($val['data']),true);;

        $order = new order_order();
        $commondity = new commodity_commodity();
        $deal = new currency_deal();
        //查询的订单信息
        $ordata = $order->find($data['out_trade_no']);
        if ($ordata['order_status'] == 200){
            return false;
        }
        //支付记录
        $deda =  $deal->findorder($ordata['id']);
        $dedata = [
            'pay_status'=>1,
            'on_status'=>1,
            'end_time'=>Order\timeout(time()),
        ];
        $deal->upid($deda['id'],$dedata);

        //推广码开关
        $switch = 0;
        if ($ordata['money_code']){
            $dispro = apiGet('Currency.receive.findcode',array('code'=>$ordata['money_code']));
            //优惠码次数减少
            apiGet('Currency.receive.reducecodenum',array('code'=>$ordata['money_code'],'num'=>1));
            if ($dispro['type'] == 'PromotionCode'){
                $switch = 1;
            }
        }
        //查询商品信息
         $condata =  $commondity->find($ordata['commodity_id'])['0'];
         $cda = explode('_',$condata['type']);
            switch ($cda['0']){
                case 'vip':
                    $urldata = [
                        'tid'=>318,
                        'uid'=>$ordata['uid'],
                        'long_time'=>$cda['1']*31*24*60*60,
                    ];
                    break;
                case 'video' || 'drama':
                    $cda['1'] = isset($cda['1'])?$cda['1']:12;
                    $res_id = apiGet('Video.drama.findcom_id',array('type'=>$cda['0'],'com_id'=>$condata['id']))['id'];
                    //视频定期任务添加
                    $videores = array(
                        'uid'=>$ordata['uid'],
                        'res_id'=>$res_id,
                        'type'=>$cda['0'],
                        'end_time'=>Order\timeout(time()+$cda['1']*31*24*60*60)
                    );
                    apiGet('Video.timedTask.authAdd',$videores);
                    $urldata = [
                        'fid'=>$ordata['uid'],
                        'ftype'=>'user',
                        'res_type'=>$cda['0'],
                        'res_id'=>$res_id,
                        'server'=>'read',
                        'value'=>1
                    ];
                    break;
            }
            $rest = apiGet($condata['url'],$urldata);
            if ($rest){
                $arrorder = [
                    'pay_status'=>1,
                    'pay_id'=>$data['trade_no'],
                    'act_status'=>1,
                    'way'=>$data['type'],
                    'order_status'=>200,
                    'end_time'=>Order\timeout(time())
                ];
                //推广码记录
                if ($switch){
                    //查询收益比例
                    $Selone =  apiGet('Configure.Configure.Selone',array('type'=>'userGetMoneyFromSpread','key'=>$ordata['uid']));
                    $sm = intval($Selone['value'])/100;
                    $arrorder['originalPrice'] =round($ordata['money']*$sm,2);
                    $arrorder['earnings'] = intval($Selone['value']);
                    $curdata = [
                        'uid'=>$dispro['uid'],
                        'money_type'=>$condata['money_type'],
                        'num'=>$arrorder['originalPrice']
                    ];
                    //增加收益
                    apiGet('Currency.receive.addcursd',$curdata);
                    //增加积分
                    addjifen('hytg',$dispro['uid']);
                    $mall = strreplace(apiGet('User.User.Userone',array('uid'=>$dispro['uid']))['telphone']);
                    $message = '您的推广码新增VIP会员'.$mall.'，获得收益'.$arrorder['originalPrice'].'元已计入您的账户余额。详情查看“个人中心-银行卡”。育视界，让教育无处不在。';
                    userMessage($dispro['uid'],'VIP服务',$message);

                    //货币收益记录
                    $dealdata = [
                        'id'=>order\createdeal_id(),
                        'uid'=>$dispro['uid'],
                        'to_uid'=>-1,
                        'money_type'=>$ordata['money_type'],
                        'money_num'=>$arrorder['originalPrice'],
                        'act'=>3,
                        'start_time'=>Order\timeout(time()),
                        'end_time'=>Order\timeout(time()),
                        'pay_status'=>1,
                        'order_id'=>$ordata['id']
                    ];
                    $deal->inser($dealdata);
                }

                //订单结束
                $order->overOrder($ordata['id'],$arrorder);
                $record = new commodity_record();
                //记录商品购买记录
                $recorddata = [
                    'uid'=>$ordata['uid'],
                    'cid'=>$ordata['commodity_id'],
                    'num'=>$ordata['num'],
                    'time'=>Order\timeout(time())
                ];
                //记录商品记录
                $record->record($recorddata);

                $res = true;
            }else{
                $res = false;
            }
                $redate = [
                    'oid'=>$ordata['id'],
                    'act'=>'update订单结束',
                    'value'=>'success',
                    'time'=>Order\timeout(time()),
                    'uid'=>0
                ];
            if ($res){
                $redate['value'] = 'success';
            }else{
                $redate['value'] = ['error'];
            }
             $orderlog = new order_log();

            return $orderlog->operationMessage($redate);




    }

    /**
     * @param $data // 支付回到自定义返回参数
     * @param $val  // 支付返回原始信息经过加密后
     * @param $money // 返回金额
     */

    //货币系统充值后的增加
    public function currensd($data,$val,$money){

        $order = new order_order();
        $toup = new currency_toup();
        $currency = new currency_currency();
        $comm = new commodity_commodity();
        //记录充值信息
        $arr = [
            'pay_id'=>$data['trade_no'],
            'pay_type'=>$data['type'],
            'order_id'=>$data['out_trade_no'],
            'desc'=>$data['desc'],
            'data'=>$val['pay_info'],
            'money'=>$money,
            'pay_status'=>1,
            'time'=>Order\timeout(time()),
            'on_statu'=>1
        ];

        //记录货币充值信息
        $toup->inserRecord($arr);

        //查询订单信息
        $ordata = $order->find($data['out_trade_no']);

        // Step 1: 开启事务
        \PhalApi\DI()->notorm->beginTransaction('db_master');

        try{


            $res = [
                'money_type	'=>$ordata['money_type'],
                'uid'=>$ordata['uid']
            ];
            //对货币进行增加
            $currency->appendTO($res,$ordata['num']);


            $arrorder = [
                'pay_status'=>1,
                'pay_id'=>$data['trade_no'],
                'act_status'=>1,
                'order_status'=>200,
                'end_time'=>Order\timeout(time())
            ];
            //订单结束
           $order->overOrder($ordata['id'],$arrorder);

            \PhalApi\DI()->notorm->commit('db_master');

            $rec =  true;

        }catch(\Exception $e){
            \PhalApi\DI()->notorm->rollback('db_master');
            $rec =  false;
        }

return $rec;





    }
    //查询订单返回信息
    public function findorder($val){


        $order = new order_order();
        $list = new currency_list();
       $data =  $order->find($val);
       $money_type = $data['money_type'];
       $data['money_type'] = $list->find($money_type)['desc'];
        return $data;
    }

   //修改订单状态
public function ordersta($val){

    $order = new order_order();
    $ordata =  $order->find($val);

    if ($ordata['order_status'] == 0){
        throw new BadRequestException('该订单状态不可修改或删除',3);
    }

    $arr = ['order_status'=>$val['order_status'],'statu'=>$val['statu']];


    return $order->overOrder($val['oid'],$arr);

}

//查看所有订单状态
public function allorder($val){
    $order = new order_order();
    if ($val['select']){
        $data = json_decode($val['select'],true);
        if (isset($data['money_code']) && !$data['money_code']){
            throw new BadRequestException('请输入指定推广码');
        }
    }

    $datas =  $order->findall($val);
    $num = $order->findall($val,true);


    outputArgs(array('num'=>$num,'pagenum'=>$val['number'],'age'=>$val['page']));
    return $datas;

}

//查询码的使用
public function findallcode(){
    $model = new order_order();

    return $model->findcode();

}

//查询指定码的使用
    public function findallcodes($code){
        $model = new order_order();

        return $model->findcodes($code);

    }

//指定类型码的使用
public function disorderll($val){
    $model = new order_order();

    return $model->alldis($val);

}

//指定码数据统计
public function allPro(){
    $arr = [];
    $arr['type'] = 'PromotionCode';
    $arr['order_status'] = 200;
    $model = new order_order();
    $res =  $this->disorderll($arr);
//    return $res;
    //总数
    $data['a'] =  count($res);
    $data['b'] = 0;
    $data['c'] = 0;
    foreach ($res as $key => $value){
        $data['c'] += $value['originalPrice'];
        $data['b'] += $value['money'];
    }

//    $start = date("Y-m-d",time());
//    $end = date('Y-m-d',strtotime('+1 day'));

    //今日新增人数
//    $data['today'] = count($model->proCode($start,$end));
//
//    $start = date('Y-m-d',strtotime('-1 day'));
//    $end = date("Y-m-d",time());;
    //昨日新增人数
//    $data['yesterday'] = count($model->proCode($start,$end));

    return $data;
}

//数据统计订单
public function allsta(){

    $model = new order_order();


//    $data['a'] =  count($res);

        $start = date("Y-m-d",time());
        $end = date('Y-m-d',strtotime('+1 day'));

    //今日新增人数
    $data['b'] = count($model->proCode($start,$end));
    $start = date('Y-m-d',strtotime('-1 day'));
    $end = date("Y-m-d",time());;
    //昨日新增人数
    $data['c'] = count($model->proCode($start,$end));

    return $data;

}



}
