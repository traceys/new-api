<?php
namespace Order;

//redis set缓存
function setCache($key,$value){
    $key = 'video'.$key;
    return \PhalApi\DI()->redis->set_forever($key,$value);

}
//redis get缓存
function getCache($key){
    $key = 'video'.$key;
    return \PhalApi\DI()->redis->get_forever($key);
}
//redis delete缓存
function deleteCache($key){
    $key = 'video'.$key;
    return \PhalApi\DI()->redis->del($key);
}
//redis 清楚指定前缀的所有 key值
function clerCache($key){
    return \PhalApi\DI()->redis->delKeys($key);
}
//将指定时间戳转换为时间格式
function timeout($val){
    return date("Y-m-d H:i:s",time());
}
//随机生成一个优惠
function getRandomString($len, $chars=null)
{
    if (is_null($chars)) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    }
    mt_srand(10000000*(double)microtime());
    for ($i = 0, $str = '', $lc = strlen($chars)-1; $i < $len; $i++) {
        $str .= $chars[mt_rand(0, $lc)];
    }
    return $str;
}
//抓取接口内容
function curls($url){
    $ch=curl_init($url);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($ch,CURLOPT_BINARYTRANSFER,true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $output= curl_exec($ch);
    curl_close($ch);
    return json_decode($output,true);
}
//解析类型后面的数据
function timemonth($val){
    $arr =  explode('_',$val);
    return $arr;
}
//生成货币交易处理
function createdeal_id(){
    return date("YmdHis",time()).rand(1000,9999);
}