<?php
namespace Video\Api;
use PhalApi\Api;
use Video\Domain\DomainTimedTask as DomainTimedTask;
use Video;
use PhalApi\Exception\BadRequestException;
class timedTask extends Api{

    private static $data = null;

    public function __construct()
    {
        self::getDomainAdd();
    }

    //实例化domin层
    static  private  function getDomainAdd(){
        if (self::$data === null){
            self::$data = new  DomainTimedTask();
        }

    }
    public function getRules() {
        return array(
            'authAdd' => array(
                'uid' => array('name' => 'uid','require'=>true,'desc' => '用户ID'),
                'res_id' => array('name' => 'res_id','require'=>true,'desc' => '资源ID'),
                'type' => array('name' => 'type','require'=>true,'desc' => '资源类型'),
                'end_time' => array('name' => 'end_time','require'=>true,'desc' => '到期时间'),
            ),
        );
    }

    /**
     * 添加定时任务
     */
    public function authAdd(){
            $arr = [
                'uid'=>$this->uid,
                'res_id'=>$this->res_id,
                'type'=>$this->type,
                'end_time'=>$this->end_time
            ];
      return self::$data->addData($arr);
    }
    /**
     * execte 执行任务
     */
    public function execte(){
        return self::$data->findExecte();
    }







}