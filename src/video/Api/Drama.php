<?php
namespace Video\Api;
use PhalApi\Api;
use Video\Domain\Addvideo as Addvideo;
use Video;
use PhalApi\Exception\BadRequestException;
class Drama extends Api{

    private static $data = null;

    public function __construct()
    {
//        return 123;
        self::getDomainAdd();
        self::getToken();
    }

    //实例化domin层
    static  private  function getDomainAdd(){
        if (self::$data === null){
            self::$data = new  Addvideo();
        }

    }
    //得到token值
    static private  function getToken(){

        $token = isset($_GET['token'])?$_GET['token']:'';

//        if (!$token){
//            throw new BadRequestException('缺少必要参数token',4);
//        }
        if (!Video\getCache($token)){

            $url = "User.User.Selecttoken&searchToken=$token";
            $res = apiGet($url);

            if ($res ){
                Video\setCache($token,$res);
            }
        }

    }
    //记录信息验证
    public function getRules() {
        return array(
            '*' => array(
                'token' => array('name' => 'token','source'=>'get','desc' => '用户身份token'),
            ),
            'uploading' => array(
                'title' => array('name' => 'title','source'=>'get','require'=>true,'desc' => '视频标题'),
                'drama_id' => array('name' => 'drama_id','source'=>'get','require'=>true,'desc' => '剧集ID'),
                'level' => array('name' => 'level','source'=>'get','require'=>true,'desc' => '视频级别'),
                'url' => array('name' => 'url','source'=>'get','require'=>true,'desc'=> '视频解析地址'),
                'desc' => array('name' => 'desc','source'=>'get','default'=>'','desc' => '视频备注'),
                'videofile' => array('name' => 'videofile','source'=>'get','default'=>null,'desc' => '视频自带文件url'),
                'tags' => array('name' => 'tags','source'=>'get','require'=>true,'desc' => '视频标签'),
                'url_type' => array('name' => 'url_type','source'=>'get','require'=>true,'desc' => '视频解析地址类型'),
                'play_url' => array('name' => 'play_url','source'=>'get','default' =>null,'desc' => '视频地址'),
                'img' => array('name' => 'img','source'=>'get','default' =>null,'desc' => '视频封面图片'),
                'info' => array('name' => 'info','source'=>'get','default' =>null,'desc' => '视频简介'),
                'money' => array('name' => 'money','source'=>'get','require' =>true,'desc' => '商品价格'),
                'con_status' => array('name' => 'con_status','source'=>'get','require' =>true,'desc' => '商品状态'),
                'on_status' => array('name' => 'on_status','source'=>'get','default' =>-1,'desc' => '视频状态'),
            ),
            'video' => array(
                'id' => array('name' => 'id','source'=>'get','require'=>true,'desc' => '视频ID'),
            ),
            'drama' => array(
                'id' => array('name' => 'id','source'=>'get','require'=>true,'desc' => '剧集ID'),
                'level' => array('name' => 'level','source'=>'get','default'=>0,'desc' => '指定视屏等级'),
                'label' => array('name' => 'label','source'=>'get','default'=>0,'desc' => '标签筛选'),
                'type' => array('name' => 'type','source'=>'get','default'=>true,'desc' => '是否展现数据类型'),
            ),
            'upvideo' => array(
                'title' => array('name' => 'title','source'=>'get','default'=>null,'desc' => '视频标题'),
                'drama_id' => array('name' => 'drama_id','source'=>'get','default'=>null,'desc' => '剧集ID'),
                'level' => array('name' => 'level','source'=>'get','default'=>null,'desc' => '视频级别'),
                'videofile' => array('name' => 'videofile','source'=>'get','default'=>null,'desc' => '视频自带文件地址'),
                'url' => array('name' => 'url','source'=>'get','default'=>null,'desc' => '视频解析地址'),
                'tags' => array('name' => 'tags','source'=>'get','default'=>null,'desc' => '视频标签'),
                'url_type' => array('name' => 'url_type','source'=>'get','default'=>null,'desc' => '视频解析地址类型'),
                'play_url' => array('name' => 'play_url','source'=>'get','default' =>null,'desc' => '视频地址'),
                'img' => array('name' => 'img','source'=>'get','default' =>null,'desc' => '视频封面图片'),
                'info' => array('name' => 'info','source'=>'get','default' =>null,'desc' => '简介'),
                'on_status' => array('name' => 'on_status','source'=>'get','default' =>null,'desc' => '状态'),
                'id' => array('name' => 'id','source'=>'get','require'=>true,'desc' => '视频ID'),
                'desc' => array('name' => 'desc','source'=>'get','default'=>null,'desc' => '视频ID'),
                'money' => array('name' => 'money','source'=>'get','default' =>null,'desc' => '商品价格'),
                'con_status' => array('name' => 'con_status','source'=>'get','default' =>null,'desc' => '商品状态'),
            ),
            'adddrama' => array(
                'title' => array('name' => 'title','source'=>'get','require'=>true,'desc' => '剧集标题'),
                'info' => array('name' => 'info','source'=>'get','default'=>'','desc' => '剧集简介'),
                'desc' => array('name' => 'desc','source'=>'get','default'=>'','desc' => '备注'),
                'img' => array('name' => 'img','source'=>'get','require'=>true,'desc' => '剧集封面'),
                'on_status' => array('name' => 'on_status','source'=>'get','default' =>'1','desc' => '开放状态'),
            ),
            'updrama' => array(
                'title' => array('name' => 'title','source'=>'get','default'=>'','desc' => '剧集标题'),
                'info' => array('name' => 'info','source'=>'get','default'=>'','desc' => '剧集简介'),
                'desc' => array('name' => 'desc','source'=>'get','default'=>'','desc' => '备注'),
                'img' => array('name' => 'img','source'=>'get','default'=>'','desc' => '剧集封面'),
                'on_status' => array('name' => 'on_status','source'=>'get','default' =>'1','desc' => '开放状态'),
                'id' => array('name' => 'id','source'=>'get','require'=>true,'desc' => '开放状态'),
            ),
            'dedrama' => array(
                'drama_id' => array('name' => 'drama_id','source'=>'get','require'=>true,'desc' => '剧集ID'),
                'video_id' => array('name' => 'video_id','source'=>'get','require'=>true,'desc' => '视频ID'),
            ),
            'inserdramav' => array(
                'drama_id' => array('name' => 'drama_id','source'=>'get','require'=>true,'desc' => '剧集ID'),
                'video_id' => array('name' => 'video_id','source'=>'get','require'=>true,'desc' => '视频ID'),
            ),
            'addlist' => array(
                'class' => array('name' => 'class','source'=>'get','require'=>true,'desc' => '类目名称'),
                'on_status' => array('name' => 'on_status','source'=>'get','default'=>'1','desc' => '类目开放状态'),
            ),
            'delist' => array(
                'class_id' => array('name' => 'class_id','source'=>'get','require'=>true,'desc' => '类目id'),
            ),
            'dedrams' => array(
                'id' => array('name' => 'id','source'=>'get','require'=>true,'desc' => '剧集ID'),
                'statu' => array('name' => 'statu','source'=>'get','require'=>true,'desc' => '状态'),
            ),
            'uplistdrama' => array(
                'class_id' => array('name' => 'class_id','source'=>'get','require'=>true,'desc' => '类别ID'),
                'drama_id' => array('name' => 'drama_id','source'=>'get','require'=>true,'desc' => '剧集ID'),
                'type'=> array('name' => 'type','source'=>'get','default'=>'insert','desc' => '类型'),
                'status' => array('name' => 'status','source'=>'get','default'=>'1','desc' => '状态 '),
            ),
            'upclass' => array(
                'class_id' => array('name' => 'class_id','source'=>'get','require'=>true,'desc' => '类别ID'),
                'class' => array('name' => 'class','source'=>'get','require'=>true,'desc' => '类别名称'),
                'index' => array('name' => 'index','source'=>'get','default'=>'0','desc' => '排序值 '),
                'on_status' => array('name' => 'on_status','source'=>'get','default'=>'1','desc' => '默认状态 '),
            ),
            'showdrama' => array(
                'class_id' => array('name' => 'class_id','source'=>'get','default'=>null,'desc' => '类别ID'),
                'tag' => array('name' => 'tag','source'=>'get','default'=>null,'desc' => '单个标签'),
                'order' => array('name' => 'order','source'=>'get','default'=>'id','desc' => '排序属性 '),
                'orderStatus' => array('name' => 'orderStatus','source'=>'get','default'=>'1','desc' => '排序方式 1 升序 2 降序 0 不排序 '),
                'page' => array('name' => 'page','source'=>'get','default'=>1,'desc' => '当前页数 '),
                'on_status' => array('name' => 'on_status','source'=>'get','default'=>'null','desc' => '状态 '),
                'keyword' => array('name' => 'keyword','source'=>'get','default'=>null,'desc' => '关键字筛选 '),
                'level' => array('name' => 'level','source'=>'get','default'=>null,'desc' => '视频级别 '),
                'pagenum' => array('name' => 'pagenum','source'=>'get','default'=>40,'desc' => '每页展示数量 '),
            ),
            'searchresource' => array(
                'keyword' => array('name' => 'keyword','source'=>'get','require'=>true,'desc' => '关键词'),
            ),
            'showvideo' => array(
                'class_id' => array('name' => 'class_id','source'=>'get','default'=>null,'desc' => '类别ID'),
                'tag' => array('name' => 'tag','source'=>'get','default'=>null,'desc' => '单个标签'),
                'order' => array('name' => 'order','source'=>'get','default'=>'','desc' => '排序属性 '),
                'orderStatus' => array('name' => 'orderStatus','source'=>'get','default'=>'1','desc' => '排序方式 1 升序 2 降序 0 不排序 '),
                'page' => array('name' => 'page','source'=>'get','default'=>1,'desc' => '当前页数 '),
                'on_status' => array('name' => 'on_status','source'=>'get','default'=>'null','desc' => '上架状态 '),
                'level' => array('name' => 'level','source'=>'get','default'=>null,'desc' => '视频等级 '),
                'drama_id' => array('name' => 'drama_id','source'=>'get','default'=>null,'desc' => '剧集ID '),
                'com_id' => array('name' => 'com_id','source'=>'get','default'=>null,'desc' => '商品ID '),
                'key' => array('name' => 'key','source'=>'get','default'=>null,'desc' => '搜索key '),
                'search' => array('name' => 'search','source'=>'get','default'=>null,'desc' => '搜索值 '),
            ),
            'recommend' => array(
                'vid' => array('name' => 'vid','source'=>'get','require'=>true,'desc' => '视频Id'),
            ),
            'uponstatu' => array(
                'rid' => array('name' => 'rid','source'=>'get','require'=>true,'desc' => '资源ID'),
                'on_status' => array('name' => 'on_status','source'=>'get','default'=>1,'desc' => '数据状态'),
                'type' => array('name' => 'type','source'=>'get','require'=>true,'desc' => '数据类型'),
            ),
            'deleteVideo' => array(
                'id' => array('name' => 'id','source'=>'get','require'=>true,'desc' => 'ID'),
                'status' => array('name' => 'status','source'=>'get','default'=>1,'desc' => '数据状态'),

            ),
            'statistice' => array(
                'on_status' => array('name' => 'on_status','source'=>'get','default'=>'false','desc' => '数据状态'),
            ),
            'demotion' => array(
                'select' => array('name' => 'select','source'=>'get','default'=>null,'desc' => 'JSON数据'),
                'type' => array('name' => 'type','source'=>'get','default'=>'select','desc' => '类型'),
//                'on_status' => array('name' => 'on_status','source'=>'get','default'=>1,'desc' => '数据状态'),
//                'time' => array('name' => 'time','source'=>'get','default'=>null,'desc' => '数据类型'),
//                'res_id' => array('name' => 'res_id','source'=>'get','default'=>null,'desc' => '资源ID'),
//                'level' => array('name' => 'level','source'=>'get','default'=>null,'desc' => '级别'),
            ),
            'videoRecord' => array(
                'select' => array('name' => 'select','source'=>'get','default'=>null,'desc' => 'JSON数据'),
                'type' => array('name' => 'type','source'=>'get','default'=>'select','desc' => '类型'),
            ),
            'findcom_id' => array(
                'type' => array('name' => 'type','source'=>'get','default'=>'video','desc' => '类型'),
                'com_id' => array('name' => 'com_id','source'=>'get','require'=>true,'desc' => '商品id'),
            ),
            'videocreateTeacher' => array(
                'value' => array('name' => 'value','source'=>'get','require'=>true,'desc' => '匹配值'),
                'drama_id' => array('name' => 'drama_id','source'=>'get','default'=>null,'desc' => '剧集ID'),
            ),
            'findClass' => array(
                'id' => array('name' => 'id','source'=>'get','require'=>null,'desc' => '类目Id'),

            ),
//            'test' => array(
//                'type' => array('name' => 'type','source'=>'post','require'=>true,'desc' => '视频Id'),
//                'time' => array('name' => 'time','source'=>'post','require'=>true,'desc' => '视频Id'),
//                'date' => array('name' => 'date','source'=>'post','require'=>true,'desc' => '视频Id'),
//            ),
        );
    }



    //视频上传接口
    public function uploading()
    {
        if (!$_GET['token'] ){
            throw new BadRequestException('请传入token',3);
        }

        $arr = [
            'title'=>$this->title,
            'level'=>$this->level,
            'url' =>$this->url,
            'desc' =>$this->desc,
            'url_type'=>$this->url_type,
            'play_url'=>$this->play_url,
            'img' =>$this->img,
            'drama_id'=>$this->drama_id,
            'videofile'=>$this->videofile,
            'tags'=>json_decode($this->tags,true),
            'info'=>$this->info,
            'money'=>$this->money,
            'con_status'=>$this->con_status,
            'on_status'=>$this->on_status
        ];

        return self::$data->index($arr);

    }
    //删除剧集
    public  function  dedrams(){

        $arr = ['id'=>$this->id,'statu'=>$this->statu];

        return self::$data->deledram($arr);

    }

    //视频基本信息返回
    public function video(){

        return self::$data->vc($this->id);

    }
    //剧集返回基本信息
    public function drama(){
//        return$this->level.$this->label;
//        return json_decode($this->id,true);
        return self::$data->dc($this->id,$this->level,$this->label,$this->type);
}
    //视频修改接口
    public function upvideo(){

        $arr = [
            'title'=>$this->title,
            'level'=>$this->level,
            'url'  =>$this->url,
            'url_type'=>$this->url_type,
            'videofile'=>$this->videofile,
            'play_url'=>$this->play_url,
            'img' =>$this->img,
            'drama_id'=>$this->drama_id,
            'tags'=>$this->tags,
            'id'=>$this->id,
            'info'=>$this->info,
            'on_status'=>$this->on_status,
            'money'=>$this->money,
            'con_status'=>$this->con_status,
            'desc'=>$this->desc
        ];
        return self::$data->index($arr,'update');


    }

    //剧集视频上下线
    public function uponstatu(){
        $arr = [
            'id'=>$this->rid,
            'on_status'=>$this->on_status,
            'type'=>$this->type
        ];
        return self::$data->upResStatus($arr);




    }


    //添加新的剧集
    public function adddrama(){

        $arr = [
            'title'=>$this->title,
            'info'=>$this->info,
            'desc'=>$this->desc,
            'img'=>$this->img,
            'on_status'=>$this->on_status
        ];
        return self::$data->addsDrama($arr);

    }
    //修改剧集
    public function updrama(){

        $arr = [
            'title'=>$this->title,
            'info'=>$this->info,
            'desc'=>$this->desc,
            'img'=>$this->img,
            'on_status'=>$this->on_status,
            'id'=>$this->id
        ];

        return self::$data->addsDrama($arr,'update');
    }
//  删除指定剧集对应下的视频
public function dedrama(){

    $arr = [
        'drama_id'=>$this->drama_id,
        'video_id'=>$this->video_id
    ];
    return self::$data->deleteDramaVideo($arr,$type="delete");

}
//向指定剧集添加视频
public function inserdramav(){

    $arr = [
        'drama_id'=>$this->drama_id,
        'video_id'=>$this->video_id
    ];
    return self::$data->deleteDramaVideo($arr,$type="insert");

}
//增加类目
public function addlist(){

    $arr = [
        'class'=>$this->class,
        'on_status'=>$this->on_status
    ];
    return self::$data->addsList($arr);

}
//删除分类
public function delist(){


    return self::$data->deleteList($this->class_id);

}
//类别下的剧集管理
public function uplistdrama(){
    $arr = [
        'class_id'=>$this->class_id,
        'drama_id'=>$this->drama_id,
        'status' => $this->status
    ];

    return self::$data->updateCategories($arr,$this->type);

}
//类别列表
public function classlist(){

    return self::$data->showList();

}
//修改分类信息
public function upclass(){

    $arr = [
        'id'=>$this->class_id,
        'class' =>$this->class,
        'index'=>$this->index,
        'on_status'=>$this->on_status
    ];
    self::$data->updateClass($arr);

}
//展示剧集列表
public function showdrama(){

    $arr= [
        'class_id'=>$this->class_id,
        'tag'=>$this->tag,
        'order'=>$this->order,
        'orderStatus'=>$this->orderStatus,
        'page'=>$this->page,
        'keyword'=>$this->keyword,
        'level'=>$this->level,
        'on_status'=>$this->on_status,
        'pagenum'=>$this->pagenum
    ];

    return self::$data->showDramaCon($arr);

}
//剧集视频数据统计
public function statistice(){

    return self::$data->allsum($this->on_status);

}


//热门视频推荐
public function hotrecommend(){

    return self::$data->showHotVideo();

}
//搜索
public function searchresource(){

    $val =  $this->keyword;
    return self::$data->search($val);
}
//视频筛选
public function showvideo(){

    $arr= [
        'class_id'=>$this->class_id,
        'tag'=>$this->tag,
        'order'=>$this->order,
        'orderStatus'=>$this->orderStatus,
        'page'=>$this->page,
        'on_status'=>$this->on_status,
        'level'=>$this->level,
        'key'=>$this->key,
        'search'=>$this->search,
        'drama_id'=>$this->drama_id,
        'com_id'=>$this->com_id
    ];
    return self::$data->showVideoCon($arr);

}
//猜你喜欢
public function lovevideo(){
    $token = $_GET['token'];
    $url = $url = "User.User.Selecttoken&searchToken=$token";
    $user = apiGet($url);
    return self::$data->videoLove($user);
}

//推荐
public function recommend()
{
    $vids=self::$data->samples();
    $vid[]=$this->vid;
    $data=\Video\getapriori($vid,$vids);
    $id=($data[count($data)-1]);
    if($id!=null){
        $ids="";
        foreach ($id as $v){
            $ids.=$v.",";
        }
        $ids=rtrim($ids,",");
        return self::$data->vc($ids);
    }
}
//delete video
public  function  deleteVideo(){

    return self::$data->deleVi($this->id,$this->status);

}
//精彩剧集
public function wonderfulDrama(){

    return self::$data->spelldrama();
}

//标签类型表
public function label(){

    return self::$data->labelTag();


}
//视频数据处理
public function statifidata(){

    return self::$data->statifidata();

}

//添加修改视频降级接口
public function demotion(){

    $arr = json_decode($this->select,true);
    return self::$data->demotions($arr,$this->type);

}
//触发接口
public function trigger(){
    return self::$data->trigger();
}

//添加修改查找视频购买记录接口
    public function videoRecord(){

        $arr = json_decode($this->select,true);
        return self::$data->videoRecords($arr,$this->type);

    }

//匹配创作人
    public function videocreateTeacher(){
        return self::$data->videocreateTeachers($this->value,$this->drama_id);

    }
//类目信息
    public function findClass(){
        return self::$data->findClasses($this->id);

    }
//通过商品id查找视频
    public function findcom_id(){
        return self::$data->findcoms($this->type,$this->com_id);
    }

//测试
    public function test(){

//        return  rus('sadfsafsadsa');
//      return Baidu\rus('1231321321');
//       return requestResource('http://test.yusj.vip/?s=Video.drama.lovevideo');

    }

}