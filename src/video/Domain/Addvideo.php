<?php
namespace Video\Domain;
use PhalApi\Exception\BadRequestException;
use Video;
use Video\Model\video_label;
use Video\Model\video_search;
use Video\Model\video_show;
use Video\Model\video_video;
use Video\Model\video_drama;
use Video\Model\video_list;
use Video\Model\video_categories;
use Video\Model\video_record;
use Video\Domain\Mda\Baidu;
use Video\Model\video_demotion;
use Video\Model\video_user;
class Addvideo {

    public function index($val,$type='insert')
    {
        checkAuth();
        $Api_video_video = new video_video();
        $Api_video_show = new video_show();
        $Api_video_label = new video_label();
        $Api_video_search = new video_search();
        $baidu = new Baidu();
            //检测是否有视频播放地址
            if (!$val['play_url'] && $val['url']) {
                $val['play_url'] = $baidu->rus($val['url'], true);
                $val['duration'] = $baidu->rus($val['url'], null,true);
            }
            //检测是否有视屏封面地址
            if (!$val['img']  && $val['url']) {
                $val['img'] = $baidu->rus($val['url']);
            }
        $val['up_time'] = date("Y-m-d H:i:s", time());
        $video = Video\array_remove(Video\array_remove($val,'drama_id'),'tags');
        //更新剧集
        apiGet('Video.drama.updrama',array('id'=>$val['drama_id']));

        //如果是添加时
        if ($type == 'insert'){
            $val['create_time'] = date("Y-m-d H:i:s", time());
            $video = Video\array_remove(Video\array_remove($video,'money'),'con_status');
            $money = $val['money'];
            $on_status = $val['con_status'];
            try {
                //视频ID
                $curl = new \PhalApi\CUrl();
                $desc = json_encode(['info'=>'视频播放次数']);
                //新增一个商品
                $comdata = [
                    'type'=>'video',
                    'name'=>$val['title'],
                    'desc'=>$desc,
                    'money_type'=>2,
                    'money'=>$money,
                    'on_status'=>$on_status
                ];
                $rs = apiGet('Commodity.Commoditys.insertcom',$comdata)['id'];
                $video['com_id'] = $rs;
                $vid = $Api_video_video->inser($video);
                //注册资源
                $resdata = [
                        'res_id'=>$vid,
                        'res_type'=>'video'
                ];
                apiGet('Auth.Apis.setAdmin',$resdata);
                if ($val['tags']){
                    $x =  array_merge([$val['title']], $val['tags']);
                }else{
                    $x =$val['title'];
                }
                //数据拆分
                $arr = [
                    //绑定剧集视频
                    'show' => [
                        'drama_id' => $val['drama_id'],
                        'video_id' => $vid,
                        'time' => $val['up_time']
                    ],
                    'label' =>$val['tags'],
                    'search' => $x
                ];

                //视频和剧集的绑定
                $Api_video_show->bound($arr['show']);
                //视频对标签
                if ($val['tags']){
                    $Api_video_label->insertag($arr['label'], $vid);
                }

                //搜索所对应视频

                $Api_video_search->insersearch($arr['search'], $vid);

                return $vid;
            } catch (\Exception $e) {

                throw new BadRequestException('系统错误');

            }
            //如果是进行update时
        }elseif($type == 'update'){

//            //权限
//            checkAuth();
//
//            $uid = Video\getCache($_GET['token'])['Id'];
//
//            Video\resUid($uid,$val['id'],3,'update');


            try{
                //    数据过滤
                foreach( $video as $k=>$v){
                    if( $v === null )
                        unset( $video[$k] );
                }
                $comndata = ['id'=>$this->vc($video['id'])['com_id']];
                if (isset($video['title'])){
                    $comndata['name'] = $video['title'];
                }
                if (isset($video['con_status']) && isset($video['money'])){
                    $comndata['con_status'] = $video['con_status'];
                    $comndata['money'] = $video['money'];
                    $video = Video\array_remove(Video\array_remove($video,'money'),'con_status');
                }
                //修改商品
                apiGet('Commodity.commoditys.upcom',$comndata);
                //进行视频修改
               $Api_video_video->upVideo(Video\array_remove($video,'id'),$video['id']);
                //进行所有的标签整合

                $arr = [
                    'label' => json_decode($val['tags'], true),
                ];

                if (isset($arr['label'])){
                    //标签修改
                    $Api_video_label->upVideoTag($arr['label'],$video['id']);
                }
                if(isset($val['title']) && isset($val['tags'])){
                    //搜索修改
                    $arr['search'] =  array_merge([$val['title']], json_decode($val['tags'], true));
                    $Api_video_search->insersearch($arr['search'], $video['id']);
                }

                //视频所属剧集的修改
                if (isset($val['drama_id'])){
                    $Api_video_show->upVideoDrama($val['drama_id'],$video['id']);
                }


                return true;

            }catch (\Exception $e){

                throw new BadRequestException('系统错误');

            }



        }



    }

    //删除指定视频
    public function deleVi($id,$statu){



//        //权限
//        checkAuth();
//
//        $uid = Video\getCache($_GET['token'])['Id'];
//
//        Video\resUid($uid,$id,3,'del');


        $Api_video_video = new video_video();
      return  $Api_video_video->deletev($id,$statu);

    }


    //展示视频信息
    public function vc($id){
        $Api_video_video = new video_video();
        $cate = new video_categories();
        $video_user = new video_user();
        $record = new video_record();
        $pos = strpos($id, ',');

        $uid = apiGet('User.User.selecttoken',array('searchToken'=>$_GET['token']))['Id'];
        if (!$pos) {

                $arr = $Api_video_video->find($id);

                if (!$arr) {
                    throw new BadRequestException('未找到指定视频', 4);
                }

                //该视频次数减少

//                $curl = new \PhalApi\CUrl();
//
//                $rs = apiGet('Commodity.Commoditys.reduce&id=' . $arr['0']['com_id'] . '&num=1');
//
//                if (!$rs) {
//                    throw new BadRequestException('视频出错', 100);
//                }

                //这里出新的tag标签
                foreach ($arr as $key => $value) {
                    $tag[] = $value['tag'];
                }
                $res = $arr['0'];
                $res['tag'] = $tag;

            $redata = [
                'vid'=>$res['id'],
                'uid'=>$uid,
                'time'=>date("Y-m-d H:i:s",time())
            ];

            //是否提供下载
            $auth =  json_decode($res['desc'],true);
            $videofile = $res['videofile'];
            $res['videofile'] = '';
            if (isset($auth['freeDownList']) && $auth['freeDownList'] && $uid || $uid === 0){
                foreach ($auth['freeDownList'] as $key => $value){
                   $ressd =  Video\checkauth($uid,'',$value);
                   if ($ressd){
                       $res['videofile'] = $videofile;
                       break;
                   }
                }
            }

                //播放记录
            if ($res['level'] == 8 && $uid){

                $record->inser($redata);
            }

            //查看用户是否拥有查看视频的权限
            if ($res['level'] > 8 && $uid !== 0) {

                    $list_id = $cate->findclass($res['drama_id'])['class_id'];

                    $data = Video\resUid($uid, $res['level'], $list_id);
                    //播放记录
                    if ($data && $uid){
                        $record->inser($redata);
                    }
                    if (!$data) {
                        //验证是否有code和是否购买了该商品
                        $userdata = ['statu'=>1,'vid'=>$res['id']];
                        $code = isset($_GET['code'])?$_GET['code']:null;
                        if ($code){
                            $userdata['code'] = $code;
                        }else{
                            $userdata['uid'] = $uid;
                        }
                        if ($uid == -1 && !$code){
                            $res['url'] = '';
                            $res['play_url'] = '';
                            $res['body'] = '权限不足';
                        }else{
                            $videoresoucse = $this->videoRecords($userdata,'select');
                            if ($videoresoucse && $videoresoucse['0']['num']){
                                $updata = ['num'=>$videoresoucse['0']['num']-1,'id'=>$videoresoucse['0']['id']];
                                $this->videoRecords($updata,'update');
                            }else{
                                $res['url'] = '';
                                $res['play_url'] = '';
                                $res['body'] = '权限不足';
                            }
                        }
                    }
            }


        return $res;

        }

       $data =  $Api_video_video->findString($id);

        //查看用户是否拥有查看视频的权限
        foreach ($data as $key => $value){
            if ($value['level'] > 8 &&  $uid!== 0){

                    $list_id = $cate->findclass($value['drama_id'])['class_id'];

                    $res = Video\resUid($uid, $value['id'], $list_id);

                    if (!$res) {
                        $data[$key]['url'] = '';
                        $data[$key]['body'] = '权限不足';
                    }
                }
            }
    return $data;
    }
    //展示剧集层
    public function dc($id,$level,$label,$type){
        //将视频赋予到剧集中
        $uid = Video\getUserDtat()['Id'];

        //特权
        $res =  Video\checkauth($uid,'team',316);

        //机构
        $organization = Video\checkauth($uid,'team',317);
        
        if ($uid === 0){
            $res = 1;
            $organization = 1;
        }
            $Api_video_drama = new video_drama();
            $Api_video_show = new video_show();
            $Api_video_video =  new video_video();
            $cate = new video_categories();
            $arr = json_decode($id,true);

            //剧集
            foreach ($arr as $key =>$value){
                $drama[] = $Api_video_drama->dramaVideo($value);
            }

            if (!$drama){
                throw new BadRequestException('未找到指定剧集',4);
            }

            foreach ($drama as $key =>$value){
                $videodrama = $Api_video_show->find($value['id']);
                $drama[$key]['class_id'] = $cate->findclass($value['id'])['class_id'];
                $video =$Api_video_video->findDramaAll($videodrama,$level,$label,$type);
                if ($video){
                    $a = Video\videodata($video,$res,$organization);
                   
                }

                $drama[$key]['video'] =$a;

            }

        return $drama;


    }
    //添加剧集逻辑
    public function addsDrama($val,$type="insert"){
        checkAuth();
        $Api_video_drama = new video_drama();
        $Api_video_search = new video_search();
        $val =  Video\undatas($val,'');
        $val['up_time'] = date("Y-m-d H:i:s",time());
        if ($type == "insert"){
//            //权限系统调用
//
//            $user = Video\getCache($_GET['token']);
//            //检测是否为系统添加或用户添加
//            if ($user['Id'] !== 0){
//                //检测token是否正确
//                if (!$user['Id']){
//                    throw new BadRequestException('未找到指定token',4);
//                }
//
//                //检测用户是否有对默认分类下的添加视频的权限
//                Video\resUid($user['Id'],'1',3,'add','3');
//
//
//
//                //用户添加则在权限系统添加其四类权限
//                $data = ['read','del','add','update'];
//                //创建时间
//                $val['create_time'] = date("Y-m-d H:i:s",time());
//
//
//                //增加剧集
//                $did =  $Api_video_drama->inserDrama($val);
//                //增加搜索索引
//                $Api_video_search->insersearch([$val['title']],$did,'2');
//
//                $arrdata = [
//                    'fid'=>$user['Id'],
//                    'ftype'=>'user',
//                    'res_type'=>'video',
//                    'res_id'=>$did,
//                    'value'=>1
//                ];
//                foreach ($data as $key => $value){
//                    $arrdata['service'] = $value;
//                    apiGet('Auth.Resource.Set',$arrdata);
//                }
//            }else{
            //添加商品
            $comdata = [
                'type'=>'drama',
                'name'=>$val['title'],
                'money_type'=>'money',
                'money'=>0,
                'num'=>1000000,
                'on_status'=>1,
                'desc'=>-1
            ];
            apiGet('Commodity.commoditys.insertcom',$comdata);

                //创建时间
                $val['create_time'] = date("Y-m-d H:i:s",time());
                //增加剧集
                $did =  $Api_video_drama->inserDrama($val);
                //增加搜索索引
                $Api_video_search->insersearch([$val['title']],$did,'2');

//            }

            //注册资源
            $resdata = [
                'res_id'=>$did,
                'res_type'=>'drama'
            ];
            apiGet('Auth.Apis.setAdmin',$resdata);

//            Video\clerCache('video');
            return $did;


        }elseif($type =="update"){

//            checkAuth();
//            $uid = Video\getCache($_GET['token'])['Id'];
//            Video\resUid($uid,$val['id'],3,'update','2');

            //查询指定商品
            $res =  $Api_video_drama->dramaVideo($val['id']);
            if (!$res){
                throw new BadRequestException('未找到指定剧集');
            }

            //修改商品名称
            if ($val['title']){
                $condata = [
                    'id'=>$res['com_id'],
                    'name'=>$val['title']
                ];
                apiGet('Commodity.commoditys.upcom',$condata);

                //修改搜索索引
                $Api_video_search->upVideoSearch([$val['title']],$val['id'],'2');

            }



                //修改剧集
            $Api_video_drama->upDrama(Video\array_remove($val,'id'),$val['id']);


//            Video\clerCache('video');
            return true;
        }


    }
    //删除指定剧集下面的视频
    public function deleteDramaVideo($val,$type="delete"){


        checkAuth();
        $uid = Video\getCache($_GET['token'])['Id'];

        $model = new video_show();
        if ($type == "delete"){
            return $model->deVideo($val);
        }elseif($type == "insert"){
            $val['time'] = date("Y-m-d H:i:s",time());
            return $model->bound($val);
        }

    }

    //添加指定类目
    public function addsList($val){
        checkAuth();
        $model = new video_list();
        //名称去重

        $data = $model->findname($val['class']);

        if ($data){
            throw new BadRequestException('名称重复',5);
        }
            $resdata = $model->inserList($val);

            $arrdata = [
                'fid'=>'318',
                'ftype'=>'team',
                'res_type'=>'videoClassLevel',
                'res_id'=>$resdata['id'].'_9',
                'server'=>'read',
                'value'=>1
            ];
        apiGet('Auth.Resource.Set',$arrdata);

        return $resdata;

    }
    //删除指定类目
    public function deleteList($val){

        checkAuth();
//        $uid = Video\getCache($_GET['token'])['Id'];
//        Video\resUid($uid,$val,3,'del',3);
//

        $Api_video_categories = new video_categories();
        $Api_video_categories->deleteDramaList($val);
        $Api_video_list = new video_list();
       return $Api_video_list->deleteList($val);

    }
    //数据资源状态修改
    public function upResStatus($arr){
        $type= $arr['type'];
        $id = $arr['id'];
        $arr = Video\array_remove($arr,'type');
        $arr = Video\array_remove($arr,'id');
        if ($type = 'drama'){
            $model = new video_drama();
           return $model->upDrama($arr,$id);
        }
        if ($type = 'video'){
            $model = new video_video();

          return  $model->upVideo($arr,$id);
        }

    }

    //类目剧集管理
    public function  updateCategories($val,$type='insert'){

//        checkAuth();
//        $uid = Video\getCache($_GET['token'])['Id'];
//        Video\resUid($uid,$val['class_id'],3,'update',3);
//

        $Api_video_categories = new video_categories();
        $data = Video\array_remove($val,'status');
        if ($type == 'insert'){

            if ($val['status'] == '1'){

                return $Api_video_categories->inserDramaList($data);

            }elseif($val['status'] == '-1'){

                return $Api_video_categories->deDramaList($val);

            }
        }
        if ($type == 'update'){

          return  $Api_video_categories->upclass($val['drama_id'],$val['class_id']);

        }

    }
    //展示类目信息
    public function showList(){

        $model = new video_list();
        $data =  $model->slist();
        return $data;

    }
    //删除剧集
    public function deledram($val){

        $show  = new video_show();

        $video = $show->find($val['id']);

        if ($video){
            throw new BadRequestException('剧集下还包含了视频,不可删除',5);
        }

        $drama = new video_drama();
        $deramadata =['statu'=>$val['statu']];

        $del = $drama->upDrama($deramadata,$val['id']);

        //删除list下面的改剧集信息
        $categories = new video_categories();
        $catedata = ['drama_id'=>$val['id']];
        $res = $categories->deDramaList($catedata);
        return $res;


    }

    //修改对应分类信息
    public function updateClass($val){


        checkAuth();
//        $uid = Video\getCache($_GET['token'])['Id'];
//        Video\resUid($uid,$val['id'],3,'update',3);
//

        $model = new video_list();

//        Video\clerCache('video');

        $data = $this->showList();
        if (in_array($val['class'],$data)){
            throw new BadRequestException('分类名重复');
        }

        $arr = Video\array_remove($val,$val['id']);

        return $model->upList($arr,$val['id']);


    }
//展示剧集信息
public function showDramaCon($arr){
        $model = new video_drama();
        $show = new video_show();
        //权限验证
        $uid = Video\getUserDtat($_GET['token'])['Id'];
        $resAuth =  Video\checkauth($uid,'team',316);

        //机构
        $organization = Video\checkauth($uid,'team',317);

        if ($uid === 0){
            $resAuth = 1;
            $organization = 1;
        }
        $drama =  $model->filtrateDrama($arr,$resAuth,$organization);
       $num =  count($model->filtrateDrama($arr,$resAuth,$organization,'num'));

       foreach ($drama as $key => $value){
           $drama[$key]['vNum'] = count($show->find($value['id']));
       }
    outputArgs(array('num'=>$num,'pagenum'=>$arr['pagenum'],'age'=>$arr['page']));
    return $drama;
}
//热门推荐
public function showHotVideo(){

    //级别验证
    $uid = Video\getUserDtat()['Id'];
    //特权
    $res = Video\checkauth($uid,'team',316);
    //机构
    $organization = Video\checkauth($uid,'team',317);

        $model = new video_video();

        $rs = apiGet('Comment.Comment.Rmvideo');

        if (!$rs){
            throw new BadRequestException('未找到指定系统推荐视频',4);
        }
        $data =  $model->hotVideo($rs);

    //数据过滤
    $a = Video\videodata($data,$res,$organization);
    return $a;
}

//精彩剧集
public function spelldrama(){

        $model = new video_drama();
//        $rs = apiGet('Comment.Comment.Rmdrama');
//        if ($rs){
//            $data =  $model->findalldrama($rs);
//        }else{
            $data =  $model->allDrama();
//        }
   return $data;

}

//猜你喜欢
public function videoLove($user){

        //视频播放记录
        $model = new video_record();
        //视频
        $video = new video_video();

    //特权
    $res = Video\checkauth($user['Id'],'team',316);
    //机构
    $organization = Video\checkauth($user['Id'],'team',317);

        //求出所有视频ID
        $vid =$video->vidAll($res,$organization);

        //游客随机推荐
        if ($user['Id'] == -1 || !$user['Id']){
            return $video->randdata(20,$res,$organization);
        }

        //标签权位
        $arr = $model->playrecord($user['Id']);

        //查看过的所有视频
        $reco = $model->playreUid($user['Id']);

        if ($reco){
            //过滤已看过的视频
            foreach ($reco as $key => $value) {
                foreach ($vid as $k => $v) {
                    if ($v['id'] == $value['vid']) {
                        unset($vid[$k]);
                    }
                }
            }
        }

        //对应视频所含标签
        $result = array();
        foreach($vid as $k=>$v){
            $result[$v['id']][] =$v;
        }

        //计算视频所对应权重
        if ($arr){
            $res = [];
            $i = 1;
            foreach ($result as $key => $value){
                $res[$key] =[];
                $res[$key]['num'] = 0;
                foreach ($value as $k => $v){
                    foreach ($arr as $a => $b){
                        if ($b['tag'] == $v['tag']){
                            if ($i>20){
                                break;
                            }
                            $res[$key]['num'] += $b['count(tag)'];
                            $res[$key]['vid'] = $key;
                            $i++;
                        }
                    }
                }

            }
            //视频权重排序

            $resvid =  Video\my_sort($res,'num');

            foreach ($resvid as $key => $value){
                if ($value['num'] > 0){
                    $data[] = $video->findVideo($value['vid']);
                }
            }

        }else{
            //取随机视频
            $data = $video->randdata(20,$res,$organization);
        }

return $data;

}

//搜索
public function search($val){

    //资源验证
    $uid = Video\getCache($_GET['token'])['Id'];
    //特权
    $resouse = Video\checkauth($uid,'team',316);
    //机构
    $organization = Video\checkauth($uid,'team',317);

        $Api_video_search = new video_search();

        $Api_video_video = new video_video();

        $Api_video_drama = new video_drama();

        $Api_video_show = new video_show();
        $data = $Api_video_search->findsearch($val);

        if (!$data){
            return $data;
        }
        $res['video'] = [];
//    $res['drama'] = [];
        //将数据进行拆分
        foreach ($data as $key => $value){
            if ($value['res_type'] == 1){
                $res['video'][] = $value;

            }
//        elseif($value['res_type'] == 2){
//            $res['drama'][] = $value;
//
//        }
        }

        $video = null;
//    $drama = null;
        $a = [];
        if ($res['video']){
            foreach ($res['video'] as $key => $value){
                $re = $Api_video_video->findVideo($value['res_id']);
                if ($re){
                    $video[$key] = $re;
                    $rs = Video\findmessage($value['res_id'],'video');
                    $video[$key]['play_num'] = $rs['play_num'];
                    $video[$key]['drama_id'] =$Api_video_show->findVideo($value['res_id'])['drama_id'];
                    $video[$key]['grade'] = $rs['grade'];
                    switch ($video[$key]['level']){
                        case 11:
                            if ($resouse){
                                $a[] = $video[$key];
                            }
                            break;
                        case 10:
                            if ($organization || $resouse){
                                $a[] = $video[$key];
                            }
                            break;
                        default:
                            $a[] = $video[$key];
                    }
                }
            }
        }
//    if ($res['drama']){
//        foreach ($res['drama'] as $key => $value){
//            $re = $Api_video_drama->dramaVideo($value['res_id']);
//            if ($re){
//                $drama[$key] =$re;
//                $rs = Video\findmessage($value['res_id'],'drama');
//                $drama[$key]['play_num'] = $rs['play_num'];
//                $drama[$key]['grade'] = $rs['grade'];
//            }
//        }
//
//    }

    $arr = [
//        'drama'=>$drama,
        'video'=>$a
    ];
    return $arr;

}
//展示视频
public function showVideoCon($val){
    $uid = apiGet('User.User.selecttoken',array('searchToken'=>$_GET['token']))['Id'];
    //特权
    $res  = Video\checkauth($uid,'team',316);
    //机构
    $organization = Video\checkauth($uid,'team',317);
        $model = new video_video();
        $data =   $model->showVideo($val);
       //数据过滤
        $data = Video\videodata($data,$res,$organization);
        $num = count($model->showVideo($val,'num'));
    outputArgs(array('num'=>$num,'pagenum'=>40,'age'=>$val['page']));
    return $data;
}

///浏览视频用户的视频
public function samples(){
    $model = new video_record();
    $user=$model->selectuser();
    $samples=array();
    foreach ($user as $key=>$v){
        $video=$model->selectvideo($v["uid"]);
        $vid="";
        foreach ($video as $c){
            $vid.=$c["vid"].",";
        }
        $vid=trim($vid,",");
        $videos=explode(",",$vid);
        $samples[$key]=$videos;
    }
    return $samples;
}

//标签类型
public function labelTag(){

       $model = new video_label();

       $data =  $model->findType();

        return $data;


}

//统计
public function allsum($on_status){
    $video = new video_video();
    $drama = new video_drama();
    $data['videoNum'] = $video->all($on_status);
    $data['dramaNum'] = $drama->all($on_status);
    return $data;
}

//数据新增处理
public function statifidata(){

    $model = new video_video();

    $start = date("Y-m-d",time());
    $end = date('Y-m-d',strtotime('+1 day'));
    //今日新增人数
    $data['b'] = count($model->proVideo($start,$end));
    $start = date('Y-m-d',strtotime('-1 day'));
    $end = date("Y-m-d",time());;
    //昨日新增人数
    $data['c'] = count($model->proVideo($start,$end));
    return $data;
}

//视频新增降级数据
public function demotions($val,$type='select'){
    $model =new video_demotion();
    switch ($type){
        case 'update':
            $where = ['res_id'=>$val['res_id'],'level'=>$val['level']];
            return $model->up($where,$val);
        break;
        case 'insert':
            return $model->inser($val);
            break;
        case 'select':
            return $model->select($val);
            break;
    }

}

    //触发修改视频级别
    public function trigger(){
        $model =new video_demotion();
        $video_video = new video_video();
        $where = [
            'on_status'=>1,
            'time'=>date("Y-m-d H:i:s",time())
        ];
        $res = $model->findWhereAll($where);
        $model->upWhereAll($where,array('on_status'=>-1));
        $data = true;
        if ($res){
            foreach ($res as $key => $value){
                $videodata = ['level'=>$value['level']];
                if ($value['type'] == 1){
                    $videodata['on_status'] = 1;
                }
                $data = $video_video->upVideo($videodata,$value['res_id']);
            }
        }

        return $data;
    }
    //视频购买数据及查询
    public function videoRecords($val,$type='select'){
        $model =new video_user();
        switch ($type){
            case 'update':
                return $model->up($val['id'],$val);
                break;
            case 'insert':
                $val['code'] = Video\getRandomString(9);
                return $model->inser($val);
                break;
            case 'select':
                return $model->select($val);
                break;
        }

    }
    //创作者推荐
    public function videocreateTeachers($value,$drama_id){
        $model = new video_video();
        $data = $model->likecert($value,$drama_id);
        return $data;

    }

    //类目信息
    public function findClasses($id){
        $model = new video_list();
        $data = $model->findId($id);
        return $data;
    }
    //商品ID查剧集
    public function findcoms($type,$com_id){
        if ($type == 'video'){
            $model = new video_video();
        }
        if ($type == 'drama'){
            $model = new video_drama();
        }

        $where = ['com_id'=>$com_id];
        $data = $model->wheredata($where);

        return $data;
    }



}



































?>