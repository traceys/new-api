<?php
namespace Video\Domain;
use PhalApi\Exception\BadRequestException;
use Video;
use Video\Model\video_checkauth as video_checkauth;
class DomainTimedTask {

    /**
     * addData  添加定时任务数据
     * arr 数据
     */
    public function addData($arr){
        $model = new video_checkauth();
        $where = [
            'uid'=>$arr['uid'],
            'res_id'=>$arr['res_id'],
            'type'=>$arr['type']
        ];
        $resdata =  $model->findData($where);
        if ($resdata){
            $model->UpdateData(array('id'=>$resdata['id']),array('on_status'=>-1));
        }
        $arr['start_time'] = Video\timeout(time());
        $res = $model->additionData($arr);
        return $res;
    }

    /**
     * findExecte
     */
    public function findExecte(){
        $model = new video_checkauth();
        $where = [
            'on_status'=>1,
            'status'=>1
        ];
       $res =  $model->findData($where,Video\timeout(time()),'All');
        $resouce = null;
       if ($res){
           foreach ( $res as $key =>$value){
               $urldata = [
                   'fid'=>$value['uid'],
                   'ftype'=>'user',
                   'res_type'=>$value['type'],
                   'res_id'=>$value['res_id'],
                   'server'=>'read',
                   'value'=>-1
               ];
               apiGet('Auth.Resource.Set',$urldata);

             $updata =   $model->UpdateData(array('id'=>$value['id']),array('on_status'=>-1));
            $resouce = true;
           }
       }
            return $resouce;
    }


}