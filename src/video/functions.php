<?php
namespace Video;

require_once 'Exit/vendor/autoload.php';

use function foo\func;
use Phpml\Association\Apriori;
use Video\Common\ItemCF as ItemCF;
use PhalApi\Exception\BadRequestException;


///推荐运算
function getapriori($vid,$vids){
    $samples = $vids;
    $labels  = [];
    /*
     参数
     support支持度
     confidence 自信度
    */
    $associator = new Apriori($support = 0.5, $confidence = 0.5);
    /* 对其进行训练   */
    $associator->train($samples, $labels);
    /*
     */
    return $associator->predict($vid);
}

// function a(){
// 	return rus('mda-iebdmn8hbtn7kedw');
// }
//删除数组中指定值
function array_remove($data, $key){
    if(!array_key_exists($key, $data)){
        return $data;
    }
    $keys = array_keys($data);
    $index = array_search($key, $keys);
    if($index !== FALSE){
        array_splice($data, $index, 1);
    }
    return $data;

}

//获取资源详细信息
function findmessage($res_id,$res_type,$play_num=0,$grade = 5){

//    $curl = new \PhalApi\CUrl();
//    $rs = json_decode($curl->get('http://test.yusj.vip/?s=Comment.Comment.Selnum&token=orkfOyxw4y8jq1hP&res_id='.$res_id.'&res_type='.$res_type.'', 3000),true);
//
//    if ($rs['ret'] != 200){
//        throw new BadRequestException('加载资源出错',5);
//    }
//
//    foreach ($rs['data'] as $k => $v){
////        if ($v['thing'] == 'play_num'){
////            $play_num = $v['value'];
////        }
////        if ($v['thing'] == 'grade'){
////            $grade = $v['value'];
////        }
////
////    }

    $arr = [
        'res_id'=>$res_id,
        'res_type'=>$res_type
    ];

    $grade = apiGet('Comment.Comment.Selgrade',$arr);

    $arr['thing'] = 'view';
    $play_num = apiGet('Comment.Comment.Selview',$arr);

    $arr['play_num'] = $play_num;
    $arr['grade'] = $grade;
    return $arr;

}








//将图片文件移动到指定位置
function moveImg($file){

    $beginToday=mktime(0,0,0,date('m'),date('d'),date('Y'));

    $uploaded_file = $file['tmp_name'];

    $file_name = $file['name'];

    $user_path = 'uploads/' .$beginToday;

    if (!file_exists($user_path)) {

        mkdir($user_path, 0777);
    }

//    $move_to_file=$user_path."/".time().rand(1,1000).substr(base64_encode($file_name),strrpos(base64_encode($file_name),".")).".".get_extension($file_name);
    $move_to_file=$user_path."/".time().rand(1,1000).getRandomString(6).".".get_extension($file_name);

    move_uploaded_file($uploaded_file, iconv("utf-8","gb2312",$move_to_file));

    return iconv("utf-8","gb2312",$move_to_file);
}
//将指定时间戳转换为时间格式
function timeout($val){
   return date("Y-m-d H:i:s",time());
}
//redis set缓存
function setCache($key,$value){
    $key = 'video'.$key;
    return \PhalApi\DI()->redis->set_forever($key,$value);

}
//redis get缓存
function getCache($key){
    $key = 'video'.$key;
    return \PhalApi\DI()->redis->get_forever($key);
}
//redis delete缓存
function deleteCache($key){
    $key = 'video'.$key;
    return \PhalApi\DI()->redis->del($key);
}
//redis 清楚指定前缀的所有 key值
function clerCache($key){
    return \PhalApi\DI()->redis->delKeys($key);
}

//抓取接口内容
function curls($url){
    $ch=curl_init($url);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($ch,CURLOPT_BINARYTRANSFER,true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $output= curl_exec($ch);
    curl_close($ch);
    return json_decode($output,true);
}
//将数组排序并保留key值
 function sort_with_keyName($arr,$orderby='asc'){
    $new_array = array();
    $new_sort = array();
    foreach($arr as $key => $value){
        $new_array[] = $value;
    }
    if($orderby=='asc'){
        asort($new_array);
    }else{
        arsort($new_array);
    }
    foreach($new_array as $k => $v){
        foreach($arr as $key => $value){
            if($v==$value){
                $new_sort[$key] = $value;
                unset($arr[$key]);
                break;
            }
        }
    }
    return $new_sort;
}
//二维数组排序
function my_sort($arrays,$sort_key,$sort_order=SORT_DESC,$sort_type=SORT_NUMERIC ){
    if(is_array($arrays)){
        foreach ($arrays as $array){
            if(is_array($array)){
                $key_arrays[] = $array[$sort_key];
            }else{
                return false;
            }
        }
    }else{
        return false;
    }
    array_multisort($key_arrays,$sort_order,$sort_type,$arrays);
    return $arrays;
}
//随机生成一个优惠
function getRandomString($len, $chars=null)
{
    if (is_null($chars)) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    }
    mt_srand(10000000*(double)microtime());
    for ($i = 0, $str = '', $lc = strlen($chars)-1; $i < $len; $i++) {
        $str .= $chars[mt_rand(0, $lc)];
    }
    return $str;
}




//检测指定用户是否拥有对这个资源的权限
/**
 * @param $uid 用户ID
 * @param $rid 资源级别ID
 * @param int $res_id   资源ID 默认0
 * @param int $auth 开始
 * @param string $service 验证类型
 * @param int $abort 结束
 * @return bool|mixed|string
 */
 function resUid($uid,$rid,$res_id=0,$auth=1,$service='read',$abort=1){
    $res = false;
     $url = 'Auth.Resource.Check';
     $data = [
         'fid'=>$uid,
         'ftype'=>'user',
         'res_id'=>$res_id.'_'.$rid,
         'server'=>$service
     ];
                $data['res_type'] = 'videoClassLevel';

                $res =  apiGet($url,$data);



        return $res;

 }
 //检测指定用户是否为指定级别
function checkauth($uid,$team_type,$team_id,$auth=false){
        $url = 'Team.User.Info';
        $data =[
            'uid'=>$uid,
            'team_type'=>$team_type,
            'tid'=>$team_id
        ];
        $res = apiGet($url,$data);
        if ($team_id == 317 && !$res){
          $datas =  apiGet('Team.User.TeamList',['uid'=>$uid]);
          $ids = array_column($datas, 'user_name');
          $res =  in_array('子账号',$ids);
        }
        if (!$auth){
            if (!$res){
                $data['tid']= 319;
                $res = apiGet($url,$data);
            }
            if (!$res){
                $data['tid']= 320;
                $res = apiGet($url,$data);
            }
        }
        return $res;


}
//数据过滤
function undatas($val,$data){
    foreach( $val as $k=>$v){
        if( $v === $data )
            unset( $val[$k] );
    }
    return $val;
}
//获取用户信息
function getUserDtat($token=null){

        if (!$token){
            $token = $_GET['token'];
        }
        $data = apiGet('User.User.selecttoken',array('searchToken'=>$token));

    return $data;
}
/**
 * 视频权限数据过滤
 * 搜索接口不共用该函数
 * @param $data
 * @param $res
 * @param $organization
 * @return array
 */
function videodata($data,$res,$organization){
    $a = [];
    foreach ($data as $key => $value){
        switch ($value['level']){
            case 11:
                if ($res){
                    $a[] = $value;
                }
                break;
            case 10:
                if ($organization || $res){
                    $a[] = $value;
                }
                break;
            default:
                $a[] = $value;
        }
    }
    return $a;


}
























