<?php
namespace Video\Model;

use PhalApi\Model\NotORMModel as NotORM;

class video_list extends NotORM {

    //增加类目
    public function inserList($val){
        $model = $this->getORM();
        clearnCache(['video.model.list'],true);
        $data =  $model->insert($val);
        return $data;

    }
    //删除指定类目
    public function deleteList($val){
        $model = $this->getORM();
        clearnCache(['video.model.list'],true);
        $data =  $model->where('id',$val)->update(['status'=> -1 ]);
        return $data;
    }

    //展示类目
    public function slist(){

        $model = $this->getORM();
        $data =  cache(['video.model.list.slist']);
        if (!$data){
            $data =  $model->where(['status'=>1])->fetchAll();
            cache(['video.model.list.slist'],$data);
        }
        return $data;
    }
    //修改指定类目信息
    public function upList($val,$id){
        $model = $this->getORM();
        clearnCache(['video.model.list'],true);
        $data =  $model->where(['id'=>$id])->update($val);
        return $data;
    }
    //查看指定名称的类目
    public  function  findname($name){
        $model = $this->getORM();
        $data =  cache(['video.model.list.findname',$name]);
        if (!$data){
            $data =  $model->where(['class'=>$name])->fetchOne();
            cache(['video.model.list.findname',$name],$data);
        }
        return $data;
    }
    //查询
    public function findId($id){
        $model = $this->getORM();
        $data =  cache(['video.model.list.findId',$id]);
        if (!$data){
            $data =  $model->where(['id'=>$id])->fetchOne();
            cache(['video.model.list.findId',$id],$data);
        }
        return $data;
    }


}