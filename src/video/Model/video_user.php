<?php
namespace Video\Model;

use PhalApi\Model\NotORMModel as NotORM;

class video_user extends NotORM {

    //修改
    public function up($id,$val){
        $model = $this->getORM();

        return $model->where(['id'=>$id])->update($val);
    }

    //添加
    public function inser($val){
        $model = $this->getORM();
        return $model->insert($val);
    }

    //查询
    public function select($val){
        $model = $this->getORM();
        $data = $model->where($val);
        if (isset($val['id'])){
            $data = $data->fetchOne();
        }else{
            $data = $data->fetchAll();
        }
        return $data;
    }











}