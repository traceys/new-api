<?php
namespace Video\Model;

use PhalApi\Model\NotORMModel as NotORM;

class video_record extends NotORM {
    //查出标签出现的相似度
    public function  playrecord($uid=4){
        $arr = cache(['video.model.record.playrecord',$uid]);
        if (!$arr){
            $model = $this->getORM();
            $sql = "SELECT tag,count(tag) from api_video_record as a LEFT JOIN api_video_label as b on a.vid=b.video_id WHERE a.statu=1 &&  b.statu=1 && a.uid=$uid GROUP BY tag ORDER BY count(tag) asc ";
            $arr = $model->queryAll($sql, array());
            cache(['video.model.record.playrecord',$uid],$arr);
        }

        return $arr;
    }
    //计算用户查看过的所有视频
    public  function playreUid($uid = 4){
        $data = cache(['video.model.record.playreUid',$uid]);
        if (!$data){
            $model = $this->getORM();
            $data =  $model->select('vid')->where(['uid'=>$uid])->group('vid')->fetchAll();
            cache(['video.model.record.playreUid',$uid],$data);
        }
        return $data;
    }


    ///查询浏览过视频的用户
    public function selectuser(){
        $model=$this->getORM();
        $sql="select uid from api_video_record  GROUP BY uid";
        return $model->queryAll($sql);
    }
    ///查询用户浏览的视频
    public function selectvideo($uid){
        $model=$this->getORM();
        $sql="select DISTINCT vid  from api_video_record where uid ='.$uid.'";
        return $model->queryAll($sql);
    }
    //插入用户播放记录
    public function inser($val){
        $model=$this->getORM();
        $data = $model->insert($val);
        return $data;
    }





}