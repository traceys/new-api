<?php
namespace Video\Model;

use PhalApi\Model\NotORMModel as NotORM;

class video_label extends NotORM {

    //插入对应的视频标签信息
    public function insertag($arr,$vid){

        $model = $this->getORM();
        $data['video_id'] = $vid;
        foreach ($arr as $key => $value){
            $data['tag']=$value;
            $model->insert($data);
        }
        return true;

    }
    //删除指定视频所有标签
    public function detag($id){
        $model = $this->getORM();
       return $model->where('video_id',$id)->delete();

    }
    //修改指定视频标签
    public function upVideoTag($arr,$vid){
        clearnCache(['video.model.label'],true);
        $this->detag($vid);
        return $this->insertag($arr,$vid);

    }

    //计算所有的视频
    public function  calculate($vid,$tag){
        $model = $this->getORM();
        foreach ($tag as $key => $value){
            $tags = $value['tag'];
            foreach ($vid as $k => $v){
                $id = $v['id'];
                $data[$key] = $model->select('video_id')->where(['video_id'=> $id,'tag'=>$tags])->fetchOne();
            }

            $val[$tags] = $data[$key];

        }
    return $val;

    }

    //查看所有标签分类
    public function findType(){
        $data = cache(['video.model.label.findType']);
        if (!$data){
            $model = $this->getORM();

            $data =  $model->where(['statu'=>1])->group('tag')->fetchAll();
        }
        return $data;
    }


}