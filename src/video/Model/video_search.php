<?php
namespace Video\Model;

use PhalApi\Model\NotORMModel as NotORM;

class video_search extends NotORM {

    //对应所需搜索添加内容
    public function insersearch($arr,$id,$type=1){
        clearnCache(['video.model.search'],true);
        $model = $this->getORM();
        $data['res_id'] = $id;
        $data['res_type']=$type;
        foreach ( $arr as $key => $value){
            $data['keyword'] = $value;
            $model->insert($data);
        }
        return true;

    }
    //删除对应资源的所有修改
    public function deleteVideoSearch($id,$type=1){
        $model = $this->getORM();
        $model->where(['res_id'=>$id,'res_type'=>$type])->delete();

    }
    //修改指定资源
    public  function upVideoSearch($arr,$id,$type=1){
        clearnCache(['video.model.search'],true);
        $this->deleteVideoSearch($id,$type);
        return $this->insersearch($arr,$id,$type);
    }
    //匹配关键字搜索
    public function findsearch($val){
        $data =cache(['video.model.search.findsearch',$val]);
        if (!$data){
            $model = $this->getORM();
            $data =  $model->where('statu',1)->where('keyword',$val)->or('keyword LIKE ?', '%'.$val.'%')->group('res_id')->fetchAll();
            cache(['video.model.search.findsearch',$val],$data);
        }
      return $data;
    }


}