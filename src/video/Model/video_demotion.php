<?php
namespace Video\Model;

use PhalApi\Model\NotORMModel as NotORM;

class video_demotion extends NotORM {

    //修改
    public function up($where,$val){
        $model = $this->getORM();

        return $model->where($where)->update($val);
    }

    //添加
    public function inser($val){
        $model = $this->getORM();
        return $model->insert($val);
    }

    //查询
    public function select($val){
        $model = $this->getORM();
        $data = $model->where($val);
        if (isset($val['id'])){
            $data = $data->fetchOne();
        }else{
            $data = $data->fetchAll();
        }
        return $data;
    }
    //指定条件查找
    public function findWhereAll($val){
        $model = $this->getORM();
        $data =  $model->where(['statu'=>1]);
        if ($val['on_status']){
            $on_status = $val['on_status'];
            $data = $data->where(['on_status'=>$on_status]);
        }
        if ($val['time']){
            $data = $data->where('time < ?',$val['time']);
        }
        $data = $data->fetchAll();

        return $data;


    }
    //指定条件修改
    public function upWhereAll($val,$arr){
        $model = $this->getORM();
        $data =  $model->where(['statu'=>1]);
        if ($val['on_status']){
            $on_status = $val['on_status'];
            $data = $data->where(['on_status'=>$on_status]);
        }
        if ($val['time']){
            $data = $data->where('time < ?',$val['time']);
        }
        $data = $data->update($arr);

        return $data;


    }



}