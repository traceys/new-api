<?php
namespace Video\Model;

use PhalApi\Model\NotORMModel as NotORM;

class video_show extends NotORM {

    //单视频绑定剧集ID
    public function bound($val){
        $model = $this->getORM();
        $data =  $model->insert($val);
        clearnCache(['video.model.show']);
        return $data;
    }
    //查询绑定了指定剧集的视频
    public function find($id){
        $data = cache(['video.model.show.find',$id]);
        if (!$data){
            $model = $this->getORM();
            $data =  $model->where(['drama_id'=>$id])->fetchAll();
            cache(['video.model.show.find',$id],$data);
        }
       return $data;
    }
    //修改指定视频绑定的剧集
    public function upVideoDrama($did,$vid){
        $model = $this->getORM();
        $data =  $model->where(['video_id'=>$vid])->update(['drama_id'=>$did]);
        clearnCache(['video.model.show']);
        return $data;
    }
    //删除指定视频绑定
    public function deVideo($data){
        $model = $this->getORM();
        $arr =  $model->where($data)->delete();
        clearnCache(['video.model.show']);
        return $arr;
    }
    //查询指定视频绑定的剧集
    public function findVideo($vid){
        $data = cache(['video.model.show.findVideo',$vid]);
        if (!$data){
            $model = $this->getORM();
            $data =  $model->where(['video_id'=>$vid])->fetchOne();
            cache(['video.model.show.findVideo',$vid],$data);
        }
        return $data;

    }

}