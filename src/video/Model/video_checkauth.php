<?php
namespace Video\Model;

use PhalApi\Model\NotORMModel as NotORM;

class video_checkauth extends NotORM {

    /**
     * find
     * where 条件
     * type 展示数据格式
     */
    public function  findData($where='',$end_time=null,$type='One'){
        $res = $this->getORM();
        if ($where){
            $res = $res->where($where);
        }
        if ($end_time){
            $res  = $res->where('end_time < ?',$end_time);
        }
        switch ($type){
            case 'One':
                $res = $res->fetchOne();
                break;
            case 'All':
                $res = $res->fetchAll();
        }
        return $res;
    }

    /**
     * update
     * where 条件
     * data 数据
     */
    public function UpdateData($where,$data){
        $res = $this->getORM();
        $res = $res->where($where)->update($data);
        return $res;
    }
    /**
     * additionData
     * data  数据
     */
    public function additionData($data){
        $res = $this->getORM();
        $res = $res->insert($data);
        return $res;
    }

}