<?php
namespace Video\Model;

use PhalApi\Model\NotORMModel as NotORM;

class video_video extends NotORM {
    //视频添加
    public  function  inser($arr){


        $model = $this->getORM();
        $data =  $model->insert($arr);
        $id =  $model->insert_id();
        clearnCache(['video.model.video'],true);
        return $id;

    }
    //展示视频基本信息包含tag
    public function find($id){
        $arr = cache(['video.model.video.find',$id]);
        if (!$arr){
            $model = $this->getORM();
            $sql = "SELECT `a`.*,`b`.tag,c.drama_id  FROM `api_video_video` AS a 
               LEFT JOIN `api_video_label` AS b ON a.id=b.video_id
               LEFT JOIN `api_video_show` as c ON a.id=c.video_id WHERE a.id=$id";
            $arr = $model->queryAll($sql, array());
            cache(['video.model.video.find',$id],$arr);
        }
        return $arr;
    }
    //查找剧集下的所有视频
    public function findDramaAll($arr,$level=null,$label=null,$type='false'){
        $data = cache(['video.model.video.findDramaAll',json_encode($arr),$level,$label,$type]);

        if (!$data){
            $data=[];
            if ($arr){
                foreach ($arr as $key => $value){
                    $model = $this->getORM();
                    $id = $value['video_id'];
                    $sql = "SELECT a.*,b.tag,b.video_id FROM api_video_video AS a LEFT JOIN
                        api_video_label AS b ON a.id=b.video_id 
                        WHERE a.id=$id && a.status=1 && a.on_status=1";
                    if ($label){
                        $sql = $sql." && b.tag='$label'";
                    }
                    if ($level){
                        $sql = $sql." && a.level=$level";
                    }
                    $sql = $sql." ORDER BY a.id DESC";
                    $arrs = $model->queryAll($sql, array());
                    if ($arrs){
                        if ($type == 'false'){
                            $arrs['0']['img'] = '';
                            $arrs['0']['url'] = '';
                            $arrs['0']['play_url']='';
                            $arrs['0']['body'] = '权限不足';
                        }
                        $data[] = $arrs['0'];
                    }
                }
                cache(['video.model.video.findDramaAll',json_encode($arr),$level,$label,$type],$data);
            }
        }
        return $data;
    }
    //视频信息更新
    public function upVideo($arr,$id){
        $model = $this->getORM();
        $data = $model->where(['id'=>$id])->update($arr);
        clearnCache(['video.model.video'],true);
        return $data;

    }
    //查看热门视频
    public function hotVideo($val){
        $data = cache(['video.model.video.hotVideo',json_encode($val)]);
        if (!$data){
            $model = $this->getORM();
            foreach ($val as $key =>$value){
                $id = $value['res_id'];
                $sql = "SELECT a.*,b.drama_id FROM `api_video_video` AS a JOIN `api_video_show` AS b ON a.id=b.video_id WHERE b.video_id=$id && b.statu=1";
                $res[] = $model->queryAll($sql, array());
            }
            foreach ($res as $key => $value){
                if (isset($value['0'])){
                    $data[] = $value['0'];
                }
            }
            cache(['video.model.video.hotVideo',json_encode($val)],$data);
        }

        return $data;
    }
    //查看指定ID视频
    public function findVideo($id){
        $data = cache(['video.model.video.findVideo',$id]);
        if (!$data){
            $model = $this->getORM();
            $data = $model->where(['id'=>$id,'status'=>1])->fetchOne();
            cache(['video.model.video.findVideo',$id],$data);
        }
        return $data;

    }
    //展示指定视频
    public function showVideo($val,$type='data'){
        $arr = cache(['video.model.video.showVideo',json_encode($val),$type]);
        if (!$arr){
            $model = $this->getORM();
            $sql = "SELECT a.*,d.id as drama_id,d.title as drama_title";
            $sql =$sql. " FROM `api_video_video` AS a LEFT JOIN 
                `api_video_label` AS b ON a.id=b.video_id LEFT JOIN
                `api_video_show` AS c ON a.id=c.video_id LEFT JOIN
                `api_video_drama` AS d ON c.drama_id=d.id LEFT JOIN 
                `api_video_categories` AS e ON d.id=e.drama_id WHERE a.status=1 ";

            //如果有类目筛选的话
            if ($val['class_id']){
                $sql = $sql.' AND e.class_id='.$val['class_id'].'';
            }
            //如果有标签筛选的话
            if ($val['tag']){
                $sql = $sql.' AND b.tag='.$val['tag'].'';
            }
            //视频上下架筛选
            if ($val['on_status'] !== 'null'){
                $sql = $sql.' AND a.on_status='.$val['on_status'].'';
            }
            //视频等级
            if ($val['level'] != null){
                $sql = $sql.' AND a.level='.$val['level'].'';
            }
            //剧集ID
            if ($val['drama_id']){
                $sql = $sql.' AND c.drama_id='.$val['drama_id'].'';
            }
            //商品ID
            if ($val['com_id']){
                $com_id = $val['com_id'];
                $sql = $sql." AND a.com_id=$com_id ";
            }
            //搜索
            if ($val['key'] != null){
                $key = $val['key'];
                $search = $val['search'];
                $sql = $sql. " AND a.$key LIKE '%$search%'";
            }

            $sql =  $sql.' GROUP BY a.id';

            //排序方式
            switch ($val['orderStatus']){
                case 1:
                    $sql = $sql.' ORDER BY a.id DESC';
                    break;
                case 2:
                    $sql =  $sql.' ORDER BY a.id DESC';
            }
            if ($type == 'data'){
                if ($val['page']) {
                    $sql = $sql . ' LIMIT ' . ($val['page'] - 1) * 40 . ',40';
                }
            }


            $arr = $model->queryAll($sql, array());
            cache(['video.model.video.showVideo',json_encode($val),$type],$arr);
        }

        return $arr;

    }

    //查询所有视频ID
    public function vidAll($res,$organization){
        $arr = cache(['video.model.video.vidAll']);
        if (!$arr){
            $model = $this->getORM();
            $sql ="SELECT a.*,b.tag FROM api_video_video AS a  JOIN api_video_label AS  b ON a.id=b.video_id WHERE a.status=1 ";
            if (!$res){
                $sql .= " && a.level<11";
            }
            if (!$organization){
                $sql .= " && a.level<10";
            }
            $arr = $model->queryAll($sql, array());
            cache(['video.model.video.vidAll'],$arr);
        }
        return $arr;

    }

//随机取出多少个视频
    public function randdata($num,$res,$organization){
        $arr = cache(['video.model.video.randdata',$num]);

        if (!$arr){
            $model = $this->getORM();
            $sql = "select * from api_video_video WHERE status=1";
            if (!$res){
                $sql .=" && `level`<11";
            }
            if (!$organization){
                $sql .=" && `level` <10";
            }
            $sql .= " order by rand() limit $num";
            $arr = $model->queryAll($sql, array());
            cache(['video.model.video.randdata',$num],$arr);
        }
        return $arr;
    }
//指定
//Delete the specified video
public function deletev($id,$statu){
    $model = $this->getORM();
    clearnCache(['video.model.video'],true);
    $data = $model->where(['id'=>$id])->update(['status'=>$statu]);
    return $data;
}
//select string ID
public function findString($val){
    $arr = cache(['video.model.video.findString',$val]);
    if (!$arr){
        $model = $this->getORM();
        $sql = "SELECT a.*,b.drama_id FROM api_video_video as a LEFT JOIN api_video_show as b ON a.id=b.video_id WHERE a.id IN ($val)";
        $arr = $model->queryAll($sql, array());
        cache(['video.model.video.findString',$val],$arr);
    }
    return $arr;
}

//查询所有视频总数
public function all($on_status){
    $model = $this->getORM();
    if($on_status != 'false' ){
        $data = $model->where(['on_status'=>$on_status])->count('id');
    }
    $data = $model->count('id');
    return $data;
}

//查询指定推广码使用结果
    public function proVideo($start,$end){
        $data = cache(['video.model.video.proVideo',$start,$end]);
        if (!$data){
            $model =  $this->getORM();
            $data = $model->where('create_time > ?',$start)->where('create_time < ?',$end)->fetchAll();

        }
        return $data;
    }
//查找所有视频
    public function findAllVideo($type=null){
        $data = cache(['video.model.video.findAllVideo']);
        if (!$data){
            $model =  $this->getORM();
            $data = $model->where(['status'=>1])->fetchAll();
            cache(['video.model.video.findAllVideo'],$data);
        }
        return $data;
    }
    //匹配创作者
    public function likecert($val,$drama_id){
        $videos = cache(['video.model.video.likecert',$val,$drama_id]);
        if (!$videos){
            $videos = [];
            $sql = "SELECT a.* FROM api_video_video AS a LEFT JOIN api_video_show AS b ON a.id=b.video_id WHERE a.status=1 ";
            if ($drama_id){
                $sql .= " && b.drama_id != $drama_id ";
            }
            $res = $this->getORM()->queryAll($sql,array());
            foreach ($res as $key => $value){
                $arr = json_decode($value['desc'],true);
                foreach ($arr['createTeacher'] as $k => $v){
                    if ($v == $val){
                        $videos[] = $res[$key];
                    }
                }
            }
            cache(['video.model.video.likecert',$val,$drama_id],$videos);
        }
           shuffle($videos);
           if(count($videos)>5){
            $videos=array_slice($videos,0,5);
           }
           

            cache(['video.model.video.likecert',$val],$videos);
            $data=$videos;

        return $data;
    }
    //查找指定条件视频
    public function wheredata($where){
        $data = cache(['video.model.video.wheredata',$where]);
        if (!$data){
            $model = $this->getORM();
            $data = $model->where($where)->fetchOne();
            cache(['video.model.video.wheredata',$where],$data);
        }
        return $data;
    }

}




