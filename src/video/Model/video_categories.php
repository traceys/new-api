<?php
namespace Video\Model;

use PhalApi\Model\NotORMModel as NotORM;

class video_categories extends NotORM {

    //通过ID删除指定绑定剧集
    public function deleteDramaList($val){
        $model = $this->getORM();
       $data =  $model->where('id',$val)->update(['statu'=>-1]);
        clearnCache(['video.model.categories'],true);
        return $data;
    }
    //添加指定剧集绑定
    public function  inserDramaList($val){
        $model = $this->getORM();
        $data =  $model->insert($val);
        clearnCache(['video.model.categories'],true);
        return $data;
    }
    //通过绑定的类和剧集删除指定绑定剧集
    public function deDramaList($val){
        $model = $this->getORM();
        $data =  $model->where($val)->update(['statu'=>'-1']);
        clearnCache(['video.model.categories'],true);
        return $data;

    }
    //查询指定剧集的类目
    public function findclass($drama_id){
        $data = cache(['video.model.categories.findclass',$drama_id]);
        if (!$data){
            $model = $this->getORM();
            $data =  $model->where(['drama_id'=>$drama_id])->fetchOne();
            cache(['video.model.categories.findclass',$drama_id],$data);
        }
        return $data;
    }
    //修改指定剧集的类目
    public function upclass($drama_id,$class_id){
        $model = $this->getORM();
        $data =  $model->where(['drama_id'=>$drama_id])->update(['class_id'=>$class_id]);
        clearnCache(['video.model.categories'],true);
        return $data;
    }
}