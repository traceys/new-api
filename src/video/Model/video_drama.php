<?php
namespace Video\Model;

use PhalApi\Model\NotORMModel as NotORM;

class video_drama extends NotORM {

    //展示剧集以及包含的视频信息
    public  function dramaVideo($id){
        $data = cache(['video.model.drama.dramaVideo',$id]);
        if (!$data){
            $model = $this->getORM();
            $data =  $model->where(['id'=>$id])->fetchOne();
            cache(['video.model.drama.dramaVideo',$id],$data);
        }
        return $data;
    }
    //添加剧集
    public function  inserDrama($arr){
        $model = $this->getORM();
        $data = $model->insert($arr);
        clearnCache(['video.model.drama'],true);
        $id =  $model->insert_id();
        return $id;
    }
    //修改指定剧集信息
    public function upDrama($arr,$id){

        $model = $this->getORM();
        $data =  $model->where(['id'=>$id])->update($arr);
        clearnCache(['video.model.drama'],true);
        return $data;
    }

    //查看筛选过的剧集
    public function  filtrateDrama($val,$resAuth=0,$organization=0,$type = 'data'){
        $arr = cache(['video.model.drama.filtrateDrama',json_encode($val),json_encode($resAuth),json_encode($organization),$type]);
        if (!$arr){
            $model = $this->getORM();
            $sql = "SELECT a.* ,group_concat(b.class_id) as class";
            $sql =$sql." FROM `api_video_drama` AS a LEFT JOIN 
                `api_video_categories` AS b ON a.id=b.drama_id LEFT JOIN
                `api_video_show` AS c ON a.id=c.drama_id LEFT JOIN 
                `api_video_video` AS e ON c.video_id=e.id LEFT JOIN
                `api_video_label` AS d ON c.video_id=d.video_id 
                WHERE a.statu=1";
            //如果有类目筛选的话
            if ($val['class_id']){
                $sql = $sql.' AND b.class_id='.$val['class_id'].'';
            }
            if ($val['keyword']){
                $keyword = $val['keyword'];
                $sql = $sql.' AND a.title like "'.$keyword.'%"';
            }
            if ($val['level']){
                $level = $val['level'];
                $sql = $sql." AND e.level=$level";
            }
            if ($val['tag']){
                $tag = $val['tag'];
                $sql = $sql.' AND d.tag like "%'.$tag.'%"';
            }
            if (!$resAuth){
                $sql =  $sql." AND e.level<11";
            }
            if (!$organization && !$resAuth){
                $sql =  $sql." AND e.level<10";
            }
            if ($val['on_status'] !== 'null'){
                $on_status = $val['on_status'];
                $sql = $sql.' AND a.on_status='.$on_status.'';
            }

                $sql = $sql .' GROUP BY a.id ';
                $order = isset($val['order'])?$val['order']:'a.id';

            //排序方式
            switch ($val['orderStatus']){
                case 1:
                    $sql = $sql.' ORDER BY '.$order.' ASC';
                    break;
                case 2:
                    $sql =  $sql.' ORDER BY '.$order.' DESC';
            }
        if ($type == 'data'){
            if ($val['page']) {
                $sql = $sql . ' LIMIT '. ($val['page'] - 1) * $val['pagenum'] . ','.$val['pagenum'].'';
            }
        }
            $arr = $model->queryAll($sql, array());
            //数据处理
            foreach ($arr as $key => $value){
                $a = explode(',',$value['class']);
                $arr[$key]['class'] = $a['0'];
            }
            cache(['video.model.drama.filtrateDrama',json_encode($val),json_encode($resAuth),json_encode($organization),$type],$arr);
        }

        return $arr;

    }


    //查找指定剧集
    public function findalldrama($arr){
        $data = cache(['video.model.drama.findalldrama',json_encode($arr)]);
        if (!$data){
            foreach ($arr as $key => $value){
                $model = $this->getORM();
                $id = $value['res_id'];
                $data[] = $model->where('id',$id)->fetchOne();
            }
            cache(['video.model.drama.findalldrama',json_encode($arr)],$data);
        }
        return $data;

    }

    //展示所有剧集
    public function allDrama(){
        $model = $this->getORM();
        $data = cache(['video.model.drama.allDrama']);
        if (!$data){
            $data = $model->where(['statu'=>1,'on_status'=>1])->fetchAll();
            cache(['video.model.drama.allDrama'],$data);
        }
       return $data;

    }

    //查询所有视频总数
    public function all($on_status){
        $model = $this->getORM();
        if($on_status != 'false' ){
            $data = $model->where(['on_status'=>$on_status])->count('id');
        }
       $data = $model->count('id');
        return $data;
    }

    //查找指定条件剧集
    public function wheredata($where){
        $data = cache(['video.model.drama.wheredata',$where]);
        if (!$data){
            $model = $this->getORM();
            $data = $model->where($where)->fetchOne();
            cache(['video.model.video.drama',$where],$data);
        }
        return $data;
    }

}