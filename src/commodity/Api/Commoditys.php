<?php
namespace Commodity\Api;
use PhalApi\Api;
use Commodity\Domain\Remode as Remode;
use Commodity;
use PhalApi\Exception\BadRequestException;
class Commoditys extends Api{

    private static $data = null;

    public function __construct()
    {
        self::getDomainAdd();
        self::getToken();
    }

    //实例化domin层
    static  private  function getDomainAdd(){
        if (self::$data === null){
            self::$data = new  Remode();
        }

    }
    //得到token值
    static private  function getToken(){


        $token = isset($_GET['token'])?$_GET['token']:'';


        if (!Commodity\getCache($token)){

            $url = "test.yusj.vip?s=Demo.User.Selecttoken&searchToken=$token&token=$token";
            $res = Commodity\curls($url);

            if ($res['ret'] == 200 ){
                Commodity\setCache($token,$res['data']);
            }
        }

    }
    //记录信息验证
    public function getRules() {
        return array(
            '*' => array(
                'token' => array('name' => 'token','source'=>'get','desc' => '用户身份token'),
            ),
            'configuration' => array(
                'type' => array('name' => 'type','require'=>true,'desc' => '类型'),
                'url' => array('name' => 'url','require'=>true,'desc' => '地址 '),
                'parameter' => array('name' => 'parameter','require'=>true,'desc' => '参数 '),
                'url_type' => array('name' => 'url_type','require'=>true,'desc' => '回调网址'),
                'on_status' => array('name' => 'on_status','require'=>true,'desc'=> '开放状态'),
                'statu' => array('name' => 'statu','require'=>true,'desc'=> '是否删除'),
            ),
            'insertcom' => array(
                'type' => array('name' => 'type','require'=>true,'desc' => '类型'),
                'name' => array('name' => 'name','require'=>true,'desc' => '商品名'),
                'money_type' => array('name' => 'money_type','default'=>0,'desc' => '货币类型'),
                'money' => array('name' => 'money','default'=>0,'type'=>'int','desc' => '货币金额'),
                'num' => array('name' => 'num','default'=>1000000,'desc' => 'num'),
                'on_status' => array('name' => 'on_status','default'=>1,'desc' => '是否开放 '),
                'desc' => array('name' => 'desc','require'=>true,'desc' => '商品说明备注'),
            ),
            'upcom' => array(
                'id' => array('name' => 'id','require'=>true,'desc' => '商品ID'),
                'name' => array('name' => 'name','default'=>null,'desc' => '商品名'),
                'money_type' => array('name' => 'money_type','default'=>null,'desc' => '货币类型'),
                'money' => array('name' => 'money','default'=>null,'desc' => '货币金额'),
                'num' => array('name' => 'num','default'=>null,'desc' => 'num'),
                'on_status' => array('name' => 'on_status','default'=>null,'desc' => '是否开放 '),
                'desc' => array('name' => 'desc','default'=>null,'desc' => '商品说明备注'),
            ),
            'checklist' => array(
                'type' => array('name' => 'type','default'=>false,'desc' => '类型'),
                'money_type' => array('name' => 'money_type','default'=>false,'desc' => '货币类型'),
                'keyword' => array('name' => 'keyword','default'=>'','desc' => '商品关键字'),
                'order' => array('name' => 'order','default'=>'id','desc' => '排序字段'),
                'order_by' => array('name' => 'order_by','default'=>'DESC','desc' => '排序方式'),
                'page' => array('name' => 'page','default'=>1,'desc' => '分页数'),
            ),
            'message' => array(
                'id' => array('name' => 'id','require'=>true,'desc' => 'ID'),
            ),
            'reduce' => array(
                'id' => array('name' => 'id','require'=>true,'desc' => 'ID'),
                'num' => array('name' => 'num','default'=>1,'desc' => 'ID'),
            ),
        );
    }



    //配置商品的业务回调
    public function configuration()
    {
        $arr = [
            'type'=>$this->type,
            'url'=>$this->url,
            'parameter'=>$this->parameter,
            'url_type'=>$this->url_type,
            'on_status'=>$this->on_status,
            'statu'=>$this->statu
        ];

        return self::$data->index($arr);

    }
    //添加商品
    public function insertcom(){

        $arr = [
            'type'=>$this->type,
            'name'=>$this->name,
            'money_type'=>$this->money_type,
            'money'=>$this->money,
            'num'=>$this->num,
            'on_status'=>$this->on_status,
            'desc'=>$this->desc
        ];

        return self::$data->vc($arr);

    }
    //修改商品
    public function upcom(){
        $arr = [
            'id'=>$this->id,
            'name'=>$this->name,
            'money_type'=>$this->money_type,
            'money'=>$this->money,
            'num'=>$this->num,
            'on_status'=>$this->on_status,
            'desc'=>$this->desc
        ];

        return self::$data->dc($arr);
}

    //查看指定列表
    public function checklist(){
        $arr = [
            'type'=>$this->type,
            'money_type'=>$this->money_type,
            'keyword'=>$this->keyword,
            'order'=>$this->order,
            'order_by'=>$this->order_by,
            'page'=>$this->page,
        ];

        return self::$data->showlist($arr);

    }

    //查找单个数据
    public function message(){

        return self::$data->mssa($this->id);

    }
    //减少单个商品数量
    public function reduce(){


        return self::$data->reduceNumber($this->id,$this->num);

    }



}