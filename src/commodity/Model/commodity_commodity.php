<?php
namespace Commodity\Model;

use PhalApi\Model\NotORMModel as NotORM;

class commodity_commodity extends NotORM {


    //插入新的商品
    public function issue($arr){
        $model = $this->getORM();

        return $model->insert($arr);
    }

    //查找单个商品
    public function find($id){
        $model = $this->getORM();
        return $model->where(['id'=>$id])->fetchOne();
    }
    //修改指定商品
    public function updatecom($where,$arr){
        $model = $this->getORM();
        return $model->where($where)->update($arr);
    }
    //只是筛选商品
    public function findall($arr,$num = 20){
        $model = $this->getORM();


        $data = $model->where(['statu'=>1]);


         $keyword = $arr['keyword'];

            if ($keyword){
             $data =$data->where('name LIKE ?', "%$keyword%")->or('type LIKE ?', "%$keyword%");
            }

            if ($arr['type']){
                $data = $data->where(['type'=>$arr['type']]);
            }
            if ($arr['money_type']){
                $data =  $data->where(['money_type'=>$arr['money_type']]);
            }

            $order = $arr['order'];
            $order_by = $arr['order_by'];

             $data = $data->order("$order $order_by");

            $page = ($arr['page']-1)*$num;

            $data = $data->limit($page,$num)->fetchAll();

        return $data;
    }



    //减少商品
    public function reducedel($id,$num){

        $model = $this->getORM();

        return  $model->where(['id'=>$id])->update(array('num' => new \NotORM_Literal("num - $num")));

    }

}