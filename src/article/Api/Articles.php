<?php
namespace Article\Api;
use PhalApi\Api;
use Article\Domain\Remode as Remodes;
use Article;
use PhalApi\Exception\BadRequestException;
class Articles extends Api{

    private static $data = null;

    public function __construct()
    {
//        return 123;
        self::getDomainAdd();
        self::getToken();
    }

    //实例化domin层
    static  private  function getDomainAdd(){
        if (self::$data === null){
            self::$data = new  Remodes();
        }

    }
    //得到token值
    static private  function getToken(){


        $token = isset($_GET['token'])?$_GET['token']:'';


        if (!Article\getCache($token)){

            $url = "test.yusj.vip?s=Demo.User.Selecttoken&searchToken=$token&token=$token";
            $res = Article\curls($url);

            if ($res['ret'] == 200 ){
                Article\setCache($token,$res['data']);
            }
        }

    }
    //记录信息验证
    public function getRules() {
        return array(
            '*' => array(
                'token' => array('name' => 'token','source'=>'get','desc' => '用户身份token'),
            ),
            'uparticle' => array(
                'type' => array('name' => 'type','require'=>true,'desc' => '分类ID'),
                'title' => array('name' => 'title','require'=>true,'desc' => '标题'),
                'body' => array('name' => 'body','require'=>true,'desc' => '文章内容'),
                'writer' => array('name' => 'writer','require'=>true,'desc' => '文章作者'),
                'platform' => array('name' => 'platform','default'=>1,'desc' => '文章所属'),
                'img' => array('name' => 'img','default'=>'','desc' => '图片'),
                'desc' => array('name' => 'desc','require'=>true,'desc'=> '文章缩略'),
                'on_statu' => array('name' => 'on_statu','default'=>-1,'desc'=> '状态'),
            ),
            'update' => array(
                'id' => array('name' => 'id','require'=>true,'desc' => '文章ID'),
                'type' => array('name' => 'type','default'=>null,'desc' => '文章分类ID'),
                'title' => array('name' => 'title','default'=>null,'desc' => '标题'),
                'body' => array('name' => 'body','default'=>null,'desc' => '文章内容'),
                'desc' => array('name' => 'desc','default'=>null,'desc' => '文章缩略'),
                'writer' => array('name' => 'writer','default'=>null,'desc' => '文章作者'),
                'time' => array('name' => 'time','default'=>null,'desc' => '发布时间'),
                'platform' => array('name' => 'platform','default'=>null,'desc' => '文章所属'),
                'img' => array('name' => 'img','default'=>null,'desc' => '图片'),
                'status' => array('name' => 'status','default'=>null,'type' => 'int','desc' => '文章状态 '),
                'statu' => array('name' => 'statu','default'=>null,'type' => 'int','desc' => '是否有效'),
            ),
            'getarticle' => array(
                'type_id' => array('name' => 'type_id','default'=>null,'desc' => '文章类型'),
                'select' => array('name' => 'select','default'=>null,'desc' => 'JSON数据'),
                'page' => array('name' => 'page','default'=>1,'desc' => '当前页数'),
                'num' => array('name' => 'num','default'=>40,'desc' => '提取条数'),
            ),

            'message' => array(
                'id' => array('name' => 'id','require'=>true,'desc' => 'id'),
                'type' => array('name' => 'type','default'=>'one','desc' => '类型'),
            ),
            'addupClass' => array(
                'id' => array('name' => 'id','default'=>null,'desc' => 'ID'),
                'name' => array('name' => 'name','default'=>null,'desc' => '名称'),
                'img' => array('name' => 'img','default'=>null,'desc' => '图片'),
                'body' => array('name' => 'body','default'=>null,'desc' => '内容'),
                'type' => array('name' => 'type','default'=>null,'desc' => '类型'),
                'on_statu' => array('name' => 'on_statu','default'=>null,'desc' => '状态'),
                'statu' => array('name' => 'statu','default'=>null,'desc' => '数据'),
            ),
            'ClassList' => array(
                'id' => array('name' => 'id','default'=>null,'desc' => 'ID'),
                'name' => array('name' => 'name','default'=>null,'desc' => '名称'),
                'type' => array('name' => 'type','default'=>null,'desc' => '类型'),
                'on_statu' => array('name' => 'on_statu','default'=>null,'desc' => '状态')
            ),
        );
    }



    //发布文章
    public function uparticle()
    {
        $arr = [
            'type'=>$this->type,
            'title'=>$this->title,
            'body'=>$this->body,
            'desc'=>$this->desc,
            'writer'=>$this->writer,
            'img'=>$this->img,
            'on_statu'=>$this->on_statu,
            'platform'=>$this->platform
        ];
        return self::$data->index($arr);

    }
    //修改文章
    public function update(){
//        return $this->id;
        $arr = [
            'type'=>$this->type,
            'title'=>$this->title,
            'body'=>$this->body,
            'desc'=>$this->desc,
            'id'=>$this->id,
            'time'=>$this->time,
            'writer'=>$this->writer,
            'on_statu'=>$this->status,
            'img'=>$this->img,
            'statu'=>$this->statu,
            'platform'=>$this->platform
        ];
        return self::$data->vc($arr);

    }
    //查看
    public function getarticle(){

        return self::$data->dc($this->type_id,$this->select,$this->page,$this->num);
}
    //指定message
    public function message(){
        return self::$data->mssa($this->id,$this->type);
    }
    //添加修改指定类目
    public function addupClass(){
        $arr = [
            'id'=>$this->id,
            'name'=>$this->name,
            'body'=>$this->body,
            'img'=>$this->img,
            'type'=>$this->type,
            'on_statu'=>$this->on_statu,
            'statu'=>$this->statu
        ];
        return self::$data->uoaddClass($arr);
    }
    //类目信息
    public function ClassList(){
        $arr = [
            'id'=>$this->id,
            'name'=>$this->name,
            'type'=>$this->type,
            'on_statu'=>$this->on_statu
        ];
        return self::$data->ClassListData($arr);
    }

}