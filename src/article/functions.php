<?php
namespace Article;



function hello() {
    return rus();
}
// function a(){
// 	return rus('mda-iebdmn8hbtn7kedw');
// }
//删除数组中指定值
function array_remove($data, $key){
    if(!array_key_exists($key, $data)){
        return $data;
    }
    $keys = array_keys($data);
    $index = array_search($key, $keys);
    if($index !== FALSE){
        array_splice($data, $index, 1);
    }
    return $data;

}
//数据过滤
function undatas($val,$data){
    foreach( $val as $k=>$v){
        if( $v === $data )
            unset( $val[$k] );
    }
    return $val;
}

//将图片文件移动到指定位置
function moveImg($file){

    $beginToday=mktime(0,0,0,date('m'),date('d'),date('Y'));

    $uploaded_file = $file['tmp_name'];

    $file_name = $file['name'];

    $user_path = 'uploads/' .$beginToday;

    if (!file_exists($user_path)) {

        mkdir($user_path, 0777);
    }

//    $move_to_file=$user_path."/".time().rand(1,1000).substr(base64_encode($file_name),strrpos(base64_encode($file_name),".")).".".get_extension($file_name);
    $move_to_file=$user_path."/".time().rand(1,1000).getRandomString(6).".".get_extension($file_name);

    move_uploaded_file($uploaded_file, iconv("utf-8","gb2312",$move_to_file));

    return iconv("utf-8","gb2312",$move_to_file);
}
//将指定时间戳转换为时间格式
function timeout($val){
   return date("Y-m-d H:i:s",time());
}
//redis set缓存
function setCache($key,$value){
    $key = 'video'.$key;
    return \PhalApi\DI()->redis->set_forever($key,$value);

}
//redis get缓存
function getCache($key){
    $key = 'video'.$key;
    return \PhalApi\DI()->redis->get_forever($key);
}
//redis delete缓存
function deleteCache($key){
    $key = 'video'.$key;
    return \PhalApi\DI()->redis->del($key);
}
//redis 清楚指定前缀的所有 key值
function clerCache($key){
    return \PhalApi\DI()->redis->delKeys($key);
}
//抓取接口内容
function curls($url){
    $ch=curl_init($url);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($ch,CURLOPT_BINARYTRANSFER,true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $output= curl_exec($ch);
    curl_close($ch);
    return json_decode($output,true);
}
//将数组排序并保留key值
 function sort_with_keyName($arr,$orderby='asc'){
    $new_array = array();
    $new_sort = array();
    foreach($arr as $key => $value){
        $new_array[] = $value;
    }
    if($orderby=='asc'){
        asort($new_array);
    }else{
        arsort($new_array);
    }
    foreach($new_array as $k => $v){
        foreach($arr as $key => $value){
            if($v==$value){
                $new_sort[$key] = $value;
                unset($arr[$key]);
                break;
            }
        }
    }
    return $new_sort;
}
//二维数组排序
function my_sort($arrays,$sort_key,$sort_order=SORT_DESC,$sort_type=SORT_NUMERIC ){
    if(is_array($arrays)){
        foreach ($arrays as $array){
            if(is_array($array)){
                $key_arrays[] = $array[$sort_key];
            }else{
                return false;
            }
        }
    }else{
        return false;
    }
    array_multisort($key_arrays,$sort_order,$sort_type,$arrays);
    return $arrays;
}