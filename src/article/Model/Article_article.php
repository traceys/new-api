<?php
namespace Article\Model;

use PhalApi\Model\NotORMModel as NotORM;

class Article_article extends NotORM {



    //发布文章
    public  function issue($arr){

        $model = $this->getORM();
        $data =  $model->insert($arr);
        clearnCache(['article.model.article'],true);
        return $data;
    }
    //提取上一篇和下一篇
    public function uper($id,$type){
        $data = cache(['article.model.article.uper',$id,$type]);
        if (!$data){
            $model = $this->getORM();
            $data =  $model->where(['statu'=>'1']);
            if ($type == 'upper'){
                $data = $data->where('id > ?',$id)->order('id DESC');
            }
            if ($type == 'lower'){
                $data = $data->where('id < ?',$id)->order('id DESC');
            }
            $data = $data->fetchOne();
            cache(['article.model.article.uper',$id,$type],$data);
        }
        return $data;
    }


    //查找单个文章信息
    public function find($id,$type="all"){
        $data = cache(['article.model.article.find',$id,$type]);
        if (!$data){
            $model = $this->getORM();
            $data =  $model->where(['id'=>$id,'statu'=>'1']);
            if ($type=="show"){
                $data = $data->where(['on_statu'=>'1']);
            }
            $data =  $data->fetchOne();
            cache(['article.model.article.find',$id,$type],$data);
        }
        return $data;



    }

    //修改视频
    public function updatearticle($arr,$id){
        $model = $this->getORM();
        $data =  $model->where(['id'=>$id])->update($arr);
        clearnCache(['article.model.article'],true);
        return $data;

    }
    //查找所有文章
    public function findall($where,$page,$num){
        $data = $this->getORM();
        if ($where){
            $data = $data->where($where);
        }
        $data =  $data->limit($page,$num)->fetchAll();
        return $data;
    }
    //展示指定条件视频
    public function wherefind($arr,$page,$num,$type = 'data'){
        $data = cache(['article.model.article.wherefind',json_encode($arr),$page,$num,$type]);
        if(!$data){
            $data = $this->getORM();
            $data = $data->where(['statu'=>1]);
            if ($arr){
                $data =  $data->where($arr);
            }
            if ($type == 'data'){
                $data =  $data->order('id DESC')->limit($page,$num)->fetchAll();
            }
            if ($type == 'num'){
                $data = $data->count('id');
            }
            cache(['article.model.article.wherefind',json_encode($arr),$page,$num,$type],$data);
        }

        return $data;
    }








}