<?php
namespace Article\Model;

use PhalApi\Model\NotORMModel as NotORM;

class Article_class extends NotORM {


    //修改
    public function up($id,$arr){
        $data = $this->getORM();
        $data = $data->where(['id'=>$id])->update($arr);
        clearnCache(['article.model.class'],true);
        return $data;

    }

    //新增
    public function add($val){
        $data = $this->getORM();
        $data = $data->insert($val);
        clearnCache(['article.model.class'],true);
        return $data;
    }

    //列表信息
    public function lists($val){
        $data = cache(['article.model.class.lists',json_encode($val)]);
        if (!$data){
            $data = $this->getORM()->where(['statu'=>1]);
            if ($val['id']){
                $data = $data->where(['id'=>$val['id']]);
            }
            if ($val['name']){
                $data = $data->where(['name'=>$val['name']]);
            }
            if ($val['type'] !== null){
                $data = $data->where(['type'=>$val['type']]);
            }
            if ($val['on_statu'] !== null){
                $data = $data->where(['on_statu'=>$val['on_statu']]);
            }
            $data = $data->fetchAll();
            cache(['article.model.class.lists',json_encode($val)],$data);
        }
        return $data;

    }




}