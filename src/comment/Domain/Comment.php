<?php
namespace Comment\Domain;
use Comment\Model\Comment\Num as cnum;
use Comment\Model\Comment\Record as crecord;
use Comment\Model\Comment\Text as ctext;
class Comment
{
    ///数量点评
    public function addnum($res_id,$res_type,$comment_type,$num,$uid)
    {
        $cnum=new cnum();
        if($cnum->sel($res_id,$res_type,$comment_type)!=null)
        {
         $cnum->upd($res_id,$res_type,$comment_type,$num);
        }
        else
        {
           $cnum->add($res_id, $res_type, $comment_type,$num);
        }
        clearnCache(["comment.video.view",$res_id,$res_type]);
        clearnCache(["comment.video.grade",$res_id,$res_type]);
        clearnCache(["comment.video.num",$res_id,$res_type]);
        $record=new crecord();
        $record->add($res_id, $res_type, $uid, $comment_type, $num, date('Y-m-d H:i:s',time()));
        return true;
    }
    ///文本点评
    public function addtext($res_id,$res_type,$comment_type,$value,$to_uid,$to_comment_id){
        $ctext=new ctext();
        $ctext->add($res_id, $res_type, $comment_type, $value, $to_uid, $to_comment_id);
        return true;
    }
    ///删除文本点评
    public function deltext($commit_id)
    {
      $ctext=new ctext();
      $ctext->del($commit_id);
      return true;
    }
    ///获取数量型点评信息
    public function selnum($res_id,$res_type){
        $cnum=new cnum();
        $record=new crecord();
        $num= $cnum->sel($res_id, $res_type,null);
        $re=$record->selnum($res_id, $res_type);
        for ($i=0;$i<count($num);$i++){
             for ($j=0;$j<count($re);$j++){
                if($num[$i]["thing"]==$re[$j]["comment_type"]){
                    $num[$i]["num"]=$re[$j]["num"];
                }
            }
        }
        return $num;
    }
    ///查询点评文本和数量信息
    public function seltextnum($res_id,$res_type,$page)
    {
        $text=new ctext();
        return $text->seltextnum($res_id, $res_type, $page);
    }
    ///点评排序
    public function thingdesc(){
        $cnum=new cnum();
        return $cnum->thingdesc();
    }
     ///查询数量点评是否点过
    public function sel($uid,$res_id,$res_type,$comment_type)
    {
        $record=new crecord();
        return $record->selone($uid, $res_id, $res_type, $comment_type);
    }
    ///查询数量点评是否点过
    public function selall($uid,$res_id,$res_type)
    {
        $record=new crecord();
        return $record->selall($uid, $res_id, $res_type);
    }
    ///播放收藏记录
    public function selsg($uid,$res_type,$comment_type,$page)
    {
        $record=new crecord();
        return $record->selsg($uid, $res_type, $comment_type,$page);
    }
    ///删除收藏播放记录
    public function delsg($uid,$res_id,$res_type,$comment_type)
    { 
        $record=new crecord();
        $record->delsg($uid, $res_id, $res_type, $comment_type);
        $cunm=new cnum();
        return $cunm->deupd($res_id, $res_type, $comment_type, 1);
    }
    ///热门视频
    public function rmvideo()
    {
        $cnum=new cnum();
        return $cnum->rmvideo();
    }
    ///热门剧集
    public function rmdrama(){
        $cnum=new cnum();
        return $cnum->rmdrama();
    }
    ///单个资源评分
    public function selgrade($res_id,$res_type)
    {
        $grade=0;
        $num=new cnum();
        $value=$num->bfnum($res_id, $res_type,"grade");
        if($value!=null)
        {
            $value=$value["value"];
            $record=new crecord();
            $pnum=$record->selgrade($res_id, $res_type);
            $pnum=$pnum[0]["num"];
            $grade=$value/$pnum;
        }
        return sprintf("%.1f",$grade);
    }
    ///单个资源播放次数
    public function selview($res_id, $res_type,$thing)
    {
        $num=new cnum();
        $value= $num->bfnum($res_id, $res_type,$thing);
        if($value==null){
            return 0;
        }
        return $value["value"];
    }
    ///修改ID
    public function upduid($uid,$nuid)
    {
        $re=new crecord();
        $re->upduid($uid, $nuid);
    }
}