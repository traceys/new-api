<?php
namespace Comment\Api;
use PhalApi\Api;
use Comment\Domain\Comment as Docoment;

class Comment extends Api
{
    public function getRules()
    {
        return array(
            'addnum' => array(
                'res_id' => array('name' => 'res_id','require' => true,'source' => 'get'),
                'res_type' => array('name' => 'res_type','require' => true,'source' =>'get'),
                'comment_type' => array('name' => 'comment_type','require' => true,'source' =>'get'),
                'num' => array('name' => 'num','require' => true,'source' =>'get'),
                'token' => array('name' => 'token','require' => true,'source' => 'get'),
            ),
            'addtext' => array(
                'res_id' => array('name' => 'res_id','require' => true,'source' => 'get'),
                'res_type' => array('name' => 'res_type','require' => true,'source' =>'get'),
                'comment_type' => array('name' => 'comment_type','require' => true,'source' =>'get'),
                'value' => array('name' => 'value','require' => true,'source' =>'get'),
                'to_uid' => array('name' => 'to_uid','require' => true,'source' =>'get'),
                'to_comment_id' => array('name' => 'to_comment_id','require' => true,'source' =>'get'),
                'token' => array('name' => 'token','require' => true,'source' => 'get'),
            ),
            'deltext' => array(
                'commit_id' => array('name' => 'commit_id','require' => true,'source' => 'get'),
                'token' => array('name' => 'token','require' => true,'source' => 'get'),
            ),
            'selnum' => array(
                'res_id' => array('name' => 'res_id','require' => true,'source' => 'get'),
                'res_type' => array('name' => 'res_type','source' => 'get'),
            ),
            'seltextnum' => array(
                'res_id' => array('name' => 'res_id','require' => true,'source' => 'get'),
                'token' => array('name' => 'token','require' => true,'source' => 'get'),
                'res_type' => array('name' => 'res_type','source' => 'get'),
                'page' => array('name' => 'page','source' => 'get'),
            ),
            'isselnum' => array(
                'res_id' => array('name' => 'res_id','require' => true,'source' => 'get'),
                'res_type' => array('name' => 'res_type','require' => true,'source' =>'get'),
                'comment_type' => array('name' => 'comment_type','require' => true,'source' =>'get'),
                'token' => array('name' => 'token','require' => true,'source' => 'get'),
            ),
            'selall' => array(
                'res_id' => array('name' => 'res_id','require' => true,'source' => 'get'),
                'res_type' => array('name' => 'res_type','require' => true,'source' =>'get'),
                'token' => array('name' => 'token','require' => true,'source' => 'get'),
            ),
            'selsg' => array(
                'res_type' => array('name' => 'res_type','require' => true,'source' =>'get'),
                'token' => array('name' => 'token','require' => true,'source' =>'get'),
                'comment_type' => array('name' => 'comment_type','require' => true,'source' =>'get'),
                'page' => array('name' => 'page','require' => true,'source' =>'get'),
            ),
            'selsgcount' => array(
                'res_type' => array('name' => 'res_type','require' => true,'source' =>'get'),
                'token' => array('name' => 'token','require' => true,'source' =>'get'),
                'comment_type' => array('name' => 'comment_type','require' => true,'source' =>'get'),
            ),
            'delsg' => array(
                'res_id' => array('name' => 'res_id','require' => true,'source' =>'get'),
                'token' => array('name' => 'token','require' => true,'source' =>'get'),
                'res_type' => array('name' => 'res_type','require' => true,'source' =>'get'),
                'comment_type' => array('name' => 'comment_type','require' => true,'source' =>'get'),
                
            ),
            'selgrade' => array(
                'res_id' => array('name' => 'res_id','require' => true,'source' => 'get'),
                'res_type' => array('name' => 'res_type','require' => true,'source' =>'get'),
            ),
            'selview' => array(
                'res_id' => array('name' => 'res_id','require' => true,'source' => 'get'),
                'res_type' => array('name' => 'res_type','require' => true,'source' =>'get'),
                'thing' => array('name' => 'thing','require' => true,'source' =>'get'),
            ),
            'upduid' => array(
                'uid' => array('name' => 'uid','require' => true,'source' => 'get'),
                'nuid' => array('name' => 'nuid','require' => true,'source' =>'get'),
            ),
         );
    }
    /**
     * 增加数量型点评
     */
    public function addnum()
    {
        $user=apiGet("User.User.Selecttoken",array("searchToken"=>$this->token));
        if($user!=null)
        {
           $com=new Docoment();
           return $com->addnum($this->res_id,$this->res_type,$this->comment_type,$this->num,$user["Id"]);
        }
        else
        {
            exceptions("token不存在！", -100);
        }
    }
    /**
     * 添加文本点评
     */
    public function addtext()
    {
//         $url="test.yusj.vip?s=User.User.Tokenverify&token=".$this->token."";
//         $isverify=curls($url)["data"];
//         if($isverify!=false)
//         {
            $com=new Docoment();
            return $com->addtext($this->res_id,$this->res_type,$this->comment_type,$this->value,$this->to_uid,$this->to_comment_id);
//         }
//         else
//         {
//             exceptions("token不存在！", -100);
//         }
    }
    /**
     * 删除文本点评
     */
    public function deltext()
    {
//         $url="test.yusj.vip?s=User.User.Tokenverify&token=".$this->token."";
//         $isverify=curls($url)["data"];
//         if($isverify!=false)
//         {
            $com=new Docoment();
            return $com->deltext($this->commit_id);
//         }
//         else
//         {
//             exceptions("token不存在！", -100);
//         }
    }
    /**
     * 获取数量型点评信息
     */
    public function selnum()
    {

//         $url="test.yusj.vip?s=User.User.Tokenverify&token=".$this->token."";
//         $isverify=curls($url)["data"];
//         if($isverify!=false)
//         {
            $list=cache(["comment.video.num",$this->res_id,$this->res_type]);
            if($list!=null){
                return $list;
            }
            else{
                $com=new Docoment();
                $info=$com->selnum($this->res_id,$this->res_type);
                cache(["comment.video.num",$this->res_id,$this->res_type],$info);
                return $info;
            }
//         }
//         else
//         {
//             exceptions("token不存在！", -100);
//         }
    }
    /**
     * 查询点评文本和数量
     * 
     */
    public function seltextnum()
    {
//         $url="test.yusj.vip?s=User.User.Tokenverify&token=".$this->token."";
//         $isverify=curls($url)["data"];
//         if($isverify!=false)
//         {
            $com=new Docoment();
            return $com->seltextnum($this->res_id,$this->res_type,$this->page);
//         }
//         else
//         {
//             exceptions("token不存在！", -100);
//         }
    }
    /**
     * 点评排序查询
     */
    public function thingdesc()
    {
//         $token=\PhalApi\DI()->cache->get('token_'.$this->token);
//         if($token!=null)
//         {
            $com=new Docoment();
            return $com->thingdesc();
//         }
//         else
//         {
//             exceptions("token不存在！", -100);
//         }
    }
    /**
     * 点赞是否点过
     */
    public function isselnum()
    {
        $user=apiGet("User.User.Selecttoken",array("searchToken"=>$this->token));
        if($user!=null)
        {
            $com=new Docoment();
            $info=$com->sel($user["Id"], $this->res_id, $this->res_type, $this->comment_type);
            if($info!=null)
            {
                exceptions("该用户已经点过赞！", 5);
            }
            else{
                return true;
            }
            }
        else
        {
            exceptions("token不存在！", -100);
        }
    }
    /**
     * 查询全部点赞是否点过
     */
    public function selall()
    {
        $user=apiGet("User.User.Selecttoken",array("searchToken"=>$this->token));
        if($user!=null)
        {
            $com=new Docoment();
            return $com->selall($user["Id"], $this->res_id, $this->res_type);
        }
        else
        {
            exceptions("token不存在！", -100);
        }
    }
    /**
     * 收藏观看记录
     */
    public function selsg()
    {
        $user=apiGet("User.User.Selecttoken",array("searchToken"=>$this->token));
        if($user!=null)
        {
            $com=new Docoment();
            $sg= $com->selsg($user["Id"], $this->res_type, $this->comment_type,$this->page);
           
             if($sg!=null)
             {
                $ids="";
                for ($i=0;$i<count($sg);$i++){
                    $ids.=$sg[$i]["res_id"].",";
                }
                $ids=rtrim($ids,",");
                $video=apiGet("Video.drama.video",array("id"=>$ids,"token"=>123456789));
                if(isset($video[0])==null){
                    $vilist[0]=$video;
                }
                else{
                    $vilist=$video;
                }
                $data=[];
                if($video!=null){
                    for ($i=0;$i<count($sg);$i++){
                        for($j=0;$j<count($vilist);$j++){
                            if($vilist[$j]["id"]==$sg[$i]["res_id"]){
                                $vilist[$j]["time"]=$sg[$i]["time"];
                                $data[$i]=$vilist[$j];
                            }
                        }
                    }
                }
                return $data;
             }
             else{
                exceptions("暂无记录！",12);
             }
        }
        else
        {
            exceptions("token不存在！", -100);
        }
    }
    /**
     * 收藏观看记录总条数
     */
    public function selsgcount()
    {
        $user=apiGet("User.User.Selecttoken",array("searchToken"=>$this->token));
        if($user!=null)
        {
            $com=new Docoment();
            $sg= $com->selsg($user["Id"], $this->res_type, $this->comment_type,-1);
            return count($sg);
        }
        else
        {
            exceptions("token不存在！", -100);
        }
    }
    /**
     * 删除播放收藏记录
     */
    public function delsg()
    {
         $user=apiGet("User.User.Selecttoken",array("searchToken"=>$this->token));
        if($user!=null)
        {
            $com=new Docoment();
            $sg= $com->delsg($user["Id"], $this->res_id, $this->res_type, $this->comment_type);
            return "删除成功！";
        }
        else
        {
            exceptions("token不存在！", -100);
        }
    }
    /**
     * 热门视频
     */
    public function rmvideo()
    {
        $com=new Docoment();
        return $com->rmvideo();
    }
    /**
     * 热门剧集
     */
    public function rmdrama()
    {
        $com=new Docoment();
        return $com->rmdrama();
    }
    /**
     * 单个资源评分
     */
    public function selgrade()
    {
        $gradecahe=cache(["comment.video.grade",$this->res_id, $this->res_type]);
        //\PhalApi\DI()->redis->get_forever('grade_'.$this->res_id.'');
        if($gradecahe!=null){
            return $gradecahe;
        }
        else
        {
         $com=new Docoment();
         $grade=$com->selgrade($this->res_id, $this->res_type);
         cache(["comment.video.grade",$this->res_id, $this->res_type],$grade);
         //\PhalApi\DI()->redis->set_forever('grade_'.$this->res_id.'',$grade);
         return $grade;
        }
    }
    /**
     * 单个资源播放次数
     */
    public function selview()
    {
        
        $viewcahe=cache(["comment.video.view",$this->res_id, $this->res_type]);
        //\PhalApi\DI()->redis->get_forever('view_'.$this->res_id.'');
        if($viewcahe!=null){
            return $viewcahe;
        }
        else{
            $com=new Docoment();
            $view=$com->selview($this->res_id, $this->res_type, $this->thing);
            cache(["comment.video.view",$this->res_id, $this->res_type],$view);
            //\PhalApi\DI()->redis->set_forever('view_'.$this->res_id.'',$view);
            return $view;
        }
    }
    /**
     * 修改ID
     */
    public function upduid()
    {
        $com=new Docoment();
        $com->upduid($this->uid, $this->nuid);
    }
}