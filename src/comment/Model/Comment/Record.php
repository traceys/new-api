<?php
namespace Comment\Model\Comment;
use PhalApi\Model\NotORMModel as Model;

class Record extends Model{
    
    ///添加数量点评记录
    public function add($res_id,$res_type,$uid,$comment_type,$num,$time)
    {
        $data=array(
            'res_id'=>$res_id,
            'res_type'=>$res_type,
            'uid'=>$uid,
            'comment_type'=>$comment_type,
            'num'=>$num,
            'time'=>$time,
            'statu'=>1
        );
        $this->getORM()->insert($data);
    }
    ///查询数量点评是否点过
    public function selone($uid,$res_id,$res_type,$comment_type)
    {
       return  $this->getORM()->where(["uid"=>$uid,'res_id'=>$res_id,'res_type'=>$res_type,'comment_type'=>$comment_type,'statu'=>1])->fetchOne();
    }
    ///播放收藏记录
    public function selsg($uid,$res_type,$comment_type,$page)
    {
        $sql="select res_id,res_type,uid,comment_type,num,max(time) as time,statu from api_comment_record where uid=".$uid." and res_type='".$res_type."' and comment_type='".$comment_type."' and statu=1  group by res_id order by time DESC";
        if($page>0){
            $pre= ($page-1)*20;
            $next=20;
            $sql.=" limit ".$pre.",".$next."";
        }
        return  $this->getORM()->queryAll($sql);
    }
    ///删除收藏播放记录
    public function delsg($uid,$res_id,$res_type,$comment_type)
    {
       return  $this->getORM()->where(["uid"=>$uid,"res_id"=>$res_id,'res_type'=>$res_type,'comment_type'=>$comment_type])->update(["statu"=>-1]);
    }
    ///查询数量点评是否点过
    public function selall($uid,$res_id,$res_type)
    {
        return  $this->getORM()->where(["uid"=>$uid,'res_id'=>$res_id,'res_type'=>$res_type,'statu'=>1])->group("comment_type")->fetchAll();
    }
    ///查询总人数
    public function selnum($res_id,$res_type)
    {
        $sql="select count(*) as num,comment_type from api_comment_record where res_id=".$res_id." and res_type='".$res_type."' and statu=1 group by comment_type";
        return  $this->getORM()->queryAll($sql);
    }
    ///查询评分总人数
    public function selgrade($res_id,$res_type)
    {
        $sql="select count(*) as num from api_comment_record where res_id=".$res_id." and res_type='".$res_type."' and statu=1 and comment_type='grade'";
        return  $this->getORM()->queryAll($sql);
    }
    ///修改ID
    public function upduid($uid,$nuid){
        $this->getORM()->where(["uid"=>$uid])->update(["uid"=>$nuid]);
    }
}