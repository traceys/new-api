<?php
namespace Comment\Model\Comment;
use PhalApi\Model\NotORMModel as Model;

class Num extends Model
{
    ///添加数据
    public function add($res_id,$res_type,$thing,$num)
    {
        $data=array(
            'res_id'=>$res_id,
            'res_type'=>$res_type,
            'thing'=>$thing,
            'value'=>$num,
            'statu'=>1
        );    
        $this->getORM()->insert($data);
    }
    ///查询
    public function sel($res_id,$res_type,$thing)
    {
        $sql="select * from api_comment_num where res_id=".$res_id." and statu=1";
        if($res_type!=null){
            $sql.=" and res_type='".$res_type."'";
        }
        if($thing!=null){
            $sql.=" and thing='".$thing."'";
        }
        return  $this->getORM()->queryAll($sql);
    }
    ///修改
    public function upd($res_id,$res_type,$thing,$value)
    {
      return  $this->getORM()->where(['res_id'=>$res_id,'res_type'=>$res_type,'thing'=>$thing])->update(array('value' => new \NotORM_Literal("value + ".$value."")));   
    }
    ///删除
    public function deupd($res_id,$res_type,$thing,$value)
    {
        return  $this->getORM()->where(['res_id'=>$res_id,'res_type'=>$res_type,'thing'=>$thing])->update(array('value' => new \NotORM_Literal("value - ".$value."")));
    }
    ///点评排序
    public function thingdesc()
    {
        return $this->getORM()->select('res_id')->where(["res_type"=>'video','thing'=>'play_num','statu'=>1])->order('value DESC')->fetchAll();
    }
   ///热门视频
   public function rmvideo(){
       $sql="select res_id from api_comment_num where res_type='video' and thing='view' and statu=1 order by value DESC limit 0,20";
       return  $this->getORM()->queryAll($sql);
   }
   ///热门剧集
   public function rmdrama(){
       $sql="select res_id from api_comment_num where res_type='drama' and thing='view' and statu=1 order by value DESC limit 0,20";
       return  $this->getORM()->queryAll($sql);
   }
   ///资源次数
   public function bfnum($res_id,$res_type,$thing)
   {
       return  $this->getORM()->select("value")->where(['res_id'=>$res_id,'res_type'=>$res_type,'thing'=>$thing,'statu'=>1])->fetchOne();
   }
}