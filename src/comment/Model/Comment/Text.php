<?php
namespace Comment\Model\Comment;
use PhalApi\Model\NotORMModel as Model;

class Text extends Model{
    
    ///添加数据
    public function add($res_id,$res_type,$comment_type,$value,$to_uid,$to_comment_id)
    {
        $data=array(
          'to_uid'=>$to_uid,
          'to_comment_id'=>$to_comment_id,
          'res_id'=>$res_id,
          'res_type'=>$res_type,
          'value'=>$value,
          'comment_type'=>$comment_type,
          'statu'=>1,
           'time'=>date('Y-m-d H:i:s',time()) 
        );
        $this->getORM()->insert($data);
    }
    ///删除文本点评
    public function del($commit_id){
        $this->getORM()->where(["id"=>$commit_id])->update(array("statu"=>-1));
    }
    ///查询文本和数据点赞
    public function seltextnum($res_id,$res_type,$page)
    {
        $sql="select t.id as tid,t.to_uid as tto_uid,t.to_comment_id as tto_comment_id,t.res_id as tres_id,t.res_type as tres_type,t.value as tvalue,t.comment_type as tcommnet_type,t.time as ttime,t.statu as tstatu,n.id as nid,n.res_id as nres_id,n.res_type as nres_type,sum(n.value) as nvalue,n.statu as nstatu from api_comment_text as t left join api_comment_num as n on t.id=n.res_id group by t.res_id having t.res_id=".$res_id."";
        if($res_type!=null){
         $sql.="  and t.res_type=".$res_type."";
        }
        if($page>0){
           $pre= ($page-1)*40;
           $next=40;
           $sql.=" limit ".$pre.",".$next."";
        }
        return  $this->getORM()->queryAll($sql);
    }
}