<?php
namespace Currency\Api;
use PhalApi\Api;
use Currency;
use Currency\Domain\Controller as Controller;
use PhalApi\Exception\BadRequestException;
class Receive extends Api{


    private static $data = null;

    public function __construct()
    {
        self::getDomainAdd();
        self::getToken();
    }

    //实例化Domin层
    static  private  function getDomainAdd(){
        if (self::$data === null){
            self::$data = new  Controller();
        }

    }
    //得到token值
    static private  function getToken(){


        $token = isset($_GET['token'])?$_GET['token']:'';

//        if (!$token){
//            throw new BadRequestException('缺少必要参数token',4);
//        }

        if (!Currency\getCache($token)){
            $res = apiGet('User.user.selecttoken',array('searchToken'=>$_GET['token']));;
            if ($res ){
                Currency\setCache($token,$res);
            }
        }

    }



    public function getRules() {
        return array(
//        '*' => array(
//                'token' => array('name' => 'token','source'=>'get','require'=>true,'desc' => '用户身份token'),
//            ),
            'reduce' => array(
                'uid' 	=> array('name' => 'uid','require'=>true, 'desc' => '用户ID'),
                'money_type'=> array('name' => 'money_type','require'=>true, 'desc' => '货币类型'),
                'num'=> array('name' => 'num','require'=>true, 'desc' => '货币数量')
            ),
            'dealcurrency' => array(
                'uid' 	=> array('name' => 'uid','require'=>true, 'desc' => '用户ID'),
                'money_type'=> array('name' => 'money_type','require'=>true, 'desc' => '货币类型'),
                'num'=> array('name' => 'num','require'=>true, 'desc' => '货币数量')
            ),
            'deposit' => array(
                'money_type'=> array('name' => 'money_type','require'=>true, 'desc' => '货币类型'),
                'num'=> array('name' => 'num','require'=>true, 'desc' => '货币数量'),
                'card_id'=> array('name' => 'card_id','require'=>true, 'desc' => '卡号ID'),
            ),
            'appenddis' => array(
                'id'=> array('name' => 'id','default'=>false, 'desc' => 'Id'),
                'uid'=> array('name' => 'uid','default'=>null, 'desc' => '用户ID'),
                'type'=> array('name' => 'type','default'=>null, 'desc' => '优惠码类型'),
                'item_id'=> array('name' => 'item_id','default'=>null, 'desc' => '商品ID'),
                'reduce_num'=> array('name' => 'reduce_num','default'=>null, 'desc' => '折扣数量'),
                'had_num'=> array('name' => 'had_num','default'=>null, 'desc' => '有效次数'),
                'time'=> array('name' => 'time','default'=>null, 'desc' => '有效时间'),
                'astype'=> array('name' => 'astype','default'=>null, 'desc' => '优惠券别名分类'),
                'startTime'=> array('name' => 'startTime','default'=>null, 'desc' => '生成时间'),
                'on_status'=> array('name' => 'on_status','default'=>null, 'desc' => '优惠码状态'),
            ),
            'deletedis' => array(
                'code'=> array('name' => 'code','require'=>true, 'desc' => '优惠码'),
            ),
            'appendcodenum' => array(
                'code'=> array('name' => 'code','require'=>true, 'desc' => '优惠码'),
                'num'=> array('name' => 'num','require'=>true, 'desc' => '有效次数'),
            ),
            'reducecodenum' => array(
                'code'=> array('name' => 'code','require'=>true, 'desc' => '优惠码'),
                'num'=> array('name' => 'num','require'=>true, 'desc' => '有效次数'),
            ),
            'findcode' => array(
                'code'=> array('name' => 'code','require'=>true, 'desc' => '优惠码'),
                'on_status'=> array('name' => 'on_status','default'=>1, 'desc' => '状态'),
            ),
            'appendcur' => array(
                'type'=> array('name' => 'type','require'=>true, 'desc' => '货币类型'),
                'name'=> array('name' => 'name','require'=>true, 'desc' => '名称'),
                'to_one'=> array('name' => 'to_one','require'=>true, 'desc' => '货币的兑换比例'),
            ),
            'monmoney' => array(
                'id'=> array('name' => 'id','require'=>true, 'desc' => '货币ID'),
                'on_status'=> array('name' => 'on_status','require'=>true, 'desc' => '状态'),
            ),
            'transactionRecord' => array(
                'act'=> array('name' => 'act','default'=>'', 'desc' => '类型'),
                'page'=> array('name' => 'page','default'=>1, 'desc' => '分页数'),
            ),
            'addcursd' => array(
                'uid'=> array('name' => 'uid','require'=>true, 'desc' => '用户ID'),
                'money_type'=> array('name' => 'money_type','require'=>true, 'desc' => '货币类型'),
                'num'=> array('name' => 'num','require'=>true, 'desc' => '货币数量'),
                'type'=> array('name' => 'type','default'=>'add', 'desc' => '货币类型'),
            ),
            'ceroreders' => array(
                'commodity_type'=> array('name' => 'commodity_type','require'=>true, 'desc' => '商品类型'),
                'commodity_id'=> array('name' => 'commodity_id','require'=>true, 'desc' => '商品ID'),
                'num'=> array('name' => 'num','require'=>true, 'desc' => '商品数量'),
                'code'=> array('name' => 'num','default'=>'', 'desc' => '优惠码'),
            ),
            'sponsor' => array(
                'oid'=> array('name' => 'oid','require'=>true, 'desc' => '订单ID'),
                'type'=> array('name' => 'type','require'=>true, 'desc' => '支付类型'),
                'agument'=> array('name' => 'agument','require'=>true, 'desc' => '支付预加参数'),
            ),
            'notifycurrencys' => array(
                'data'=> array('name' => 'data','require'=>true, 'desc' => '返回主要信息'),
            ),
            'orderfindmess' => array(
                'oid'=> array('name' => 'oid','require'=>true, 'desc' => '订单ID'),
            ),
            'overorderon_sta' => array(
                'oid'=> array('name' => 'oid','require'=>true, 'desc' => '订单ID'),
                'order_status'=> array('name' => 'order_status','require'=>true, 'desc' => '订单状态'),
                'statu'=> array('name' => 'statu','require'=>true, 'desc' => '订单状态'),
            ),
            'orderall' => array(
                'page'=> array('name' => 'page','require'=>true, 'desc' => '当前页数'),
                'select'=> array('name' => 'select','require'=>true, 'desc' => '查询信息，json数据'),
                'order'=> array('name' => 'order','require'=>true, 'desc' => '排序值'),
                'order_aes'=> array('name' => 'order_aes','require'=>true, 'desc' => '默认正序还是倒序'),
            ),
            'finddiscounts' => array(
                'type'=> array('name' => 'type','require'=>true, 'desc' => '折扣类型'),
                'uid'=> array('name' => 'uid','default'=>'', 'desc' => '折扣类型'),
            ),
            'alldis' => array(
                'type'=> array('name' => 'type','default'=>null, 'desc' => '折扣类型'),
                'page'=> array('name' => 'page','require'=>true, 'desc' => '当前页数'),
                'startTime'=> array('name' => 'startTime','default'=>null, 'desc' => '开始时间'),
                'time'=> array('name' => 'time','default'=>null, 'desc' => '结束时间'),
                'on_status'=> array('name' => 'on_status','default'=>null, 'desc' => '状态'),
                'key'=> array('name' => 'key','default'=>null, 'desc' => 'key'),
                'value'=> array('name' => 'value','default'=>null, 'desc' => 'value'),
            ),
            'depositList' => array(
                'id'=> array('name' => 'id','default'=>null, 'desc' => 'id'),
                'uid'=> array('name' => 'uid','default'=>null, 'desc' => '用户ID'),
                'act'=> array('name' => 'act','default'=>null, 'desc' => '当前页数'),
                'page'=> array('name' => 'page','default'=>1, 'desc' => '当前页数'),
                'money_type'=> array('name' => 'money_type','default'=>null, 'desc' => '货币类型'),
                'starttime'=> array('name' => 'starttime','default'=>'', 'desc' => '开始时间'),
                'endtime'=> array('name' => 'endtime','default'=>'', 'desc' => '结束时间'),
                'sort'=> array('name' => 'sort','default'=>'1', 'desc' => '排序方式'),
                'pay_status'=> array('name' => 'pay_status','default'=>'false', 'desc' => '状态'),
                'handler'=> array('name' => 'handler','default'=>false, 'desc' => '处理人'),
                'number'=> array('name' => 'number','default'=>40, 'desc' => '每页多少条'),
                'on_status'=> array('name' => 'on_status','default'=>1, 'desc' => '数据状态'),
            ),
            'createCard' => array(
                'uid'=> array('name' => 'uid','require'=>true, 'desc' => '用户ID'),
                'card'=> array('name' => 'card','require'=>true, 'desc' => '卡号'),
                'user'=> array('name' => 'user','require'=>true, 'desc' => '用户'),
                'url'=> array('name' => 'url','default'=>'', 'desc' => '图片地址'),
                'subbranch'=> array('name' => 'subbranch','require'=>true, 'desc' => '支行'),
                'bankDeposit'=> array('name' => 'bankDeposit','require'=>true, 'desc' => '开户行'),
            ),
            'delebanca' => array(
                'id'=> array('name' => 'id','require'=>true, 'desc' => '用户ID'),
            ),
            'dispose' => array(
                'handler'=> array('name' => 'handler','require'=>true, 'desc' => '处理人'),
                'pay_status'=> array('name' => 'pay_status','require'=>true, 'desc' => '处理结果'),
                'id'=> array('name' => 'id','require'=>true, 'desc' => '处理ID'),
                'imgs'=> array('name' => 'imgs','default'=>'', 'desc' => 'json'),
                'body'=> array('name' => 'body','default'=>'', 'desc' => '详情'),
            ),
            'findUidPro' => array(
                'type'=> array('name' => 'type','default'=>'', 'desc' => '处理结果'),
                'uid'=> array('name' => 'uid','default'=>'', 'desc' => '处理ID'),
            ),
            'disstoptimme' => array(
                'code'=> array('name' => 'code','require'=>true, 'desc' => '优惠码'),
                'time'=> array('name' => 'time','require'=>true, 'desc' => '截止日期'),
            ),
            'mycard' => array(
                'uid'=> array('name' => 'uid','default'=>'', 'desc' => '处理ID'),
            ),
            'findcurrency' => array(
                'uid'=> array('name' => 'uid','default'=>'', 'desc' => '用户ID'),
                'money_type'=> array('name' => 'money_type','default'=>'', 'desc' => '类型'),
            ),
            'overdueTodata' => array(
                'page'=> array('name' => 'page','default'=>1, 'desc' => '当前页数'),
                'num'=> array('name' => 'num','default'=>40, 'desc' => '默认展示数量'),
            ),
        );
    }

    //减少货币
    public function reduce(){
        $arr = [
            'uid'=>$this->uid,
            'money_type'=>$this->money_type,
            'num'=>$this->num
        ];

        return self::$data->reduceCurrency($arr);
    }
    //转账用户接口
    public function dealcurrency(){
        $arr = [
            'uid'=>$this->uid,
            'money_type'=>$this->money_type,
            'num'=>$this->num
        ];


        return self::$data->accounts($arr);

    }
    //用户提现接口
    public function deposit(){

        $arr = [
            'money_type'=>$this->money_type,
            'num'=>$this->num,
            'card_id'=>$this->card_id,

        ];

        return self::$data->withdrawDeposit($arr);
    }


    //增加修改优惠码
    public function appenddis(){
        $arr = [
            'id'=>$this->id,
            'uid'=> $this->uid,
            'type'=> $this->type,
            'item_id'=>$this->item_id,
            'reduce_num'=>$this->reduce_num,
            'had_num'=>$this->had_num,
            'time'=>$this->time,
            'astype'=>$this->astype,
            'startTime'=>$this->startTime,
            'on_status'=>$this->on_status
        ];
        return self::$data->appendD($arr);

    }

    //删除指定优惠码
    public function deletedis(){


        return self::$data->upCode($this->code);

    }
    //增加优惠优惠码次数
    public function  appendcodenum(){

        $arr = [
            'code'=>$this->code,
            'num'=>$this->num
        ];
        return self::$data->upCode($arr,'update');
    }
    //修改指定优惠码截止时期
    public function disstoptimme(){
        $arr = [
            'code'=>$this->code,
            'time'=>$this->time
        ];
        return self::$data->sotoptime($arr);

    }

    //减少优惠优惠码次数
    public function  reducecodenum(){

        $arr = [
            'code'=>$this->code,
            'num'=>$this->num
        ];
        return self::$data->upCode($arr,'update','-');
    }

//查询优惠码信息
public function findcode(){

    return self::$data->findCodes($this->code,$this->on_status);

}

//查询指定用户指定码
public function findUidPro(){

        if ($this->uid){
            $uid =$this->uid;
        }else{
//            $uid =  $_GET['token'];
            $uid = apiGet('User.user.selecttoken',array('searchToken'=>$_GET['token']))['Id'];
        }

    return self::$data->Promotion($uid,$this->type);

}

//创建货币类型
public function  appendcur(){

    $arr = [
        'money_type'=>$this->type,
        'desc'=>$this->name,
        'to_one'=>$this->to_one
    ];
    return self::$data->appendList($arr);

}
//管理货币
public function monmoney(){

    $arr = [
        'id'=>$this->id,
        'on_status'=>$this->on_status
    ];

    return self::$data->mldmone($arr);

}
//个人货币信息
public function findcurrency(){



    return self::$data->findCuren($this->money_type,$this->uid);


}
//交易记录
public function transactionRecord(){

    $arr = [
        'act'=>$this->act,
        'page'=>$this->page
    ];
    return self::$data->dealType($arr);

}
//添加货币
public function addcursd(){
    $arr = [
        'uid'=>$this->uid,
        'money_type'=>$this->money_type,
        'num'=>$this->num,
        'type'=>$this->type
    ];
    return self::$data->appendencs($arr);

}

//提现列表
public  function depositList(){
    $arr = [
        'id'=>$this->id,
        'act'=>$this->act,
        'uid'=>$this->uid,
        'money_type'=>$this->money_type,
        'page'=>$this->page,
        'sort'=>$this->sort,
        'starttime'=>$this->starttime,
        'endtime'=>$this->endtime,
        'pay_status'=>$this->pay_status,
        'handler'=>$this->handler,
        'number'=>$this->number,
        'on_status'=>$this->on_status
    ];

    return self::$data->dealList($arr);

}
//提现处理
public function dispose(){

            $arr = [
                'id'=>$this->id,
                'pay_status'=>$this->pay_status,
                'handler'=>$this->handler,
                'imgs'=>$this->imgs,
                'body'=>$this->body
            ];

    return self::$data->dealdispose($arr);

}

//查询当前用户使用优惠码详情
public function finddiscounts(){
        $arr = [
            'type'=>$this->type,
            'uid'=>$this->uid
        ];
    return self::$data->discount($arr);

}

//查询所有优惠码使用记录
public function alldis(){
    $arr = [
        'type'=>$this->type,
        'page'=>$this->page,
        'startTime'=>$this->startTime,
        'time'=>$this->time,
        'key'=>$this->key,
        'value'=>$this->value,
        'on_status'=>$this->on_status
    ];

return self::$data->alldiscount($arr);

}
//开通银行卡号
public function createCard(){

    $arr = [
        'uid'=>$this->uid,
        'card'=>$this->card,
        'user'=>$this->user,
        'url'=>$this->url,
        'bankDeposit'=>$this->bankDeposit,
        'subbranch'=>$this->subbranch
    ];
    return self::$data->bingBank($arr);

}
//删除卡号
public  function delebanca(){

    return self::$data->delecar($this->id);

}
//展示指定用户的银行卡
public function mycard(){

    if ($this->uid){
        $uid = $this->uid;
    }else{
        $uid = Currency\getCache($_GET['token'])['Id'];

    }
    return self::$data->mysca($uid);

}

//优惠码数据统计
public function disAll(){

    return self::$data->allDis();

}
//优惠劵过期处理
public function overdue(){

    return self::$data->overout();

}
//优惠券过期历史
public function overdueTodata(){

    return self::$data->todata($this->page,$this->num);

}





}