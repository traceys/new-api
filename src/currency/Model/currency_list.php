<?php
namespace Currency\Model;

use PhalApi\Model\NotORMModel as NotORM;

class currency_list extends NotORM {



    //添加
    public function inser($arr){
        $model = $this->getORM();
       return $model->insert($arr);
    }

    //查询货币
    public function find($id){
        $model = $this->getORM();
        return $model->where(['id'=>$id])->fetchOne();

    }
    //将指定货币状态进行改变
    public function upStatu($id,$type){
        $model = $this->getORM();
      return $model->where('id',$id)->update(['on_statu'=>$type]);
    }
    //查找指定类型的货币
    public function findmoney_type($money_id){
        $model = $this->getORM();
        return $model->where(['id'=>$money_id,'statu'=>1])->fetchOne();
    }

}

