<?php
namespace Currency\Model;

use PhalApi\Model\NotORMModel as NotORM;

class currency_deal extends NotORM {


    //货币减少进入到指定状态
    public  function  subtractres($val){

        $model = $this->getORM();
        return $model->insert($val);

    }

    //查询指定贸易信息
    public function findu($uid,$act,$page){
        $model = $this->getORM();

        $data = $model->where(['statu'=>1]);

        if ($uid >0){
            $data = $data->where(['uid'=>$uid]);
        }
        if ($act){
            $data = $data->where(['act'=>$act]);
        }
        $num = 10;
        $page = ($page-1)*$num;

       return $data->limit($page,$num)->fetchAll();

    }
    //提现列表
    public function rlds($arr,$type = '-1',$newtype='data'){
        $model = $this->getORM();
        $data = $model->where(['statu'=>1]);
        if ($arr['uid']){
            $data = $data->where(['uid'=>$arr['uid']]);
        }
        if ($arr['on_status']){
            $data = $data->where(['on_status'=>$arr['on_status']]);
        }
        if ($arr['id']){
            $data = $data->where(['id'=>$arr['id']]);
        }
        //状态
        if ($arr['act']){
            $data = $data->where(['act'=>$arr['act']]);
        }
        //如果提现的话
        if ($type == '-1'){
            $data = $data->where(['to_uid'=>'-1']);
        }
        if ($arr['money_type']){
            $data = $data->where(['money_type'=>$arr['money_type']]);
        }
        if ($arr['pay_status'] !== 'false'){
            $data = $data->where(['pay_status'=>$arr['pay_status']]);
        }
        //时间
        if ($arr['starttime']){
            $data = $data->where('start_time > ?',$arr['starttime']);
        }
        //时间
        if ($arr['endtime']){
            $data = $data->where('start_time < ?',$arr['endtime']);
        }
        //处理人
        if ($arr['handler']){
            $data = $data->where(['handler'=>$arr['handler']]);
        }
        if ($arr['sort']){
            $data = $data->order('id DESC');
        }
        if ($newtype == 'data'){
            $num = $arr['number'];
            $page = ($arr['page']-1)*$num;
            $data = $data->limit($page,$num)->fetchAll();
        }
        if ($newtype == 'num'){
            $data = $data->count('id');
        }

        return $data;

    }

//查询指定用户的信息
    public function findId($id){
        $model = $this->getORM();
        return $model->where(['id'=>$id,'statu'=>1])->fetchOne();

    }
//修改指定信息
public function updateDeal($val,$id){
    $model = $this->getORM();
    return $model->where(['id'=>$id])->update($val);
}
}

