<?php
namespace Currency\Model;

use PhalApi\Model\NotORMModel as NotORM;

class currency_bank extends NotORM
{
    /**
     * bing the specified user's bank card
     */
    public function  create($arr){

        $model = $this->getORM();

        return $model->insert($arr);

    }
     /**
      * to the find card number
      */
    public function findcard($card){

        $model = $this->getORM();

        return $model->where(['card'=>$card,'statu'=>1])->fetchOne();

    }


    /**
     * delete the user's bank card
     */
    public function deleteCard($id){
        $model = $this->getORM();
        return $model->where(['id'=>$id])->update(['statu'=>-1]);

    }


    /**
     * to the find card id
     */
    public function findId($id){

        $model = $this->getORM();

        return $model->where(['id'=>$id,'statu'=>1])->fetchOne();

    }

    /**
    *find uid data
     */
    public function findUid($uid){
        $model = $this->getORM();
        return $model->where(['uid'=>$uid,'statu'=>1])->fetchAll();


    }

}