<?php
namespace Currency\Model;

use PhalApi\Model\NotORMModel as NotORM;

class currency_currency extends NotORM {

        //减少指定用户的货币
        public function rezyu($val,$num){

            $model = $this->getORM();

          return  $model->where($val)->update(array('money_num' => new \NotORM_Literal("money_num - $num")));

        }

        //检查指定用户的货币系统
        public function  find($val){

            $model = $this->getORM();

            return  $model->where($val)->fetchOne();

        }
        //增加指定用户货币

    public function appendTO($val,$num){

        $model = $this->getORM();

        return  $model->where($val)->update(array('money_num' => new \NotORM_Literal("money_num + $num")));

    }
    //将所有用户的货币转换为唯一币
    /**

     * type 是什么类型转换
     * num  是基本币转指定币的比例
     * newtype 是要转换的货币
     *
     */
    public function transition($type,$num,$newtype=1){
        $model = $this->getORM();
        $arr = $this->findalld($type);
        //是否为空
        if (!$arr){
            return $arr;
        }
        //梳理用户的新货币信息
        foreach ($arr as $key =>$value){
          $a = $model->where(['id'=>$value['id'],'money_type'=>$newtype,'statu'=>1])->fetchOne();
            if (!$a){
                $arr = [
                    'uid'=>$value['uid'],
                    'money_type'=>$newtype,
                    'money_num'=>0
                ];
                $model->insert($arr);
            }

        }


        //用户货币转入
        foreach ($arr as $key =>$value){
            $mon = $value["money_num"];
            //修改用户原有数量
            $model->where(['id'=>$value['id'],'money_type'=>$newtype,'statu'=>1])->update(['money_num'=>new \NotORM_Literal("money_num +".$mon*$num)]);
            //删除用户员货币状态
          $model->where(['id'=>$value['id'],'money_type'=>$type,'statu'=>1])->update(['on-statu'=>-1]);


        }

        return true;

    }

    //查询拥有指定货币的人
    public function findalld($val){
        $model = $this->getORM();
      return $model->where(['money_type'=>$val,'statu'=>1])->fetchAll();



    }
    //查询个人货币信息
    public function finduid($type,$id){
        $model = $this->getORM();
        $data =  $model->where(['uid'=>$id,'statu'=>1]);

        if ($type){
            $data = $data->where('money_type',$type);
        }
        return  $data = $data->fetchOne();

    }
    //增加指定用户的指定货币
    public function addcur($uid,$type,$num,$types='add'){
        $model = $this->getORM();
        $data = null;
        if ($types == 'add'){
            $data =  $model->where(['uid'=>$uid,'money_type'=>$type])->update(['money_num'=>new \NotORM_Literal("money_num + $num")]);

            if (!$data){
                $data = $model->insert(['uid'=>$uid,'money_type'=>$type,'money_num'=>$num]);

            }
        }
        if ($types == 'update'){

            $data =  $model->where(['uid'=>$uid,'money_type'=>$type])->update(['money_num'=>$num]);
        }


        return $data;
    }

    //推广码金额导入
    public function excodemoney(){
        $model = $this->getORM();
        $sql = "SELECT * FROM api_order_order where originalPrice>0 && pay_status=1 && code_type='PromotionCode' ORDER BY end_time ASC LIMIT 8";
        $data = $model->queryAll($sql,array());
       return $data;


    }



}

