<?php
namespace Currency\Model;

use PhalApi\Model\NotORMModel as NotORM;

class currency_discounts extends NotORM {



    //添加指定优惠码
    public function  add($data){
        $model = $this->getORM();

       return $model->insert($data);

    }
    //修改指定优惠码信息
    public function updateCode($code,$val){
        $model = $this->getORM();

        return $model->where(['`key`'=>$code])->update($val);

    }
    //修改优惠码可使用次数
    public function upNum($val,$num,$calculate){
        $model = $this->getORM();

        return  $model->where(['`key`'=>$val])->update(array('had_num' => new \NotORM_Literal("had_num $calculate $num")));


    }
    //查询优惠码信息
    public function find($code,$on_status=1){
        $model = $this->getORM();

        $data =  $model->where(['`key`'=>$code]);
            if($data !== 'null'){
            $data =$data->where(['on_status'=>$on_status]);
            }
            $data = $data->fetchOne();
           return $data;

    }
    //查询指定用户优惠码详情
    public function finuser($uid,$type){
        $model = $this->getORM();
        return $model->where(['uid'=>$uid,'type'=>$type,'statu'=>1])->fetchAll();
    }

    //  查询优惠码所有次数
    public function alls($arr,$page,$num='40',$types = 'data'){
        $model = $this->getORM();
        $type = $arr['type'];
        $data = $model->where(['statu'=>1])->where('id >= ?',51784);
        if ($type){
            $data = $data->where(['type'=>$type]);
        }
        if ($arr['startTime']){
            $startTime = $arr['startTime'];
            $time = $arr['time'];

            $data = $data->where('startTime > ?',$startTime)->where('time < ?',$time);

        }
        if ($arr['on_status']){
            $on_status = $arr['on_status'];
            $data = $data->where(['on_status'=>$on_status]);
        }
        if ($arr['key']){
            $key = $arr['key'];
            $value = $arr['value'];
            $data = $data->where("`$key` like ?","%$value%");
        }
        if ($types == 'data'){
            $data = $data->limit($page,$num)->order('id DESC')->fetchAll();
        }
        if ($types == 'num'){
            $data = $data->count('id');
        }
        return $data;
    }

    //查询指定用户的优惠券
    public function findpro($uid,$type){
        $model = $this->getORM();
        return $model->where(['uid'=>$uid,'type'=>'PromotionCode'])->fetchAll();

    }

    //查询所有用户的优惠券
    public function findallUser($val,$type){
        $model = $this->getORM();
        $sql = "SELECT * FROM api_currency_discounts WHERE `type`='$type'  && `uid` IN ($val)";
        $arr = $model->queryAll($sql,array());
        return $arr;
    }
    //修改指定用户数据
    public function updis($id,$data){
        $model = $this->getORM();
        return $model->where(['id'=>$id])->update($data);


    }
    //查询所有优惠码数据
    public function findalldis(){
        $model = $this->getORM();
        return $model->where(['statu'=>1])->fetchAll();

    }
    //查询所有过期优惠码
    public function overouts(){
        $time = date("Y-m-d H:i:s",time());
        $model = $this->getORM();
        $data = $model->where('time < ?',$time)->where(['statu=>1','on_status'=>1,'type'=>'discounts'])->fetchAll();
        return $data;
    }
    //查询过期优惠券信息
    public function outdiscounts($page,$num,$type='data'){
        $model = $this->getORM();
        $data = $model->where('id < ?',51784)->where(['type'=>'discounts']);
        if ($type == 'data'){
            $data = $data->limit(($page-1)*$num,$num)->fetchAll();
        }
        if ($type == 'num'){
            $data = $data->count('id');
        }
        return $data;
    }
}

