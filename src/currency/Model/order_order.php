<?php
namespace Currency\Model;

use PhalApi\Model\NotORMModel as NotORM;

class order_order extends NotORM {


    //创建订单
    public function cert($val){
        $model = $this->getORM();
       return $model->insert($val);

    }
    //查询指定订单信息
    public  function find($id){
        $model = $this->getORM();
        return $model->where(['id'=>$id])->fetchOne();

    }

    //修改订单
    public function overOrder($id,$val){
        $model = $this->getORM();

        return $model->where(['id'=>$id])->update($val);


    }

//展示所有订单
public function findall($val){
    $model = $this->getORM();

    $data = $model->where($val['select']);

    $act = $val['order_aes'];
    if ($val['order_aes']){
        $data = $data->order("$act ASC");
    }else{
        $data = $data->order("$act DESC");
    }
    $data = $data->limit($val['page'],20);

return $data;

}

//展示所有码的订单
public function findalldisorder(){
    $model = $this->getORM();
    return $model->where('money_code IS NOT ?', null)->where(['order_status'=>200,'statu'=>1])->group('money_code')->fetchAll();
}


}