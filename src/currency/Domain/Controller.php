<?php
namespace Currency\Domain;
use Currency\Model\currency_deal;
use Currency\Model\currency_currency;
use Currency\Model\currency_discounts;
use Currency\Model\currency_list;
use Currency\Model\currency_toup;
use Currency\Model\order_order;
use Currency\Model\commodity_commodity;
use Currency\Model\order_log;
use Currency\Model\commodity_record;
use PhalApi\Exception\BadRequestException;
use Currency;
use Currency\Model\currency_bank;
class Controller {


    //删除用户货币
    public function  reduceCurrency($val){
        checkAuth();
        $deal = new currency_deal();

        $currency = new currency_currency();

        //减少金额
        $arr = [
            'uid'=>$val['uid'],
            'money_type'=>$val['money_type']
        ];

        $a =  $currency->find($arr);

        if ($a['statu'] == '-1'){
            throw new BadRequestException('用户货币失效不可减少',3);
        }


       $num = $a['money_num'];
        //判断用户货币是否足够
        if ($num < $val['num']){
            throw new BadRequestException('用户货币不足',1);
        }

        $dealarr = [
            'uid'=>$val['uid'],
            'to_uid'=>-1,
            'money_type'=>$val['money_type'],
            'money_num'=>$val['num'],
            'act'=>-1,
            'start_time'=>Currency\timeout(time()),
            'end_time'=>Currency\timeout(time()+1),
            'pay_status'=>1
        ];
        //记录状态
        $state = $deal->subtractres($dealarr);

        if ($state){


       $res =  $currency->rezyu($arr,$val['num']);

        return $res;

        }

    }


    //转账货币
    public function  accounts($val){

        $deal = new currency_deal();

        $currency = new currency_currency();

        //减少金额
        $arr = [
            'uid'=>Currency\getCache($_GET['token'])['Id'],
            'money_type'=>$val['money_type']
        ];


        $a =  $currency->find($arr);

        if ($a['statu'] == '-1'){
            throw new BadRequestException('用户货币失效不可减少',3);
        }


        $num = $a['money_num'];
        //判断用户货币是否足够
        if ($num < $val['num']){
            throw new BadRequestException('用户货币不足',4);
        }
        //转账数据
        $dealarr = [
            'uid'=> Currency\getCache($_GET['token'])['Id'],
            'to_uid'=>$val['uid'],
            'money_type'=>$val['money_type'],
            'money_num'=>$val['num'],
            'act'=>1,
            'start_time'=>Currency\timeout(time()),
            'end_time'=>Currency\timeout(time()+1),
            'pay_status'=>1
        ];

        //记录状态
        $state = $deal->subtractres($dealarr);


        if ($state){

                //减少该用户货币
              $currency->rezyu($arr,$val['num']);
                //增加指定用户货币
            $arr['uid'] = $val['uid'];

            $res = $currency->appendTO($arr,$val['num']);

            return $res;


    }

    }
    //用户提现
    public  function  withdrawDeposit($val){

        $deal = new currency_deal();
        $currency = new currency_currency();

        $uid = Currency\getCache($_GET['token'])['Id'];

        $arr = [
            'uid'=>$uid,
            'money_type'=>$val['money_type']
        ];
        //货币计算
        $sumnum =  $currency->find($arr)['money_num'];
        if ($sumnum < $val['num']){
            throw  new BadRequestException('货币不足,无法体现',3);
        }
        $dealarr = [
            'id'=>currency\createdeal_id(),
            'uid'=>$uid,
            'to_uid'=>-1,
            'money_type'=>$val['money_type'],
            'money_num'=>$val['num'],
            'act'=>1,
            'start_time'=>Currency\timeout(time()),
            'pay_status'=>0,
            'card_id'=>$val['card_id']
        ];
        $state = $deal->subtractres($dealarr);
        if($state){
            $mall = strreplace(apiGet('User.User.Userone',array('uid'=>$uid))['telphone']);
            $message = '申请提现：您的提现申请已提交成功，申请金额'.$val['num'].'元。审核大概需要1-2个工作日，请耐心等待…… 详询400-991-4418。育视界，让教育无处不在。';
            userMessage($uid,'提现',$message);
            $res =  $currency->rezyu($arr,$val['num']);
            return $res;
        }


    }

    //提现列表
    public function dealList($val){

//        $curl = new \PhalApi\CUrl();
//        json_decode($curl->get('http://test.yusj.vip/?s=User.User.Userone&uid='.$value['uid'], 3000),true)['data']
//
        $deal = new currency_deal();
        $bank = new currency_bank();
        $list = new currency_list();
        $currency = new currency_currency();
        $curl = new \PhalApi\CUrl();
        $data =  $deal->rlds($val,1);
        $num = $deal->rlds($val,1,'num');
//        return $data;
        foreach ($data as $key => $value){
            //条件货币
            $curs = [
                'uid'=>$value['uid'],
                'money_type'=>$value['money_type']
            ];
            $a = $list->findmoney_type($value['money_type'])['desc'];
            $card = $bank->findId($value['card_id']);
            $data[$key]['money_type'] = $a;
            $data[$key]['card'] = $card['card'];
            $data[$key]['bank'] = $card['bankDeposit'];
            $data[$key]['name'] = $card['user'];
            $data[$key]['subbranch'] = $card['subbranch'];
            $use  = apiGet('http://test.yusj.vip/?s=User.User.Userone&uid='.$value['uid'].'');
            if (!$use){
                $user = '未知';
            }
            if ($use){
                $user = $use['name'];
            }
            $data[$key]['user'] =$user;
            $data[$key]['balance'] = $currency->find($curs)['money_num'];
        }
        outputArgs(array('num'=>$num,'pagenum'=>$val['number'],'age'=>$val['page']));
        return $data;
    }
    //提现处理
    public  function dealdispose($val){

        $currency = new currency_currency();
        $deal = new currency_deal();
        $bank = new  currency_bank();
        $res = $deal->findId($val['id']);
        if (!$res){
            throw new BadRequestException('未找到指定提现内容',3);
        }
        $mall = strreplace(apiGet('User.User.Userone',array('uid'=>$res['uid']))['telphone']);
        //如果申请提现失败了的话
        if ($val['pay_status'] == '-1'){
            $userId = [
                'uid'=>$res['uid'],
                'money_type'=>$res['money_type']
            ];
           $currency->appendTO($userId,$res['money_num']);
            $message = '很抱歉，您于'.$res['start_time'].'提交的提现申请，申请单号'.$res['id'].'，提现金额'.$res['money_num'].'元，审核未通过，原因：'.$val['body'].'，请核实后重新提交。详询400-991-4418。育视界，让教育无处不在。';
            userMessage($res['uid'],'提现',$message);
        }
        if ($val['pay_status'] == '1'){
            $card = $bank->findId($res['card_id']);
            $money =  $currency->finduid('2',$res['uid'])['money_num'];
            $message = '您于'.$res['start_time'].'提交的xx账户提现申请，申请单号'.$res['id'].'，提现金额'.$res['money_num'].'元，已处理完毕，请留意您的'.$card['bankDeposit'].''.strreplace($card['card']).'账户余额变动信息。育视界账户当前余额'.$money.'元。详询400-991-4418。育视界，让教育无处不在。';
            userMessage($res['uid'],'提现',$message);
        }
        $id = $val['id'];
        $val['end_time'] = Currency\timeout(time());
        $val = Currency\array_remove($val,'id');
        return  $deal->updateDeal($val,$id);
    }

    //增加修改优惠码
    public function appendD($val){
        $dis = new currency_discounts();

        foreach( $val as $k=>$v){
            if( $v === null )
                unset( $val[$k] );
        }

        if ($val['id']){

            return $dis->updis($val['id'],$val);

        }

        $data = [
            'key'=>Currency\getRandomString(13),
            'uid'=>$val['uid'],
            'type'=>$val['type'],
            'item_id'=>$val['item_id'],
            'reduce_num'=>$val['reduce_num'],
            'had_num'=>$val['had_num'],
            'time'=>$val['time'],
            'startTime'=>Currency\timeout($val['startTime']),
            'astype'=>$val['astype'],
            'sum'=>$val['had_num']
        ];
        if ($val['type'] == 'PromotionCode'){
            $data['key']=Currency\getRandomString(8);
        }
        return $dis->add($data);
    }

    //优惠码修改
    public function upCode($val,$type='delete',$calculate = '+'){
        $dis = new currency_discounts();
        if ($type == 'delete'){
            $arr = [
                'statu'=> -1
            ];
            return $dis->updateCode($val,$arr);
            //修改优惠码次数
        }else if($type == 'update'){
            $code = $val['code'];
            $num = $val['num'];

            return $dis->upNum($code,$num,$calculate);


        }

    }

    //优惠码截止日期
    public function sotoptime($val){

        $dis = new currency_discounts();
        $code = $val['code'];
        $val =  Currency\array_remove($val,'code');
        return $dis->updateCode($code,$val);

    }

    //查询优惠码信息
    public function findCodes($val,$on_status=1){

        $dis = new currency_discounts();

        return $dis->find($val,$on_status);


    }
    //创建货币类型
    public function appendList($val){
        $list = new currency_list();

        return $list->inser($val);

    }
    //查看指定用户的推广码
    public function Promotion($uid,$type){
        $model = new currency_discounts();

        return $model->findpro($uid,$type);

    }
    //查询指定用户银行卡信息
    public function mysca($uid){
        $model = new currency_bank();

        $data =  $model->findUid($uid);

        foreach ($data as $key =>$value){

            $data[$key]['card'] = strreplace($value['card']);

        }
        return $data;

    }

    //管理货币
    public function mldmone($val){
        $list = new currency_list();
        $currency = new currency_currency();
        if ($val['on_status'] == '-1'){
            $data = $list->find($val['id']);
         $one = $data['to_one'];
         $type = $data['money_type'];
         $currency->transition($type,$one);
        }
    return $list->upStatu($val['id'],$val['type']);


    }
    //查询个人货币系统
    public function findCuren($money_type,$uid){
        $currency = new currency_currency();
        $list = new currency_list();

        if (!$uid){
          $uid =   apiGet('User.User.Selecttoken',array('searchToken'=>$_GET['token']))['Id'];
        }

        $data =  $currency->finduid($money_type,$uid);
//        $data['0']['money_type'] = $list->findmoney_type( $data['0']['money_type'])['desc'];

        return $data;

    }
    //查询指定货币交易类型
    public function dealType($val){
        $deal = new currency_deal();
        $cards = new currency_bank();
        if ($_GET['token']){
            $token = apiGet('User.user.selecttoken',array('searchToken'=>$_GET['token']));
        }else{
            $token = ['Id'=>-1];
        }

        $data =  $deal->findu($token['Id'],$val['act'],$val['page']);

        if ($data){
            foreach ($data as $key => $value){
                $data[$key]['card'] = null;
                if ($value['card_id']){
                    $ca = $cards->findId($value['card_id']);

                    $data[$key]['card'] = $ca['card'];
                    $data[$key]['bank'] = $ca['bankDeposit'];
                }

            }
        }else{
            $data = false;
        }
        outputArgs(array('number'=>count($data)));

        return $data;



    }
    //增加个人货币
    public function appendencs($val){
        checkAuth();
        $currency = new currency_currency();
       return $currency->addcur($val['uid'],$val['money_type'],$val['num'],$val['type']);
    }
    //创建订单
    public function cerOrder($val){
        $order = new order_order();
        $list = new currency_list();

        $code = $this->findCodes($val['code']);
        $money = 0.01;
        //优惠码信息
        if (!$code){
            $val['code'] = null;
        }else{

            switch ($code['type']){
                //如果为代金券的话
                case 0:
                    $money = $money-$code['reduce_num']<0?0:$money-$code['reduce_num'];
                    break;
                    //如果为折扣卷的话
                case 1:
                    $money = ceil($money-$money*($code['reduce_num']/100)*100)/100<0?0:ceil($money-$money*($code['reduce_num']/100)*100)/100;
                    break;
                    //如果为商品卷的话
                case 2 && $val['commodity_id'] == $code['item_id'] :
                    $money = $money-$code['reduce_num']<0?0:$money-$code['reduce_num'];
                    break;
                case 3 :
                    $money = $money-$code['reduce_num']<0?0:$money-$code['reduce_num'];
                    break;
            }

        }

        $arr = [
            'uid'=>Currency\getCache($_GET['token'])['uid'],
            'commodity_type	'=>$val['commodity_type'],
            'commodity_id'=>$val['commodity_id'],
            'num'=>$val['num'],
            'money_type'=>$list->find($val['commodity_id'])['money_type'],
            'money'=>$money,
            'money_code'=>$val['code'],
            'pay_status'=>0,
            'order_status'=>0,
            'create_time'=>Currency\timeout(time()),
        ];


        return $order->cert($arr);

    }
    //发起支付请求
    public  function  sponsors($val){

        $order = new order_order();

        $res = $order->find($val['oid']);

        if ($res['money'] <= 0){
            //做回调请求

            //订单号
            //交易号
            //交易类型
            //备注
            $data = [
              'out_trade_no'=>$res['id'],
                'trade_no'=>time().rand(1000,9999),
                'type'=>'system',
                'desc'=>'支付金额等于0元，直接进入订单环节'
            ];

           return  $this->payBack(base64_encode(json_encode($data)));


        }
        $arr = ['title'=>'充值','order'=>$res['id'],'money'=>$res['money']];
        //如果请求为支付宝的话
        if ($val['type'] == 1){
            return $arr;
        }

        //检测其余请求方式
        switch ($val['type']){
            //微信二维码
            case 2:
                $url = "https://www.yusj.vip/Exit/Wxpaywb/example/native.php";
                break;
                //微信jsapi
            case 3:
                $url = "https://www.yusj.vip/Exit/Wxpayprocedure/example/jsapi.php";
                break;
            default:
                $url = null;
                break;
        }
            //检测是否有openid存在
            if ($val['agument']){
                $arr['openid'] = json_decode($val['agument'],true)['openid'];
            }
            //sign验证
            $arr['sign'] = '798bdd1ec31d173a';

        try {
            // 实例化时也可指定失败重试次数，这里是2次，即最多会进行3次请求
            $curl = new \PhalApi\CUrl(2);
            // 第二个参数为待POST的数据；第三个参数表示超时时间，单位为毫秒
            $rs = $curl->post($url, $arr, 3000);
            // 一样的输出
            echo $rs;
        } catch (\PhalApi\Exception\InternalServerErrorException $ex) {
            throw new BadRequestException('支付请求失败',100);
        }

    }

    //支付返回结果操作
    public function payBack($val){
        //回调数据
        $data = json_decode(base64_decode($val['data']),true);



        $order = new order_order();
        $commondity = new commodity_commodity;
        //查询的订单信息
        $ordata = $order->find($data['out_trade_no']);

        //查询商品信息
         $condata =  $commondity->find($ordata['commodity_id']);

        if ($condata['urlType'] == 'service'){
        //调用内部
        $res = $this->$condata['url']($data,$val,$ordata['money']);

        }else{
            //调用外部
            $curl = new \PhalApi\CUrl(2);

            $urldata = [
                'type'=>$condata['type'],
                'time'=>$condata['num']*3600*24,
                'date'=>$condata['timeout']
            ];


            $rest = $curl->post($condata['url'], $urldata, 3000);

            if ($rest['code'] == 200){

                $arrorder = [
                    'pay_status'=>1,
                    'pay_id'=>$data['trade_no'],
                    'act_status'=>1,
                    'order_status'=>200,
                    'end_time'=>Currency\timeout(time())
                ];
                //订单结束
                $order->overOrder($ordata['id'],$arrorder);
                $record = new commodity_record();
                //记录商品购买记录
                $recorddata = [
                    'uid'=>$ordata['uid'],
                    'cid'=>$ordata['commodity_id'],
                    'num'=>$ordata['num'],
                    'time'=>Currency\timeout(time())
                ];
                //记录商品记录
                $record->record($recorddata);

                $res = true;

            }else{
                $res = false;
            }



                $redate = [
                    'oid'=>$ordata['id'],
                    'act'=>'update订单结束',
                    'value'=>'success',
                    'time'=>Currency\timeout(time()),
                    'uid'=>0
                ];
            if ($res){
                $redate['value'] = 'success';
            }else{
                $redate['value'] = ['error'];
            }
             $orderlog = new order_log();

            return $orderlog->operationMessage($redate);

        }



    }

    /**
     * @param $data // 支付回到自定义返回参数
     * @param $val  // 支付返回原始信息经过加密后
     * @param $money // 返回金额
     */

        //货币系统充值后的增加
    public function currensd($data,$val,$money){
        $order = new order_order();
        $toup = new currency_toup();
        $currency = new currency_currency();
        $comm = new commodity_commodity();
        //记录充值信息
        $arr = [
            'pay_id'=>$data['trade_no'],
            'pay_type'=>$data['type'],
            'order_id'=>$data['out_trade_no'],
            'desc'=>$data['desc'],
            'data'=>$val['pay_info'],
            'money'=>$money,
            'pay_status'=>1,
            'time'=>Currency\timeout(time()),
            'on_statu'=>1
        ];

        //记录货币充值信息
        $toup->inserRecord($arr);

        //查询订单信息
        $ordata = $order->find($data['out_trade_no']);

        // Step 1: 开启事务
        \PhalApi\DI()->notorm->beginTransaction('db_master');

        try{


            $res = [
                'money_type	'=>$ordata['money_type'],
                'uid'=>$ordata['uid']
            ];
            //对货币进行增加
            $currency->appendTO($res,$ordata['num']);


            $arrorder = [
                'pay_status'=>1,
                'pay_id'=>$data['trade_no'],
                'act_status'=>1,
                'order_status'=>200,
                'end_time'=>Currency\timeout(time())
            ];
            //订单结束
           $order->overOrder($ordata['id'],$arrorder);

            \PhalApi\DI()->notorm->commit('db_master');

            $rec =  true;

        }catch(\Exception $e){

            \PhalApi\DI()->notorm->rollback('db_master');

            $rec =  false;

        }







    }
    //查询订单返回信息
    public function findorder($val){

        $order = new order_order();

       return $order->find($val);

    }

   //修改订单状态
public function ordersta($val){

    $order = new order_order();
    $ordata =  $order->find($val);

    if ($ordata['order_status'] == 0){
        throw new BadRequestException('该订单状态不可修改或删除',3);
    }

    $arr = ['order_status'=>$val['order_status'],'statu'=>$val['statu']];


    return $order->overOrder($val['oid'],$arr);

}

//查看所有订单状态
public function allorder($val){

    $order = new order_order();

    return $order->findall($val);

}

//指定用户的优惠状态查询
public function discount($val){

    $model = new currency_discounts();


    if (!$val['uid']){
        $uid = apiGet('User.user.selecttoken',array('searchToken'=>$_GET['token']))['Id'];
        $data =  $model->finuser($uid,$val['type']);
    }
    if ($val['uid']){
        $data = $model->findallUser($val['uid'],$val['type']);

       $arr = apiGet('Order.receive.findcode');

       for($i=0;$i<count($data);$i++){
           if (!$arr){
               $data[$i]['nums'] = 0;
           }else{

               for ($j=0;$j<count($arr);$j++){

                   $data[$i]["nums"]=0;
                   if($data[$i]["key"]==$arr[$j]["money_code"]){
                       $data[$i]["nums"]=$arr[$j]["num"];
                   }else{
                       $data[$i]["nums"]=0;
                   }
               }
           }

       }

    }
    return $data;

}

//查询优惠码所有记录
public function alldiscount($val,$num = 40){

    $model = new currency_discounts();

    $page = ($val['page']-1)*$num;

    $data =  $model->alls($val,$page,$num,'data');
    $nums =  $model->alls($val,$page,$num,'num');
    outputArgs(["num"=>$nums,"pagenum"=>$num,"age"=>$val['page']]);
    return $data;
}

/**
  *  users bing bank cards
  * $arr  An array of data
  */
public function bingBank($arr){

    $model = new currency_bank();

        $res = $model->findcard($arr['card']);
        if ($res){
            throw new BadRequestException('卡号已经绑定',3);
        }


        return $model->create($arr);

}
public function delecar($id){

    $model = new currency_bank();

    return $model->deleteCard($id);

}

//优惠券数据统计
    public function allDis(){
        $dis = new currency_discounts();
        $order = new order_order();
        $data['a'] = count($dis->findalldis());
        $data['b'] = count($order->findalldisorder());
        $data['c'] = $data['a']-$data['b'];
        return $data;

    }
//优惠券过期处理
public function overout(){

    $model = new currency_discounts();
    $res = $model->overouts();
    if ($res){
        foreach ($res as $key => $value){

            $model->updis($value['id'],['on_status'=>-1]);


        }
    }
    return true;
}
//优惠券过期列表
public function todata($page,$num){
    $model = new currency_discounts();
    $data = $model->outdiscounts($page,$num);
    $nums = $model->outdiscounts($page,$num,'num');
    outputArgs(["num"=>$nums,"pagenum"=>$num,"age"=>$page]);
    return $data;

}








}
